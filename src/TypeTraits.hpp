#ifndef TYPE_TRAITS_HPP
#define TYPE_TRAITS_HPP

#include <type_traits>

template <typename RMP, typename = void>
struct has_get_column : std::false_type {};

template <typename RMP>
struct has_get_column<RMP, std::void_t<std::enable_if_t<std::is_same<int,
                               decltype(std::declval<RMP>().getColumn(
                                   typename RMP::Column()))>::value>>>
    : std::true_type {};

template <typename PricingProblem>
struct PricingProblemTraits {
    using Column = typename PricingProblem::Column;
    using IsUnique = std::false_type;
};

#endif
