#ifndef ROWGENERATION_HPP
#define ROWGENERATION_HPP

namespace RowGeneration {

struct Parameters {
    bool verbose;
    int nbThreads;
};

/*
template <typename RMP, typename Separator>
bool solve(RMP& _rmp, Separator& _separator, Parameters _params) {

    do {
        if (!_rmp.solve()) {
            return false;
        }
        _separator.solve(_rmp);
        for (auto& row : _separator.getRows()) {
        }
    } while (foundRows);
}
*/

} // namespace RowGeneration
#endif
