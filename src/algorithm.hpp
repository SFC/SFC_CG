#ifndef ALGORITHM_HPP
#define ALGORITHM_HPP

#include "SFC.hpp"
#include "models.hpp"
#include <CppRO/ColumnGeneration.hpp>
#include <CppRO/ConstrainedShortestPath.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/multi_array.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <exception>
#include <numeric>
#include <optional>
#include <random>
#include <type_traits>

template <typename Algorithm, typename Instance>
struct Pricing {
    explicit Pricing(const Instance& _inst)
        : inst(&_inst)
        , algo(_inst) {}
    const Instance* inst;
    Algorithm algo;

    auto operator()(const auto& _demand, const auto& _duals) {
        return algo(*inst, _demand, _duals);
    }
};

namespace SFC {

std::optional<Solution> twoPhaseHeuristic(const Instance& _inst,
    std::size_t _nbLicenses, std::mt19937& _gen, std::size_t _nbThreads);

struct CGBnBVisitor {
    double bound{std::numeric_limits<double>::quiet_NaN()};
    std::chrono::time_point<std::chrono::system_clock> algorithmStartTime;
    std::chrono::time_point<std::chrono::system_clock> cgConvergedTime;
    std::chrono::time_point<std::chrono::system_clock> algorithmEndTime;

    CppRO::ColumnGeneration::Visitor::TimeLogger cgVisitor;

    struct AlgorithmStarted {};
    struct CGConverged {};
    struct BnBConverged {};

    void operator()(AlgorithmStarted /*_unused*/) {
        algorithmStartTime = std::chrono::system_clock::now();
    }

    void operator()(const IloCplex& _cplex, CGConverged /*_unused*/) {
        cgConvergedTime = std::chrono::system_clock::now();
        bound = _cplex.getObjValue();
    }

    void operator()(BnBConverged /*_unused*/) {
        algorithmEndTime = std::chrono::system_clock::now();
    }
};

/**
 * Solve the instance using column generation followed by an BnB over the
 * generated columns
 */
std::optional<Solution> columnGenerationBranchAndBound(const Instance& _inst,
    std::size_t _nbThreads, double _timeLimit, CGBnBVisitor& _visitor);

std::optional<Solution> columnGenerationBranchAndBound(const Instance& _inst,
    const boost::multi_array<bool, 2>& _fixedLocations, std::size_t _nbThreads);

/**
 * Given an instance and a limit on the number of replicas, returns the solution
 * find by the column generation algorithm.
 */
std::optional<Solution> columnGenerationBranchAndBound(const Instance& _inst,
    std::size_t _nbLicenses, const std::optional<Solution>& _warmStart,
    std::size_t _nbThreads, double _timeLimit, CGBnBVisitor& _visitor);

struct FunctionLocationPredicate {
    const LayeredGraph<Network>* graph;
    const std::vector<function_descriptor>* functions;
    const boost::multi_array<bool, 2>* locations;

    bool operator()(const auto _ed) const {
        return std::visit(
            [&](auto&& _val) {
                using T = std::decay_t<decltype(_val)>;
                if constexpr (std::is_same_v<Network::vertex_descriptor, T>) {
                    const auto f = (*functions)[graph->getLayer(
                        source(_ed, graph->getGraph()))];
                    return (*locations)[static_cast<long>(_val)]
                                       [static_cast<long>(f)];
                }
                if constexpr (std::is_same_v<Network::edge_descriptor, T>) {
                    return true;
                }
            },
            graph->getGraph()[_ed].originalDescriptor);
    }
};
/**
 * VisitorLambda is a structure that enables lambda to be use as visitors
 */
template <typename EventFilterType, typename FunctionType>
class VisitorLambda {
    FunctionType m_f;

  public:
    using event_filter = EventFilterType;

    explicit VisitorLambda(FunctionType&& _f)
        : m_f(_f) {}

    VisitorLambda(FunctionType _f, EventFilterType /*unused*/)
        : m_f(std::move(_f)) {}

    template <typename... Args>
    void operator()(Args&&... _args) {
        m_f(std::forward<Args>(_args)...);
    }
};

/**
 * ShortestPathPerDemand solves the shortest path problem for a given demand
 */
class ShortestPathPerDemand {
    std::size_t m_demandID{0};
    LayeredGraph<Network> m_layeredGraph;
    std::vector<double> m_weights;
    // Property maps for dijkstra
    std::vector<double> m_distance;
    std::vector<LayeredGraph<Network>::Graph::edge_descriptor> m_predecessors;

    struct DjikstraVisitor : public boost::base_visitor<DjikstraVisitor> {
        std::size_t dest;
        using event_filter = boost::on_finish_vertex;
        template <typename G>
        void operator()(std::size_t _u, const G& /*g*/) const {
            if (_u == dest) {
                throw std::exception{};
            }
        }
    } m_djikstra_visitor{};
    Path<LayeredGraph<Network>::Graph> m_path{};

  public:
    using ColumnType = PathExtendedModel::ColumnType;

    explicit ShortestPathPerDemand(const Instance& _inst);

    void setPricingID(std::size_t _demandID);

    template <typename WeightPropertyMap>
    void updateWeight(const WeightPropertyMap& _weightPropertyMap) {
        for (const auto ed :
            boost::make_iterator_range(edges(m_layeredGraph.getGraph()))) {
            const auto& lu = source(ed, m_layeredGraph.getGraph());
            std::visit(
                [&](auto&& _val) {
                    using T = std::decay_t<decltype(_val)>;
                    if constexpr (std::is_same_v<Network::vertex_descriptor,
                                      T>) {
                        m_weights[m_layeredGraph.getGraph()[ed].id] =
                            _weightPropertyMap(CrossLayerLink{
                                m_demandID, _val, m_layeredGraph.getLayer(lu)});
                    }
                    if constexpr (std::is_same_v<Network::edge_descriptor, T>) {
                        m_weights[m_layeredGraph.getGraph()[ed].id] =
                            _weightPropertyMap(IntraLayerLink{
                                m_demandID, _val, m_layeredGraph.getLayer(lu)});
                    }
                },
                m_layeredGraph.getGraph()[ed].originalDescriptor);
        }
    }

    void solve(const Demand& _demand);

    template <typename EdgePredicate, typename VertexPredicate>
    void solve(const Demand& _demand, EdgePredicate _edgePredicate,
        VertexPredicate _vertexPredicate) {
        const auto dest = m_layeredGraph.getVertexDescriptor(
            _demand.functions.size(), _demand.t);
        m_djikstra_visitor.dest = dest;
        const auto vis = VisitorLambda(
            [&](const auto& _ed, const auto& _g) {
                m_predecessors[target(_ed, _g)] = _ed;
            },
            boost::on_edge_relaxed{});

        boost::filtered_graph filteredGraph(
            m_layeredGraph.getGraph(), _edgePredicate, _vertexPredicate);
        const auto& vertexIndexMap = get(boost::vertex_index, filteredGraph);
        try {
            dijkstra_shortest_paths(filteredGraph, _demand.s,
                boost::dummy_property_map{},
                boost::iterator_property_map(
                    m_distance.begin(), vertexIndexMap),
                boost::iterator_property_map(m_weights.begin(),
                    get(&LayeredGraph<Network>::Arc::id, filteredGraph)),
                vertexIndexMap, epsilon_less<double>(), std::plus<>(),
                std::numeric_limits<double>::infinity(), 0.0,
                boost::make_dijkstra_visitor(
                    std::make_pair(vis, m_djikstra_visitor)));
        } catch (const std::exception&) {
            // Found dest
        }
        m_path = getPathFromPredecessors(_demand.s, dest, filteredGraph,
            boost::iterator_property_map(
                m_predecessors.begin(), vertexIndexMap));
        SFC_ASSERT(!m_path.empty());
    }

    [[nodiscard]] double getObjValue() const;

    [[nodiscard]] ServicePath getColumn() const;

    template <typename DualValues>
    SFC::ServicePath operator()(const Instance& _inst,
        const boost::multi_array<bool, 2>& _locations, const Demand& _demand,
        const DualValues& _dualValues) {
        setPricingID(_demand.id);
        const auto weightPropertyMap = [&](const auto& _link) {
            return _dualValues.getReducedCost(_inst, _link);
        };
        updateWeight(weightPropertyMap);
        // Filter out unavailable locations
        FunctionLocationPredicate edgePredicate{
            &m_layeredGraph, &_demand.functions, &_locations};
        solve(_demand, edgePredicate, [](const auto& /*_vd*/) { return true; });
        return getColumn();
    }

    template <typename DualValues>
    SFC::ServicePath operator()(const Instance& _inst, const Demand& _demand,
        const DualValues& _dualValues) {
        setPricingID(_demand.id);
        const auto weightPropertyMap = [&](const auto& _link) {
            return _dualValues.getReducedCost(_inst, _link);
        };
        updateWeight(weightPropertyMap);
        solve(_demand);
        return getColumn();
    }
};

std::optional<Solution> routingColumnGenerationBranchAndBound(
    const Instance& _inst, std::size_t _nbThreads);

/**
 * Given a LazyPathExtendedAlphaModel and a PathFunctionLinking, checks whether
 * the constraints is violated
 */
struct LicenceExceededChecker {
    const LazyPathExtendedAlphaModel& rmp;

    explicit LicenceExceededChecker(const LazyPathExtendedAlphaModel& _rmp)
        : rmp(_rmp) {}

    std::optional<LazyPathExtendedAlphaModel::PathFunctionLinking> operator()(
        const LazyPathExtendedAlphaModel::PathFunctionLinking& _linking,
        const LazyPathExtendedAlphaModel::RawSolution& _sol) const;
};

struct CRGBnBVisitor {
    double bound{std::numeric_limits<double>::quiet_NaN()};
    std::chrono::time_point<std::chrono::system_clock> algorithmStartTime;
    std::chrono::time_point<std::chrono::system_clock> crgConvergedTime;
    std::chrono::time_point<std::chrono::system_clock> algorithmEndTime;
    std::vector<std::chrono::duration<double>> rowGenerationTime;
    std::vector<std::chrono::duration<double>> rowMasterSolvingTime;
    std::vector<std::chrono::duration<double>> columnGenerationTime;
    std::vector<std::chrono::duration<double>> columnMasterSolvingTime;

    struct AlgorithmStarted {};
    struct CRGConverged {};
    struct BnBConverged {};

    void operator()(AlgorithmStarted /*_unused*/) {
        algorithmStartTime = std::chrono::system_clock::now();
    }

    void operator()(double _bound, CRGConverged /*_unused*/) {
        crgConvergedTime = std::chrono::system_clock::now();
        bound = _bound;
    }

    void operator()(BnBConverged /*_unused*/) {
        algorithmEndTime = std::chrono::system_clock::now();
    }
};

std::optional<Solution> columnAndRowGenerationBranchAndBound(
    const Instance& _inst, std::size_t _nbLicenses,
    const std::optional<Solution>& _warmStart, std::size_t _nbThreads,
    double _timeLimit, CRGBnBVisitor& _visitor);
/**
 * Given a problem instance, return the placement of all functions
 */
std::optional<boost::multi_array<bool, 2>> getFunctionPlacementPerFunc(
    const SFC::Instance& _inst, std::size_t _nbLicenses,
    const std::vector<SFC::function_descriptor>& _funcs);

std::optional<boost::multi_array<bool, 2>> getFunctionPlacementMedoids(
    const SFC::Instance& _inst, std::size_t _nbLicenses,
    const std::vector<SFC::function_descriptor>& _funcs, std::mt19937& _gen);

class DelayConstrainedShortestPathPerDemand {
    std::size_t m_demandID{0};
    LayeredGraph<Network> m_layeredGraph;

    IloEnvWrapper m_env;
    CppRO::CompactConstrainedShortestPathModel m_cspModel{
        CppRO::CompactConstrainedShortestPathModel(m_env,
            m_layeredGraph.getGraph(),
            get(&LayeredGraph<Network>::Arc::id, m_layeredGraph.getGraph()))};
    IloObjective m_obj{IloObjective(m_env)};
    IloModel m_model;
    IloCplex m_solver;

    Path<LayeredGraph<Network>::Graph> m_path{};

  public:
    using ColumnType = PathExtendedModel::ColumnType;

    explicit DelayConstrainedShortestPathPerDemand(const Instance& _inst);

    void setPricingID(std::size_t _demandID);

    template <typename WeightPropertyMap>
    void updateWeight(const WeightPropertyMap& _weightPropertyMap) {
        IloExpr objExpression(m_env);
        for (const auto ed :
            boost::make_iterator_range(edges(m_layeredGraph.getGraph()))) {
            const auto& lu = source(ed, m_layeredGraph.getGraph());
            std::visit(
                [&](auto&& _val) {
                    using T = std::decay_t<decltype(_val)>;
                    if constexpr (std::is_same_v<Network::vertex_descriptor,
                                      T>) {
                        objExpression +=
                            m_cspModel
                                .getFlowVars()[m_layeredGraph.getGraph()[ed].id]
                            * _weightPropertyMap(CrossLayerLink{
                                m_demandID, _val, m_layeredGraph.getLayer(lu)});
                    }
                    if constexpr (std::is_same_v<Network::edge_descriptor, T>) {
                        objExpression +=
                            m_cspModel
                                .getFlowVars()[m_layeredGraph.getGraph()[ed].id]
                            * _weightPropertyMap(IntraLayerLink{
                                m_demandID, _val, m_layeredGraph.getLayer(lu)});
                    }
                },
                m_layeredGraph.getGraph()[ed].originalDescriptor);
        }
    }

    template <typename DualValues>
    void updateDual(const Instance& _inst, const DualValues& _dualValues) {
        IloExpr objExpression(m_env);
        for (const auto ed :
            boost::make_iterator_range(edges(m_layeredGraph.getGraph()))) {
            const auto& lu = source(ed, m_layeredGraph.getGraph());
            std::visit(
                [&](auto&& _val) {
                    using T = std::decay_t<decltype(_val)>;
                    if constexpr (std::is_same_v<Network::vertex_descriptor,
                                      T>) {
                        objExpression +=
                            m_cspModel
                                .getFlowVars()[m_layeredGraph.getGraph()[ed].id]
                            * _dualValues.getReducedCost(
                                _inst, CrossLayerLink{m_demandID, _val,
                                           m_layeredGraph.getLayer(lu)});
                    }
                    if constexpr (std::is_same_v<Network::edge_descriptor, T>) {
                        objExpression +=
                            m_cspModel
                                .getFlowVars()[m_layeredGraph.getGraph()[ed].id]
                            * _dualValues.getReducedCost(
                                _inst, IntraLayerLink{m_demandID, _val,
                                           m_layeredGraph.getLayer(lu)});
                    }
                },
                m_layeredGraph.getGraph()[ed].originalDescriptor);
        }
        m_obj.setExpr(objExpression);
    }

    void solve(const Demand& _demand);
    [[nodiscard]] double getObjValue() const;
    [[nodiscard]] ServicePath getColumn() const;

    template <typename DualValues>
    SFC::ServicePath operator()(const Instance& _inst, const Demand& _demand,
        const DualValues& _dualValues) {
        setPricingID(_demand.id);
        updateDual(_inst, _dualValues);
        solve(_demand);
        return getColumn();
    }
};

} // namespace SFC

#endif
