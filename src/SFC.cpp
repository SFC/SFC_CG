#include "SFC.hpp"

#include <algorithm>
#include <boost/graph/properties.hpp>
#include <boost/pending/property.hpp>
#include <cmath>
#include <fstream>
#include <istream>
#include <iterator>
#include <nlohmann/json.hpp>
#include <numeric>
#include <range/v3/all.hpp>
#include <type_traits>
#include <utility>

namespace SFC {

std::vector<double> loadNodeCapa(
    const std::string& _filename, double _percent) {
    std::cout << _filename << '\n';
    std::vector<double> cpuCapas;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        throw std::runtime_error(_filename + " does not exists!\n");
    }
    std::cout << "Opening :" << _filename << '\n';

    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (!line.empty()) {
            int u = 0;
            double capa = 0.0;
            std::stringstream lineStream(line);
            lineStream >> u >> capa;
            cpuCapas.emplace_back(
                static_cast<std::size_t>(capa == -1 ? 0 : capa * _percent));
        }
    }
    return cpuCapas;
}

// bool operator==(const SFC::Demand& _d1, const SFC::Demand& _d2) {
//     return std::make_tuple(_d1.s, _d1.t, _d1.functions) ==
//     std::make_tuple(_d2.s, _d2.t, _d2.functions);
// }

// LayeredGraph::LayeredGraph(const Instance& _inst)
//     : m_inst(&_inst)
//     , m_graph([&]() {
//         DiGraph retval(num_vertices(m_inst->network) * m_inst->maxChainSize +
//         1);
//         // Build layers
//         for (std::size_t f = 0; f < m_inst->maxChainSize + 1; ++f) {
//             for (const auto& edge : m_inst->network.getEdges()) {
//                 retval.addEdge(num_vertices(m_inst->network) * f +
//                 edge.first,
//                     num_vertices(m_inst->network) * f + edge.second);
//             }
//         }
//         // Add arcs between layers
//         for (std::size_t fi = 0; fi < m_inst->maxChainSize; ++fi) {
//             for (const auto u : m_inst->NFVNodes) {
//                 retval.addEdge(num_vertices(m_inst->network) * fi + u,
//                     num_vertices(m_inst->network) * (fi + 1) + u);
//             }
//         }
//         return retval;
//     }()) {}

// void LayeredGraph::removeCrossLayerLink(const Node _u, const int _j1) {
//     m_graph.removeEdge(getRealNode({_u, _j1}), getRealNode({_u, _j1 + 1}));
// }

// double LayeredGraph::getCrossLayerWeight(const Node _u,
//     const int _j1) const {
//     return m_graph.getEdgeWeight(getRealNode({_u, _j1}),
//         getRealNode({_u, _j1 + 1}));
// }

// void LayeredGraph::setCrossLayerWeight(const Node _u, const int _j1,
//     double _weight) {
//     m_graph.setEdgeWeight(getRealNode({_u, _j1}), getRealNode({_u, _j1 + 1}),
//         _weight);
// }

// void LayeredGraph::removeInLayerEdge(const Edge _edge, const int _j1) {
//     m_graph.removeEdge(getRealNode({_edge.first, _j1}),
//         getRealNode({_edge.second, _j1}));
// }

// double LayeredGraph::getInLayerWeight(const Edge _edge,
//     const int _j1) const {
//     return m_graph.getEdgeWeight(getRealNode({_edge.first, _j1}),
//         getRealNode({_edge.second, _j1}));
// }

// void LayeredGraph::setInLayerWeight(const Edge _edge, const int _j1,
//     double _weight) {
//     m_graph.setEdgeWeight(getRealNode({_edge.first, _j1}),
//         getRealNode({_edge.second, _j1}), _weight);
// }

bool operator==(const ServicePath& _sp1, const ServicePath& _sp2) {
    return _sp1.nPath == _sp2.nPath && _sp1.locations == _sp2.locations
           && _sp1.demand == _sp2.demand;
}

double Instance::getNbCores(const demand_descriptor _demandID) const {
    double totalNbCores = 0.0;
    for (std::size_t j = 0; j < demands[_demandID].functions.size(); ++j) {
        totalNbCores += demands[_demandID].cpuCharges[j];
    }
    return totalNbCores;
}

std::vector<double> loadFunctions(const std::string& _filename) {
    std::cout << _filename << '\n';
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        throw std::runtime_error(_filename + " does not exists!\n");
    }
    std::vector<double> functions;
    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (!line.empty()) {
            function_descriptor f = 0;
            double cpu = 0.0;
            std::stringstream lineStream(line);
            lineStream >> f >> cpu;
            functions.push_back(cpu);
        }
    }
    return functions;
}

std::size_t getTotalNeededCores(const std::vector<Demand>& _demands,
    const std::vector<double>& _funcCharge) {
    std::size_t totalNbCores = 0;
    for (const auto& demand : _demands) {
        for (std::size_t j = 0; j < demand.functions.size(); ++j) {
            totalNbCores += std::size_t(
                ceil(demand.bandwidth / _funcCharge[demand.functions[j]]));
        }
    }
    return totalNbCores;
}

boost::multi_array<bool, 2> getFunctionsLocations(
    const Instance& _instance, const std::vector<ServicePath>& _sPath) {
    boost::multi_array<bool, 2> retval(
        boost::extents[static_cast<long>(num_vertices(_instance.network))]
                      [static_cast<long>(_instance.nbFunctions)]);
    std::fill_n(retval.data(), retval.num_elements(), false);
    for (const auto& [demand, sPath] :
        ranges::views::zip(_instance.demands, _sPath)) {
        for (const auto& [u, f] :
            ranges::views::zip(sPath.locations, demand.functions)) {
            retval[static_cast<long>(u)][static_cast<long>(f)] = true;
        }
    }
    return retval;
}

std::vector<function_descriptor> getFunctionsAscendingCores(
    const Instance& _inst) {
    // Get functions ascending order of used cores
    std::vector<double> totalCores(_inst.nbFunctions);
    for (std::size_t i = 0; i < _inst.demands.size(); ++i) {
        for (std::size_t j = 0; j < _inst.demands[i].functions.size(); ++j) {
            totalCores[_inst.demands[i].functions[j]] +=
                _inst.demands[i].cpuCharges[j];
        }
    }
    std::vector<function_descriptor> functions(_inst.nbFunctions);
    std::iota(functions.begin(), functions.end(), 0);
    std::sort(functions.begin(), functions.end(),
        [&](const SFC::function_descriptor _f1,
            const SFC::function_descriptor _f2) {
            return totalCores[_f1] < totalCores[_f2];
        });
    return functions;
}

std::vector<function_descriptor> getFunctionsDescendingCores(
    const Instance& _inst) {
    // Get functions ascending order of used cores
    std::vector<double> totalCores(_inst.nbFunctions);
    for (std::size_t i = 0; i < _inst.demands.size(); ++i) {
        for (std::size_t j = 0; j < _inst.demands[i].functions.size(); ++j) {
            totalCores[_inst.demands[i].functions[j]] +=
                _inst.demands[i].cpuCharges[j];
        }
    }
    std::vector<std::size_t> functions(_inst.nbFunctions);
    std::iota(functions.begin(), functions.end(), 0);
    std::sort(functions.begin(), functions.end(),
        [&](const auto _f1, const auto _f2) {
            return totalCores[_f1] > totalCores[_f2];
        });
    return functions;
}

Instance::Instance(const Network& _network, std::vector<double> _linkCapa,
    std::vector<double> _linkDelay, std::vector<double> _cpuCapa,
    std::vector<double> _ramCapa, std::vector<double> _storageCapa,
    std::vector<Demand> _demands)
    : network(_network)
    , linkCapacity(std::move(_linkCapa))
    , linkDelay(std::move(_linkDelay))
    , cpuCapacity(std::move(_cpuCapa))
    , ramCapacity(std::move(_ramCapa))
    , storageCapacity(std::move(_storageCapa))
    , demands(std::move(_demands))
    , maxChainSize(std::max_element(demands.begin(), demands.end(),
          [&](auto&& _d1, auto&& _d2) {
              return _d1.functions.size() < _d2.functions.size();
          })->functions.size())
    , nbFunctions(std::accumulate(demands.begin(), demands.end(),
          std::set<function_descriptor>{},
          [](auto _functions, const auto& _demand) {
              for (auto f : _demand.functions) {
                  _functions.insert(f);
              }
              return _functions;
          }).size())

{}

Instance loadInstance(const std::string& _filename) {
    const auto inst_dict = nlohmann::json::parse(std::ifstream(_filename));

    std::vector<std::pair<std::size_t, std::size_t>> edgeList;
    std::vector<Link> properties;
    std::vector<double> delays;
    std::vector<double> bandwidths;
    for (const auto& edge : inst_dict["edge_list"]) {
        edgeList.emplace_back(edge["src"], edge["dst"]);
        properties.emplace_back(Link{properties.size()});
        bandwidths.emplace_back(edge["bandwidth"]);
        delays.emplace_back(edge.value("delay", 0));
        edgeList.emplace_back(edge["dst"], edge["src"]);
        properties.emplace_back(Link{properties.size()});
        bandwidths.emplace_back(edge["bandwidth"]);
        delays.emplace_back(edge.value("delay", 0));
    }

    std::vector<Demand> demands;
    for (const auto& demand : inst_dict["demands"]) {
        demands.emplace_back(Demand{demands.size(), demand["src"],
            demand["dst"], demand["bandwidth"], demand["functions"],
            demand["cpuCharges"], demand["ramCharges"],
            demand["storageCharges"],
            demand.value("delay", std::numeric_limits<double>::infinity())});
    }
    return {Network{edgeList.begin(), edgeList.end(), properties.begin(),
                inst_dict["order"]},
        bandwidths, delays, inst_dict["cpuCapacity"], inst_dict["ramCapacity"],
        inst_dict["storageCapacity"], demands};
}

} // namespace SFC
