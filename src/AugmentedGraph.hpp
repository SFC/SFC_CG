#ifndef AUGMENTED_GRAPH_HPP
#define AUGMENTED_GRAPH_HPP

#include "SFC.hpp"

namespace SFC {
class AugmentedGraph {
  public:
    explicit AugmentedGraph(const Instance& _inst);
    AugmentedGraph(const Instance& _inst, Matrix<char> _funcPlacement);

    AugmentedGraph(const AugmentedGraph&) = default;
    AugmentedGraph& operator=(const AugmentedGraph&) = default;
    AugmentedGraph(AugmentedGraph&&) noexcept = default;
    AugmentedGraph& operator=(AugmentedGraph&&) noexcept = default;
    ~AugmentedGraph() = default;

    void removeNodeCapa(Node _node);
    void showNetworkUsage() const;
    void updateCapacities(const ServicePath& _sPath);
    std::vector<Node> getNodesSortedByUsage() const;
    std::vector<ServicePath> solve();
    std::vector<ServicePath> getInitialConfigurationNoCapa(
        const std::vector<Demand>& _demands);

  private:
    Path getAugmentedPath(const Demand& _demand) const;
    Path getAugmentedPathNoCapa(const Demand& _demand) const;
    ServicePath getServicePath(const Path& augPath, int _demandID) const;

    const Instance* m_inst;
    int m_nbLayers;
    LayeredGraph m_layeredGraph;
    DiGraph m_flowGraph;
    std::vector<double> m_nodeUsage;
    Matrix<char> m_funcPlacement;
};
} // namespace SFC
#endif
