#include "SFC.hpp"

int main(int, char**) {
    SFC::ServicePath sPath({3, 5, 12, 10}, {3, 3, 3, 3, 5}, 360);
    SFC::CrossLayerLink cLink1{360, 3, 0};
    SFC::CrossLayerLink cLink2{360, 3, 4};
    assert(contains(sPath, cLink1));
    assert(!contains(sPath, cLink2));
}
