#include "SFC.hpp"
#include "algorithm.hpp"
#include <CppRO/MyTimer.hpp>
#include <fmt/format.h>
#include <iostream>
#include <limits>
#include <lyra/lyra.hpp>
#include <nlohmann/json.hpp>
#include <numeric>
#include <optional>
#include <range/v3/algorithm/transform.hpp>
#include <range/v3/view/transform.hpp>
#include <spdlog/spdlog.h>
#include <string>
#include <type_traits>

constexpr auto TEN_MINUTES = 600;

struct CliArgs {
    std::string model{"CGBnB"};
    std::string name{};
    std::size_t nbLicenses{std::numeric_limits<std::size_t>::max()};
    std::size_t seed{0};
    std::size_t nbThreads{1};
    double timeLimit{TEN_MINUTES};
    bool skipWarmstart{false};
    bool help{false};

    std::string outputFilename{};

    [[nodiscard]] auto getCli() {
        return lyra::cli() | lyra::help(help)
               | lyra::arg(model, "model")("Specify the model used")
                     .choices("CGBnB", "CRGBnB", "TP-Heur")
               | lyra::arg(name, "networkName")("Specify the network name")
               | lyra::opt(nbLicenses, "nbLicenses")["-l"](
                   "Specify the number max of licenses")

               | lyra::opt(skipWarmstart)["--skipWarmstart"](
                   "Skip the warmstart")
               | lyra::opt(seed, "seed")["-s"]("Seed")
               | lyra::opt(outputFilename, "outputFilename")["-o"](
                   "Output filename")
               | lyra::opt(timeLimit, "timeLimit")["-t"](
                   "Specify the time limit")
               | lyra::opt(nbThreads, "nbThreads")["-j"](
                   "Specify the number of threads");
    }
};

int main(int argc, char** argv) {
    try {
        CliArgs params;
        auto cli = params.getCli();
        auto result = cli.parse({argc, argv});
        if (!result) {
            std::cerr << "Error in command line: " << result.message() << '\n';
            return 1;
        }
        if (params.help) {
            std::cout << cli << '\n';
            return 0;
        }
        nlohmann::json runInformations;
        runInformations["model"] = params.model;
        runInformations["nbLicenses"] = params.nbLicenses;
        runInformations["nbThreads"] = params.nbThreads;
        runInformations["instance"] = params.name;
        SFC::Instance inst = SFC::loadInstance(params.name);
        spdlog::info("Loaded network has {} nodes and {} edges\n",
            num_vertices(inst.network), num_edges(inst.network));
        auto start = std::chrono::steady_clock::now();
        std::seed_seq seed{{params.seed}};
        std::mt19937 gen(seed);
        const auto warmStart = [&]() -> std::optional<SFC::Solution> {
            if (params.nbLicenses >= num_vertices(inst.network)
                || params.skipWarmstart) {
                return std::nullopt;
            }
            return twoPhaseHeuristic(
                inst, params.nbLicenses, gen, params.nbThreads);
        }();
        if (!warmStart) {
            spdlog::info("No warmstart found!");
        } else {
            spdlog::info(
                "Warm start found with obj. value: {}", warmStart->objValue);
        }
        const auto toSecondsView = ranges::views::transform(
            [](const auto& _duration) { return _duration.count(); });
        const auto sol = [&]() -> std::optional<SFC::Solution> {
            if (params.model == "CGBnB") {
                SFC::CGBnBVisitor visitor;
                auto retval =
                    params.nbLicenses < num_vertices(inst.network)
                        ? SFC::columnGenerationBranchAndBound(inst,
                            params.nbLicenses, warmStart, params.nbThreads,
                            params.timeLimit, visitor)
                        : SFC::columnGenerationBranchAndBound(
                            inst, params.nbThreads, params.timeLimit, visitor);
                runInformations["lowerBound"] = visitor.bound;
                runInformations["generationTime"] =
                    std::chrono::duration<double>(
                        visitor.cgConvergedTime - visitor.algorithmStartTime)
                        .count();
                runInformations["columnMasterSolvingTime"] =
                    visitor.cgVisitor.masterSolvingDuration | toSecondsView;
                runInformations["columnGenerationTime"] =
                    visitor.cgVisitor.generationDuration | toSecondsView;
                runInformations["BnBTime"] = std::chrono::duration<double>(
                    visitor.algorithmEndTime - visitor.cgConvergedTime)
                                                 .count();
                return retval;
            } else if (params.model == "CRGBnB") {
                if (params.nbLicenses
                    == std::numeric_limits<std::size_t>::infinity()) {
                    spdlog::error("Model not adapted for no licence replicas");
                }
                SFC::CRGBnBVisitor visitor;
                auto retval = SFC::columnAndRowGenerationBranchAndBound(inst,
                    params.nbLicenses, warmStart, params.nbThreads,
                    params.timeLimit, visitor);
                runInformations["lowerBound"] = visitor.bound;
                runInformations["rowGenerationTime"] =
                    visitor.rowGenerationTime | toSecondsView;
                runInformations["rowMasterSolvingTime"] =
                    visitor.rowMasterSolvingTime | toSecondsView;
                runInformations["columnMasterSolvingTime"] =
                    visitor.columnMasterSolvingTime | toSecondsView;
                runInformations["columnGenerationTime"] =
                    visitor.columnGenerationTime | toSecondsView;
                runInformations["generationTime"] =
                    std::chrono::duration<double>(
                        visitor.crgConvergedTime - visitor.algorithmStartTime)
                        .count();
                runInformations["BnBTime"] = std::chrono::duration<double>(
                    visitor.algorithmEndTime - visitor.crgConvergedTime)
                                                 .count();
                return retval;
            } else if (params.model == "TP-Heur") {
                if (params.nbLicenses
                    == std::numeric_limits<std::size_t>::infinity()) {
                    SFC::CGBnBVisitor visitor;
                    return SFC::columnGenerationBranchAndBound(
                        inst, params.nbThreads, params.timeLimit, visitor);
                }
                return warmStart;
            } else {
                std::cout << "No method specified!\n";
                return std::nullopt;
            }
        }();
        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double> const elapsed_seconds = end - start;
        runInformations["executionTime"] = elapsed_seconds.count();
        if (!sol) {
            spdlog::info("No solution found\n");
        } else {
            spdlog::info("Found solution with {} paths after {}s: objValue: {}",
                sol->getServicePaths().size(), elapsed_seconds.count(),
                sol->objValue);
            runInformations["objValue"] = sol->objValue;
            for (const auto& path : sol->m_paths) {
                runInformations["paths"][path.demand] = {{"id", path.demand},
                    {"locations", path.locations},
                    {"path",
                        path.nPath
                            | ranges::views::transform([&](const auto& ed) {
                                  return inst.network[ed].id;
                              })}};
            }
        }
        if (!params.outputFilename.empty()) {
            std::ofstream ofs(params.outputFilename);
            ofs << runInformations;
        }
    } catch (const std::exception& e) {
        spdlog::error(e.what());
    }
}

