#ifndef STEINER_TREE_HPP
#define STEINER_TREE_HPP

#include <algorithm>
#include <memory>
#include <vector>

#include <CppRO/cplex_utility.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graph_utility.hpp>
#include <ilcplex/ilocplex.h>

template <typename Graph>
class SteinerTree {
  public:
    using Node = typename boost::graph_traits<Graph>::vertex_descriptor;

    SteinerTree(const Graph& _g, Node _r, std::vector<Node> _terminals)
        : m_graph(&_g)
        , m_root(_r)
        , m_terminals(std::move(_terminals))
        , m_f([&]() {
            std::vector<IloNumVar> retval(num_edges(*m_graph));
            std::generate(retval.begin(), retval.end(), [&]() {
                return IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
            });
            return retval;
        }())
        , m_fVal(m_f.size())
        , m_obj([&]() {
            IloExpr expr(m_env);
            for (const auto ed : boost::make_iterator_range(edges(*m_graph))) {
                expr += get(boost::edge_weight, *m_graph, ed)
                        * m_f[get(boost::edge_index, *m_graph, ed)];
            }
            return IloAdd(m_model, IloObjective(m_env, expr));
        }())
        , m_solver([&]() {
            IloCplex solver(m_model);
            solver.setParam(IloCplex::Threads, 1);
            solver.setOut(m_env.getNullStream());
            solver.setWarning(m_env.getNullStream());
            return solver;
        }()) {}

    ~SteinerTree() { m_env.end(); }

    bool solve(bool _verbose = false) {
        bool hasInvalidCuts = true;
        while (hasInvalidCuts) {
            if (m_solver.solve() == 0) {
                return false;
            }
            // Get current tree
            std::transform(
                m_f.begin(), m_f.end(), m_fVal.begin(), [&](auto&& _var) {
                    return epsilon_equal<double>()(
                        m_solver.getValue(_var), 1.0);
                });
            struct ActiveFilter {
                bool operator()(
                    typename boost::graph_traits<Graph>::edge_descriptor _ed)
                    const {
                    return (*fVal)[get(boost::edge_index, *graph, _ed)];
                }
                const Graph* graph;
                const std::vector<bool>* fVal;
            };
            boost::filtered_graph tree(
                *m_graph, ActiveFilter{m_graph, &m_fVal});
            std::vector<boost::default_color_type> inRootComponent(
                num_vertices(tree));
            depth_first_visit(tree, m_root,
                boost::dfs_visitor<boost::null_visitor>{},
                boost::make_iterator_property_map(inRootComponent.begin(),
                    get(boost::vertex_index, *m_graph)));

            if (std::any_of(
                    m_terminals.begin(), m_terminals.end(), [&](auto&& _vd) {
                        return inRootComponent[_vd]
                               != boost::default_color_type::black_color;
                    })) {
                // Current solution is not a valid steiner tree
                hasInvalidCuts = true;
                IloExpr expr(m_env);
                for (const auto ed :
                    boost::make_iterator_range(edges(*m_graph))) {
                    const auto [u, v] = incident(ed, *m_graph);
                    if (inRootComponent[u]
                            == boost::default_color_type::black_color
                        && inRootComponent[v]
                               != boost::default_color_type::black_color) {
                        expr += m_f[get(boost::edge_index, *m_graph, ed)];
                    }
                }
                IloAdd(m_model, m_cuts.emplace_back(expr >= 1));
                expr.end();
            } else {
                hasInvalidCuts = false;
            }
        }
        if (_verbose) {
            std::cout << m_cuts.size() << " generated\n";
        }
        return true;
    }

    /**
     * Return the predecessors of each node in the tree
     */
    std::vector<Node> getSolution() const {
        std::vector<Node> retval(num_vertices(*m_graph));
        for (Node u = 0; u < num_vertices(*m_graph); ++u) {
            retval[u] = u;
        }
        for (const auto ed : boost::make_iterator_range(edges(*m_graph))) {
            if (m_fVal[get(boost::edge_index, *m_graph, ed)]) {
                const auto [u, v] = incident(ed, *m_graph);
                retval[v] = u;
            }
        }
        return retval;
    }

    [[nodiscard]] double getObjValue() const { return m_solver.getObjValue(); }

    SteinerTree(const SteinerTree& _other)
        : SteinerTree(_other.m_graph, _other.m_root, _other.m_terminals) {}

    SteinerTree& operator=(const SteinerTree& _other) {
        if (this != &_other) {
            *this =
                SteinerTree(_other.m_graph, _other.m_root, _other.m_terminals);
        }
        return *this;
    }

    SteinerTree(SteinerTree&& _other) noexcept
        : m_graph(std::move(_other.m_graph))
        , m_root(std::move(_other.m_root))
        , m_terminals(std::move(_other.m_terminals))
        , m_env(std::move(_other.m_env))
        , m_model(std::move(_other.m_model))
        , m_obj(std::move(_other.m_obj))
        , m_f(std::move(_other.m_f))
        , m_fVal(std::move(_other.m_fVal))
        , m_solver(std::move(_other.m_solver)) {
        _other.m_env = IloEnv();
    }

    SteinerTree& operator=(SteinerTree&& _other) noexcept {
        if (this != &_other) {
            m_graph = std::move(_other.m_graph);
            m_root = std::move(_other.m_root);
            m_terminals = std::move(_other.m_terminals);
            m_env = std::move(_other.m_env);
            m_model = std::move(_other.m_model);
            m_obj = std::move(_other.m_obj);
            m_f = std::move(_other.m_f);
            m_fVal = std::move(_other.m_fVal);
            m_solver = std::move(_other.m_solver);
            _other.m_env = IloEnv();
        }
        return *this;
    }

  private:
    const Graph* m_graph;
    Node m_root;
    std::vector<Node> m_terminals;
    IloEnvWrapper m_env{};
    IloModel m_model{m_env};

    std::vector<IloNumVar> m_f; // m_f[l] = 1 if l belong to the multicast tree
    std::vector<bool> m_fVal;

    IloObjective m_obj;

    std::vector<IloRange> m_cuts;
    IloCplex m_solver;
};

#endif
