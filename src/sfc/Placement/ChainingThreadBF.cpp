#include "ChainingThreadBF.hpp"
#include "Decomposition.hpp"

namespace SFC {

PlacementDWD_Path::PlacementDWD_Path(const Instance& _inst)
    : m_inst(&_inst)
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_columns(_inst.demands.size())
    , m_onePathCons(getOnePathPerDemandConstraints(*m_inst, m_model))
    , m_linkCapasCons(getLinkCapacityConstraints(*m_inst, m_model))
    , m_nodeCapasCons(getNodeCapacityConstraints(*m_inst, m_model))
    , m_dummyPaths([&]() {
        std::vector<IloNumVar> retval(m_inst->demands.size());
        for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
            retval[i] = m_onePathCons[i](1.0)
                        + m_obj(2 * m_inst->demands[i].d
                                * int(num_vertices(m_inst->network)));
            m_model.add(retval[i]);
            setIloName(retval[i], "dummyPaths" + toString(m_inst->demands[i]));
        }
        return retval;
    }())
    , m_linkCharge(num_edges(m_inst->network))
    , m_nodeCharge(num_vertices(m_inst->network))

{}

void PlacementDWD_Path::addColumn(const PlacementDWD_Path::ColumnType& _col) {
    const auto& [nPath, locations, demandID] = _col;
    const auto& [id, s, t, d, chain] = m_inst->demands[demandID];
    IloNumColumn col =
        m_onePathCons[demandID](1.0) + m_obj(d * int(nPath.size() - 1));

    std::fill(m_linkCharge.begin(), m_linkCharge.end(), 0.0);
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
         ++iteU, ++iteV) {
        m_linkCharge[m_inst->network[edge(*iteU, *iteV, m_inst->network).first]
                         .id] += d;
    }
    const auto mult = [](const IloRange& _const, double _val) {
        return _const(_val);
    };
    const auto add = [](auto&& _acc, auto&& _newVal) {
        return _acc += _newVal;
    };
    col += std::inner_product(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_linkCharge.begin(), IloNumColumn(m_env), add, mult);

    std::fill(m_nodeCharge.begin(), m_nodeCharge.end(), 0.0);
    for (std::size_t j = 0; j < locations.size(); ++j) {
        m_nodeCharge[locations[j]] += m_inst->nbCores[long(demandID)][long(j)];
    }
    col += std::inner_product(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_nodeCharge.begin(), IloNumColumn(m_env), add, mult);
    m_columns[_col.demand].emplace_back(IloNumVar(col), _col);
    IloAdd(m_model, m_columns[_col.demand].back().var);
}

double PlacementDWD_Path::getReducedCost(
    const PlacementDWD_Path::ColumnType& _col,
    const DualValues& _dualValues) const {
    const auto& [nPath, locations, demandID] = _col;
    double retval = -_dualValues.m_onePathDuals[demandID];
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
         ++iteU, ++iteV) {
        retval += _dualValues.getReducedCost(IntraLayerLink{demandID,
            {*iteU, *iteV}, 0}); /// layer # is not use for the reduced cost
    }
    for (std::size_t j = 0; j < locations.size(); ++j) {
        retval += _dualValues.getReducedCost(
            CrossLayerLink{demandID, locations[j], j});
    }
    return retval;
}

bool PlacementDWD_Path::isImprovingColumn(
    const ColumnType& _col, const SFC::DualValues& _dualValues) const {
    return epsilon_less<double>()(getReducedCost(_col, _dualValues), 0.0);
}

DualValues PlacementDWD_Path::getDualValues(const IloCplex& _solver) const {
    DualValues retval(*m_inst);
    const auto getDual = [&](const IloRange& _cons) {
        return _solver.getDual(_cons);
    };
    std::transform(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        retval.m_linkCapasDuals.begin(), getDual);
    std::transform(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        retval.m_nodeCapasDuals.begin(), getDual);
    std::transform(m_onePathCons.begin(), m_onePathCons.end(),
        retval.m_onePathDuals.begin(), getDual);
    return retval;
}

bool PlacementDWD_Path::isImprovingColumn(
    const ColumnType& _col, const DualValues& _duals) const {
    return epsilon_less<double>()(getReducedCost(_col, _duals), 0.0);
}

DualValues::DualValues(const Instance& _inst)
    : inst(&_inst)
    , m_onePathDuals(_inst.demands.size(), 0.0)
    , m_linkCapasDuals(num_edges(_inst.network), 0.0)
    , m_nodeCapasDuals(num_vertices(_inst.network), 0.0) {}

IloNum DualValues::getReducedCost(const CrossLayerLink& _link) const {
    return -inst->nbCores[long(_link.demandID)][long(_link.j)]
           * m_nodeCapasDuals[_link.u];
}

IloNum DualValues::getReducedCost(const IntraLayerLink& _link) const {
    return inst->demands[_link.demandID].d
           * (1
               - m_linkCapasDuals[inst
                                      ->network[edge(_link.edge.first,
                                          _link.edge.second, inst->network)
                                                    .first]
                                      .id]);
}

double DualValues::getDualSumRHS() const {
    double retval = 0.0;
    for (const auto u : boost::make_iterator_range(vertices(inst->network))) {
        if (inst->network[u].isNFV) {
            retval += m_nodeCapasDuals[u] * inst->network[u].capacity;
        }
    }
    for (const auto ed : boost::make_iterator_range(edges(inst->network))) {
        retval +=
            m_linkCapasDuals[inst->network[ed].id] * inst->network[ed].capacity;
    }
    retval +=
        std::accumulate(m_onePathDuals.begin(), m_onePathDuals.end(), 0.0);
    return retval;
}

double DualValues::getAdditionalVariable() const { return 0.0; }

} // namespace SFC
