#include <CppRO/cplex_utility.hpp>
#include <KMedoids.hpp>
#include <iosfwd>
#include <optional>
#include <tclap/CmdLine.hpp>
#include <tclap/UnlabeledValueArg.hpp>
#include <tclap/ValueArg.hpp>
#include <tclap/ValuesConstraint.hpp>

#include "BranchAndBound.hpp"
#include "ColumnGeneration.hpp"
#include "LinearProgramming.hpp"
#include "MyTimer.hpp"
#include "PricingProblem.hpp"

#include "SFC/ChainingPathOccLimit.hpp"
#include "SFC/ChainingPathOccLimitDoublePP.hpp"
#include "SFC/ChainingThreadBF.hpp"
#include "SFC/Localisation.hpp"
#include "SFC/LocalisationPerFunction.hpp"
#include "SFC/OccLim.hpp"
#include "SFC/OccLim/Algorithm.hpp"
#include "SFC/OccLim/CompactFormulation.hpp"

struct Param {
    std::string model;
    std::string name;
    int nbThreads;
    bool verbose;
    double factor;
    double alpha;
    double epsilon;
    double percentDemand;
    int replicationLimit;
};

Param getParams(int argc, char** argv);
std::optional<SFC::OccLim::Solution> dispatch(
    const SFC::OccLim::Instance& _inst, const Param& _params,
    const ColumnGenerationParameters& _cgParams,
    const StabilizationParameters& _stabParams,
    const BaseBranchAndPriceParameters& _bapParams);

Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with limits on the function "
                       "replicas with column generation",
        ' ', "1.0");

    TCLAP::ValuesConstraint<std::string> vCons(
        {"lowerBound", "path_loc", "ILP", "benders", "pathPred", "path_heur_pf",
            "heur", "heur_pf", "ILP", "LP", "heur_pf_asc", "heur_pf_desc", "BB",
            "heur_pf_nr", "heur_pf_med_asc", "heur_pf_med_desc",
            "pathThread_stab", "path_alpha", "path_alpha_bab", "path_em"});
    TCLAP::ValueArg<std::string> modelName(
        "m", "model", "Specify the model used", false, "path_alpha", &vCons);
    cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> networkName(
        "network", "Specify the network name", true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<int> replicationLimit("r", "replicationLimit",
        "Specify the maximum amount of replication in functions", true, 3,
        "int >= 0");
    cmd.add(&replicationLimit);

    TCLAP::ValueArg<double> demandFactor("d", "demandFactor",
        "Specify the multiplicative factor for the demands", false, 1,
        "double");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<double> alpha(
        "a", "alpha", "Specify the alpha", false, -1, "double");
    cmd.add(&alpha);

    TCLAP::ValueArg<double> epsilon(
        "e", "epsilon", "Specify the epsilon", false, 1e-6, "double");
    cmd.add(&epsilon);

    TCLAP::ValueArg<bool> verbosity(
        "v", "verbosity", "Specify the verbosity", false, false, "bool");
    cmd.add(&verbosity);

    TCLAP::ValueArg<int> nbThreads(
        "j", "nbThreads", "Specify the number of threads", false, 1, "int");
    cmd.add(&nbThreads);

    // TCLAP::ValueArg<int> functionChages(
    //     "c", "functionChages", "Specify the index for the function charge
    //     file", false, 4, "int >= 0");
    // cmd.add(&functionChages);

    TCLAP::ValueArg<double> percentDemand("p", "percentDemand",
        "Specify the percentage of demand used", false, 100.0, "double");
    cmd.add(&percentDemand);

    cmd.parse(argc, argv);

    return {modelName.getValue(), networkName.getValue(), nbThreads.getValue(),
        verbosity.getValue(), demandFactor.getValue(), alpha.getValue(),
        epsilon.getValue(), percentDemand.getValue(),
        replicationLimit.getValue()};
}

int main(const int argc, char** argv) {
    std::cout << std::fixed;
    auto params = getParams(argc, argv);

    ColumnGenerationParameters cgParams{params.nbThreads, params.verbose};
    StabilizationParameters stabParams{
        params.nbThreads, params.verbose, params.alpha, 1e-6};
    BaseBranchAndPriceParameters bapParams{
        params.nbThreads, params.verbose, std::chrono::seconds(3600), 1e-4};
    std::string folderName = "./instances/occlim/";
    DiGraph network = SFC::loadNetwork(folderName + params.name + "_topo.txt");
    const auto [allDemands, funcCharge] = SFC::loadDemands(
        folderName + params.name + "_demand.txt", params.factor);
    const auto& allDemands_ref = allDemands;
    std::cout << "Function requirements: " << funcCharge << '\n';
    auto nodeCapas =
        SFC::loadNodeCapa(folderName + params.name + "_nodeCapa.txt");
    std::cout << "Files loaded...\n";

    const int nbDemands = static_cast<int>(
        std::ceil(double(allDemands.size()) * params.percentDemand / 100.0));
    std::vector<SFC::Demand<>> demands(
        allDemands.begin(), allDemands.begin() + nbDemands);

    std::cout << "# demands: " << nbDemands << '\n';
    int totalNbCores = 0;
    for (const auto& demand : demands) {
        for (int j = 0; j < demand.functions.size(); ++j) {
            totalNbCores +=
                int(ceil(demand.d / funcCharge[demand.functions[j]]));
        }
    }
    std::cout << totalNbCores << " needed cores!" << '\n';

    std::vector<Node> NFVNodes;
    for (Node u = 0; u < num_vertices(network); ++u) {
        if (nodeCapas[u] != -1) {
            NFVNodes.push_back(u);
        }
    }
    int availableCores = 0;
    for (const auto& u : NFVNodes) {
        availableCores += nodeCapas[u];
    }
    std::cout << availableCores << " cores available !\n";

    SFC::OccLim::Instance inst(
        network, nodeCapas, demands, funcCharge, params.replicationLimit);
    saveAsGraphviz("layeredGraph.dot", getLayeredGraph(inst));
    const std::string filename = [&]() {
        std::string retval = "./results/occlim/" + params.name;
        if (nbDemands != allDemands_ref.size()) {
            retval += "_" + std::to_string(nbDemands);
        }
        retval += "_" + params.model;
        if (params.alpha >= 0.0) {
            retval += "_stab_" + std::to_string(params.alpha);
        }
        retval += "_" + std::to_string(params.factor) + "_"
                  + std::to_string(params.replicationLimit) + ".res";
        return retval;
    }();

    Time timer;
    timer.start();
    const auto sol = dispatch(inst, params, cgParams, stabParams, bapParams);
    if (sol.has_value() and sol->isValid()) {
        sol->save(filename, timer.get());
    } else {
        std::cout << "No solution found\n";
        timer.get(true);
    }
}

template <typename Master, typename... Pricing>
std::optional<SFC::OccLim::Solution> solveWithCG(
    const SFC::OccLim::Instance& _inst,
    const ColumnGenerationParameters& _cgParams,
    const StabilizationParameters& _stabParams) {
    Master rmp(_inst);
    bool solved = false;
    if (_stabParams.alpha >= 0.0) {
        solved = solveStabilized(
            rmp, std::make_tuple(Pricing(_inst)...), _stabParams, true);
    } else {
        solved =
            solve(rmp, std::make_tuple(Pricing(_inst)...), _cgParams, true);
    }
    if (solved) {
        return rmp.getSolution();
    }
    return std::nullopt;
}

std::optional<SFC::OccLim::Solution> dispatch(
    const SFC::OccLim::Instance& _inst, const Param& _params,
    const ColumnGenerationParameters& _cgParams,
    const StabilizationParameters& _stabParams,
    const BaseBranchAndPriceParameters& _bapParams) {
    if (_params.model == "path_alpha") {
        return solveWithCG<SFC::OccLim::Alpha::Master,
            SFC::ShortestPathPerDemand>(_inst, _cgParams, _stabParams);
    } else if (_params.model == "path_loc") {
        return solveWithCG<SFC::OccLim::DoublePlacement,
            SFC::ShortestPathPerDemand, SFC::OccLim::LocationPricingProblem>(
            _inst, _cgParams, _stabParams);
    } else if (_params.model == "path_alpha_bab") {
        return solveWithBandP(_inst, _cgParams, _stabParams, _bapParams);
    } else if (_params.model == "heur") {
        /**
         * First solve the CG model by fixing the values of the function
         * placement then resolve it by removing the constraints
         */
        return heur(_inst, _cgParams, _stabParams, _bapParams);
    } else if (_params.model == "ILP") {
        std::cout << "Solving the problem with the compact formulation\n";
        SFC::OccLim::CompactFormulation ilp(_inst);
        IloCplex solver(ilp.getModel());
        if (solver.solve()) {
            return ilp.getSolution(solver);
        }
    } else if (_params.model == "path_heur_pf") {
        // First, get solution from heur
        // heur_pf
        std::vector<int> functions(_inst.funcCharge.size());
        std::generate(functions.begin(), functions.end(),
            [i = 0]() mutable { return i++; });

        SFC::ShortestPathPerDemand_Fixed pricing1(
            _inst, getFunctionPlacement_perFunc(_inst, functions));
        SFC::ShortestPathPerDemand_Fixed pricing2(
            _inst, getFunctionPlacement_perFunc(
                       _inst, getFunctionsAscendingCores(_inst)));
        SFC::ShortestPathPerDemand_Fixed pricing3(
            _inst, getFunctionPlacement_perFunc(
                       _inst, getFunctionsDescendingCores(_inst)));

        std::optional<SFC::Solution> bestSolution = std::nullopt;
        for (auto& pricing : {pricing1, pricing2, pricing3}) {
            IloCplex solver;
            SFC::PlacementDWD_Path rmp(_inst);
            if (solve(rmp, solver, std::make_tuple(pricing), _cgParams)) {
                if (bestSolution
                    && bestSolution->getObjValue() > rmp.getObjValue()) {
                    bestSolution = rmp.getSolution();
                }
            }
        }
        if (bestSolution) {
            SFC::OccLim::Alpha::Master freeRMP(_inst);
            const auto columns = bestSolution->getServicePaths();
            freeRMP.addColumns(columns.begin(), columns.end());
            if (solve(freeRMP,
                    std::make_tuple(SFC::ShortestPathPerDemand(_inst)),
                    _cgParams)) {
                return freeRMP.getSolution();
            }
        }
    } else if (_params.model == "path_heur_pf_med") {
        // First, get solution from heur
        // heur_pf
        std::vector<int> functions(_inst.funcCharge.size());
        std::generate(functions.begin(), functions.end(),
            [i = 0]() mutable { return i++; });

        using Pricing = SFC::ShortestPathPerDemand_Fixed;
        const auto locations1 = getFunctionPlacement_medoids(_inst, functions);
        const auto locations2 = getFunctionPlacement_medoids(
            _inst, getFunctionsAscendingCores(_inst));
        const auto locations3 = getFunctionPlacement_medoids(
            _inst, getFunctionsDescendingCores(_inst));

        std::optional<SFC::Solution> bestSolution = std::nullopt;
        for (auto& locations : {locations1, locations2, locations3}) {
            SFC::PlacementDWD_Path rmp(_inst);
            if (solve(rmp, std::make_tuple(Pricing(_inst, locations)),
                    _cgParams)) {
                if (bestSolution
                    && bestSolution->getObjValue() > rmp.getObjValue()) {
                    bestSolution = rmp.getSolution();
                }
            } else {
                std::cout << "No solution found\n";
            }
        }
        if (bestSolution) {
            SFC::OccLim::Alpha::Master freeRMP(_inst);
            const auto columns = bestSolution->getServicePaths();
            freeRMP.addColumns(columns.begin(), columns.end());
            if (solve(freeRMP,
                    std::make_tuple(SFC::ShortestPathPerDemand(_inst)),
                    _cgParams)) {
                return freeRMP.getSolution();
            }
        }
    } else if (_params.model == "heur_pf_asc") {
        /**
         * First solve a location problem each function based on the
         * capacities of the nodes. Then solve the routing on the network
         * with the previously found locations
         */
        using Pricing = SFC::ShortestPathPerDemand_Fixed;
        Pricing pricing(_inst, getFunctionPlacement_perFunc(
                                   _inst, getFunctionsAscendingCores(_inst)));

        SFC::PlacementDWD_Path rmp(_inst);
        if (solve(rmp, std::make_tuple(pricing), _cgParams)) {
            return SFC::OccLim::Solution{_inst, rmp.getSolution()};
        }
    } else if (_params.model == "heur_pf_desc") {
        /*
         * First solve a location problem each function based on the
         * capacities of the nodes. Then solve the routing on the network
         * with the previously found locations
         */
        using Pricing = SFC::ShortestPathPerDemand_Fixed;
        Pricing pricing(_inst, getFunctionPlacement_perFunc(
                                   _inst, getFunctionsDescendingCores(_inst)));

        SFC::PlacementDWD_Path rmp(_inst);
        if (solve(rmp, std::make_tuple(pricing), _cgParams)) {
            return SFC::OccLim::Solution{_inst, rmp.getSolution()};
        }
    } else if (_params.model == "heur_pf_med_asc") {
        /*
        First solve a location problem each function based on the capacities
        of the nodes. Then solve the routing on the network with the
        previously found locations
        */
        using Pricing = SFC::ShortestPathPerDemand_Fixed;
        Pricing pricing(_inst, getFunctionPlacement_medoids(
                                   _inst, getFunctionsAscendingCores(_inst)));

        SFC::PlacementDWD_Path rmp(_inst);
        if (solve(rmp, std::make_tuple(pricing), _cgParams)) {
            return SFC::OccLim::Solution{_inst, rmp.getSolution()};
        }
    } else if (_params.model == "heur_pf") {
        std::vector<int> functions(_inst.funcCharge.size());
        std::generate(functions.begin(), functions.end(),
            [i = 0]() mutable { return i++; });

        using Pricing = SFC::ShortestPathPerDemand_Fixed;
        Pricing pricing(_inst, getFunctionPlacement_perFunc(_inst, functions));

        SFC::PlacementDWD_Path rmp(_inst);
        if (solve(rmp, std::make_tuple(pricing), _cgParams)) {
            return SFC::OccLim::Solution{_inst, rmp.getSolution()};
        }
    } else if (_params.model == "heur_pf_med_desc") {
        using Pricing = SFC::ShortestPathPerDemand_Fixed;
        Pricing pricing(_inst, getFunctionPlacement_medoids(
                                   _inst, getFunctionsDescendingCores(_inst)));

        SFC::PlacementDWD_Path rmp(_inst);
        if (solve(rmp, std::make_tuple(pricing), _cgParams)) {
            return SFC::OccLim::Solution{_inst, rmp.getSolution()};
        }
    }
    return std::nullopt;
}

