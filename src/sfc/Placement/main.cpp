#include "PricingProblem.hpp"
#include "SFC.hpp"
#include <iostream>
#include <string>

#include "MyTimer.hpp"
#include "SFC/ChainingThreadBF.hpp"
#include "SFC/SFCIlp.hpp"
#include <lyra/lyra.hpp>

struct CliArgs {
    std::string model;
    std::string name;
    std::size_t nbThreads{1};
    double factor{1};
    double percentDemand{100};
    bool help{false};

    auto getCli() {
        return lyra::cli() | lyra::help(help)
               | lyra::arg(model, "model")("Specify the model used")
                     .choices("pathThreadBF", "pathThread", "ILP")
               | lyra::arg(name, "networkName")("Specify the network name")
               | lyra::opt(factor, "-d")(
                   "Specify the multiplicative factor for the demands")
               | lyra::opt(nbThreads, "-j")("Specify the number of threads")
               | lyra::opt(percentDemand, "-p")(
                   "Specify the percentage of demands");
    }
};

int main(int argc, char** argv) {
    std::cout << std::fixed;
    CliArgs params;
    auto cli = params.getCli();
    auto result = cli.parse({argc, argv});
    if (!result) {
        std::cerr << "Error in command line: " << result.message() << std::endl;
        return 1;
    }
    if (params.help) {
        std::cout << cli << '\n';
        return 0;
    }

    const DiGraph network =
        SFC::loadNetwork("./instances/" + params.name + "_topo.txt");
    const auto [allDemands, funcCharge] = SFC::loadDemands(
        "./instances/" + params.name + "_demand.txt", params.factor);
    auto nodeCapas =
        SFC::loadNodeCapa("./instances/" + params.name + "_nodeCapa.txt");
    std::cout << "Files loaded...\n";
    std::cout << "Loaded network has " << num_vertices(network) << " nodes and "
              << num_edges(network) << " edges\n";

    const auto nbDemands = static_cast<std::size_t>(std::ceil(
        params.percentDemand * static_cast<double>(allDemands.size()) / 100.0));
    if (params.percentDemand < 100.0) {
        std::cout << "Using only " << nbDemands << " / " << allDemands.size()
                  << " demands.\n";
    }
    std::vector<SFC::Demand<>> demands(
        allDemands.begin(), allDemands.begin() + int(nbDemands));

    std::size_t totalNbCores = 0;
    for (const auto& demand : demands) {
        for (std::size_t j = 0; j < demand.functions.size(); ++j) {
            totalNbCores +=
                std::size_t(ceil(demand.d / funcCharge[demand.functions[j]]));
        }
    }
    std::cout << totalNbCores << " needed cores!" << '\n';

    std::vector<Node> NFVNodes;
    for (Node u = 0; u < num_vertices(network); ++u) {
        if (nodeCapas[u] > 0) {
            NFVNodes.push_back(u);
        }
    }
    double availableCores = 0;
    for (const auto& u : NFVNodes) {
        availableCores += nodeCapas[u];
    }
    std::cout << availableCores << " cores available !\n";

    SFC::Instance inst(network, nodeCapas, demands, funcCharge);
    Time timer;
    timer.start();
    // std::cout << "Getting initial configuration...\n";
    // SFC::AugmentedGraph augGraph(inst);
    // /* Find first initial configuration */
    // const auto sPaths = augGraph.getInitialConfiguration(demands);
    // // augGraph.showNetworkUsage();

    // if (sPaths.size() < demands.size()) {
    //     std::cout << "No initial configuration\n";
    //     return 1;
    // }
    std::string filename =
        epsilon_equal<double>()(params.percentDemand, 100)
            ? "./results/" + params.name + "_" + params.model + "_"
                  + std::to_string(params.factor) + ".res"
            : "./results/" + params.name + "_" + std::to_string(nbDemands) + "_"
                  + params.model + "_" + std::to_string(params.factor) + ".res";
    if (params.model == "pathThreadBF") {
        using Master = SFC::PlacementDWD_Path;
        using Pricing = SFC::ShortestPathPerDemand;
        Master rmp(inst);
        CppRO::ColumnGeneration::ParallelPricer<Pricing,
            std::vector<SFC::Demand<>>::iterator>
            pricer(params.nbThreads, {inst.demands.begin(), inst.demands.end()},
                inst);
        IloCplex solver;
        solver.extract(rmp.getModel());
        auto solveFunc =
            [&](const Master& _rmp) -> std::optional<SFC::DualValues> {
            return CppRO::ColumnGeneration::solve<Master, SFC::DualValues>(
                _rmp, solver);
        };
        auto genColFunc = [&](auto& _rmp, const auto& _dualValues) {
            pricer.generateColumnns(_dualValues);
            return moveColumns(_rmp, pricer, _dualValues);
        };

        try {
            CppRO::ColumnGeneration::solve(rmp, solveFunc, genColFunc, true);
        } catch (IloException& e) {
            std::cout << e.getMessage() << '\n';
        }
    } else if (params.model == "pathThread") {
        /*
        using Master = SFC::PlacementDWD_Path;
        using Pricing = SFC::ShortestPathPerDemand_LP;
        Master rmp(inst);
        IloCplex solver;
        solver.extract(rmp.getModel());
        auto solveFunc = [&] { return solver.solve(); };
        try {
            if (solve(rmp, solveFunc, std::make_tuple(Pricing(inst)),
                    {params.nbThreads, true})) {
                const auto sol = rmp.getSolution();
                const auto time = timer.get();
                if (!sol.isValid()) {
                    std::cerr << "Solution is not valid\n";
                    return 86;
                }
                sol.save(filename, time);
            }
        } catch (IloException& e) {
            std::cout << e.getMessage() << '\n';
        }
    } else if (params.model == "ILP") {
        SFC::ILP ilp(inst);
        if (ilp.solve()) {
            const auto time = timer.get();
            ilp.save(filename, time);
        }
        */
    } else {
        std::cout << "No method specified!\n";
    }
}
