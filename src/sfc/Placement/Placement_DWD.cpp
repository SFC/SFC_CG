#include "ChainingPathOccLimit.hpp"
#include "Decomposition.hpp"

namespace SFC::OccLim::Beta {

using ranges = boost::multi_array_types::index_range;

Master::Master(const Instance* _inst)
    : Base(DualValues(_inst))
    , m_inst(_inst)
    , m_b([&]() {
        boost::multi_array<IloNumVar, 2> b(boost::extents[num_vertices(m_inst->network)][m_inst->funcCharge.size()]);
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            for (Node u = 0; u < num_vertices(m_inst->network); ++u) {
                b[u][f] = IloNumVar(m_env);
                IloAdd(m_integerConversions, IloConversion(m_env, b[u][f], ILOBOOL));
                IloAdd(m_model, b[u][f]);
                setIloName(b[u][f], "b[u=" + std::to_string(u) + ", f=" + std::to_string(f) + "]");
            }
        }
        return b;
    }())
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_onePathCons(getOnePathPerDemandConstraints(*m_inst, m_model))
    , m_linkCapasCons(getLinkCapacityConstraints(*m_inst, m_model))
    , m_nodeCapasCons(getNodeCapacityConstraints(*m_inst, m_model))
    , m_nbLicensesCons([&]() {
        std::vector<IloRange> funcLicensesCons(m_inst->funcCharge.size());
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            const auto allNode_f = m_b[boost::indices[ranges(0, num_vertices(m_inst->network))][f]];
            funcLicensesCons[f] =
                IloAdd(m_model, IloRange(m_env, -IloInfinity,
                                    std::accumulate(allNode_f.begin(), allNode_f.end(), IloExpr(m_env)),
                                    m_inst->nbLicenses));
            setIloName(funcLicensesCons[f],
                "funcLicensesCons(" + toString(f) + ")");
            // vars.end();
        }
        return funcLicensesCons;
    }())
    , m_pathFuncCons([&]() {
        boost::multi_array<IloRange, 3> pathFuncCons(boost::extents[m_inst->demands.size()][m_inst->funcCharge.size()][num_vertices(m_inst->network)]);
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
                for (const auto u : boost::make_iterator_range(vertices(m_inst->network))) {
                    if (m_inst->network[u].isNFV) {
                        pathFuncCons[i][f][u] = IloAdd(m_model, IloRange(m_env, 0.0, m_b[u][f], IloInfinity));
                    }
                }
            }
        }
        return pathFuncCons;
    }())
    , m_dummyPaths([&]() {
        std::vector<IloNumVar> retval(m_inst->demands.size());
        for (int i = 0; i < retval.size(); ++i) {
            retval[i] = m_onePathCons[i](1.0)
                        + m_obj(2 * m_inst->demands[i].d * num_vertices(m_inst->network));
            m_model.add(retval[i]);
            m_integerConversions.add(retval[i] == 0);
            setIloName(retval[i], "dummyPaths" + toString(m_inst->demands[i]));
        }
        return retval;
    }())
    , m_linkCharge(num_edges(m_inst->network))
    , m_nodeCharge(num_vertices(m_inst->network))
    , m_funcPath(boost::extents[m_inst->funcCharge.size()][num_vertices(m_inst->network)]) {}

double Master::getReducedCost_impl(const ServicePath& _col, const DualValues& _dualValues) const {
    const auto& [nPath, locations, demandID] = _col;
    double retval = -_dualValues.m_onePathDuals[demandID];

    for (auto iteU = nPath.begin(), iteV = std::next(iteU);
         iteV != nPath.end(); ++iteU, ++iteV) {
        retval += _dualValues.getReducedCost(IntraLayerLink{demandID, {*iteU, *iteV}, -1}); /// layer # is not use for the reduced cost
    }

    for (int j = 0; j < locations.size(); ++j) {
        retval += _dualValues.getReducedCost(CrossLayerLink{demandID, locations[j], j});
    }
    return retval;
}

IloNumColumn Master::getColumn_impl(const ServicePath& _sPath) {
    const auto mult = [](const IloRange& _const, double _val) {
        return _const(_val);
    };
    const auto add = [](auto&& _acc, auto&& _newVal) {
        return _acc += _newVal;
    };

    IloNumColumn col = m_onePathCons[_sPath.demand](1.0)
                       + m_obj(m_inst->demands[_sPath.demand].d * (_sPath.nPath.size() - 1));

    std::fill(m_linkCharge.begin(), m_linkCharge.end(), 0.0);
    for (auto iteU = _sPath.nPath.begin(), iteV = std::next(iteU); iteV != _sPath.nPath.end();
         ++iteU, ++iteV) {
        m_linkCharge[m_inst->network[edge(*iteU, *iteV, m_inst->network).first].id] += m_inst->demands[_sPath.demand].d;
    }
    col += std::inner_product(m_linkCapasCons.begin(), m_linkCapasCons.end(), m_linkCharge.begin(),
        IloNumColumn(m_env), add, mult);

    std::fill_n(m_funcPath.data(), m_funcPath.num_elements(), 0.0);
    std::fill(m_nodeCharge.begin(), m_nodeCharge.end(), 0.0);
    for (int j = 0; j < _sPath.locations.size(); ++j) {
        // Add to node capa constraints
        m_nodeCharge[_sPath.locations[j]] += m_inst->nbCores[_sPath.demand][j];
        // Add to placement constraint
        m_funcPath[m_inst->demands[_sPath.demand].functions[j]][_sPath.locations[j]] = -1.0;
    }

    col += std::inner_product(m_nodeCapasCons.begin(), m_nodeCapasCons.end(), m_nodeCharge.begin(),
        IloNumColumn(m_env), add, mult);
    col += std::inner_product(m_pathFuncCons[_sPath.demand].begin(), m_pathFuncCons[_sPath.demand].end(), m_funcPath.data(),
        IloNumColumn(m_env), add, mult);
    return col;
}

void Master::getDuals_impl() {
    const auto get = [&](auto&& _cons) {
        return getDual(m_solver, _cons);
    };
    std::transform(m_linkCapasCons.begin(), m_linkCapasCons.end(), m_duals.m_linkCapasDuals.begin(), get);
    std::transform(m_nodeCapasCons.begin(), m_nodeCapasCons.end(), m_duals.m_nodeCapasDuals.begin(), get);
    std::transform(m_pathFuncCons.data(), m_pathFuncCons.data() + m_pathFuncCons.num_elements(),
        m_duals.m_pathFuncDuals.data(), get);
    std::transform(m_onePathCons.begin(), m_onePathCons.end(), m_duals.m_onePathDuals.begin(), get);
    std::transform(m_nbLicensesCons.begin(), m_nbLicensesCons.end(), m_duals.m_nbLicensesDuals.begin(), get);
}

void Master::removeDummyIfPossible() {
    if (m_dummyActive) {
        const double dummySum = std::accumulate(m_dummyPaths.begin(), m_dummyPaths.end(), 0.0,
            [&](auto&& _acc, auto&& _var) {
                return _acc + m_solver.getValue(_var);
            });
        // std::cout << "dummySum: " << dummySum << '\n';
        if (epsilon_equal<double>()(dummySum, 0.0)) {
            m_dummyActive = false;
            for (auto& dummy : m_dummyPaths) {
                m_model.add(dummy == 0);
                m_integerConversions.add(dummy == 0);
            }
        }
    }
}

const DualValues& Master::getDualValues_impl() const {
    return m_duals;
}

std::vector<ServicePath> Master::getServicePaths() const {
    std::vector<ServicePath> sPaths(m_inst->demands.size());
    for (const auto& [var, sPath] : getColumnStorage<ServicePath>()) {
        if (epsilon_equal<double>()(m_solver.getValue(var), IloTrue)) {
            sPaths[sPath.demand] = sPath;
        }
    }
    return sPaths;
}

Master::Solution Master::getSolution() const {
    return Master::Solution(*m_inst, getIntegerObjValue(), getRelaxationObjValue(),
        getServicePaths(), getNbColumns());
}

void barycenter(const DualValues& _nDuals, const DualValues& _bDuals,
    DualValues& _sDuals, double _alpha) {
    const auto update = [_alpha](const double _a, const double _b) -> double {
        return _alpha * _a + ((1 - _alpha) * _b);
    };

    std::transform(_nDuals.m_linkCapasDuals.begin(), _nDuals.m_linkCapasDuals.end(),
        _bDuals.m_linkCapasDuals.begin(), _sDuals.m_linkCapasDuals.begin(), update);
    std::transform(_nDuals.m_nodeCapasDuals.begin(), _nDuals.m_nodeCapasDuals.end(),
        _bDuals.m_nodeCapasDuals.begin(), _sDuals.m_nodeCapasDuals.begin(), update);
    std::transform(_nDuals.m_onePathDuals.begin(), _nDuals.m_onePathDuals.end(),
        _bDuals.m_onePathDuals.begin(), _sDuals.m_onePathDuals.begin(), update);
    std::transform(_nDuals.m_pathFuncDuals.data(), _nDuals.m_pathFuncDuals.data() + _nDuals.m_pathFuncDuals.num_elements(),
        _bDuals.m_pathFuncDuals.data(), _sDuals.m_pathFuncDuals.data(), update);
    std::transform(_nDuals.m_nbLicensesDuals.begin(), _nDuals.m_nbLicensesDuals.end(),
        _bDuals.m_nbLicensesDuals.begin(), _sDuals.m_nbLicensesDuals.begin(), update);
}

DualValues::DualValues(const Instance* _inst)
    : inst(_inst)
    , m_onePathDuals(inst->demands.size(), 0.0)
    , m_linkCapasDuals(num_edges(inst->network), 0.0)
    , m_nodeCapasDuals(num_vertices(inst->network), 0.0)
    , m_pathFuncDuals(boost::extents[_inst->demands.size()][_inst->funcCharge.size()][num_vertices(_inst->network)])
    , m_nbLicensesDuals(inst->funcCharge.size(), 0.0) {}

IloNum DualValues::getReducedCost(const CrossLayerLink& _link) const {
    return -inst->nbCores[_link.demandID][_link.j] * m_nodeCapasDuals[_link.u]
           - m_pathFuncDuals[_link.demandID][inst->demands[_link.demandID].functions[_link.j]][_link.u];
}

IloNum DualValues::getReducedCost(const IntraLayerLink& _link) const {
    return inst->demands[_link.demandID].d * (1 - m_linkCapasDuals[inst->network[edge(_link.edge.first, _link.edge.second, inst->network).first].id]);
}

double DualValues::getDualSumRHS() const {
    double retval = std::inner_product(m_nodeCapasDuals.begin(), m_nodeCapasDuals.end(), vertices(inst->network), 0.0, std::plus<>(), [&](auto&& dual, auto&& u) {
        return dual * inst->network[u].capacity;
    });
    for (const auto ed : boost::make_iterator_range(edges(inst->network))) {
        retval += m_linkCapasDuals[inst->network[ed].id] * inst->network[ed].capacity;
    }
    retval += std::accumulate(m_onePathDuals.begin(), m_onePathDuals.end(), 0.0);
    retval += std::accumulate(m_nbLicensesDuals.begin(), m_nbLicensesDuals.end(), 0.0) * inst->nbLicenses;
    return retval;
}

} // namespace SFC::OccLim::Beta
