#include "AugmentedGraph.hpp"
#include "ChainingPathBuilder.hpp"
#include <MyTimer.hpp>
#include <cstdlib>

std::string getDemandFile(const std::string& name, int _i);
std::vector<Demand> loadDemands(const std::string& _filename, double _percent);
DiGraph loadNetwork(const std::string& _filename);
std::vector<double> loadFunctions(const std::string& _filename);
void saveNodeCapas(const std::vector<int>& _nodeCapas,
    const std::string& _filename);
void saveTopo(const DiGraph& _graph, const std::string& _filename);

int main(int argc, char** argv) {
    try {
        if (argc < 6) {
            std::cerr << "Missing arguments! -> {genType} {name} {percent} "
                         "{funcCoef} {nbNFV}"
                      << std::endl;
        } else {
            const std::string genType = argv[1];
            const std::string name = argv[2];
            const double percent = std::stod(argv[3]);
            const int funcCoef = std::stoi(argv[4]);
            const int nbNFV = std::stoi(argv[5]);

            const DiGraph network = loadNetwork("./instances/" + name + "_topo.txt");
            const std::vector<Demand> demands =
                loadDemands("./instances/" + name + "_demand.txt", percent);
            const std::vector<double> funcCharge =
                loadFunctions("./instances/func" + std::to_string(funcCoef) + ".txt");
            std::vector<int> NFVNodes;
            if (genType == "rand") {
                srand(time(NULL));
                int totalNbCores = 0;
                for (int i = 0; i < int(demands.size()); ++i) {
                    const auto& funcs = std::get<3>(demands[i]);
                    for (int j(0); j < int(funcs.size()); ++j) {
                        totalNbCores +=
                            ceil(std::get<2>(demands[i]) / funcCharge[funcs[j]]);
                    }
                }
                std::cout << totalNbCores << " needed cores!" << '\n';

                std::vector<int> nodes = network.getNodes();
                std::random_shuffle(nodes.begin(), nodes.end());
                NFVNodes = std::vector<int>{nodes.begin(), nodes.begin() + nbNFV};
            } else if (genType == "emb") {
                if (argc < 6) {
                    std::cerr << "Missing arguments! -> {genType} {name} {percent} "
                                 "{funcCoef} {nbNFV} {ordering}"
                              << std::endl;
                } else {
                    const int ordering = std::stoi(argv[6]);
                    NFVNodes =
                        loadNFV("instances/" + name + "_nodes.txt", nbNFV, ordering);
                }

            } else {
                std::cout << "Invalid generator type: " << genType << '\n';
                return -1;
            }
            std::cout << "NFVNodes: " << NFVNodes << std::endl;

            ChainingPathBuilder cpb(network, demands, funcCharge, NFVNodes);
            cpb.addInitConf(AugmentedGraph(network, funcCharge, NFVNodes)
                                .getInitialConfigurationNoCapa(demands));
            const auto pair_GraphNodecapa = cpb.solve();
            const std::string timestamp = std::to_string(int(time(NULL)));

            const std::string instanceName =
                genType == "emb" ? name + "_emb_" + std::to_string(nbNFV)
                                 : name + "_" + timestamp;

            saveTopo(pair_GraphNodecapa.first,
                "./instances/" + instanceName + "_topo.txt");
            saveNodeCapas(pair_GraphNodecapa.second,
                "./instances/" + instanceName + "_nodeCapa.txt");
            std::cout << instanceName << " created !" << std::endl;
            // Copy demands to {name_timestamp}_demand file
            const std::string systemCall1 = "cp  ./instances/" + name + "_demand.txt ./instances/" + instanceName + "_demand.txt";
            system(systemCall1.c_str());

            if (genType == "rand") {
                // Append the name of the instance to the indexing file for nbNFV
                const std::string appendFileName = "echo " + instanceName + " >> " + name + "_" + std::to_string(nbNFV) + ".txt";
                system(appendFileName.c_str());
            }
        }
    } catch (IloException& e) {
        std::cout << e.getMessage() << std::endl;
    }
}

void saveTopo(const DiGraph& _graph, const std::string& _filename) {
    std::ofstream ofs(_filename);
    ofs << _graph.getOrder() << '\t' << _graph.getOrder() << '\t'
        << _graph.getOrder() << std::endl;
    for (const auto& edge : _graph.getEdges()) {
        if (edge.first < edge.second) {
            ofs << edge.first << '\t' << edge.second << '\t'
                << _graph.getEdgeWeight(edge) << std::endl;
        }
    }
}

void saveNodeCapas(const std::vector<int>& _nodeCapas,
    const std::string& _filename) {
    std::ofstream ofs(_filename);
    for (int i = 0; i < int(_nodeCapas.size()); ++i) {
        ofs << i << '\t' << _nodeCapas[i] << std::endl;
    }
}

std::vector<Demand> loadDemands(const std::string& _filename,
    const double _percent) {
    std::vector<Demand> demands;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!" << std::endl;
        exit(-1);
    } else {
        std::cout << "Opening :" << _filename << std::endl;
    }

    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (line != "") {
            int s, t;
            double d;
            std::stringstream lineStream(line);
            lineStream >> s >> t >> d;
            FunctionDescriptor f;
            std::vector<FunctionDescriptor> functions;
            while (lineStream.good()) {
                lineStream >> f;
                functions.push_back(f);
            }
            functions.pop_back(); // Dirty hack
            demands.emplace_back(s, t, d * _percent, functions);
        }
    }
    return demands;
}

std::string getDemandFile(const std::string& _name, const int _n) {
    std::ifstream ifs("./instances/" + _name + "_demands.txt");
    if (!ifs) {
        std::cerr << ("./instances/" + _name + "_demands.txt")
                  << " does not exists!" << std::endl;
        exit(-1);
    }
    std::string filename;
    int i = 0;
    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (i >= _n) {
            filename = line;
            break;
        }
        ++i;
    }
    return "./instances/" + filename;
}

DiGraph loadNetwork(const std::string& _filename) {
    const auto network = DiGraph::loadFromFile(_filename);
    return std::get<0>(network);
}

std::vector<double> loadFunctions(const std::string& _filename) {
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!" << std::endl;
        exit(-1);
    }
    std::vector<double> functions;
    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (line != "") {
            int f;
            double cpu;
            std::stringstream lineStream(line);
            lineStream >> f >> cpu;
            functions.push_back(cpu);
        }
    }
    return functions;
}