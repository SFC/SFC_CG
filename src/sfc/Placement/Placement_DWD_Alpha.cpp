#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/multi_array/base.hpp>
#include <boost/pending/property.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <ilconcert/iloexpression.h>
#include <ilconcert/ilolinear.h>
#include <ilconcert/ilosys.h>
#include <iostream>

#include "Decomposition.hpp"
#include "SFC/ChainingPathOccLimit.hpp"
#include "SFC/OccLim.hpp"

namespace SFC {
struct CrossLayerLink;
struct IntraLayerLink;
struct ServicePath;
} // namespace SFC

using ranges = boost::multi_array_types::index_range;

namespace SFC::OccLim::Alpha {

Master::Master(const Instance& _inst)
    : Base(DualValues(_inst))
    , m_inst(&_inst)
    , m_b([&]() {
        boost::multi_array<IloNumVar, 2> b(boost::extents[num_vertices(
            m_inst->network)][m_inst->funcCharge.size()]);
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            for (Node u = 0; u < num_vertices(m_inst->network); ++u) {
                b[u][f] = IloNumVar(m_env);
                IloAdd(m_integerModel, IloConversion(m_env, b[u][f], ILOBOOL));
                IloAdd(m_model, b[u][f]);
                setIloName(b[u][f], "b[u=" + std::to_string(u)
                                        + ", f=" + std::to_string(f) + "]");
            }
        }
        return b;
    }())
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_onePathCons(getOnePathPerDemandConstraints(*m_inst, m_model))
    , m_linkCapasCons(getLinkCapacityConstraints(*m_inst, m_model))
    , m_nodeCapasCons(getNodeCapacityConstraints(*m_inst, m_model))
    , m_nbLicensesCons([&]() {
        std::vector<IloRange> funcLicensesCons(m_inst->funcCharge.size());
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            const auto allNode_f = m_b[boost::indices[ranges(
                0, num_vertices(m_inst->network))][f]];
            funcLicensesCons[f] =
                IloAdd(m_model, IloRange(m_env, -IloInfinity,
                                    std::accumulate(allNode_f.begin(),
                                        allNode_f.end(), IloExpr(m_env)),
                                    m_inst->nbLicenses));
            setIloName(
                funcLicensesCons[f], "funcLicensesCons(" + toString(f) + ")");
            // vars.end();
        }
        return funcLicensesCons;
    }())
    , m_pathFuncCons([&]() {
        boost::multi_array<LazyConstraint, 3> pathFuncCons(
            boost::extents[m_inst->demands.size()][m_inst->maxChainSize]
                          [num_vertices(m_inst->network)]);

        for (int i = 0; i < m_inst->demands.size(); ++i) {
            for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                for (Node u = 0; u < num_vertices(m_inst->network); ++u) {
                    pathFuncCons[i][j][u] =
                        LazyConstraint(m_model, -IloInfinity,
                            -m_b[u][m_inst->demands[i].functions[j]], 0.0);
                    setIloName(pathFuncCons[i][j][u],
                        "pathFuncCons" + toString(std::make_tuple(i, j, u)));
                }
            }
        }
        return pathFuncCons;
    }())
    , m_dummyPaths([&]() {
        std::vector<IloNumVar> retval(m_inst->demands.size());
        for (int i = 0; i < retval.size(); ++i) {
            retval[i] = m_onePathCons[i](1.0)
                        + m_obj(2 * m_inst->demands[i].d
                                * int(num_vertices(m_inst->network)));
            m_model.add(retval[i]);
            m_integerModel.add(retval[i] == 0);
            setIloName(retval[i], "dummyPaths" + toString(m_inst->demands[i]));
        }
        return retval;
    }())
    , m_linkCharge(num_edges(m_inst->network))
    , m_nodeCharge(num_vertices(m_inst->network))
    , m_edgeUsage(
          boost::extents[m_inst->demands.size()][m_inst->maxChainSize + 1]
                        [num_edges(m_inst->network)])
    , m_nodeUsage(
          boost::extents[m_inst->demands.size()][m_inst->maxChainSize + 1]
                        [num_vertices(m_inst->network)]) {
    m_solver.setOut(m_solver.getEnv().getNullStream());
    m_solver.setWarning(m_solver.getEnv().getNullStream());
    /*
    m_solver.setParam(IloCplex::Threads, 1);
    m_solver.setParam(IloCplex::RootAlg, IloCplex::Dual);
    m_solver.setParam(IloCplex::DPriInd, IloCplex::DPriIndSteep);
    m_solver.setParam(IloCplex::Param::Advance, 0);
    */
}

double Master::getReducedCost_impl(
    const ServicePath& _col, const DualValues& _dualValues) const {
    const auto& [nPath, locations, demandID] = _col;
    double retval = -_dualValues.m_onePathDuals[demandID];

    std::vector<double> linkRC(nPath.size());
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
         ++iteU, ++iteV) {
        retval += _dualValues.getReducedCost(IntraLayerLink{demandID,
            {*iteU, *iteV}, -1}); /// layer # is not use for the reduced cost
    }

    for (int j = 0; j < locations.size(); ++j) {
        retval += _dualValues.getReducedCost(
            CrossLayerLink{demandID, locations[j], j});
    }
    return retval;
}

IloNumColumn Master::getColumn_impl(const ServicePath& _col) {
    const auto& [nPath, locations, demandID] = _col;
    const auto& [id, s, t, d, chain] = m_inst->demands[demandID];

    std::fill(m_linkCharge.begin(), m_linkCharge.end(), 0.0);
    std::fill(m_nodeCharge.begin(), m_nodeCharge.end(), 0.0);

    IloNumColumn col =
        m_onePathCons[demandID](1.0) + m_obj(d * int(nPath.size() - 1));
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
         ++iteU, ++iteV) {
        m_linkCharge[m_inst->network[edge(*iteU, *iteV, m_inst->network).first]
                         .id] += d;
    }
    const auto mult = [](const IloRange& _const, double _val) {
        return _const(_val);
    };
    const auto add = [](IloNumColumn _acc, IloNumColumn _newVal) {
        return _acc += _newVal;
    };
    col += std::inner_product(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_linkCharge.begin(), IloNumColumn(m_env), add, mult);

    for (int j = 0; j < locations.size(); ++j) {
        m_nodeCharge[locations[j]] += m_inst->nbCores[demandID][j];
        col += m_pathFuncCons[demandID][j][locations[j]](1.0);
    }
    col += std::inner_product(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_nodeCharge.begin(), IloNumColumn(m_env), add, mult);
    return col;
}

void Master::getDuals_impl() {
    const auto get = [&](auto&& _cons) { return getDual(m_solver, _cons); };
    std::transform(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_duals.m_linkCapasDuals.begin(), get);
    std::transform(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_duals.m_nodeCapasDuals.begin(), get);
    std::transform(m_pathFuncCons.data(),
        m_pathFuncCons.data() + m_pathFuncCons.num_elements(),
        m_duals.m_pathFuncDuals.data(), get);
    std::transform(m_onePathCons.begin(), m_onePathCons.end(),
        m_duals.m_onePathDuals.begin(), get);
    std::transform(m_nbLicensesCons.begin(), m_nbLicensesCons.end(),
        m_duals.m_nbLicensesDuals.begin(), get);
    m_duals.m_branchesDual = std::accumulate(m_branches.begin(),
        m_branches.end(), 0.0, [&](auto&& _acc, auto&& _cons) {
            const auto dual = getDual(m_solver, _cons);
            if (epsilon_less<double>()(_cons.getUB(), IloInfinity)) {
                return _acc + dual * _cons.getUB();
            }
            if (epsilon_less<double>()(-IloInfinity, _cons.getLB())) {
                return _acc + dual * _cons.getLB();
            }
            assert(false);
            return 0.0;
        });
}

Solution<int> Master::getSolution() const {
    return {*m_inst, getIntegerObjValue(), getRelaxationObjValue(),
        getServicePaths(), getNbColumns()};
}

void Master::removeDummyIfPossible() {
    if (m_dummyActive) {
        const double dummySum = std::accumulate(m_dummyPaths.begin(),
            m_dummyPaths.end(), 0.0, [&](auto&& _acc, auto&& _var) {
                return _acc + m_solver.getValue(_var);
            });
        if (epsilon_equal<double>()(dummySum, 0.0)) {
            std::cout << "Removed dummy\n";
            m_dummyActive = false;
            for (auto& dummy : m_dummyPaths) {
                m_model.add(dummy == 0);
            }
            solve();
        }
    }
}

std::vector<IloRange> Master::getBranch_impl() const {
    const auto bVar = *std::min_element(m_b.data(),
        m_b.data() + m_b.num_elements(), [&](auto&& _var1, auto&& _var2) {
            return std::fabs(m_solver.getValue(_var1) - 0.5)
                   < std::fabs(m_solver.getValue(_var2) - 0.5);
        });
    if (!isInteger(m_solver.getValue(bVar))) {
        std::cout << "Selected " << bVar << " with value of "
                  << m_solver.getValue(bVar) << '\n';
        std::vector<IloRange> retval = {
            IloRange(m_env, 0.0, bVar, 0.0), IloRange(m_env, 1.0, bVar, 1.0)};
        std::cout << "Branches: " << retval << '\n';
        return retval;
    } else {
        std::vector<double> linkUsage(num_edges(m_inst->network), 0.0);
        for (const auto& [var, path] : getColumnStorage<ServicePath>()) {
            const auto val = m_solver.getValue(var);
            for (auto iteU = path.nPath.begin(), iteV = std::next(iteU);
                 iteV != path.nPath.end(); ++iteU, ++iteV) {
                linkUsage
                    [m_inst->network[edge(*iteU, *iteV, m_inst->network).first]
                            .id] += val;
            }
        }
        std::cout << "linkUsage: [";
        printContainer(std::cout, linkUsage.begin(), linkUsage.end());
        std::cout << "]\n";

        const auto ite = std::min_element(
            linkUsage.begin(), linkUsage.end(), isMostFractional<double>);
        const auto e = std::distance(linkUsage.begin(), ite);
        std::cout << "Best e : " << linkUsage[e] << ": "
                  << std::floor(linkUsage[e]) << ", " << std::ceil(linkUsage[e])
                  << '\n';
        if (!isInteger(linkUsage[e])) {
            IloExpr expr(m_env);
            for (const auto& [var, path] : getColumnStorage<ServicePath>()) {
                for (auto iteU = path.nPath.begin(), iteV = std::next(iteU);
                     iteV != path.nPath.end(); ++iteU, ++iteV) {
                    if (e
                        == m_inst
                               ->network[edge(*iteU, *iteV, m_inst->network)
                                             .first]
                               .id) {
                        expr += var;
                    }
                }
            }
            const auto ceil = std::ceil(linkUsage[e]),
                       floor = std::floor(linkUsage[e]);
            for (const auto ed :
                boost::make_iterator_range(edges(m_inst->network))) {
                if (m_inst->network[ed].id == e) {
                    std::cout << "Selected edge ("
                              << source(ed, m_inst->network) << ", "
                              << target(ed, m_inst->network)
                              << ") with usage of " << linkUsage[e] << '\n';
                    break;
                }
            }
            std::vector<IloRange> retval = {
                IloRange(m_env, -IloInfinity, expr, floor),
                IloRange(m_env, ceil, expr, IloInfinity)};
            return retval;
        } else {
            std::vector<double> nodeUsage(num_vertices(m_inst->network), 0.0);
            for (const auto& [var, path] : getColumnStorage<ServicePath>()) {
                const auto val = m_solver.getValue(var);
                for (const auto u : path.locations) {
                    nodeUsage[u] += val;
                }
            }

            std::cout << "nodeUsage: ";
            printContainer(std::cout, nodeUsage.begin(), nodeUsage.end());
            std::cout << '\n';
            const auto nodeIte = std::min_element(
                nodeUsage.begin(), nodeUsage.end(), isMostFractional<double>);
            const auto bestU = std::distance(nodeUsage.begin(), nodeIte);
            std::cout << "Best u : " << nodeUsage[bestU] << '\n';
            if (!isInteger(nodeUsage[bestU])) {
                IloExpr expr(m_env);
                const auto ceil = std::ceil(nodeUsage[bestU]),
                           floor = std::floor(nodeUsage[bestU]);
                for (const auto& [var, path] :
                    getColumnStorage<ServicePath>()) {
                    for (const auto u : path.locations) {
                        if (u == bestU) {
                            expr += var;
                            break;
                        }
                    }
                }
                std::cout << "Selected node " << bestU << " with usage of "
                          << nodeUsage[bestU] << '\n';
                std::vector<IloRange> retval = {
                    IloRange(m_env, -IloInfinity, expr, floor),
                    IloRange(m_env, ceil, expr, IloInfinity)};
                return retval;
            }
        }
    }
    return {};
}

const DualValues& Master::getDualValues_impl() const { return m_duals; }

std::vector<ServicePath> Master::getServicePaths() const {
    std::vector<ServicePath> sPaths(m_inst->demands.size());
    for (const auto& [var, sPath] : getColumnStorage<ServicePath>()) {
        if (epsilon_equal<double>()(m_solver.getValue(var), IloTrue)) {
            sPaths[sPath.demand] = sPath;
        }
    }
    return sPaths;
}

std::ostream& operator<<(std::ostream& _out, const DualValues& _dualValues) {
    return _out << "{lc: "
                << std::accumulate(_dualValues.m_linkCapasDuals.begin(),
                       _dualValues.m_linkCapasDuals.end(), 0.0)
                << ", nc: "
                << std::accumulate(_dualValues.m_nodeCapasDuals.begin(),
                       _dualValues.m_nodeCapasDuals.end(), 0.0)
                << ", pf: "
                << std::accumulate(_dualValues.m_pathFuncDuals.data(),
                       _dualValues.m_pathFuncDuals.data()
                           + _dualValues.m_pathFuncDuals.num_elements(),
                       0.0)
                << ", op: "
                << std::accumulate(_dualValues.m_onePathDuals.begin(),
                       _dualValues.m_onePathDuals.end(), 0.0)
                << ", nl: "
                << std::accumulate(_dualValues.m_nbLicensesDuals.begin(),
                       _dualValues.m_nbLicensesDuals.end(), 0.0)
                << '}';
}

void barycenter(const DualValues& _nDuals, const DualValues& _bDuals,
    DualValues& _sDuals, double _alpha) {
    const auto update = [_alpha](const double _a, const double _b) -> double {
        return _alpha * _a + ((1 - _alpha) * _b);
    };
    std::transform(_nDuals.m_linkCapasDuals.begin(),
        _nDuals.m_linkCapasDuals.end(), _bDuals.m_linkCapasDuals.begin(),
        _sDuals.m_linkCapasDuals.begin(), update);
    std::transform(_nDuals.m_nodeCapasDuals.begin(),
        _nDuals.m_nodeCapasDuals.end(), _bDuals.m_nodeCapasDuals.begin(),
        _sDuals.m_nodeCapasDuals.begin(), update);
    std::transform(_nDuals.m_onePathDuals.begin(), _nDuals.m_onePathDuals.end(),
        _bDuals.m_onePathDuals.begin(), _sDuals.m_onePathDuals.begin(), update);
    std::transform(_nDuals.m_pathFuncDuals.data(),
        _nDuals.m_pathFuncDuals.data() + _nDuals.m_pathFuncDuals.num_elements(),
        _bDuals.m_pathFuncDuals.data(), _sDuals.m_pathFuncDuals.data(), update);
    std::transform(_nDuals.m_nbLicensesDuals.begin(),
        _nDuals.m_nbLicensesDuals.end(), _bDuals.m_nbLicensesDuals.begin(),
        _sDuals.m_nbLicensesDuals.begin(), update);
    _sDuals.m_branchesDual =
        update(_nDuals.m_branchesDual, _bDuals.m_branchesDual);
}

DualValues::DualValues(const Instance& _inst)
    : inst(&_inst)
    , m_onePathDuals(inst->demands.size(), 0.0)
    , m_linkCapasDuals(num_edges(inst->network), 0.0)
    , m_nodeCapasDuals(num_vertices(inst->network), 0.0)
    , m_pathFuncDuals(boost::extents[inst->demands.size()][inst->maxChainSize]
                                    [num_vertices(inst->network)])
    , m_nbLicensesDuals(inst->funcCharge.size(), 0.0)
    , m_branchesDual(0.0)

{}

IloNum DualValues::getReducedCost(const CrossLayerLink& _link) const {
    return -inst->nbCores[_link.demandID][_link.j] * m_nodeCapasDuals[_link.u]
           - m_pathFuncDuals[_link.demandID][_link.j][_link.u];
}

IloNum DualValues::getReducedCost(const IntraLayerLink& _link) const {
    return inst->demands[_link.demandID].d
           * (1
                 - m_linkCapasDuals[inst
                                        ->network[edge(_link.edge.first,
                                            _link.edge.second, inst->network)
                                                      .first]
                                        .id]);
}

double DualValues::getDualSumRHS() const {
    double retval = std::inner_product(m_nodeCapasDuals.begin(),
        m_nodeCapasDuals.end(), vertices(inst->network).first, 0.0,
        std::plus<>(), [&](auto&& dual, auto&& u) {
            return dual * inst->network[u].capacity;
        });
    for (const auto ed : boost::make_iterator_range(edges(inst->network))) {
        retval +=
            m_linkCapasDuals[inst->network[ed].id] * inst->network[ed].capacity;
    }
    retval +=
        std::accumulate(m_onePathDuals.begin(), m_onePathDuals.end(), 0.0);
    retval +=
        std::accumulate(m_nbLicensesDuals.begin(), m_nbLicensesDuals.end(), 0.0)
        * inst->nbLicenses;
    retval += m_branchesDual;
    return retval;
}

std::optional<Weighted<std::vector<Edge>>> Master::getMostFractionalCut(
    const int _demandID) const {
    const auto& [id, s, t, d, chain] = m_inst->demands[_demandID];

    const auto layeredGraph = getLayeredGraph(*m_inst);
    std::vector<double> edgeWeight(num_edges(layeredGraph));
    for (const auto ed : boost::make_iterator_range(edges(layeredGraph))) {
        const auto [u, v] = incident(ed, layeredGraph);
        const function_descriptor layerU = u / num_vertices(m_inst->network),
                                  layerV = v / num_vertices(m_inst->network);
        const Node baseU = u % num_vertices(m_inst->network),
                   baseV = v % num_vertices(m_inst->network);
        if (layerU != layerV) {
            edgeWeight[get(boost::edge_index, layeredGraph, ed)] +=
                m_nodeUsage[_demandID][layerU][baseU];
        } else {
            edgeWeight[get(boost::edge_index, layeredGraph, ed)] += m_edgeUsage
                [_demandID][layerU]
                [m_inst->network[edge(baseU, baseV, m_inst->network).first].id];
        }
    }
    saveAsGraphviz("layeredGraph.dot", layeredGraph, edgeWeight);
    assert(edgeWeight.size() == num_edges(layeredGraph));
    if (std::all_of(edgeWeight.begin(), edgeWeight.end(), isInteger<double>)) {
        return std::nullopt;
    }
    int n = num_vertices(m_inst->network);

    assert(std::accumulate(edgeWeight.begin(), edgeWeight.end(), 0.0) > 0.0);
    std::transform(edgeWeight.begin(), edgeWeight.end(), edgeWeight.begin(),
        [](auto&& _val) { return _val * _val; });

    const Node dest = n * chain.size() + t;
    const auto cut = getEdgeCut(layeredGraph, edgeWeight, s, dest);
    assert(!cut.empty());
    std::vector<Edge> retval;
    std::transform(cut.begin(), cut.end(), std::back_inserter(retval),
        [&](auto&& ed) { return incident(ed, layeredGraph); });
    const auto sumUsage =
        std::accumulate(cut.begin(), cut.end(), 0.0, [&](auto _acc, auto _ed) {
            return _acc + edgeWeight[get(boost::edge_index, layeredGraph, _ed)];
        });
    return Weighted<std::vector<Edge>>{retval, sumUsage};
}

std::optional<Weighted<EdgeCut>> Master::getMostFractionalCut(
    const int _demandID, const int _layer) const {
    const auto& [id, s, t, d, chain] = m_inst->demands[_demandID];

    std::vector<double> edgeWeight(2 * num_edges(m_inst->network), 0.0);
    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        const auto e = m_inst->network[ed].id;
        edgeWeight[e] += m_edgeUsage[id][_layer][e];
    }
    if (std::all_of(edgeWeight.begin(), edgeWeight.end(), isInteger<double>)) {
        return std::nullopt;
    }
    assert(std::accumulate(edgeWeight.begin(), edgeWeight.end(), 0.0) > 0.0);
    std::transform(edgeWeight.begin(), edgeWeight.end(), edgeWeight.begin(),
        [](auto&& _val) { return _val * _val; });

    using AugGraph =
        boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,
            boost::no_property, boost::property<boost::edge_index_t, int>>;
    AugGraph augGraph(num_vertices(m_inst->network));
    std::vector<AugGraph::edge_descriptor> reverse_edges(
        2 * num_edges(m_inst->network));
    std::vector<double> weights(edgeWeight);
    std::vector<double> resCapa(2 * num_edges(m_inst->network));

    // Copy edges
    int i = 0;
    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        reverse_edges[i + num_edges(m_inst->network)] =
            add_edge(source(ed, m_inst->network), target(ed, m_inst->network),
                i, augGraph)
                .first;
        i++;
    }
    // Add reverse edges
    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        reverse_edges[i - num_edges(m_inst->network)] =
            add_edge(target(ed, m_inst->network), source(ed, m_inst->network),
                i, augGraph)
                .first;
        weights.emplace_back(0.0);
        i++;
    }
    const auto getAugEdgeGrapProp = [&](auto&& _vector) {
        return boost::make_iterator_property_map(
            _vector.begin(), get(boost::edge_index, augGraph));
    };
#ifndef NDEBUG
    for (const auto ed : boost::make_iterator_range(edges(augGraph))) {
        assert(source(ed, augGraph)
               == target(reverse_edges[get(boost::edge_index, augGraph, ed)],
                      augGraph));
        assert(target(ed, augGraph)
               == source(reverse_edges[get(boost::edge_index, augGraph, ed)],
                      augGraph));
    }
#endif

    auto capacity = getAugEdgeGrapProp(weights);
    auto reverse = getAugEdgeGrapProp(reverse_edges);
    auto residcap = getAugEdgeGrapProp(resCapa);
    auto indexMap = get(boost::vertex_index, augGraph);
    const auto cutSize = boost::push_relabel_max_flow(
        augGraph, s, t, capacity, residcap, reverse, indexMap);
    (void)cutSize;
    struct Filter {
        const std::vector<double>* capa;
        const Network* network;
        bool operator()(const int u) const {
            for (const auto ed :
                boost::make_iterator_range(in_edges(u, *network))) {
                if ((*capa)[(*network)[ed].id] > 1e-6) {
                    return true;
                }
            }
            for (const auto ed :
                boost::make_iterator_range(out_edges(u, *network))) {
                if ((*capa)[(*network)[ed].id] > 1e-6) {
                    return true;
                }
            }
            return false;
        }

        bool operator()(const edge_descriptor ed) const {
            return (*capa)[(*network)[ed].id] > 1e-6;
        }
    };
    const auto graphFilter = Filter{&resCapa, &m_inst->network};
    std::vector<bool> preds(num_edges(m_inst->network));
    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        preds[m_inst->network[ed].id] = graphFilter(ed);
    }
    boost::filtered_graph<Network, Filter, Filter> g(
        m_inst->network, graphFilter, graphFilter);
    std::vector<boost::default_color_type> colors(
        num_vertices(m_inst->network), boost::default_color_type::white_color);

    const auto getNodePropMap = [&](auto&& _vector) {
        return boost::make_iterator_property_map(
            _vector.begin(), get(&Router::id, g));
    };
    auto color_map = getNodePropMap(colors);
    auto root_vertex = s;
    auto index_map = boost::vertex_index_map(get(&Router::id, m_inst->network));
    auto vis = boost::dfs_visitor<boost::null_visitor>();
    boost::depth_first_search(g, boost::root_vertex(root_vertex)
                                     .visitor(vis)
                                     .vertex_index_map(index_map)
                                     .color_map(color_map));
    EdgeCut cut{id, _layer, {}};
    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        if (colors[source(ed, m_inst->network)]
                == boost::default_color_type::black_color
            && colors[target(ed, m_inst->network)]
                   == boost::default_color_type::white_color) {
            if (isInteger<double>(edgeWeight[m_inst->network[ed].id])) {
                return std::nullopt;
            }
            cut.cut.emplace_back(ed);
        }
    }
    assert(!cut.cut.empty());
    const auto sumUsage = std::accumulate(
        cut.cut.begin(), cut.cut.end(), 0.0, [&](auto _acc, auto _ed) {
            return _acc + edgeWeight[m_inst->network[_ed].id];
        });
    return Weighted<EdgeCut>{cut, sumUsage};
}

std::optional<std::vector<ForbiddenLayeredCut>>
Master::getLayeredCutBranches() const {

    // Get most fractional cut
    std::optional<Weighted<std::vector<Edge>>> mostFrac;
    int demandID = -1;
    for (int i = 0; i < m_inst->demands.size(); ++i) {
        const auto tmpMostFrac = getMostFractionalCut(i);
        if (tmpMostFrac
            && (!mostFrac
                   || isMostFractional(tmpMostFrac->value, mostFrac->value))) {
            mostFrac = std::move(tmpMostFrac);
            const auto paths = getWeightedServicePath(i);
            (void)paths;
            demandID = i;
        }
        for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
        }
    }
    if (!mostFrac) {
#ifndef NDEBUG
        std::cout << "Could not generate ForbiddenEdges'\n";
#endif
        return std::nullopt;
    }
    assert(demandID != -1);
    const auto n = num_vertices(m_inst->network);
    const auto& edgeCut = mostFrac->var;
    std::vector<ForbiddenLayeredCut> retval;
    for (int i = 0; i < edgeCut.size(); ++i) {
        auto& set = retval.emplace_back();
        for (int j = 0; j < edgeCut.size(); ++j) {
            if (i != j) {
                const auto [u, v] = edgeCut[j];
                const function_descriptor layerU = u / n, layerV = v / n;
                const Node baseU = u % n, baseV = v % n;
                if (layerU != layerV) {
                    set.cross.emplace_back(
                        CrossLayerLink{demandID, baseU, layerU});
                } else {
                    set.intra.emplace_back(IntraLayerLink{
                        demandID, std::make_pair(baseU, baseV), layerU});
                }
            }
        }
    }
    return std::move(retval);
}
std::optional<std::vector<std::vector<NetworkLink>>>
Master::getEdgeCutBranches() const {

    // Get most fractional cut
    std::optional<Weighted<EdgeCut>> mostFrac;
    for (int i = 0; i < m_inst->demands.size(); ++i) {
        for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
            const auto tmpMostFrac = getMostFractionalCut(i, j);
            if (tmpMostFrac
                && (!mostFrac
                       || isMostFractional(
                              tmpMostFrac->value, mostFrac->value))) {
                mostFrac = std::move(tmpMostFrac);
            }
        }
    }
    if (!mostFrac) {
#ifndef NDEBUG
        std::cout << "Could not generate NetworkLink'\n";
#endif
        return std::nullopt;
    }
    const auto& edgeCut = mostFrac->var;
    std::vector<std::vector<NetworkLink>> retval;
    for (int i = 0; i < edgeCut.cut.size(); ++i) {
        auto& set = retval.emplace_back();
        for (int j = 0; j < edgeCut.cut.size(); ++j) {
            if (i != j) {
                set.emplace_back(NetworkLink{edgeCut.demandId,
                    incident(edgeCut.cut[j], m_inst->network)});
            }
        }
    }
#ifndef NDEBUG
    std::cout << "Generated ForbiddenEdges': ";
    for (const auto& fe : retval) {
        std::cout << '(';
        std::cout << fe.front().demandID << ", ";
        for (const auto& nl : fe) {
            std::cout << '(' << nl.edge.first << ", " << nl.edge.second << ')';
        }
        std::cout << ")";
    }
    std::cout << '\n';
#endif
    return std::move(retval);
}
std::optional<std::vector<FixedVariable<IntraLayerLink>>>
Master::getLinkBranches() const {

    // Get most fractional value
    int minI = 0;
    auto minEd = *edges(m_inst->network).first;
    int minLayer = 0;
    double minValue = m_edgeUsage[minI][minLayer][m_inst->network[minEd].id];

    for (int i = 0; i < m_inst->demands.size(); ++i) {
        for (const auto ed :
            boost::make_iterator_range(edges(m_inst->network))) {
            for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                if (isMostFractional(
                        m_edgeUsage[i][j][m_inst->network[ed].id], minValue)) {
                    minValue = m_edgeUsage[i][j][m_inst->network[ed].id];
                    minI = i;
                    minEd = ed;
                    minLayer = j;
                }
            }
        }
    }
    if (!isInteger(m_edgeUsage[minI][minLayer][m_inst->network[minEd].id])) {
        const IntraLayerLink ilink{
            minI, incident(minEd, m_inst->network), minLayer};
        return {{{ilink, true}, {ilink, false}}};
    }
    return std::nullopt;
}
std::optional<std::vector<LocationSum>> Master::getLocationsBranches() const {
    // Find most fractional m_b
    struct FractionalFuncLocation {
        int u;
        int f;
        double value;
    };
    const FractionalFuncLocation mostFrac = [&] {
        FractionalFuncLocation retval{0, 0, m_solver.getValue(m_b[0][0])};
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            for (int u = 0; u < num_vertices(m_inst->network); ++u) {
                const auto currentVal = m_solver.getValue(m_b[u][f]);
                if (isMostFractional(currentVal, retval.value)) {
                    retval = {u, f, currentVal};
                }
            }
        }
        return retval;
    }();
    if (isInteger(mostFrac.value)) {
        return std::nullopt;
    }

    // Get expression
    IloExpr expr(m_env);
    int total = 1;
    for (int u = 0; u < num_vertices(m_inst->network); ++u) {
        const auto var = m_b[u][mostFrac.f];
        if (epsilon_equal<double>()(m_solver.getValue(var), 1.0)) {
            expr += var;
            total++;
        }
    }
    expr += m_b[mostFrac.u][mostFrac.f];
    return std::vector<LocationSum>{{IloRange(m_env, total, expr, IloInfinity)},
        {IloRange(m_env, -IloInfinity, expr, total - 1)}};
}

std::optional<std::vector<FixedVariable<CrossLayerLink>>>
Master::getNodeBranches() const {
    return std::nullopt;
}

void Master::remove(const std::vector<NetworkLink>& _edges) {
    const auto demandID = _edges.front().demandID;
    auto& forbidden = m_forbiddenIntraLinks[demandID];
    for (const auto& nl : _edges) {
        for (int j = 0; j < m_inst->demands[demandID].functions.size(); ++j) {
            forbidden.erase(IntraLayerLink{demandID, nl.edge, j});
        }
    }
}

void Master::add(const std::vector<NetworkLink>& _edges) {
#ifndef NDEBUG
    std::cout << "Adding ForbiddenEdges(";
    std::cout << _edges.front().demandID << ": ";
    for (const auto& nl : _edges) {
        std::cout << '(' << nl.edge.first << ", " << nl.edge.second << ')';
    }
    std::cout << ")\n";
    const auto nbCols = getColumnStorage<ServicePath>().size();
#endif
    const auto demandID = _edges.front().demandID;
    auto& forbidden = m_forbiddenIntraLinks[demandID];
    for (const auto& nl : _edges) {
        for (int j = 0; j < m_inst->demands[demandID].functions.size(); ++j) {
            forbidden.insert(IntraLayerLink{demandID, nl.edge, j});
            add(FixedVariable<IntraLayerLink>{{demandID, nl.edge, j}, false});
        }
    }
#ifndef NDEBUG
    std::cout << "Removed " << nbCols - getColumnStorage<ServicePath>().size()
              << " columns\n";
#endif
}

void Master::remove(const ForbiddenLayeredCut& _layeredCut) {
    for (const auto& intra : _layeredCut.intra) {
        remove(FixedVariable<IntraLayerLink>{intra, false});
        m_forbiddenIntraLinks[_layeredCut.demandID].erase(intra);
    }
    for (const auto& cross : _layeredCut.cross) {
        remove(FixedVariable<CrossLayerLink>{cross, false});
        m_forbiddenCrossLinks[_layeredCut.demandID].erase(cross);
    }
}

void Master::add(const ForbiddenLayeredCut& _layeredCut) {
#ifndef NDEBUG
    const auto nbCols = getColumnStorage<ServicePath>().size();
#endif
    for (const auto& intra : _layeredCut.intra) {
        add(FixedVariable<IntraLayerLink>{intra, false});
        m_forbiddenIntraLinks[_layeredCut.demandID].insert(intra);
    }
    for (const auto& cross : _layeredCut.cross) {
        add(FixedVariable<CrossLayerLink>{cross, false});
        m_forbiddenCrossLinks[_layeredCut.demandID].insert(cross);
    }
#ifndef NDEBUG
    const auto removedNbCols = nbCols - getColumnStorage<ServicePath>().size();
    assert(removedNbCols > 0);
    std::cout << "Removed " << removedNbCols << " columns.\n";
#endif
}

void Master::add(const FixedVariable<IntraLayerLink>& _link) {
    // const int before = getColumnStorage<ServicePath>().size();
    getColumnStorage<ServicePath>().erase(
        std::remove_if(getColumnStorage<ServicePath>().begin(),
            getColumnStorage<ServicePath>().end(),
            [&](auto&& _varPath) {
                if (_varPath.obj.demand != _link.var.demandID) {
                    return false;
                }
                const auto isIn = contains(_varPath.obj, _link.var);
                if ((_link.use && !isIn) || (!_link.use && isIn)) {
                    // std::cout << "Removing " << _varPath.obj << " (" <<
                    // _varPath.var << ")\n";
                    _varPath.var.end();
                    return true;
                }
                return false;
            }),
        getColumnStorage<ServicePath>().end());
}

void Master::add(const FixedVariable<CrossLayerLink>& _link) {
    getColumnStorage<ServicePath>().erase(
        std::remove_if(getColumnStorage<ServicePath>().begin(),
            getColumnStorage<ServicePath>().end(),
            [&](auto&& _varPath) {
                if (_varPath.obj.demand != _link.var.demandID) {
                    return false;
                }
                const auto isIn = contains(_varPath.obj, _link.var);
                if ((_link.use && !isIn) || (!_link.use && isIn)) {
                    _varPath.var.end();
                    return true;
                }
                return false;
            }),
        getColumnStorage<ServicePath>().end());
}

void Master::remove(const FixedVariable<IntraLayerLink>& /*_link*/) {}

void Master::remove(const FixedVariable<CrossLayerLink>& /*_link*/) {}
void Master::remove(LocationSum _ls) {
    m_model.remove(_ls.rng);
    m_branches.erase(
        std::remove_if(m_branches.begin(), m_branches.end(),
            [&](auto&& _cons) { return _cons.getImpl() == _ls.rng.getImpl(); }),
        m_branches.end());
}

void Master::add(LocationSum _ls) {
    m_model.add(_ls.rng);
    m_branches.emplace_back(_ls.rng);
}

void Master::clearBranches() {
    for (auto& rng : m_branches) {
        m_model.remove(rng);
    }
    for (auto& f : m_forbiddenIntraLinks) {
        f.clear();
    }
    for (auto& f : m_forbiddenCrossLinks) {
        f.clear();
    }
    m_branches.clear();
}

bool Master::isIntegral() const {
    // Get node usage for each requests
    std::fill(m_nodeUsage.data(),
        m_nodeUsage.data() + m_nodeUsage.num_elements(), 0.0);
    for (const auto& [var, path] : getColumnStorage<ServicePath>()) {
        for (int j = 0; j < path.locations.size(); ++j) {
            m_nodeUsage[path.demand][j][path.locations[j]] +=
                m_solver.getValue(var);
        }
    }
    for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
        for (Node u = 0; u < num_vertices(m_inst->network); ++u) {
            if (!isInteger(m_solver.getValue(m_b[u][f]))) {
#ifndef NDEBUG
                std::cout << "License var is fractional\n";
#endif
                return false;
            }
        }
    }
    m_edgeUsage = getEdgeUsage();
    for (int i = 0; i < m_inst->demands.size(); ++i) {
        for (const auto ed :
            boost::make_iterator_range(edges(m_inst->network))) {
            for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                const auto val = m_edgeUsage[i][j][m_inst->network[ed].id];
                if (!isInteger<double>(val)) {
#ifndef NDEBUG
                    std::cout << "Edge var is fractional\n";
#endif
                    return false;
                }
            }
        }
    }

    for (int i = 0; i < m_nodeUsage.shape()[0]; ++i) {
        for (int j = 0; j < m_nodeUsage.shape()[1]; ++j) {
            for (int k = 0; k < m_nodeUsage.shape()[2]; ++k) {
                if (!isInteger(m_nodeUsage[i][j][k])) {
#ifndef NDEBUG
                    std::cout << "Node var is fractional\n";
#endif
                    return false;
                }
            }
        }
    }
    return true;
}

boost::multi_array<double, 3> Master::getEdgeUsage() const {
    boost::multi_array<double, 3> retval(
        boost::extents[m_inst->demands.size()][m_inst->maxChainSize + 1]
                      [num_edges(m_inst->network)]);
    std::fill(retval.data(), retval.data() + retval.num_elements(), 0.0);
    for (const auto& [var, sPath] : getColumnStorage<ServicePath>()) {
        const auto val = m_solver.getValue(var);
        int j = 0;
        auto iteU = sPath.nPath.begin(), iteV = std::next(iteU);
        while (j <= sPath.locations.size() && iteV != sPath.nPath.end()) {
            if (j == sPath.locations.size() || sPath.locations[j] != *iteU) {
                retval
                    [sPath.demand][j]
                    [m_inst->network[edge(*iteU, *iteV, m_inst->network).first]
                            .id] += val;
                ++iteU;
                ++iteV;
            } else {
                ++j;
            }
        }
    }

#ifndef NDEBUG
    const auto getSum = [](auto&& _acc, auto&& _range) {
        return std::accumulate(_range.begin(), _range.end(), _acc);
    };
    for (int i = 0; i < m_inst->demands.size(); ++i) {
        assert(std::accumulate(retval[i].begin(), retval[i].end(), 0.0, getSum)
               > 0.0);
    }
#endif
    return retval;
}

std::vector<Weighted<ServicePath>> Master::getWeightedServicePath(
    int _demandId) const {
    std::vector<Weighted<ServicePath>> retval;
    for (const auto& [var, path] : getColumnStorage<ServicePath>()) {
        if (path.demand == _demandId) {
            retval.emplace_back(
                Weighted<ServicePath>{path, m_solver.getValue(var)});
        }
    }
    return retval;
}
} // namespace SFC::OccLim::Alpha

