#include "CppRO/cplex_utility.hpp"

#include <boost/graph/johnson_all_pairs_shortest.hpp>
#include <ilconcert/iloexpression.h>
#include <ilconcert/ilosys.h>

#include "SFC/Localisation.hpp"

namespace SFC {
namespace OccLim {
struct Instance;
} // namespace OccLim
} // namespace SFC

constexpr ConstantWeight::value_type get(
    const ConstantWeight& _w, const ConstantWeight::key_type& /*unused*/) {
    return _w.value;
}

namespace SFC::OccLim {
Localisation::Localisation(const Instance& _instance)
    : m_instance(&_instance)
    , m_model(m_env)
    , m_x([&]() {
        boost::multi_array<IloNumVar, 3> x(
            boost::extents[num_vertices(m_instance->network)]
                          [m_instance->demands.size()]
                          [m_instance->maxChainSize]);

        for (const auto u :
            boost::make_iterator_range(vertices(m_instance->network))) {
            if (m_instance->network[u].isNFV) {
                for (int i = 0; i < m_instance->demands.size(); ++i) {
                    for (int j = 0; j < m_instance->demands[i].functions.size();
                         ++j) {
                        x[u][i][j] = IloAdd(
                            m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                    }
                }
            }
        }
        return x;
    }())
    , m_b([&]() {
        boost::multi_array<IloNumVar, 2> b(boost::extents[num_vertices(
            m_instance->network)][m_instance->funcCharge.size()]);
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance->network))) {
            if (m_instance->network[u].isNFV) {
                for (int f = 0; f < m_instance->funcCharge.size(); ++f) {
                    b[u][f] =
                        IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                }
            }
        }
        return b;
    }())
    , m_obj([&]() {
        boost::multi_array<int, 2> dist(boost::extents[num_vertices(
            m_instance->network)][num_vertices(m_instance->network)]);
        johnson_all_pairs_shortest_paths(
            m_instance->network, dist, boost::weight_map(ConstantWeight{1}));

        IloExpr expr(m_env);
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance->network))) {
            if (m_instance->network[u].isNFV) {
                for (int i = 0; i < m_instance->demands.size(); ++i) {
                    const auto distance = dist[m_instance->demands[i].s][u]
                                          + dist[u][m_instance->demands[i].t];
                    for (int j = 0; j < m_instance->demands[i].functions.size();
                         ++j) {
                        expr += distance * m_x[u][i][j];
                    }
                }
            }
        }
        return IloAdd(m_model, IloObjective(m_env, expr));
    }())
    , m_oneNodePerDemands([&]() {
        boost::multi_array<IloRange, 2> oneNodePerDemands(
            boost::extents[m_instance->demands.size()]
                          [m_instance->maxChainSize]);
        for (int i = 0; i < m_instance->demands.size(); ++i) {
            for (int j = 0; j < m_instance->demands[i].functions.size(); ++j) {
                IloNumVarArray vars(m_env);
                for (const auto u :
                    boost::make_iterator_range(vertices(m_instance->network))) {
                    if (m_instance->network[u].isNFV) {
                        vars.add(m_x[u][i][j]);
                    }
                }
                oneNodePerDemands[i][j] =
                    IloAdd(m_model, IloRange(m_env, 1.0, IloSum(vars), 1.0));
                vars.end();
            }
        }
        return oneNodePerDemands;
    }())
    , m_nodeCapacities([&]() {
        std::vector<IloRange> nodeCapacities(num_vertices(m_instance->network));
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance->network))) {
            if (m_instance->network[u].isNFV) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (int i = 0; i < m_instance->demands.size(); ++i) {
                    for (int j = 0; j < m_instance->demands[i].functions.size();
                         ++j) {
                        vars.add(m_x[u][i][j]);
                        vals.add(m_instance->nbCores[i][j]);
                    }
                }
                nodeCapacities[u] = IloAdd(
                    m_model, IloRange(m_env, 0.0, IloScalProd(vars, vals),
                                 m_instance->network[u].capacity));
                vars.end();
                vals.end();
            }
        }
        return nodeCapacities;
    }())
    , m_nbLicenses([&]() {
        std::vector<IloRange> nbLicenses(m_instance->funcCharge.size());
        for (int f = 0; f < m_instance->funcCharge.size(); ++f) {
            IloNumVarArray vars(m_env);
            for (const auto u :
                boost::make_iterator_range(vertices(m_instance->network))) {
                if (m_instance->network[u].isNFV) {
                    vars.add(m_b[u][f]);
                }
            }
            nbLicenses[f] = IloAdd(m_model,
                IloRange(m_env, 0.0, IloSum(vars), m_instance->nbLicenses));
            vars.end();
        }
        return nbLicenses;
    }())
    , m_demandNodeLinkage([&]() {
        boost::multi_array<IloRange, 3> demandNodeLinkage(
            boost::extents[num_vertices(m_instance->network)]
                          [m_instance->demands.size()]
                          [m_instance->maxChainSize]);
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance->network))) {
            if (m_instance->network[u].isNFV) {
                for (int i = 0; i < m_instance->demands.size(); ++i) {
                    for (int j = 0; j < m_instance->demands[i].functions.size();
                         ++j) {
                        const function_descriptor f =
                            m_instance->demands[i].functions[j];
                        demandNodeLinkage[u][i][j] = IloRange(
                            m_env, 0.0, m_b[u][f] - m_x[u][i][j], IloInfinity);
                    }
                }
            }
        }
        return demandNodeLinkage;
    }())
    , m_solver([&]() {
        IloCplex solver(m_model);
        // solver.setOut(m_env.getNullStream());
        solver.setParam(IloCplex::Threads, 1);
        std::for_each(m_b.data(), m_b.data() + m_b.num_elements(),
            [&](auto&& b) { solver.setPriority(b, 2); });
        IloRangeArray rng(m_env, m_demandNodeLinkage.num_elements());
        std::for_each(m_demandNodeLinkage.data(),
            m_demandNodeLinkage.data() + m_demandNodeLinkage.num_elements(),
            [&, i = 0](auto&& _rng) mutable { rng[i++] = _rng; });
        solver.addLazyConstraints(rng);
        rng.end();
        return solver;
    }()) {}

bool Localisation::solve() { return m_solver.solve(); }

boost::multi_array<bool, 2> Localisation::getLocalisation() const {
    // Check node capas
    for (const auto u :
        boost::make_iterator_range(vertices(m_instance->network))) {
        if (m_instance->network[u].isNFV) {
            // const int index = m_instance->NFVIndices[u];
            double charge = 0.0;
            for (int i = 0; i < m_instance->demands.size(); ++i) {
                for (int j = 0; j < m_instance->demands[i].functions.size();
                     ++j) {
                    if (epsilon_equal<double>()(
                            m_solver.getValue(m_x[u][i][j]), 1.0)) {
                        charge += m_instance->nbCores[i][j];
                    }
                }
            }
            std::cout << charge << " / " << m_instance->network[u].capacity
                      << '\n';
        }
    }

    std::vector<double> nbCores(m_instance->funcCharge.size(), 0);
    for (int i = 0; i < m_instance->demands.size(); ++i) {
        for (int j = 0; j < m_instance->demands[i].functions.size(); ++j) {
            nbCores[m_instance->demands[i].functions[j]] +=
                m_instance->nbCores[i][j];
        }
    }

    std::cout << m_solver.getObjValue() << '\n';
    boost::multi_array<bool, 2> retval(boost::extents[num_vertices(
        m_instance->network)][m_instance->funcCharge.size()]);
    std::transform(m_b.data(), m_b.data() + m_b.num_elements(), retval.data(),
        [&](auto&& _var) { return m_solver.getValue(_var); });
    for (int f = 0; f < m_instance->funcCharge.size(); ++f) {
        std::cout << "Function " << f << " on ";
        double totalCapa = 0;
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance->network))) {
            if (m_instance->network[u].isNFV) {
                if (epsilon_equal<double>()(retval[u][f], 1.0)) {
                    std::cout << u << ", ";
                    totalCapa += m_instance->network[u].capacity;
                }
            }
            std::cout << " -> " << nbCores[f] << '/' << totalCapa << '\n';
        }
    }
    return retval;
}

Localisation::~Localisation() { m_env.end(); }

} // namespace SFC::OccLim
