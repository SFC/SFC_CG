#include <boost/multi_array/base.hpp>
#include <ilconcert/ilosys.h>

#include "SFC/ChainingPathOccLimitDoublePP.hpp"

using ranges = boost::multi_array_types::index_range;

namespace SFC::OccLim {

std::ostream& operator<<(
    std::ostream& _out, const FunctionLocation& _fLocations) {
    return _out << _fLocations.f << ": " << _fLocations.locations;
}

bool operator==(const FunctionLocation& _lhs, const FunctionLocation& _rhs) {
    return _lhs.f == _rhs.f && _lhs.locations == _rhs.locations;
}

std::vector<IloRange> getOneLocationPerFunctionConstraints(
    const Instance& _inst, IloModel& _model) {
    std::vector<IloRange> retval(_inst.funcCharge.size());
    std::generate(retval.begin(), retval.end(),
        [&]() { return IloAdd(_model, IloRange(_model.getEnv(), 1.0, 1.0)); });
    return retval;
}

boost::multi_array<LazyConstraint, 3> getPathLocationsLinkConstraints(
    const Instance& _inst, IloModel& _model) {
    boost::multi_array<LazyConstraint, 3> pathFuncCons(
        boost::extents[_inst.demands.size()][_inst.maxChainSize]
                      [num_vertices(_inst.network)]);

    for (int i = 0; i < _inst.demands.size(); ++i) {
        for (int j = 0; j < _inst.demands[i].functions.size(); ++j) {
            for (Node u = 0; u < num_vertices(_inst.network); ++u) {
                pathFuncCons[i][j][u] =
                    LazyConstraint(_model, -IloInfinity, 0.0);
                setIloName(pathFuncCons[i][j][u],
                    "pathFuncCons" + toString(std::make_tuple(i, j, u)));
            }
        }
    }
    return pathFuncCons;
}

DoublePlacement::DoublePlacement(const Instance& _inst)
    : Base(PathLocDualValues(_inst))
    , m_inst(&_inst)
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_onePathCons(getOnePathPerDemandConstraints(*m_inst, m_model))
    , m_linkCapasCons(getLinkCapacityConstraints(*m_inst, m_model))
    , m_nodeCapasCons(getNodeCapacityConstraints(*m_inst, m_model))
    , m_oneLocationCons(getOneLocationPerFunctionConstraints(*m_inst, m_model))
    , m_pathFuncCons(getPathLocationsLinkConstraints(*m_inst, m_model))
    , m_dummyPaths([&]() {
        std::vector<IloNumVar> retval(m_inst->demands.size());
        for (int i = 0; i < retval.size(); ++i) {
            retval[i] = m_onePathCons[i](1.0)
                        + m_obj(2 * m_inst->demands[i].d
                                * int(num_vertices(m_inst->network)));
            m_model.add(retval[i]);
            m_integerModel.add(retval[i] == 0);
            setIloName(retval[i], "dummyPaths" + toString(m_inst->demands[i]));
        }
        return retval;
    }())
    , m_dummyLocations([&]() {
        std::vector<IloNumVar> retval(m_inst->funcCharge.size());
        for (int f = 0; f < retval.size(); ++f) {
            retval[f] = m_oneLocationCons[f](1.0) + m_obj(1000);
            m_model.add(retval[f]);
            m_integerModel.add(retval[f] == 0);
            setIloName(retval[f], "dummyLocations" + toString(f));
        }
        return retval;
    }())
    , m_linkCharge(num_edges(m_inst->network))
    , m_nodeCharge(num_vertices(m_inst->network)) {
    m_solver.extract(m_model);
    m_solver.setOut(m_env.getNullStream());
    m_solver.setParam(IloCplex::Threads, 1);
    m_solver.setParam(IloCplex::RootAlg, IloCplex::Dual);
    // std::cout << m_model << '\n';
}

double DoublePlacement::getReducedCost_impl(
    const ServicePath& _sPath, const PathLocDualValues& _dualValues) const {
    const auto& [nPath, locations, demandID] = _sPath;
    double retval = -_dualValues.m_onePathDuals[demandID];

    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
         ++iteU, ++iteV) {
        retval += _dualValues.getReducedCost(IntraLayerLink{demandID,
            {*iteU, *iteV}, -1}); /// layer # is not use for the reduced cost
    }

    for (int j = 0; j < locations.size(); ++j) {
        retval += _dualValues.getReducedCost(
            CrossLayerLink{demandID, locations[j], j});
    }
    return retval;
}

Solution<int> DoublePlacement::getSolution() const {
    return {*m_inst, getIntegerObjValue(), getRelaxationObjValue(),
        getServicePaths(), getNbColumns()};
}

double DoublePlacement::getReducedCost_impl(const FunctionLocation& _fLocations,
    const PathLocDualValues& _dualValues) const {
    const auto& [f, locations] = _fLocations;
    double retval = -_dualValues.m_oneLocationDuals[f];
    for (const auto u : boost::make_iterator_range(vertices(m_inst->network))) {
        if (m_inst->network[u].isNFV && locations[u]) {
            retval += _dualValues.getReducedCost(LicenseUsage{u, f});
        }
    }
    return retval;
}

IloNumColumn DoublePlacement::getColumn_impl(const ServicePath& _sPath) {
    const auto& [nPath, locations, demandID] = _sPath;
    const auto& [id, s, t, d, chain] = m_inst->demands[demandID];
    std::fill(m_linkCharge.begin(), m_linkCharge.end(), 0.0);
    std::fill(m_nodeCharge.begin(), m_nodeCharge.end(), 0.0);
    IloNumColumn col =
        m_onePathCons[demandID](1.0) + m_obj(d * int(nPath.size() - 1));
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
         ++iteU, ++iteV) {
        m_linkCharge[m_inst->network[edge(*iteU, *iteV, m_inst->network).first]
                         .id] += d;
    }
    const auto mult = [](const IloRange& _const, double _val) {
        return _const(_val);
    };
    const auto add = [](auto&& _acc, auto&& _newVal) {
        return _acc += _newVal;
    };
    col += std::inner_product(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_linkCharge.begin(), IloNumColumn(m_env), add, mult);

    for (int j = 0; j < locations.size(); ++j) {
        m_nodeCharge[locations[j]] += m_inst->nbCores[demandID][j];
        col += m_pathFuncCons[demandID][j][locations[j]](1.0);
    }
    col += std::inner_product(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_nodeCharge.begin(), IloNumColumn(m_env), add, mult);
    return col;
}

IloNumColumn DoublePlacement::getColumn_impl(
    const FunctionLocation& _fLocations) {
    const auto& [f, locations] = _fLocations;
    IloNumColumn col = m_oneLocationCons[f](1.0);
    for (const auto u : boost::make_iterator_range(vertices(m_inst->network))) {
        if (m_inst->network[u].isNFV && locations[u]) {
            for (demand_descriptor i = 0; i < m_inst->demands.size(); ++i) {
                for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                    if (f == m_inst->demands[i].functions[j]) {
                        col += m_pathFuncCons[i][j][u](-1.0);
                    }
                }
            }
        }
    }
    return col;
}

void DoublePlacement::getDuals_impl() {
    const auto get = [&](auto&& _cons) { return getDual(m_solver, _cons); };
    std::transform(m_onePathCons.begin(), m_onePathCons.end(),
        m_duals.m_onePathDuals.begin(), get);
    std::transform(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_duals.m_linkCapasDuals.begin(), get);
    std::transform(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_duals.m_nodeCapasDuals.begin(), get);
    std::transform(m_oneLocationCons.begin(), m_oneLocationCons.end(),
        m_duals.m_oneLocationDuals.begin(), get);
    std::transform(m_pathFuncCons.data(),
        m_pathFuncCons.data() + m_pathFuncCons.num_elements(),
        m_duals.m_pathFuncDuals.data(), get);
}

void DoublePlacement::removeDummyIfPossible() {
    if (m_dummyActive) {
        const double dummySum =
            std::accumulate(m_dummyPaths.begin(), m_dummyPaths.end(), 0.0,
                [&](auto&& _acc, auto&& _var) {
                    return _acc + m_solver.getValue(_var);
                })
            + std::accumulate(m_dummyLocations.begin(), m_dummyLocations.end(),
                  0.0, [&](auto&& _acc, auto&& _var) {
                      return _acc + m_solver.getValue(_var);
                  });
        // std::cout << "dummySum: " << dummySum << '\n';
        if (epsilon_equal<double>()(dummySum, 0.0)) {
            m_dummyActive = false;
            std::cout << "\n###################################################"
                         "###\nRemoved "
                         "dummy...\n###########################################"
                         "###########\n";
            for (auto& dummy : m_dummyPaths) {
                IloAdd(m_model, dummy == 0);
            }
            for (auto& dummy : m_dummyLocations) {
                IloAdd(m_model, dummy == 0);
            }
        }
    }
}

std::vector<ServicePath> DoublePlacement::getServicePaths() const {
    std::vector<ServicePath> sPaths(m_inst->demands.size());
    for (const auto& [var, sPath] : getColumnStorage<ServicePath>()) {
        if (epsilon_equal<double>()(m_solver.getValue(var), IloTrue)) {
            sPaths[sPath.demand] = sPath;
        }
    }
    return sPaths;
}

// ##################################################################################################
// ##################################################################################################
// ##################################################################################################

LocationPricingProblem::LocationPricingProblem(const Instance& _inst)
    : m_inst(&_inst)
    , m_funcLocation(
          {-1, std::vector<bool>(num_vertices(m_inst->network), false)})
    , m_weight(num_vertices(m_inst->network), 0.0)
    , m_sortedNodes([&]() {
        std::vector<Node> retval;
        for (const auto u :
            boost::make_iterator_range(vertices(m_inst->network))) {
            if (m_inst->network[u].isNFV) {
                retval.push_back(u);
            }
        }
        return retval;
    }()) {}

FunctionLocation LocationPricingProblem::getColumn() const {
    return m_funcLocation;
}

bool LocationPricingProblem::solve() {
    m_funcLocation.f = m_f;
    std::fill(m_funcLocation.locations.begin(), m_funcLocation.locations.end(),
        false);
    // Get L_f first function
    std::for_each(m_sortedNodes.begin(),
        m_sortedNodes.begin() + m_inst->nbLicenses, [&](auto&& u) {
            m_funcLocation.locations[u] = true;
            m_objValue += m_weight[u];
        });
    return true;
}

void LocationPricingProblem::setPricingID(const function_descriptor _f) {
    m_f = _f;
}

double LocationPricingProblem::getObjValue() const { return m_objValue; }

// ##################################################################################################
// ##################################################################################################
// ##################################################################################################

PathLocDualValues::PathLocDualValues(const Instance& _inst)
    : inst(&_inst)
    , m_onePathDuals(inst->demands.size(), 0.0)
    , m_oneLocationDuals(inst->funcCharge.size(), 0.0)
    , m_linkCapasDuals(num_edges(inst->network), 0.0)
    , m_nodeCapasDuals(num_vertices(inst->network), 0.0)
    , m_pathFuncDuals(boost::extents[inst->demands.size()][inst->maxChainSize]
                                    [num_vertices(inst->network)]) {}

double PathLocDualValues::getReducedCost(const CrossLayerLink& _link) const {
    return -inst->nbCores[_link.demandID][_link.j] * m_nodeCapasDuals[_link.u]
           - m_pathFuncDuals[_link.demandID][_link.j][_link.u];
}

double PathLocDualValues::getReducedCost(const IntraLayerLink& _link) const {
    return inst->demands[_link.demandID].d
           * (1
                 - m_linkCapasDuals[inst
                                        ->network[edge(_link.edge.first,
                                            _link.edge.second, inst->network)
                                                      .first]
                                        .id]);
}

double PathLocDualValues::getReducedCost(const LicenseUsage& _link) const {
    double retval = 0.0;
    for (int i = 0; i < inst->demands.size(); ++i) {
        for (int j = 0; j < inst->demands[i].functions.size(); ++j) {
            if (_link.f == inst->demands[i].functions[j]) {
                retval += m_pathFuncDuals[i][j][_link.u];
            }
        }
    }
    return retval;
}

double PathLocDualValues::getDualSumRHS() const {
    double retval = std::inner_product(m_nodeCapasDuals.begin(),
        m_nodeCapasDuals.end(), vertices(inst->network).first, 0.0,
        std::plus<>(), [&](auto&& dual, auto&& u) {
            return dual * inst->network[u].capacity;
        });
    for (auto [ite, end] = edges(inst->network); ite != end; ++ite) {
        retval += m_linkCapasDuals[inst->network[*ite].id]
                  * inst->network[*ite].capacity;
    }
    retval +=
        std::accumulate(m_onePathDuals.begin(), m_onePathDuals.end(), 0.0);
    retval += std::accumulate(
        m_oneLocationDuals.begin(), m_oneLocationDuals.end(), 0.0);
    return retval;
}

void barycenter(const PathLocDualValues& _nDuals,
    const PathLocDualValues& _bDuals, PathLocDualValues& _sDuals,
    double _alpha) {
    const auto update = [_alpha](const double _a, const double _b) -> double {
        return _alpha * _a + ((1 - _alpha) * _b);
    };
    std::transform(_nDuals.m_linkCapasDuals.begin(),
        _nDuals.m_linkCapasDuals.end(), _bDuals.m_linkCapasDuals.begin(),
        _sDuals.m_linkCapasDuals.begin(), update);
    std::transform(_nDuals.m_nodeCapasDuals.begin(),
        _nDuals.m_nodeCapasDuals.end(), _bDuals.m_nodeCapasDuals.begin(),
        _sDuals.m_nodeCapasDuals.begin(), update);
    std::transform(_nDuals.m_onePathDuals.begin(), _nDuals.m_onePathDuals.end(),
        _bDuals.m_onePathDuals.begin(), _sDuals.m_onePathDuals.begin(), update);
    std::transform(_nDuals.m_pathFuncDuals.data(),
        _nDuals.m_pathFuncDuals.data() + _nDuals.m_pathFuncDuals.num_elements(),
        _bDuals.m_pathFuncDuals.data(), _sDuals.m_pathFuncDuals.data(), update);
    std::transform(_nDuals.m_oneLocationDuals.begin(),
        _nDuals.m_oneLocationDuals.end(), _bDuals.m_oneLocationDuals.begin(),
        _sDuals.m_oneLocationDuals.begin(), update);
}
} // namespace SFC::OccLim
