#include "SFC/OccLim.hpp"

namespace SFC::OccLim {
Instance::Instance(DiGraph _network, std::vector<int> _nodeCapa,
    std::vector<Demand<>> _demands, std::vector<double> _funcCharge,
    int _nbLicenses)
    : SFC::Instance(_network, _nodeCapa, _demands, _funcCharge)
    , nbLicenses(_nbLicenses) {}

boost::multi_array<bool, 2> getFunctionPlacement(
    const std::vector<ServicePath>& _sPaths, const Instance& _inst) {
    boost::multi_array<bool, 2> funcPlacement(
        boost::extents[num_vertices(_inst.network)][_inst.funcCharge.size()]);
    for (const auto& sPath : _sPaths) {
        for (int j = 0; j < sPath.locations.size(); ++j) {
            funcPlacement[sPath.locations[j]]
                         [_inst.demands[sPath.demand].functions[j]] = 1;
        }
    }
    return funcPlacement;
}
} // namespace SFC::OccLim
