#include "SFC/OccLim/Algorithm.hpp"

#include "PricingProblem.hpp"
#include "SFC/ChainingPathOccLimit.hpp"

namespace SFC::OccLim {

std::optional<Solution<int>> solveWithBandP(const Instance& _inst,
    const ColumnGenerationParameters& _cgParams,
    const StabilizationParameters& /*_stabParams*/,
    const BaseBranchAndPriceParameters& _bapParams) {

    Alpha::Master rmp(_inst);
    ThreadingBase<std::tuple<SFC::LayeredFilteredShortestPathPerDemand>> base(
        _cgParams.nbThreads,
        std::make_tuple(SFC::LayeredFilteredShortestPathPerDemand(_inst,
            rmp.getForbiddenIntraLinks(), rmp.getForbiddenCrossLinks())));
    const auto solveFunc = [&](auto&& _rmp, bool isRoot) {
        if (isRoot) {
            return solve_impl(_rmp, base, _cgParams);
        } else {
            auto params = _cgParams;
#ifdef NDEBUG
            params.verbose = false;
#endif
            return solve_impl(_rmp, base, params);
        }
    };

    const auto branchOnLocSum = [](auto&& _rmp)
        -> std::vector<std::variant<LocationSum, ForbiddenLayeredCut>> {
        const auto lbr = _rmp.getLocationsBranches();
        if (lbr) {
            return {lbr->begin(), lbr->end()};
        }
        const auto linkBr = _rmp.getLayeredCutBranches();
        if (linkBr) {
            return {linkBr->begin(), linkBr->end()};
        }
        return {};
    };
    const auto noRounding = [](auto &&) -> std::optional<Solution<int>> {
        return std::nullopt;
    };
    const auto sol = branchAndPriceOnMaster(rmp, solveFunc, noRounding,
        branchOnLocSum, _bapParams, DeepestBestBoundExplorationPolicy());
    return sol;
}

} // namespace SFC::OccLim
