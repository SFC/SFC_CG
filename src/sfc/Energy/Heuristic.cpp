#include "Heuristic.hpp"

#include "utility.hpp"
#include <CppRO/cplex_utility.hpp>
#include <boost/graph/copy.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/graphviz.hpp>

namespace SFC::Energy {

Heuristic::Heuristic(const Instance& _inst)
    : m_inst(&_inst)
    , m_flowGraph([&]() {
        MutableDiGraph retval;
        const auto nodeCopy = [&](auto&& _orig, auto&& _dest) {
            const auto index = get(boost::vertex_index, m_inst->network, _orig);
            retval[_dest] = {index, 0, m_inst->network[_orig].capacity};
        };
        const auto edgeCopy = [&](auto&& _orig, auto&& _dest) {
            retval[_dest].capacity = m_inst->network[_orig].capacity;
            retval[_dest].residualCapacity = m_inst->network[_orig].capacity;
            retval[_dest].index = m_inst->network[_orig].id;
        };

        copy_graph(m_inst->network, retval,
            boost::vertex_copy(nodeCopy).edge_copy(edgeCopy));
        return retval;
    }())
    , m_distance(num_vertices(m_flowGraph))
    , m_predecessors(num_vertices(m_flowGraph))
    , m_paths(m_inst->demands.size())
    , m_chainLocations(m_inst->demands.size(), num_vertices(m_flowGraph))
    , m_chainsToDemands([&]() {
        std::map<std::vector<function_descriptor>, std::vector<int>>
            chainsToDemands;
        for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
            chainsToDemands[m_inst->demands[i].functions].push_back(i);
        }
        return chainsToDemands;
    }())
    , m_state(State::NotSolved) {}

bool Heuristic::getAllShortestPaths() {
    const auto vIndex_map = get(&Router::index, m_flowGraph);
    auto properties = predecessor_map(
        boost::make_iterator_property_map(m_predecessors.begin(), vIndex_map))
                          .distance_map(boost::make_iterator_property_map(
                              m_distance.begin(), vIndex_map))
                          .weight_map(get(&Arc::weight, m_flowGraph))
                          .distance_compare(epsilon_less<double>());

    return std::all_of(m_inst->demands.begin(), m_inst->demands.end(),
        [&, i = 0](auto&& _demand) mutable {
            m_djikstra_visitor.dest = _demand.t;
            try {
                boost::filtered_graph filtered(
                    m_flowGraph, ActiveFilter{&m_flowGraph});
                // std::ofstream ofs("test"+std::to_string(i)+".dot");
                // boost::write_graphviz(ofs, filtered);
                dijkstra_shortest_paths(filtered, _demand.s,
                    properties.visitor(
                        boost::make_dijkstra_visitor(m_djikstra_visitor)));
            } catch (int) {
                // Found dest
            }
            if (m_predecessors[_demand.t] == _demand.t) {
                std::cout << "No path found for " << _demand << '\n';
                return false;
            }
            m_paths[i] =
                getPathFromPredecessors(_demand.s, _demand.t, m_predecessors);
            for (auto iteU = m_paths[i].begin(), iteV = std::next(iteU);
                 iteV != m_paths[i].end(); ++iteU, ++iteV) {
                const auto e = edge(*iteU, *iteV, m_flowGraph).first;
                m_flowGraph[e].residualCapacity -= _demand.d;
                m_flowGraph[e].demands.push_back(i);
                m_flowGraph[e].weight = (1
                                         - (m_flowGraph[e].residualCapacity
                                             / m_flowGraph[e].capacity));
            }
            i++;
            return true;
        });
}

bool Heuristic::placeFunctions() {
    int totalNbDemandsAssigned = 0;
    // For each chains
    for (const auto& [function, demandsID] : m_chainsToDemands) {
        const auto ids = demandsID;
        // Count number of demands going though each node
        const std::vector<Node> descNodesByOcc = [&]() {
            std::vector<int> nodeOcc(num_vertices(m_flowGraph));
            for (const int demandID : ids) {
                for (const Node u : m_paths[demandID]) {
                    ++nodeOcc[u];
                }
            }
            std::vector<Node> retval(
                vertices(m_flowGraph).first, vertices(m_flowGraph).second);
            std::sort(
                retval.begin(), retval.end(), [&](const int u, const int v) {
                    return nodeOcc[u] > nodeOcc[v];
                });
            return retval;
        }();

        const int nbToAssigned = std::count_if(demandsID.begin(),
            demandsID.end(), [&](const demand_descriptor _demandID) {
                return m_chainLocations[_demandID] == num_vertices(m_flowGraph);
            });

        int nbDemandsAssigned = 0;
        for (const Node u : descNodesByOcc) {
            if (nbDemandsAssigned == nbToAssigned) {
                break;
            }
            // If not all demands have their chain assigned to a node
            for (const int demandID : demandsID) {
                const auto path = m_paths[demandID];
                assert(!path.empty());
                if (m_chainLocations[demandID] == num_vertices(m_flowGraph)
                    && std::find(path.begin(), path.end(), u) != path.end()) {
                    const auto totalNbCores = m_inst->getNbCores(demandID);
                    if (totalNbCores + m_flowGraph[u].usage
                        <= m_flowGraph[u].capacity) {
                        m_chainLocations[demandID] = u;
                        m_flowGraph[u].usage += totalNbCores;
                        assert(m_flowGraph[u].usage <= m_flowGraph[u].capacity);
                        ++totalNbDemandsAssigned;
                        ++nbDemandsAssigned;
                    }
                }
            }
        }
        if (nbDemandsAssigned < nbToAssigned) {
            std::cout << "Not enough demand assigned: " << nbDemandsAssigned
                      << " instead of " << nbToAssigned << '\n';
            return false;
        }
    }
    for (std::size_t demandID = 0; demandID < m_inst->demands.size();
         ++demandID) {
        assert(m_chainLocations[demandID] != num_vertices(m_flowGraph));
    }
    assert(totalNbDemandsAssigned == m_inst->demands.size());
    return true;
}

double Heuristic::getEnergyUsedForLinks() const {
    double retval = 0.0;
    for (const auto& ed : boost::make_iterator_range(edges(m_inst->network))) {
        if (m_flowGraph[ed].active) {
            retval += m_inst->energyCons.activeLink
                      + m_inst->energyCons.propLink
                            * (1
                                - (m_flowGraph[ed].residualCapacity
                                    / m_flowGraph[ed].capacity));
        }
    }
    return retval;
}

double Heuristic::getEnergyUsedForNodes() const {
    double retval = 0.0;
    for (Node u = 0; u < num_vertices(m_flowGraph); ++u) {
        retval +=
            std::ceil(m_flowGraph[u].usage) * m_inst->energyCons.activeCore;
    }
    return retval;
}

double Heuristic::getEnergyUsed() const {
    return getEnergyUsedForLinks() + getEnergyUsedForNodes();
}

std::vector<Link> Heuristic::getRemovableLinks() const {
    std::vector<Link> edgelist;
    edgelist.reserve(num_edges(m_flowGraph));
    for (auto [ite, end] = edges(m_flowGraph); ite != end; ++ite) {
        const auto [u, v] = incident(*ite, m_flowGraph);
        if (u < v && m_flowGraph[*ite].removable && m_flowGraph[*ite].active) {
            edgelist.emplace_back(*ite, edge(v, u, m_flowGraph).first);
        }
    }

    std::sort(edgelist.begin(), edgelist.end(),
        [&](const auto& _e1, const auto& _e2) {
            return m_flowGraph[_e1.first].capacity
                       + m_flowGraph[_e1.second].capacity
                   < m_flowGraph[_e2.first].capacity
                         + m_flowGraph[_e2.second].capacity;
        });
    return edgelist;
}

void Heuristic::restoreLink(const Link& _link) {
    m_flowGraph[_link.first].active = true;
    m_flowGraph[_link.second].active = true;
    m_flowGraph[_link.first].removable = false;
    m_flowGraph[_link.second].removable = false;
    clear();
    if (getAllShortestPaths() && placeFunctions()) {
        m_state = State::Solved;
    }
}

void Heuristic::removeLink(const Link& _link) {
    m_flowGraph[_link.first].active = false;
    m_flowGraph[_link.second].active = false;
    clear();
}

void Heuristic::clear() {
    for (auto [ite, end] = edges(m_flowGraph); ite != end; ++ite) {
        m_flowGraph[*ite].residualCapacity = m_flowGraph[*ite].capacity;
    }

    for (auto [ite, end] = vertices(m_flowGraph); ite != end; ++ite) {
        m_flowGraph[*ite].usage = 0.0;
    }

    for (auto& p : m_paths) {
        p.clear();
    }
    std::fill(m_chainLocations.begin(), m_chainLocations.end(),
        num_vertices(m_flowGraph));
}

std::vector<ServicePath> Heuristic::getServicePaths() const {
    std::vector<ServicePath> sPaths;
    sPaths.reserve(m_inst->demands.size());
    std::transform(m_paths.begin(), m_paths.end(), m_chainLocations.begin(),
        std::back_inserter(sPaths),
        [&, i = 0](auto&& _sPath, auto&& _location) mutable {
            assert(_location != -1);
            return ServicePath{_sPath,
                std::vector<Node>(
                    m_inst->demands[i].functions.size(), _location),
                i++};
        });
    return sPaths;
}

Heuristic::State Heuristic::getState() const { return m_state; }

Heuristic::State Heuristic::solve() {
    if (!getAllShortestPaths() || !placeFunctions()) {
        return m_state = State::Infeasible;
    }
    m_state = State::Solved;
    double energyUsed = getEnergyUsed();
    std::vector<Link> linksToRemove = getRemovableLinks();
    do {
        for (const auto& link : linksToRemove) {
            removeLink(link);
            std::cout << "Trying to remove "
                      << incident(link.first, m_flowGraph) << "..."
                      << std::flush;
            if (!getAllShortestPaths() || !placeFunctions()) {
                m_state = State::Infeasible;
                std::cout << "failure!\n";
                restoreLink(link);
            } else {
                m_state = State::Solved;
                energyUsed = getEnergyUsed();
                std::cout << "success: -> " << energyUsed
                          << ", links: " << getEnergyUsedForLinks()
                          << ", nodes: " << getEnergyUsedForNodes() << '\n';
                break;
            }
        }
    } while (!(linksToRemove = getRemovableLinks()).empty());
    return m_state;
}

std::vector<ServicePath> loadFromDisk(
    const std::string& _filename, const std::vector<Demand<>>& _demands) {
    std::vector<ServicePath> sPaths;

    std::ifstream ifs(_filename);
    if (!ifs) {
        std::cerr << _filename << " does not exists!" << std::endl;
        throw std::ios_base::failure(_filename + " does not exists!");
    } else {
        std::string line;
        std::size_t i = 0;
        while (std::getline(ifs, line)) {
            std::stringstream linestream(line);
            int pathLength;
            Path path;
            Node chainLocation;

            linestream >> pathLength;
            path.reserve(pathLength);
            for (std::size_t j = 0; j < pathLength; ++j) {
                Node u;
                linestream >> u;
                path.push_back(u);
            }
            linestream >> chainLocation;
            sPaths.emplace_back(path,
                std::vector<Node>(_demands[i].functions.size(), chainLocation),
                i);
            ++i;
        }
    }
    return sPaths;
}
} // namespace SFC::Energy
