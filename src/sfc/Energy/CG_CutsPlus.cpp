#include "CG_CutsPlus.hpp"
#include "Decomposition.hpp"
#include "utility.hpp"
#include <boost/format.hpp>

using ::operator<<;

namespace SFC::Energy::CG_CutsPlus {

Master::Master(const Instance& _inst)
    : Base(DualValues{_inst})
    , m_inst(_inst)
    , m_x(getLinkActivationVariables(*m_inst, m_model))
    , m_k(getNumberActiveCoreVariables(*m_inst, m_model))
    , m_obj([&]() {
        return IloAdd(m_model, IloMinimize(m_env, getNetworkEnergyConsumption(*m_inst, m_x, m_k)));
    }())
    , m_onePathCons(getOnePathPerDemandConstraints(*m_inst, m_model))
    , m_linkCapasCons(getLinkCapacityConstraints(*m_inst, m_model, m_x))
    , m_linkUsableCons([&]() {
        boost::format fmter("linkUsableCons((%1% %2%), %3%)");
        boost::multi_array<LazyConstraint, 2> linkUsableCons(boost::extents[num_edges(m_inst->network)][m_inst->demands.size()]);
        for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
            for (int i = 0; i < m_inst->demands.size(); ++i) {
                linkUsableCons[m_inst->network[ed].id][i] = LazyConstraint(m_model, -IloInfinity, -m_x[m_inst->network[ed].id], 0.0);
                linkUsableCons[m_inst->network[ed].id][i].setName(
                    (fmter % source(ed, m_inst->network)
                        % target(ed, m_inst->network) % i)
                        .str());
            }
        }
        return linkUsableCons;
    }())
    , m_nodeCapasCons(getNodeCapacityConstraints(*m_inst, m_model, m_k))
    , m_kBounds([&]() {
        boost::format fmter("kBounds(%1%)");
        std::vector<IloRange> nodeCapasCons(num_vertices(m_inst->network));
        for (int u = 0; u < num_vertices(m_inst->network); ++u) {
            if (m_inst->network[u].isNFV) {
                nodeCapasCons[u] = IloAdd(m_model, IloRange(m_env, 0.0, m_k[u], m_inst->network[u].capacity));
                setIloName(nodeCapasCons[u], (fmter % u).str());
            }
        }
        return nodeCapasCons;
    }())
    , m_networkCutCons(getConnectivityConstraint(*m_inst, m_model, m_x))
    , m_neighborhoodCutsIn(getNeighborhoodConstraints(*m_inst, m_model, m_x))
    , m_valsLinkCapa(num_edges(m_inst->network))
    , m_valsLinkUsed(num_edges(m_inst->network))
    , m_valsNode(num_vertices(m_inst->network)) {

    m_solver.extract(m_model);
    m_solver.setOut(m_env.getNullStream());
    m_solver.setParam(IloCplex::EpRHS, 1e-9);
    m_solver.setParam(IloCplex::Threads, 1);
    m_solver.setParam(IloCplex::RootAlg, IloCplex::Dual);

    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        const auto [u, v] = incident(ed, m_inst->network);
        if (u < v) {
            m_integerModel.add(IloConversion(m_env, m_x[m_inst->network[ed].id], ILOBOOL));
        }
    }

    for (const auto &var : m_k) {
        m_integerModel.add(IloConversion(m_env, var, ILOBOOL));
    }
}

double Master::getReducedCost_impl(const Column& _sPath,
    const DualValues& _dualValues) const noexcept {

    const auto& [nPath, locations, demandID] = _sPath;
    std::vector<bool> edgeUsed(num_edges(m_inst->network), false);
    double retval = -_dualValues.m_onePathDuals[demandID];
    for (auto iteU = nPath.begin(), iteV = std::next(iteU);
         iteV != nPath.end(); ++iteU, ++iteV) {
        /// layer # is not use for the reduced cost
        const auto ed = edge(*iteU, *iteV, m_inst->network).first;
        retval += _dualValues.getReducedCost(IntraLayerLink{demandID, {*iteU, *iteV}, -1});
        edgeUsed[m_inst->network[ed].id] = true;
    }
    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        const auto [u, v] = incident(ed, m_inst->network);
        if (edgeUsed[m_inst->network[ed].id]) {
            retval += _dualValues.getReducedCost(UsedLink{demandID, {u, v}});
        }
    }

    for (int j = 0; j < locations.size(); ++j) {
        retval += _dualValues.getReducedCost(CrossLayerLink{demandID,
            locations[j], j});
    }
    return retval;
}

IloNumColumn Master::getColumn_impl(const Column& _sPath) noexcept {
    const auto [nPath, locations, demandID] = _sPath;
    // For each link in the _sPath.first, add to the corresponding constraints (link capa & link usage)
    std::fill(m_valsLinkCapa.begin(), m_valsLinkCapa.end(), 0.0);
    std::fill(m_valsLinkUsed.begin(), m_valsLinkUsed.end(), 0.0);

    double objCoef = 0.0;
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end(); ++iteU, ++iteV) {
        const auto ed = edge(*iteU, *iteV, m_inst->network).first;
        const auto e = m_inst->network[ed].id;
        m_valsLinkCapa[e] += m_inst->demands[demandID].d;
        m_valsLinkUsed[e] = 1.0;
        objCoef += m_inst->energyCons.propLink * m_inst->demands[demandID].d / m_inst->network[ed].capacity;
    }

    const auto mult = [](auto&& _const, auto&& _val) {
        return _const(_val);
    };
    const auto add = [](auto&& _acc, auto&& _newVal) {
        return _acc += _newVal;
    };

    IloNumColumn retval = m_onePathCons[demandID](1.0) + m_obj(objCoef);

    retval += std::inner_product(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_valsLinkCapa.begin(), IloNumColumn(m_env), add, mult);

    std::fill(m_valsNode.begin(), m_valsNode.end(), 0.0);
    for (int j = 0; j < locations.size(); ++j) {
        m_valsNode[locations[j]] += m_inst->nbCores[demandID][j];
    }
    retval += std::inner_product(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_valsNode.begin(), IloNumColumn(m_env), add, mult);

    using range = boost::multi_array_types::index_range;
    auto refLinkUsage = m_linkUsableCons[boost::indices[range(0, num_edges(m_inst->network))][demandID]];
    retval += std::inner_product(refLinkUsage.begin(), refLinkUsage.end(),
        m_valsLinkUsed.begin(), IloNumColumn(m_env), add, mult);
    return retval;
}

void Master::removeDummyIfPossible() noexcept {
}

//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################

PricingProblem::PricingProblem(const Instance& _inst)
    : m_inst(&_inst)
    , n(num_vertices(m_inst->network))
    , m(num_edges(m_inst->network))
    , nbLayers(m_inst->maxChainSize + 1)
    , m_model(m_env)
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_a([&]() {
        boost::format fmter("a(u:%1%, j:%2%)");
        boost::multi_array<IloNumVar, 2> retval(boost::extents[num_vertices(m_inst->network)][nbLayers - 1]);
        for (int u = 0; u < num_vertices(m_inst->network); ++u) {
            for (int j = 0; j < nbLayers - 1; ++j) {
                retval[u][j] = IloAdd(m_model,
                    IloNumVar(m_env, 0, 1, ILOBOOL));
                setIloName(retval[u][j], (fmter % u % j).str());
            }
        }
        return retval;
    }())
    , m_f([&]() {
        boost::multi_array<IloNumVar, 2> retval(boost::extents[m][nbLayers]);
        boost::format fmter("f(l:(%1%, %2%), j:%3%)");
        for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
            for (int j = 0; j < nbLayers; ++j) {
                retval[m_inst->network[ed].id][j] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                setIloName(retval[m_inst->network[ed].id][j],
                    (fmter % source(ed, m_inst->network) % target(ed, m_inst->network) % j).str());
            }
        }
        return retval;
    }())
    , m_x(getLinkActivationVariables(*m_inst, m_model))
    , m_flowConsCons([&]() {
        boost::multi_array<IloRange, 2> retval(boost::extents[n][nbLayers]);
        IloExpr expr(m_env);
        boost::format fmter("flowCons(u:%1%, j:%2%)");
        for (Node u = 0; u < num_vertices(m_inst->network); ++u) {
            for (int j = 0; j < nbLayers; ++j) {
                expr.clear();
                for (const auto ed : boost::make_iterator_range(out_edges(u, m_inst->network))) {
                    expr += m_f[m_inst->network[ed].id][j];
                }
                for (const auto ed : boost::make_iterator_range(in_edges(u, m_inst->network))) {
                    expr += -m_f[m_inst->network[ed].id][j];
                }
                if (m_inst->network[u].isNFV) {
                    if (j > 0) {
                        expr += -m_a[u][j - 1];
                    }
                    if (j < nbLayers - 1) {
                        expr += m_a[u][j];
                    }
                }
                retval[u][j] = IloAdd(m_model,
                    IloRange(m_env, 0.0, expr, 0.0));
                setIloName(retval[u][j], (fmter % u % j).str());
            }
        }
        expr.end();
        return retval;
    }())
    , m_usableLink([&]() {
        boost::multi_array<IloRange, 2> retval(boost::extents[m][nbLayers]);
        boost::format fmter("linkFX(l:(%1%, %2%), j:%3%)");
        for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
            const auto e = m_inst->network[ed].id;
            for (int j = 0; j < nbLayers; ++j) {
                retval[e][j] = IloAdd(m_model,
                    IloRange(m_env, 0.0, m_x[e] - m_f[e][j], IloInfinity));
                setIloName(retval[e][j],
                    str(fmter % source(ed, m_inst->network) % target(ed, m_inst->network) % j));
            }
        }
        return retval;
    }())
    , m_linkCapaCons([&]() {
        boost::format fmter("linkCapaCons(l:(%1%, %2%))");
        std::vector<IloRange> retval(m);
        for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
            const auto e = m_inst->network[ed].id;
            retval[e] = IloAdd(m_model, IloRange(m_env, 0.0, m_inst->network[ed].capacity));
            setIloName(retval[e],
                str(fmter % source(ed, m_inst->network) % target(ed, m_inst->network)));
        }
        return retval;
    }())
    , m_nodeCapaCons([&]() {
        std::vector<IloRange> retval(n);
        boost::format fmter("nodeCapaCons(u:%1%)");
        for (int u = 0; u < num_vertices(m_inst->network); ++u) {
            retval[u] = IloAdd(m_model,
                IloRange(m_env, 0.0, m_inst->network[u].capacity));
            setIloName(retval[u], str(fmter % u));
        }
        return retval;
    }())
    , m_noInLoopCons([&]() {
        boost::multi_array<IloRange, 2> retval(boost::extents[n][nbLayers]);
        boost::format fmter("noInLoopCons(u:%1%, j:%2%)");
        IloExpr expr(m_env);
        for (Node u = 0; u < num_vertices(m_inst->network); ++u) {
            for (int j = 0; j < nbLayers; ++j) {
                expr.clear();
                for (const auto ed : boost::make_iterator_range(in_edges(u, m_inst->network))) {
                    expr += m_f[m_inst->network[ed].id][j];
                }
                if (j > 0) {
                    expr += m_a[u][j - 1];
                }
                retval[u][j] = IloAdd(m_model, IloRange(m_env, 0.0, expr, 1.0));
                setIloName(retval[u][j], str(fmter % u % j));
            }
        }
        return retval;
    }())
    , m_noOutLoopCons([&]() {
        boost::multi_array<IloRange, 2> retval(boost::extents[n][nbLayers]);
        boost::format fmter("noOutLoopCons(u:%1%, j:%2%)");
        IloExpr expr(m_env);
        for (Node u = 0; u < num_vertices(m_inst->network); ++u) {
            for (int j = 0; j < nbLayers; ++j) {
                expr.clear();
                for (const auto ed : boost::make_iterator_range(out_edges(u, m_inst->network))) {
                    expr += m_f[m_inst->network[ed].id][j];
                }
                if (j < nbLayers - 1) {
                    expr += m_a[u][j];
                }
                retval[u][j] = IloAdd(m_model, IloRange(m_env, 0.0, expr, 1.0));
                setIloName(retval[u][j], str(fmter % u % j));
            }
        }
        return retval;
    }())
    , m_solver([&]() {
        IloCplex solver(m_model);
        solver.setParam(IloCplex::Threads, 1);
        solver.setParam(IloCplex::EpRHS, 1e-9);
        solver.setOut(m_env.getNullStream());
        solver.setWarning(m_env.getNullStream());
        return solver;
    }()) {
}

PricingProblem::PricingProblem(const PricingProblem& _other)
    : PricingProblem(_other.m_inst) {}

PricingProblem& PricingProblem::operator=(const PricingProblem& _other) {
    if (this != &_other) {
        *this = PricingProblem(_other);
    }
    return *this;
}

PricingProblem::PricingProblem(PricingProblem&& _other)
    : m_inst(std::move(_other.m_inst))
    , m_env(std::move(_other.m_env))
    , m_demandID(std::move(_other.m_demandID))
    , n(std::move(_other.n))
    , m(std::move(_other.m))
    , nbLayers(std::move(_other.nbLayers))
    , m_model(std::move(_other.m_model))
    , m_obj(std::move(_other.m_obj))
    , m_a(std::move(_other.m_a))
    , m_f(std::move(_other.m_f))
    , m_x(std::move(_other.m_x))
    , m_flowConsCons(std::move(_other.m_flowConsCons))
    , m_usableLink(std::move(_other.m_usableLink))
    , m_linkCapaCons(std::move(_other.m_linkCapaCons))
    , m_nodeCapaCons(std::move(_other.m_nodeCapaCons))
    , m_noInLoopCons(std::move(_other.m_noInLoopCons))
    , m_noOutLoopCons(std::move(_other.m_noOutLoopCons))
    , m_solver(std::move(_other.m_solver))
    , m_servicePath(std::move(_other.m_servicePath)) {
    _other.m_env = IloEnv(); // Empty _other IloEnv for destruction
}

PricingProblem& PricingProblem::operator=(PricingProblem&& _other) {
    if (this != &_other) {
        *this = std::move(_other);
    }
    return *this;
}

void PricingProblem::setPricingID(const int _demandID) {
    int nbFunctions = m_inst->demands[m_demandID].functions.size();
    m_flowConsCons[m_inst->demands[m_demandID].s][0].setBounds(0.0, 0.0);
    m_flowConsCons[m_inst->demands[m_demandID].t][nbFunctions].setBounds(0.0, 0.0);

    m_demandID = _demandID;
    nbFunctions = m_inst->demands[m_demandID].functions.size();
    m_flowConsCons[m_inst->demands[m_demandID].s][0].setBounds(1.0, 1.0);
    m_flowConsCons[m_inst->demands[m_demandID].t][nbFunctions].setBounds(-1.0, -1.0);

    IloExpr expr(m_env);
    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        const auto e = m_inst->network[ed].id;
        expr.clear();
        for (int j = 0; j <= nbFunctions; ++j) {
            expr += m_inst->demands[m_demandID].d * m_f[e][j];
        }
        m_linkCapaCons[e].setExpr(expr);
    }

    for (const auto u : boost::make_iterator_range(vertices(m_inst->network))) {
        if (m_inst->network[u].isNFV) {
            expr.clear();
            for (int j = 0; j < nbFunctions; ++j) {
                expr += m_inst->nbCores[m_demandID][j] * m_a[u][j];
            }
            m_nodeCapaCons[u].setExpr(expr);
        }
    }
    expr.end();
}

void PricingProblem::updateDual(const DualValues& _dualValues) {
    IloExpr objExpr(m_env);
    const int nbFunctions = m_inst->demands[m_demandID].functions.size();

    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        const auto [u, v] = incident(ed, m_inst->network);
        const auto e = m_inst->network[ed].id;

        objExpr += _dualValues.getReducedCost(UsedLink{m_demandID, {u, v}})
                   * m_x[e];
        for (int j = 0; j <= nbFunctions; ++j) {
            objExpr += _dualValues.getReducedCost(IntraLayerLink{m_demandID, {u, v}, j})
                       * m_f[e][j];
        }
    }

    for (int u = 0; u < num_vertices(m_inst->network); ++u) {
        if (m_inst->network[u].isNFV) {
            for (int j = 0; j < nbFunctions; ++j) {
                objExpr += _dualValues.getReducedCost(CrossLayerLink{m_demandID, u, j})
                           * m_a[u][j];
            }
        }
    }
    m_obj.setConstant(-_dualValues.m_onePathDuals[m_demandID]);
    m_obj.setExpr(objExpr);
    m_solver.extract(m_model);
    objExpr.end();
}

ServicePath PricingProblem::getColumn() const {
    return m_servicePath;
}

double PricingProblem::getObjValue() const {
    return m_solver.getObjValue();
}

bool PricingProblem::solve() {
    if (!m_solver.solve()) {
        m_solver.exportModel("pp.lp");
        return false;
    }
    if (epsilon_less<double>()(0.0, m_solver.getObjValue())) {
        return true;
    }

    std::vector<IloNumVarArray> cycles;
    // Get values
    const auto getValue = [&](auto&& _var) {
        return m_solver.getValue(_var);
    };
    boost::multi_array<double, 2> fVal(boost::extents[num_edges(m_inst->network)][nbLayers]);
    std::vector<double> xVal(num_edges(m_inst->network));

    do {
        cycles.clear();

        std::transform(m_f.data(), m_f.data() + m_f.num_elements(), fVal.data(), getValue);
        std::transform(m_x.begin(), m_x.end(), xVal.begin(), getValue);

        // Rebuild the path
        m_servicePath = [&]() {
            const auto& funcs = m_inst->demands[m_demandID].functions;
            const auto& t = m_inst->demands[m_demandID].t;

            Node u = m_inst->demands[m_demandID].s;
            ServicePath retval{{u}, {}, m_demandID};

            int j = 0;
            while (u != t || j < funcs.size()) {
                if (j < funcs.size() && m_inst->network[u].isNFV
                    && epsilon_equal<double>()(m_solver.getValue(m_a[u][j]), IloTrue)) {
                    retval.locations.push_back(u);
                    ++j;
                } else {
                    for (const auto ed : boost::make_iterator_range(out_edges(u, m_inst->network))) {
                        if (epsilon_equal<double>()(fVal[m_inst->network[ed].id][j],
                                IloTrue)) {
                            fVal[m_inst->network[ed].id][j] = 0;
                            xVal[m_inst->network[ed].id] = 0;
                            retval.nPath.push_back(target(ed, m_inst->network));
                            u = target(ed, m_inst->network);
                            break;
                        }
                    }
                }
            }
            assert(retval.nPath.back() == t);
            return retval;
        }();

        // Check for cycles
        for (int j = 0; j < nbLayers; ++j) {
            for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
                const auto [u, v] = incident(ed, m_inst->network);
                const auto e = m_inst->network[ed].id;
                if (u < v) {
                    if (epsilon_equal<double>()(fVal[e][j], IloTrue)) {
                        fVal[e][j] = 0;
                        cycles.emplace_back(m_env);
                        cycles.back().add(m_f[e][j]);
                        Node node = v;
                        const Node t = u;
                        while (node != t) {
                            for (const auto out_ed : boost::make_iterator_range(out_edges(node, m_inst->network))) {
                                if (epsilon_equal<double>()(fVal[m_inst->network[out_ed].id][j], IloTrue)) {
                                    fVal[m_inst->network[out_ed].id][j] = 0;
                                    cycles.back().add(m_f[m_inst->network[out_ed].id][j]);
                                    node = v;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!cycles.empty()) {
            DBOUT(std::cout << "Found " << cycles.size() << " cycles "
                            << " for demand " << m_inst->demands[m_demandID] << '\n';)
            for (const auto& cycle : cycles) {
                IloRange rng(m_env, 0.0, IloSum(cycle), cycle.getSize() - 1);
                m_model.add(rng);
            }
            if (!m_solver.solve()) {
                return false;
            }
            if (epsilon_less<double>()(0.0, m_solver.getObjValue())) {
                return true;
            }
        }
    } while (!cycles.empty());
    return true;
}

void Master::getDuals_impl() noexcept {
    const auto get = [&](auto&& _cons) {
        return getDual(m_solver, _cons);
    };

    std::transform(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_duals.m_linkCapasDuals.begin(), get);
    std::transform(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_duals.m_nodeCapaDuals.begin(), get);
    std::transform(m_kBounds.begin(), m_kBounds.end(),
        m_duals.m_kBoundsDuals.begin(), get);
    std::transform(m_onePathCons.begin(), m_onePathCons.end(),
        m_duals.m_onePathDuals.begin(), get);
    std::transform(m_linkUsableCons.data(), m_linkUsableCons.data() + m_linkUsableCons.num_elements(),
        m_duals.m_linkUsableDuals.data(), get);
    m_duals.m_networkCutsDual = get(m_networkCutCons);
    std::transform(m_neighborhoodCutsIn.begin(), m_neighborhoodCutsIn.end(),
        m_duals.m_neighborhoodCutsDuals.begin(), get);
}

DualValues::DualValues(const Instance& _inst) noexcept
    : inst(&_inst)
    , m_onePathDuals(inst->demands.size(), 0.0)
    , m_linkCapasDuals(num_edges(inst->network), 0.0)
    , m_nodeCapaDuals(num_vertices(inst->network), 0.0)
    , m_kBoundsDuals(num_vertices(inst->network), 0.0)
    , m_linkUsableDuals(boost::extents[num_edges(inst->network)][inst->demands.size()])
    , m_neighborhoodCutsDuals(num_vertices(inst->network)) {}

IloNum DualValues::getReducedCost(const CrossLayerLink& _link) const noexcept {
    return -m_nodeCapaDuals[_link.u]
           * inst->nbCores[_link.demandID][_link.j];
}

IloNum DualValues::getReducedCost(const IntraLayerLink& _link) const noexcept {
    const auto ed = edge(_link.edge.first, _link.edge.second, inst->network).first;
    return inst->demands[_link.demandID].d
           * (inst->energyCons.propLink / inst->network[ed].capacity
                 - m_linkCapasDuals[inst->network[ed].id]);
}

double DualValues::getReducedCost(const UsedLink& _link) const noexcept {
    const auto ed = edge(_link.edge.first, _link.edge.second, inst->network).first;
    return -m_linkUsableDuals[inst->network[ed].id][_link.demandID];
}

double DualValues::getDualSumRHS() const noexcept {
    std::terminate();
}

double DualValues::getAdditionalVariable() const noexcept {
    return 0.0;
}

} // namespace SFC::Energy::CG_CutsPlus
