#include "Chaining.hpp"
#include "Decomposition.hpp"
#include <boost/graph/graph_utility.hpp>

namespace SFC::Energy {
Placement_Path::Placement_Path(const Instance& _inst) noexcept
    : Base(DualValues(_inst))
    , m_inst(_inst)
    , m_x(getLinkActivationVariables(*m_inst, m_model))
    , m_k(getNumberActiveCoreVariables(*m_inst, m_model))
    , m_obj([&]() {
        return IloAdd(m_model, IloMinimize(m_env, getNetworkEnergyConsumption(*m_inst, m_x, m_k)));
    }())
    , m_onePathCons(getOnePathPerDemandConstraints(*m_inst, m_model))
    , m_linkCapasCons(getLinkCapacityConstraints(*m_inst, m_model, m_x))
    , m_nodeCapasCons1(getNodeCapacityConstraints(*m_inst, m_model, m_k))
    , m_nodeCapasCons2([&]() {
        std::vector<IloRange> nodeCapasCons(num_vertices(m_inst->network));
        for (const auto u : boost::make_iterator_range(vertices(m_inst->network))) {
            nodeCapasCons[u] = IloAdd(m_model,
                IloRange(m_env, 0.0, m_k[u], m_inst->network[u].capacity));
            setIloName(nodeCapasCons[u], "nodeCapasCons2" + std::to_string(u));
        }
        return nodeCapasCons;
    }())
    , m_linkActiveEqConstraints([&]() {
        std::vector<IloRange> retval;
        for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
            const auto [u, v] = incident(ed, m_inst->network);
            if (u < v) {
                retval.emplace_back(IloAdd(m_model,
                    m_x[m_inst->network[ed].id] - m_x[m_inst->network[edge(v, u, m_inst->network).first].id] == 0));
            }
        }
        return retval;
    }())
    , m_networkCutCons(getConnectivityConstraint(*m_inst, m_model, m_x))
    , m_neighborhoodCutsIn(getNeighborhoodConstraints(*m_inst, m_model, m_x))
    , m_valsLink(num_edges(m_inst->network))
    , m_valsNode(num_vertices(m_inst->network)) {
    m_solver.extract(m_model);
    m_solver.setOut(m_env.getNullStream());
    m_solver.setParam(IloCplex::EpRHS, 1e-9);
    m_solver.setParam(IloCplex::Threads, 1);
    m_solver.setParam(IloCplex::RootAlg, IloCplex::Dual);

    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        const auto [u, v] = incident(ed, m_inst->network);
        if (u < v) {
            m_integerModel.add(IloConversion(m_env, m_x[m_inst->network[ed].id], ILOBOOL));
        }
    }

    for (const auto& var : m_k) {
        m_integerModel.add(IloConversion(m_env, var, ILOBOOL));
    }
}

double Placement_Path::getReducedCost_impl(const ServicePath& _sPath,
    const DualValues& _dualValues) const noexcept {
    const auto& [nPath, locations, demandID] = _sPath;
    double retval = -_dualValues.m_onePathDuals[demandID];
    for (auto iteU = nPath.begin(), iteV = std::next(iteU);
         iteV != nPath.end(); ++iteU, ++iteV) {
        /// layer # is not use for the reduced cost
        retval += _dualValues.getReducedCost(IntraLayerLink{demandID, {*iteU, *iteV}, -1});
    }
    for (int j = 0; j < locations.size(); ++j) {
        retval += _dualValues.getReducedCost(CrossLayerLink{demandID,
            locations[j], j});
    }
    return retval;
}

IloNumColumn Placement_Path::getColumn_impl(const Column& _sPath) noexcept {
    const auto& [nPath, locations, demandID] = _sPath;
    const auto& [id, s, t, d, chain] = m_inst->demands[demandID];

    assert(nPath.front() == s);
    assert(nPath.back() == t);

    std::fill(m_valsLink.begin(), m_valsLink.end(), 0.0);

    IloNumColumn col = m_onePathCons[_sPath.demand](1.0);
    double objCoef = 0.0;
    for (auto iteU = nPath.begin(), iteV = std::next(iteU);
         iteV != nPath.end(); ++iteU, ++iteV) {
        const auto& ed = edge(*iteU, *iteV, m_inst->network).first;
        m_valsLink[m_inst->network[ed].id] += d;
        objCoef += m_inst->energyCons.propLink
                   * (d / m_inst->network[ed].capacity);
    }
    col += m_obj(objCoef);

    const auto mult = [](const IloRange& _const, double _val) {
        return _const(_val);
    };
    const auto add = [](auto&& _acc, auto&& _newVal) {
        return _acc += _newVal;
    };

    col += std::inner_product(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_valsLink.begin(), IloNumColumn(m_env), add, mult);

    std::fill(m_valsNode.begin(), m_valsNode.end(), 0.0);
    for (int j = 0; j < locations.size(); ++j) {
        m_valsNode[locations[j]] += m_inst->nbCores[_sPath.demand][j];
    }
    col += std::inner_product(m_nodeCapasCons1.begin(), m_nodeCapasCons1.end(),
        m_valsNode.begin(), IloNumColumn(m_env), add, mult);
    return col;
}

void Placement_Path::removeDummyIfPossible() noexcept {
    if (m_dummyActive) {
        const double dummySum = std::accumulate(m_dummyPaths.begin(),
            m_dummyPaths.end(), 0.0,
            [&](auto&& _acc, auto&& _var) {
                return _acc + m_solver.getValue(_var);
            });
        if (epsilon_equal<double>()(dummySum, 0.0)) {
            m_dummyActive = false;
            std::cout << "\n################################################";
            std::cout << "\nRemoved dummy...";
            std::cout << "\n##############################################\n";
            for (const auto& dummy : m_dummyPaths) {
                m_model.add(dummy == 0);
            }
        }
    }
}

std::vector<ServicePath> Placement_Path::getServicePaths() const noexcept {
    const auto& cols = getColumnStorage<ServicePath>();
    std::vector<ServicePath> retval(m_inst->demands.size());
    for (auto& [var, sPath] : cols) {
        if (epsilon_equal<double>()(m_solver.getValue(var), IloTrue)) {
            retval[sPath.demand] = sPath;
        }
    }
    return retval;
}

// Solution Placement_Path::getSolution() const {

// }

void Placement_Path::getDuals_impl() noexcept {
    const auto getDual = [&](const IloRange& _cons) {
        return m_solver.getDual(_cons);
    };
    std::transform(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_duals.m_linkCapasDuals.begin(), getDual);
    std::transform(m_nodeCapasCons1.begin(), m_nodeCapasCons1.end(),
        m_duals.m_nodeCapasDuals1.begin(), getDual);
    std::transform(m_nodeCapasCons2.begin(), m_nodeCapasCons2.end(),
        m_duals.m_nodeCapasDuals2.begin(), getDual);
    std::transform(m_onePathCons.begin(), m_onePathCons.end(),
        m_duals.m_onePathDuals.begin(), getDual);
}

DiGraph Placement_Path::getActiveNetwork() const noexcept {
    std::vector<Edge> edgeList;
    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        if (epsilon_equal<double>()(m_solver.getValue(m_x[m_inst->network[ed].id]), IloTrue)) {
            edgeList.emplace_back(incident(ed, m_inst->network));
        }
    }
    return {edgeList.begin(), edgeList.end(), num_vertices(m_inst->network)};
}

Solution<int> Placement_Path::getSolution() const noexcept {
    return {*m_inst, m_intObj, m_fracObj, getActiveNetwork(), getServicePaths(), getNbColumns()};
}

DualValues::DualValues(const Instance& _inst) noexcept
    : inst(&_inst)
    , m_onePathDuals(inst->demands.size(), 0.0)
    , m_linkCapasDuals(num_edges(inst->network), 0.0)
    , m_nodeCapasDuals1(num_vertices(inst->network), 0.0)
    , m_nodeCapasDuals2(num_vertices(inst->network), 0.0) {}

IloNum DualValues::getReducedCost(const CrossLayerLink& _link) const noexcept {
    return -m_nodeCapasDuals1[_link.u]
           * inst->nbCores[_link.demandID][_link.j];
}

IloNum DualValues::getReducedCost(const IntraLayerLink& _link) const noexcept {
    auto ed = edge(_link.edge.first, _link.edge.second, inst->network).first;
    return inst->demands[_link.demandID].d
           * (inst->energyCons.propLink / inst->network[ed].capacity
                 - m_linkCapasDuals[inst->network[ed].id]);
}

double DualValues::getDualSumRHS() const noexcept {
    double retval = 0.0;
    for (const auto u : boost::make_iterator_range(vertices(inst->network))) {
        retval += m_nodeCapasDuals1[u] * inst->network[u].capacity;
    }
    for (const auto u : boost::make_iterator_range(vertices(inst->network))) {
        retval += m_nodeCapasDuals2[u] * inst->network[u].capacity;
    }
    for (auto [ite, end] = edges(inst->network); ite != end; ++ite) {
        retval += m_linkCapasDuals[inst->network[*ite].id]
                  * inst->network[*ite].capacity;
    }
    retval += std::accumulate(m_onePathDuals.begin(), m_onePathDuals.end(), 0.0);
    return retval;
}

double DualValues::getAdditionalVariable() const noexcept {
    return 0.0;
}

} // namespace SFC::Energy
