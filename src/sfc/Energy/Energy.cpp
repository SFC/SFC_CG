#include "Energy.hpp"
#include "CppRO/cplex_utility.hpp"
#include <boost/format.hpp>

namespace SFC::Energy {
Consumption Consumption::fromFile(const std::string& _filename) {
    std::cout << "Getting energy consumption from " << _filename << '\n';
    std::ifstream ifs(_filename);
    Consumption cons;
    if (!ifs) {
        throw std::runtime_error("Not a valid file for consumption\n");
    }
    ifs >> cons.activeLink >> cons.propLink >> cons.activeCore;
    return cons;
}

Instance::Instance(DiGraph _network, std::vector<int> _nodeCapa,
    std::vector<Demand<>> _demands, std::vector<double> _funcCharge,
    Consumption _energyCons)
    : SFC::Instance(_network, _nodeCapa, _demands, _funcCharge)
    , energyCons(std::move(_energyCons)) {
    for (int i = 0; i < demands.size(); ++i) {
        for (int j = 0; j < demands[i].functions.size(); ++j) {
            nbCores[i][j] = demands[i].d / funcCharge[demands[i].functions[j]];
        }
    }
}

std::ostream& operator<<(
    std::ostream& _out, const SFC::Energy::Consumption& _cons) {
    return _out << "[activeLink:" << _cons.activeLink
                << ", propLink: " << _cons.propLink
                << ", activeCore:" << _cons.activeCore << "]";
}

std::vector<IloNumVar> getLinkActivationVariables(
    const Instance& _inst, IloModel& _model) {
    boost::format fmter("x(%1%, %2%)");
    std::vector<IloNumVar> x(num_edges(_inst.network));
    for (const auto ed : boost::make_iterator_range(edges(_inst.network))) {
        const auto [u, v] = incident(ed, _inst.network);
        if (u < v) {
            x[_inst.network[ed].id] = IloAdd(
                _model, IloNumVar(_model.getEnv(), 0.0, IloInfinity, ILOFLOAT));
            setIloName(x[_inst.network[ed].id], str(fmter % u % v));
        }
    }
    for (const auto ed : boost::make_iterator_range(edges(_inst.network))) {
        const auto [u, v] = incident(ed, _inst.network);
        if (v < u) { // u always different from v
            x[_inst.network[ed].id] =
                x[_inst.network[edge(v, u, _inst.network).first].id];
        }
    }
    return x;
}

std::vector<IloNumVar> getNumberActiveCoreVariables(
    const Instance& _inst, IloModel& _model) {
    std::vector<IloNumVar> k(num_vertices(_inst.network));
    boost::format fmter("k(%1%)");
    for (const auto u : boost::make_iterator_range(vertices(_inst.network))) {
        k[_inst.network[u].id] = IloAdd(
            _model, IloNumVar(_model.getEnv(), 0.0, IloInfinity, ILOFLOAT));
        setIloName(k[_inst.network[u].id], str(fmter % u));
    }
    return k;
}

/**
 * In a network with a all-to-all traffic, we need at least n-1 active links.
 * Otherwise, the network is not connected
 */
IloRange getConnectivityConstraint(
    const Instance& _inst, IloModel& _model, const std::vector<IloNumVar>& _x) {
    const auto expr =
        std::accumulate(_x.begin(), _x.end(), IloExpr(_model.getEnv()));
    return IloAdd(_model, expr >= 2 * (num_vertices(_inst.network) - 1.0));
}

std::vector<IloRange> getNeighborhoodConstraints(
    const Instance& _inst, IloModel& _model, const std::vector<IloNumVar>& _x) {
    boost::format fmter("neighCuts(u:%1%)");
    std::vector<IloRange> neighborhoodCutsIn(num_vertices(_inst.network));
    for (Node u = 0; u < num_vertices(_inst.network); ++u) {
        IloExpr expr(_model.getEnv());
        auto [ite, end] = out_edges(u, _inst.network);
        expr = std::accumulate(ite, end, IloExpr(_model.getEnv()),
            [&](auto&& _expr, auto&& _edge) {
                return _expr + _x[_inst.network[_edge].id];
            });
        neighborhoodCutsIn[u] =
            IloAdd(_model, IloRange(_model.getEnv(), 1.0, expr, IloInfinity));
        setIloName(neighborhoodCutsIn[u], str(fmter % u));
    }
    return neighborhoodCutsIn;
}

std::vector<IloRange> getLinkCapacityConstraints(
    const Instance& _inst, IloModel& _model, const std::vector<IloNumVar>& _x) {
    std::vector<IloRange> linkCapaCons(num_edges(_inst.network));
    boost::format fmter("linkCapaCons(l:(%1%, %2%))");
    for (auto [ite, end] = edges(_inst.network); ite != end; ++ite) {
        const auto [u, v] = incident(*ite, _inst.network);
        const auto e = _inst.network[*ite].id;
        linkCapaCons[e] =
            IloAdd(_model, IloRange(_model.getEnv(), -IloInfinity,
                               -_inst.network[*ite].capacity * _x[e], 0.0));
        setIloName(linkCapaCons[e], (fmter % u % v).str());
    }
    return linkCapaCons;
}

std::vector<IloRange> getNodeCapacityConstraints(
    const Instance& _inst, IloModel& _model, const std::vector<IloNumVar>& _k) {
    std::vector<IloRange> nodeCapasCons(num_vertices(_inst.network));
    boost::format fmter("nodeCapasCons(%1%)");
    std::generate(
        nodeCapasCons.begin(), nodeCapasCons.end(), [&, u = 0]() mutable {
            auto rng = IloAdd(
                _model, IloRange(_model.getEnv(), -IloInfinity, -_k[u], 0.0));
            u++;
            setIloName(rng, (fmter % u).str());
            return rng;
        });
    return nodeCapasCons;
}

std::vector<SFC::ServicePath> getServicePathsFromILPHeur(
    const std::vector<Demand<>>& _demands, const std::string& _name,
    const double& _factor, const int _funcCoef) {
    const std::string filename = "./results/energy/" + _name + "_ILPHeur_"
                                 + std::to_string(_factor) + "_"
                                 + std::to_string(_funcCoef) + ".res";
    std::cout << "Opening " << filename << '\n';
    std::ifstream ifs(filename);
    std::vector<SFC::ServicePath> sPaths;
    if (!ifs.good()) {
        std::cout << "ifs not good\n";
        return sPaths;
    }
    sPaths.reserve(_demands.size());
    double totalEnergy, linkEnergy, nodeEnergy;
    ifs >> totalEnergy >> linkEnergy >> nodeEnergy;
    std::size_t i = 0;
    do {
        ifs >> i;
        std::cout << i << '\n';
        int pathSize;
        ifs >> pathSize;
        Path path(pathSize);
        for (int k = 0; k < pathSize; ++k) {
            ifs >> path[k];
        }

        std::vector<Node> locations;
        Node loc;
        for (std::size_t j = 0; j < _demands[i].functions.size(); ++j) {
            ifs >> loc;
            locations.push_back(loc);
        }
        sPaths.emplace_back(path, locations, i);
        ++i;
    } while (i < _demands.size() && ifs.good());
    return sPaths;
}

IloExpr getNetworkEnergyConsumption(const Instance& _inst,
    const std::vector<IloNumVar>& _x, const std::vector<IloNumVar>& _k) {
    IloExpr expr(_x.front().getEnv());
    expr += std::accumulate(_x.begin(), _x.end(), IloExpr(_x.front().getEnv()),
        [&](auto&& _expr, auto&& x) {
            return _expr + _inst.energyCons.activeLink * x;
        });
    for (const auto u : boost::make_iterator_range(vertices(_inst.network))) {
        expr += _inst.energyCons.activeCore * _k[u];
    }
    std::cout << expr << '\n';
    return expr;
}

} // namespace SFC::Energy
