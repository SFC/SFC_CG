#include <ArgException.hpp>
#include <MyTimer.hpp>
#include <UnlabeledValueArg.hpp>
#include <ValueArg.hpp>
#include <ValuesConstraint.hpp>
#include <ilconcert/ilosys.h>
#include <iosfwd>
#include <tclap/CmdLine.hpp>

#include "CG_CutsPlus.hpp"
#include "Chaining.hpp"
#include "ColumnGeneration.hpp"
#include "Energy.hpp"
#include "Heuristic.hpp"
#include "PricingProblem.hpp"

struct Param {
    std::string model;
    std::string name;
    int nbThreads;
    double factor;
    double percentDemand;
    int energyCons;
};

Param getParams(int argc, char** argv);
Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with column generation", ' ', "1.0");

    TCLAP::ValuesConstraint<std::string> vCons({
        "path", "pathBF", "cuts+"
        // "checkSolution", , "path2", "path3", "pathGen", "heur", "heur_path",
        // "path_fromILPHeur", "path2_fromILPHeur", "path3_fromILPHeur"
    });
    TCLAP::ValueArg<std::string> modelName("m", "model", "Specify the model used",
        false, "path", &vCons);
    cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> networkName("network",
        "Specify the network name", true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<double> demandFactor("d", "demandFactor",
        "Specify the multiplicative factor for the demands",
        false, 1, "int");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<int> nbThreads(
        "j", "nbThreads", "Specify the number of threads",
        false, 1, "int");
    cmd.add(&nbThreads);

    TCLAP::ValueArg<int> energyCons("e", "energyCons",
        "Specify the index for the energy consumption file",
        false, 1, "int");
    cmd.add(&energyCons);

    TCLAP::ValueArg<double> percentDemand("p", "percentDemand",
        "Specify the percentage of demand used",
        false, 100, "double");
    cmd.add(&percentDemand);

    cmd.parse(argc, argv);

    return {modelName.getValue(), networkName.getValue(), nbThreads.getValue(),
        demandFactor.getValue(), percentDemand.getValue(), energyCons.getValue()};
}

int main(int argc, char** argv) {
    const auto params = getParams(argc, argv);
    std::string folderName = "./instances/nrj/";
    DiGraph network = SFC::loadNetwork(folderName + params.name + "_topo.txt");
    std::cout << "Loaded " << params.name << " with "
              << num_vertices(network) << " vertices and "
              << num_edges(network) << " edges\n";

    std::vector<int> nodeCapas = SFC::loadNodeCapa(folderName + params.name
                                                   + "_nodeCapa.txt");
    SFC::Energy::Consumption energyCons = SFC::Energy::Consumption::fromFile(folderName
                                                                             + "energy" + std::to_string(params.energyCons) + ".txt");
    std::cout << "Files loaded...\n";

    auto [demands, funcChargeName] = SFC::loadDemands(folderName + params.name
                                                          + "_demand.txt",
        params.factor);
    const int nbDemands = static_cast<int>(std::ceil(demands.size()
                                                     * params.percentDemand / 100.0));
    demands.erase(demands.begin() + nbDemands, demands.end());
    const auto& funcCharge = funcChargeName;
    const double totalNbCores = std::accumulate(demands.begin(), demands.end(), 0.0,
        [&](const double t__nbCores, const SFC::Demand<>& t_demand) {
            // Sum of cores needed for the demand
            return t__nbCores + std::accumulate(t_demand.functions.begin(), t_demand.functions.end(), 0.0, [&](const double t_nbCores, const int t_func) {
                return t_nbCores + t_demand.d / funcCharge[t_func];
            });
        });
    std::cout << totalNbCores << " needed cores!\n";

    std::vector<Node> NFVNodes;
    for (Node u = 0; u < num_vertices(network); ++u) {
        if (nodeCapas[u] != -1) {
            NFVNodes.push_back(u);
        }
    }
    int availableCores = 0;
    for (const auto& u : NFVNodes) {
        availableCores += nodeCapas[u];
    }
    std::cout << availableCores << " cores available !\n";

    std::cout << std::accumulate(demands.begin(), demands.end(), 0.0,
                     [&](const double& _bandwidth, const SFC::Demand<>& _demand) {
                         return _bandwidth + _demand.d;
                     })
              << " total bandwidth!\n";

    SFC::Energy::Instance instance(network, nodeCapas, demands, funcCharge, energyCons);

    Time timer;
    timer.start();
    std::string filename = params.percentDemand != 100.0
                               ? "./results/energy/" + params.name + "_"
                                     + std::to_string(nbDemands) + "_" + params.model
                                     + "_" + std::to_string(params.factor) + ".res"
                               : "./results/energy/" + params.name + "_"
                                     + params.model + "_" + std::to_string(params.factor)
                                     + ".res";

    if (params.model == "path") {
        SFC::Energy::Heuristic heur(instance);
        if (heur.solve() == SFC::Energy::Heuristic::State::Solved) {
            std::cout << "Energy used: " << heur.getEnergyUsed() << '\n';
            SFC::Energy::Placement_Path rmp(instance);
            const auto sPaths = heur.getServicePaths();
            rmp.addColumns(sPaths.begin(), sPaths.end());
            if (solve<SFC::Energy::Placement_Path, SFC::ShortestPathPerDemand_LP>(rmp, SFC::ShortestPathPerDemand_LP(instance), params.nbThreads, true, true)) {
                const auto sol = rmp.getSolution();
                if (sol.isValid()) {
                    sol.save(filename, timer.get());
                } else {
                    std::cout << "Solution is not valid\n";
                }
            }
        }

    } else if (params.model == "pathBF") {
        SFC::Energy::Heuristic heur(instance);
        if (heur.solve() == SFC::Energy::Heuristic::State::Solved) {
            std::cout << "Energy used: " << heur.getEnergyUsed() << '\n';
            SFC::Energy::Placement_Path rmp(&instance);
            const auto sPaths = heur.getServicePaths();
            rmp.addColumns(sPaths.begin(), sPaths.end());
            if (solve<SFC::Energy::Placement_Path, SFC::ShortestPathPerDemand>(rmp, SFC::ShortestPathPerDemand(&instance), params.nbThreads, true, true)) {
                const auto sol = rmp.getSolution();
                if (sol.isValid()) {
                    sol.save(filename, timer.get());
                } else {
                    std::cout << "Solution is not valid\n";
                }
            }
        }} else if (params.model == "cuts+") {
        SFC::Energy::Heuristic heur(instance);
        if (heur.solve() == SFC::Energy::Heuristic::State::Solved) {
            std::cout << "Energy used: " << heur.getEnergyUsed() << '\n';
            SFC::Energy::CG_CutsPlus::Master rmp(&instance);
            const auto sPaths = heur.getServicePaths();
            rmp.addColumns(sPaths.begin(), sPaths.end());
            if (solve<SFC::Energy::CG_CutsPlus::Master, SFC::Energy::CG_CutsPlus::PricingProblem>(
                    rmp, SFC::Energy::CG_CutsPlus::PricingProblem(&instance), params.nbThreads)) {
                // cgModel.getSolution().save(filename, timer.get());
            }
        }
    }
    // if (params.model == "heur") {
    //     Energy::Heuristic heur = Energy::solveEESFC(instance);
    //     if (heur.getState() == Energy::Heuristic::State::Solved) {
    //         std::cout << "Energy used: " << heur.getEnergyUsed() << '\n';
    //         assert(heur.checkSolution());
    //         heur.save(filename, timer.show(), network);
    //     } else {
    //         std::cout << "No solution found!\n";
    //         return -1;
    //     }
    // } else if (params.model == "path") {
    //     std::cout << "Getting initial configuration...\n";
    //     Energy::Heuristic heur = Energy::solveEESFC(instance);
    //     if (heur.getState() == Energy::Heuristic::State::Solved) {
    //         std::cout << "Energy used: " << heur.getEnergyUsed() << '\n';
    //         if (!heur.checkSolution()) {
    //             std::cerr << "Solution invalid!\n";
    //             exit(-1);
    //         }
    //         Energy::ChainPlacement css(&instance);
    //         css.addInitConf(heur.getServicePaths());
    //         if (css.solveInteger()) {
    //             css.save(filename, timer.show());
    //         }
    //     } else {
    //         std::cout << "No initial configuration\n";
    //         return -1;
    //     }
    // } else if (params.model == "path_fromILPHeur") {
    //     std::cout << "Getting initial configuration...\n";
    //     Energy::ChainPlacement css(&instance);
    //     auto sPaths = getServicePathsFromILPHeur(demands, params.name, params.factor, params.funcCoef);
    //     instance.checkSolution(sPaths);
    //     css.addInitConf(sPaths);
    //     if (css.solveInteger()) {
    //         css.save(filename, timer.show());
    //     }
    // } else if (params.model == "checkSolution") {
    //     std::cout << "Loading solution...\n";
    //     auto sPaths = getServicePathsFromILPHeur(demands, params.name, params.factor, params.funcCoef);
    //     std::cout << "Checking solution...\n";
    //     instance.checkSolution(sPaths);
    //     instance.save(sPaths, filename, timer.show());
    // } else if (params.model == "path2_fromILPHeur") {
    //     std::cout << "Getting initial configuration...\n";
    //     Energy::ChainPlacementPath2 css(&instance);
    //     auto sPaths = getServicePathsFromILPHeur(demands, params.name, params.factor, params.funcCoef);
    //     instance.checkSolution(sPaths);
    //     css.addInitConf(sPaths);
    //     if (css.solveInteger()) {
    //         css.save(filename, timer.show());
    //     }
    // } else if (params.model == "path3_fromILPHeur") {
    //     std::cout << "Getting initial configuration...\n";
    //     Energy::ChainPlacement3 css(&instance);
    //     auto sPaths = getServicePathsFromILPHeur(demands, params.name, params.factor, params.funcCoef);
    //     instance.checkSolution(sPaths);
    //     css.addInitConf(sPaths);
    //     if (css.solveInteger()) {
    //         css.save(filename, timer.show());
    //     }
    // } else if (params.model == "pathGen") {
    //     std::cout << "Getting initial configuration...\n";
    //     Energy::Heuristic heur = Energy::solveEESFC(instance);
    //     if (heur.getState() == Energy::Heuristic::State::Solved) {
    //         std::cout << "Energy used: " << heur.getEnergyUsed() << '\n';
    //         assert(heur.checkSolution());
    //         Energy::GenCut genCut(instance);
    //         std::vector<double> outDemands(num_vertices(instance.network));
    //         for (const auto& demand : instance.demands) {
    //             outDemands[demand.s] += demand.d;
    //         }

    //         for (Node u = 0; u < num_vertices(instance.network); ++u) {
    //             auto neighbors = instance.network.getNeighbors(u);
    //             std::sort(neighbors.begin(), neighbors.end(),
    //                  [&](const Node& __v1, const Node& __v2) {
    //                      return instance.network.getEdgeWeight(u, __v1)
    //                          > instance.network.getEdgeWeight(u, __v2);
    //             });
    //             std::size_t i = 0;
    //             while (outDemands[u] > 0) {
    //                 outDemands[u] -= instance.network.getEdgeWeight(u, neighbors[i]);
    //                 ++i;
    //             }
    //             Energy::GenCut::Column col;
    //             for (const auto& v : neighbors) {
    //                 int realE = u < v ? instance.edgeID(u, v) : instance.edgeID(v, u);
    //                 col.edgeIDs.push_back(realE);
    //             }
    //             col.minEdge = i;
    //             genCut.addColumn(col);
    //         }
    //         genCut.solve();

    //         Energy::ChainPlacement css(&instance);
    //         for (const auto& col : genCut.getCuts()) {
    //             css.addCut(col);
    //         }
    //         css.addInitConf(heur.getServicePaths());
    //         if (css.solveInteger()) {
    //             css.save(filename, timer.show());
    //         }
    //     }
    // } else if (params.model == "path2") {
    //     std::cout << "Getting initial configuration...\n";
    //     Energy::Heuristic heur = Energy::solveEESFC(instance);
    //     if (heur.getState() == Energy::Heuristic::State::Solved) {
    //         std::cout << "Energy used: " << heur.getEnergyUsed() << '\n';
    //         assert(heur.checkSolution());
    //         Energy::ChainPlacementPath2 css(&instance);
    //         css.addInitConf(heur.getServicePaths());
    //         if (css.solveInteger()) {
    //             css.save(filename, timer.show());
    //         }
    //     } else {
    //         std::cout << "No model specified!\n";
    //         return -1;
    //     }
    // } else if (params.model == "path3") {
    //     std::cout << "Getting initial configuration...\n";
    //     Energy::Heuristic heur = Energy::solveEESFC(instance);
    //     if (heur.getState() == Energy::Heuristic::State::Solved) {
    //         std::cout << "Energy used: " << heur.getEnergyUsed() << '\n';
    //         assert(heur.checkSolution());
    //         Energy::ChainPlacement3 css(&instance);
    //         css.addInitConf(heur.getServicePaths());
    //         if (css.solveInteger()) {
    //             css.save(filename, timer.show());
    //         }
    //     } else {
    //         std::cout << "No initial configuration\n";
    //         return -1;
    //     }
    // } else if (params.model == "heur_path") {
    //     std::cout << "Getting initial configuration...\n";
    //     Energy::Heuristic heur = Energy::solveEESFC(instance);
    //     if (heur.getState() == Energy::Heuristic::State::Solved) {
    //         std::cout << "Energy used: " << heur.getEnergyUsed() << '\n';
    //         assert(heur.checkSolution());
    //         Energy::ChainingPlacementFixedEdge css(instance);
    //         // css.addInitConf(heur.getServicePaths());

    //         if (!css.solveInteger()) {
    //             std::cerr << "No solution found!\n";
    //             exit(-1);
    //         }
    //         // auto edgesToRemove = css.getEdgesByUsage();
    //         double bestSol = css.getObjValue();
    //         for (std::size_t i = 0; i < instance.network.size() / 2; ++i) {
    //             // while(!edgesToRemove.empty()) {
    //             // css is in solved state
    //             auto edge = css.getLessUsedEdge();
    //             // for(const auto& edge : edgesToRemove) {
    //             css.removeEdge(edge);
    //             if (!css.solveInteger()
    //                 || css.getObjValue() > bestSol) {
    //                 std::cout << "Failed to remove " << edge << "\n";
    //                 css.restoreEdge(edge);
    //                 css.solveInteger();
    //             } else {
    //                 bestSol = css.getObjValue();
    //                 std::cout << "Removed " << edge << "\n";
    //                 // edgesToRemove = css.getEdgesByUsage();
    //                 // break;
    //             }
    //             // }
    //         }
    //         css.save(filename, timer.show());                               }
    //     } else {
    //         std::cout << "No initial configuration\n";
    //         return -1;
    //     }
    // } else {
    //     std::cout << "No initial configuration\n";
    //     return -1;
    // }
}
