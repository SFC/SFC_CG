find_package(
    CPLEX
    REQUIRED
    Concert
    IloCplex
    CP)
find_package(OpenMP REQUIRED)

find_package(spdlog CONFIG REQUIRED)
find_package(CppRO CONFIG REQUIRED)
find_package(pybind11 CONFIG REQUIRED)
find_package(lyra CONFIG REQUIRED)
find_package(Boost CONFIG REQUIRED)
find_package(nlohmann_json CONFIG REQUIRED)
find_package(range-v3 CONFIG REQUIRED)
find_package(TBB CONFIG REQUIRED)

add_library(
    SFC
    SFC.cpp
    algorithm.cpp
    models.cpp)
target_include_directories(SFC PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(SFC PUBLIC project_options project_warnings)
target_link_system_libraries(
    SFC
    PUBLIC
    bfg::lyra
    TBB::tbb
    nlohmann_json::nlohmann_json
    range-v3::range-v3
    spdlog::spdlog
    OpenMP::OpenMP_CXX
    CPLEX::CPLEX
    CPLEX::Concert
    CPLEX::IloCplex
    CPLEX::CP
    CppRO::CppRO
    Boost::boost)
option(DISABLE_SFC_ASSERT "Disable assert" OFF)
if (DISABLE_SFC_ASSERT)
    target_compile_definitions(SFC PUBLIC DISABLE_SFC_ASSERT)
endif()


add_executable(Placement main.cpp)
target_link_libraries(
    Placement
    PUBLIC SFC
    )

find_package(pybind11 CONFIG REQUIRED)
pybind11_add_module(_core MODULE python.cpp)
target_link_libraries(_core PRIVATE SFC)
target_link_system_libraries(
    _core
    PUBLIC
    SFC
    TBB::tbb
    )

target_compile_definitions(_core PRIVATE VERSION_INFO=${PROJECT_VERSION})
install(TARGETS _core LIBRARY DESTINATION .)
if(SKBUILD)
    # Scikit-Build does not add your site-packages to the search path
    # Scikit-Build does not add your site-packages to the search path
    # automatically, so we need to add it _or_ the pybind11 specific directory
    # here.
    execute_process(
        COMMAND "${PYTHON_EXECUTABLE}" -c
        "import pybind11; print(pybind11.get_cmake_dir())"
        OUTPUT_VARIABLE _tmp_dir
        OUTPUT_STRIP_TRAILING_WHITESPACE COMMAND_ECHO STDOUT)
    list(APPEND CMAKE_PREFIX_PATH "${_tmp_dir}")
endif()
