#ifndef SFC_CHAININGPATHOCCLIMIT_HPP
#define SFC_CHAININGPATHOCCLIMIT_HPP

#include <variant>

#include "CppRO/cplex_utility.hpp"
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/multi_array.hpp>

#include "CppRO/ColumnGeneration.hpp"
#include "LinearProgramming.hpp"
#include "OccLim.hpp"

namespace SFC::OccLim {

namespace Beta {

struct DualValues {
    DualValues(const Instance& _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;

    IloNum getReducedCost(const CrossLayerLink& _link) const;
    IloNum getReducedCost(const IntraLayerLink& _link) const;
    double getDualSumRHS() const;

    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;
    boost::multi_array<IloNum, 3> m_pathFuncDuals;
    std::vector<IloNum> m_nbLicensesDuals;
    double m_branchesDual;
};

class Master {
    friend DualValues;

  public:
    using Column = ServicePath;

    explicit Master(const Instance& _inst);
    Master& operator=(const Master&) = delete;
    Master& operator=(Master&&) = delete;
    Master(const Master&) = delete;
    Master(Master&&) = delete;
    ~Master() = default;

    double getReducedCost_impl(
        const ServicePath& _col, const DualValues& _dualValues) const;
    IloNumColumn getColumn_impl(const ServicePath& _col);
    void getDuals_impl();
    void removeDummyIfPossible();
    const DualValues& getDualValues_impl() const;
    std::vector<ServicePath> getServicePaths() const;
    template <typename T>
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }
    Solution getSolution() const;
    void setNbLicenses(const int _nbLicenses) {
        for (auto& cons : m_nbLicensesCons) {
            cons.setUB(_nbLicenses);
        }
    }
    int getNbColumns() const;

  private:
    std::vector<std::vector<ColumnPair<ServicePath>>> m_columns;
    DualValues m_duals;
    LinearModel m_model;

    const Instance* m_inst;

    boost::multi_array<IloNumVar, 2> m_b;

    IloObjective m_obj;

    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;
    std::vector<IloRange> m_nbLicensesCons;
    boost::multi_array<IloRange, 2> m_pathFuncCons;

    std::vector<IloNumVar> m_dummyPaths;
    std::vector<double> m_linkCharge;
    std::vector<double> m_nodeCharge;
    boost::multi_array<double, 2> m_funcPath;
};

void barycenter(const DualValues& _nDuals, const DualValues& _bDual,
    DualValues& _sDuals, double _alpha);

} // namespace Beta

namespace Alpha {

struct DualValues {
    DualValues(const Instance& _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;

    IloNum getReducedCost(const CrossLayerLink& _link) const;
    IloNum getReducedCost(const IntraLayerLink& _link) const;
    double getDualSumRHS() const;

    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;
    boost::multi_array<IloNum, 3> m_pathFuncDuals;
    std::vector<IloNum> m_nbLicensesDuals;
    double m_branchesDual;
};

void barycenter(const DualValues& _nDuals, const DualValues& _bDual,
    DualValues& _sDuals, double _alpha);

std::ostream& operator<<(std::ostream& _out, const DualValues& _dualValues);
} // namespace Alpha

} // namespace SFC::OccLim
#endif
