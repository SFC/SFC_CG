#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/multi_array/base.hpp>
#include <boost/pending/property.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <ilconcert/iloexpression.h>
#include <ilconcert/ilolinear.h>
#include <ilconcert/ilosys.h>
#include <iostream>

#include "Decomposition.hpp"
#include "SFC/ChainingPathOccLimit.hpp"
#include "SFC/OccLim.hpp"

namespace SFC {
struct CrossLayerLink;
struct IntraLayerLink;
struct ServicePath;
} // namespace SFC

using ranges = boost::multi_array_types::index_range;

namespace SFC::OccLim::Alpha {} // namespace SFC::OccLim::Alpha

