#include "algorithm.hpp"
#include "SFC.hpp"
#include "models.hpp"
#include <CppRO/ColumnGeneration.hpp>
#include <CppRO/KMedoids.hpp>
#include <CppRO/RowGeneration.hpp>
#include <bits/ranges_util.h>
#include <boost/graph/named_function_params.hpp>
#include <boost/graph/properties.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <fmt/core.h>
#include <limits>
#include <numeric>
#include <oneapi/tbb/blocked_range.h>
#include <oneapi/tbb/enumerable_thread_specific.h>
#include <oneapi/tbb/global_control.h>
#include <oneapi/tbb/parallel_reduce.h>
#include <optional>
#include <range/v3/view/zip.hpp>
#include <spdlog/spdlog.h>
#include <type_traits>

namespace SFC {

std::optional<Solution> twoPhaseHeuristic(const Instance& _inst,
    std::size_t _nbLicenses, std::mt19937& _gen, std::size_t _nbThreads) {
    std::optional<SFC::Solution> bestSol;
    const auto ascendingFunctions = getFunctionsAscendingCores(_inst);
    const auto descendingFunctions = getFunctionsDescendingCores(_inst);
    for (const auto& locations : {SFC::getFunctionPlacementMedoids(_inst,
                                      _nbLicenses, ascendingFunctions, _gen),
             SFC::getFunctionPlacementMedoids(
                 _inst, _nbLicenses, descendingFunctions, _gen),
             SFC::getFunctionPlacementPerFunc(
                 _inst, _nbLicenses, ascendingFunctions),
             SFC::getFunctionPlacementPerFunc(
                 _inst, _nbLicenses, descendingFunctions)}) {
        if (!locations.has_value()) {
            spdlog::info("Failed to find warm start");
            return std::nullopt;
        }
        spdlog::info("Running FixedLocation CGBnB");
        auto sol =
            SFC::columnGenerationBranchAndBound(_inst, *locations, _nbThreads);
        if (sol.has_value()
            && (!bestSol.has_value()
                || epsilon_greater<double>()(
                    bestSol->objValue, sol->objValue))) {
            bestSol = std::move(sol);
        }
    }
    return bestSol;
}

std::optional<Solution> columnGenerationBranchAndBound(const Instance& _inst,
    std::size_t _nbThreads, double _timeLimit, CGBnBVisitor& _visitor) {
    spdlog::info("Running CGBnB without licence constraints");
    const auto dummyRange =
        _inst.demands
        | boost::adaptors::transformed([&](const Demand& _demand) {
              return 2 * _demand.bandwidth
                     * static_cast<double>(num_vertices(_inst.network));
          });
    IloEnv env;
    const OnDestroy onDestroy([&] { env.end(); });
    PathExtendedModel rmp(env, _inst.demands.size(), dummyRange,
        _inst.getBandwidthRange(), _inst.getCpuRange(), _inst.getRAMRange(),
        _inst.getStorageRange());

    IloCplex solver(rmp.getModel());
    solver.setParam(IloCplex::Param::TimeLimit, _timeLimit);
    solver.setParam(IloCplex::Param::Threads, 1);

    auto solveFunc = [&](const auto& _cols) {
        if constexpr (!std::is_same<std::decay_t<decltype(_cols)>,
                          std::nullopt_t>::value) {
            for (const auto& col : _cols) {
                rmp.addColumn(col);
            }
        }
        auto retval = CppRO::ColumnGeneration::solve<PathExtendedModel,
            PathExtendedModel::DualValues>(rmp, solver);

#ifndef NDEBUG
        const auto duals = rmp.getDualValues(solver);
        for (const auto& cols : rmp.getColumns()) {
            for (const auto& col : cols) {
                const auto myRC = rmp.getReducedCost(col.obj, duals);
                const auto cplexRC = solver.getReducedCost(col.var);
                SFC_ASSERT(epsilon_equal<double>()(myRC, cplexRC));
            }
        }
#endif
        return retval;
    };

    oneapi::tbb::enumerable_thread_specific<
        Pricing<ShortestPathPerDemand, Instance>>
        simplePricings(std::cref(_inst));
    oneapi::tbb::enumerable_thread_specific<
        Pricing<DelayConstrainedShortestPathPerDemand, Instance>>
        delayPricings(std::cref(_inst));

    oneapi::tbb::global_control gc(
        oneapi::tbb::global_control::max_allowed_parallelism, _nbThreads);

    auto genColFunc = [&](const auto& _dualValues)
        -> std::optional<std::vector<SFC::PathExtendedModel::ColumnType>> {
        const auto range =
            oneapi::tbb::blocked_range<std::vector<Demand>::const_iterator>(
                _inst.demands.cbegin(), _inst.demands.cend());
        const std::vector<PathExtendedModel::ColumnType> columns =
            oneapi::tbb::parallel_deterministic_reduce(
                range, std::vector<PathExtendedModel::ColumnType>{},
                [&](const auto& _demands,
                    std::vector<PathExtendedModel::ColumnType> _cols) {
                    for (const auto& demand : _demands) {
                        const auto sPath =
                            demand.maxDelay
                                    == std::numeric_limits<double>::infinity()
                                ? simplePricings.local()(demand, _dualValues)
                                : delayPricings.local()(demand, _dualValues);
                        auto col = PathExtendedModel::ColumnType{
                            std::move(sPath), _inst, demand};
                        if (rmp.isImprovingColumn(col, _dualValues)) {
                            _cols.emplace_back(std::move(col));
                        }
                    }
                    return _cols;
                },
                [](auto _c1, auto _c2) {
                    _c1.insert(_c1.end(), _c2.begin(), _c2.end());
                    return _c1;
                });
        if (columns.empty()) {
            return std::nullopt;
        }
        return columns;
    };
    _visitor(CGBnBVisitor::AlgorithmStarted{});
    try {
        CppRO::ColumnGeneration::solve(
            std::nullopt, solveFunc, genColFunc, _visitor.cgVisitor);
    } catch (IloException& e) {
        std::cout << e.getMessage() << '\n';
    }
    _visitor(solver, CGBnBVisitor::CGConverged{});
    // Set model to integer
    solver.extract(rmp.getIntegerModel());
    const auto state = solver.solve();
    _visitor(CGBnBVisitor::BnBConverged{});
    solver.exportModel("RMP_ILP.lp");
    if (state == IloFalse) {
        return std::nullopt;
    }
    const auto getUsedPath =
        [&](const auto& _cols) -> std::optional<SFC::ServicePath> {
        for (const auto& col : _cols) {
            if (epsilon_equal<double>()(solver.getValue(col.var), 1.0)) {
                return col.obj.path;
            }
        }
        return std::nullopt;
    };

    std::vector<SFC::ServicePath> paths;
    for (const auto& cols : rmp.getColumns()) {
        if (auto path = getUsedPath(cols); path) {
            paths.emplace_back(*path);
        } else {
            return std::nullopt;
        }
    }
    return Solution{solver.getObjValue(), std::move(paths)};
}

struct FixedPricing {
    FixedPricing(
        const Instance& _inst, const boost::multi_array<bool, 2>& _locations)
        : inst(_inst)
        , locations(_locations)
        , algo(_inst) {}

    const Instance& inst;
    const boost::multi_array<bool, 2>& locations;
    ShortestPathPerDemand algo;

    auto operator()(const auto& _demand, const auto& _duals) {
        return algo(inst, locations, _demand, _duals);
    }
};

std::optional<Solution> columnGenerationBranchAndBound(const Instance& _inst,
    const boost::multi_array<bool, 2>& _fixedLocations,
    std::size_t _nbThreads) {
    spdlog::info("Running CGBnB with fixed functions locations");
    const auto dummyRange =
        _inst.demands
        | boost::adaptors::transformed([&](const Demand& _demand) {
              return 2 * _demand.bandwidth
                     * static_cast<double>(num_vertices(_inst.network));
          });
    IloEnv env;
    const OnDestroy onDestroy([&] { env.end(); });
    PathExtendedModel rmp(env, _inst.demands.size(), dummyRange,
        _inst.getBandwidthRange(), _inst.getCpuRange(), _inst.getRAMRange(),
        _inst.getStorageRange());

    IloCplex solver(rmp.getModel());
    auto solveFunc = [&](const PathExtendedModel& _rmp) {
        auto retval = CppRO::ColumnGeneration::solve<PathExtendedModel,
            PathExtendedModel::DualValues>(_rmp, solver);

#ifndef NDEBUG
        const auto duals = _rmp.getDualValues(solver);
        for (const auto& cols : _rmp.getColumns()) {
            for (const auto& col : cols) {
                const auto myRC = _rmp.getReducedCost(col.obj, duals);
                const auto cplexRC = solver.getReducedCost(col.var);
                SFC_ASSERT(epsilon_equal<double>()(myRC, cplexRC));
            }
        }
#endif
        return retval;
    };

    oneapi::tbb::enumerable_thread_specific<FixedPricing> pricings(
        std::cref(_inst), _fixedLocations);

    oneapi::tbb::global_control gc(
        oneapi::tbb::global_control::max_allowed_parallelism, _nbThreads);

    auto genColFunc = [&](auto& _rmp, const auto& _dualValues) {
        const auto range =
            oneapi::tbb::blocked_range<std::vector<Demand>::const_iterator>(
                _inst.demands.cbegin(), _inst.demands.cend());
        const std::vector<PathExtendedModel::ColumnType> columns =
            oneapi::tbb::parallel_deterministic_reduce(
                range, std::vector<PathExtendedModel::ColumnType>{},
                [&](const auto& _demands,
                    std::vector<PathExtendedModel::ColumnType> _cols) {
                    for (const auto& demand : _demands) {
                        auto col = PathExtendedModel::ColumnType{
                            pricings.local()(demand, _dualValues), _inst,
                            demand};
                        if (_rmp.isImprovingColumn(col, _dualValues)) {
                            _cols.emplace_back(std::move(col));
                        }
                    }
                    return _cols;
                },
                [](auto _c1, auto _c2) {
                    _c1.insert(_c1.end(), _c2.begin(), _c2.end());
                    return _c1;
                });

        for (const auto& col : columns) {
            _rmp.addColumn(col);
        }
        return columns.size();
    };

    try {
        CppRO::ColumnGeneration::solve(rmp, solveFunc, genColFunc, true);
    } catch (IloException& e) {
        std::cout << e.getMessage() << '\n';
    }
    // Set model to integer
    solver.extract(rmp.getIntegerModel());
    solver.exportModel("RMP_ILP.lp");
    if (solver.solve() == IloFalse) {
        return std::nullopt;
    }
    const auto getUsedPath =
        [&](const auto& _cols) -> std::optional<SFC::ServicePath> {
        for (const auto& col : _cols) {
            if (epsilon_equal<double>()(solver.getValue(col.var), 1.0)) {
                return col.obj.path;
            }
        }
        return std::nullopt;
    };

    std::vector<SFC::ServicePath> paths;
    for (const auto& cols : rmp.getColumns()) {
        if (auto path = getUsedPath(cols); path) {
            paths.emplace_back(*path);
        } else {
            return std::nullopt;
        }
    }
    return Solution{solver.getObjValue(), std::move(paths)};
}

ShortestPathPerDemand::ShortestPathPerDemand(const Instance& _inst)
    : m_layeredGraph(_inst.network, _inst.maxChainSize + 1)
    , m_weights(num_edges(m_layeredGraph.getGraph()))
    , m_distance(num_vertices(m_layeredGraph.getGraph()))
    , m_predecessors(num_vertices(m_layeredGraph.getGraph())) {}

void ShortestPathPerDemand::setPricingID(const std::size_t _demandID) {
    m_demandID = _demandID;
}

void ShortestPathPerDemand::solve(const Demand& _demand) {
    const auto dest =
        m_layeredGraph.getVertexDescriptor(_demand.functions.size(), _demand.t);
    m_djikstra_visitor.dest = dest;
    const auto vis = VisitorLambda(
        [&](const auto& _ed, const auto& _g) {
            m_predecessors[target(_ed, _g)] = _ed;
        },
        boost::on_edge_relaxed{});
    const auto& vertexIndexMap =
        get(boost::vertex_index, m_layeredGraph.getGraph());
    try {
        dijkstra_shortest_paths(m_layeredGraph.getGraph(), _demand.s,
            boost::dummy_property_map{},
            boost::iterator_property_map(m_distance.begin(), vertexIndexMap),
            boost::iterator_property_map(
                m_weights.begin(), get(&LayeredGraph<Network>::Arc::id,
                                       m_layeredGraph.getGraph())),
            vertexIndexMap, epsilon_less<double>(), std::plus<>(),
            std::numeric_limits<double>::infinity(), 0.0,
            boost::make_dijkstra_visitor(
                std::make_pair(vis, m_djikstra_visitor)));
    } catch (const std::exception&) {
        // Found dest
    }
    m_path = getPathFromPredecessors(_demand.s, dest, m_layeredGraph.getGraph(),
        boost::iterator_property_map(m_predecessors.begin(), vertexIndexMap));
    SFC_ASSERT(!m_path.empty());
}

ServicePath ShortestPathPerDemand::getColumn() const {
    return SFC::getServicePath(m_path, m_layeredGraph, m_demandID);
}

std::optional<Solution> routingColumnGenerationBranchAndBound(
    const Instance& _inst, std::size_t _nbThreads) {
    IloEnv env;
    const OnDestroy onDestroy([&] { env.end(); });
    const auto dummyRange =
        _inst.demands
        | boost::adaptors::transformed([&](const Demand& _demand) {
              return 2 * _demand.bandwidth
                     * static_cast<double>(num_vertices(_inst.network));
          });
    RoutingExtendedModel rmp(env, _inst.demands.size(), dummyRange,
        _inst.getBandwidthRange(), _inst.getCpuRange(), _inst.getRAMRange(),
        _inst.getStorageRange());

    IloCplex solver(rmp.getModel());
    auto solveFunc = [&](const auto& _col) {
        if constexpr (!std::is_same<std::decay_t<decltype(_col)>,
                          std::nullopt_t>::value) {
            rmp.addColumn(_col);
        }
        auto retval = CppRO::ColumnGeneration::solve<RoutingExtendedModel,
            RoutingExtendedModel::DualValues>(rmp, solver);
        spdlog::info("Obj,. value: {}", solver.getObjValue());

#ifndef NDEBUG
        const auto duals = rmp.getDualValues(solver);
        for (const auto& col : rmp.getColumns()) {
            const auto myRC = rmp.getReducedCost(col.obj, duals);
            const auto cplexRC = solver.getReducedCost(col.var);
            SFC_ASSERT(epsilon_equal<double>()(myRC, cplexRC));
        }
#endif
        return retval;
    };

    oneapi::tbb::enumerable_thread_specific<
        Pricing<ShortestPathPerDemand, Instance>>
        pricings(std::cref(_inst));

    const oneapi::tbb::global_control gc(
        oneapi::tbb::global_control::max_allowed_parallelism, _nbThreads);

    auto genColFunc = [&](const auto& _dualValues)
        -> std::optional<RoutingExtendedModel::ColumnType> {
        const auto range =
            oneapi::tbb::blocked_range<std::vector<Demand>::const_iterator>(
                _inst.demands.cbegin(), _inst.demands.cend());
        const std::vector<SFC::ServicePath> paths =
            oneapi::tbb::parallel_deterministic_reduce(
                range, std::vector<SFC::ServicePath>{},
                [&](const auto& _demands, std::vector<SFC::ServicePath> _col) {
                    for (const auto& demand : _demands) {
                        auto col = pricings.local()(demand, _dualValues);
                        if (rmp.isImprovingColumn(
                                {col, _inst, demand}, _dualValues)) {
                            _col.emplace_back(col);
                        }
                    }
                    return _col;
                },
                [](auto _c1, auto _c2) {
                    _c1.insert(_c1.end(), _c2.begin(), _c2.end());
                    return _c1;
                });
        if (!paths.empty()) {
            SFC_ASSERT(rmp.isImprovingColumn({paths, _inst}, _dualValues));
            return RoutingExtendedModel::ColumnType{paths, _inst};
        }
        return std::nullopt;
    };

    CppRO::ColumnGeneration::Visitor::TimeLogger cgVisitor;

    try {
        CppRO::ColumnGeneration::solve(
            std::nullopt, solveFunc, genColFunc, cgVisitor);
    } catch (IloException& e) {
        std::cout << e.getMessage() << '\n';
    }
    // Set model to integer
    PathExtendedModel pathRMP(env, _inst.demands.size(), dummyRange,
        _inst.getBandwidthRange(), _inst.getCpuRange(), _inst.getRAMRange(),
        _inst.getStorageRange());
    for (const auto& col : rmp.getColumns()) {
        for (const auto& path : col.obj.paths) {
            pathRMP.addColumn({path, _inst, _inst.demands[path.demand]});
        }
    }
    solver.extract(pathRMP.getIntegerModel());
    solver.exportModel("RMP_ILP.lp");
    if (solver.solve() == IloFalse) {
        return std::nullopt;
    }
    const auto getUsedPath =
        [&](const auto& _cols) -> std::optional<SFC::ServicePath> {
        for (const auto& col : _cols) {
            if (epsilon_equal<double>()(solver.getValue(col.var), 1.0)) {
                return col.obj.path;
            }
        }
        return std::nullopt;
    };

    std::vector<SFC::ServicePath> paths;
    for (const auto& cols : pathRMP.getColumns()) {
        if (auto path = getUsedPath(cols); path) {
            paths.emplace_back(*path);
        } else {
            return std::nullopt;
        }
    }
    return Solution{solver.getObjValue(), paths};
}

DelayConstrainedShortestPathPerDemand::DelayConstrainedShortestPathPerDemand(
    const Instance& _inst)
    : m_layeredGraph(_inst.network, _inst.maxChainSize + 1)
    , m_model([&] {
        IloModel retval(m_env);
        retval.add(m_cspModel.getModel());
        retval.add(m_obj);
        return retval;
    }())
    , m_solver(m_model)

{
    std::vector<double> delays(num_edges(m_layeredGraph.getGraph()));
    // Link between two layers
    for (const auto ed :
        boost::make_iterator_range(edges(m_layeredGraph.getGraph()))) {
        std::visit(
            [&](auto&& _val) {
                using T = std::decay_t<decltype(_val)>;
                if constexpr (std::is_same_v<Network::vertex_descriptor, T>) {
                    delays[m_layeredGraph.getGraph()[ed].id] = 0.0;
                }
                if constexpr (std::is_same_v<Network::edge_descriptor, T>) {
                    delays[m_layeredGraph.getGraph()[ed].id] =
                        _inst.linkDelay[_inst.network[_val].id];
                }
            },
            m_layeredGraph.getGraph()[ed].originalDescriptor);
    }
    m_cspModel.setDelays(delays);
}

void DelayConstrainedShortestPathPerDemand::setPricingID(
    const std::size_t _demandID) {
    m_demandID = _demandID;
}

void DelayConstrainedShortestPathPerDemand::solve(const Demand& _demand) {
    const auto dest =
        m_layeredGraph.getVertexDescriptor(_demand.functions.size(), _demand.t);
    m_cspModel.setNodePair(_demand.s, dest);
    m_cspModel.setMaxDelay(_demand.maxDelay);
    const auto isActivated = [&](const auto _ed) {
        return m_solver.getValue(
            m_cspModel.getFlowVars()[m_layeredGraph.getGraph()[_ed].id]);
    };

    // Build path from CPLEX solution
    m_path.clear();
    auto ed = *std::find_if(
        out_edges(_demand.s, m_layeredGraph.getGraph()).first,
        out_edges(_demand.s, m_layeredGraph.getGraph()).second, isActivated);
    while (target(ed, m_layeredGraph.getGraph()) != dest) {
        m_path.emplace_back(ed);
        ed = *std::find_if(out_edges(target(ed, m_layeredGraph.getGraph()),
                               m_layeredGraph.getGraph())
                               .first,
            out_edges(target(ed, m_layeredGraph.getGraph()),
                m_layeredGraph.getGraph())
                .second,
            isActivated);
    }
}

ServicePath DelayConstrainedShortestPathPerDemand::getColumn() const {
    return SFC::getServicePath(m_path, m_layeredGraph, m_demandID);
}

std::optional<Solution> columnGenerationBranchAndBound(const Instance& _inst,
    std::size_t _nbLicenses, const std::optional<Solution>& _warmStart,
    std::size_t _nbThreads, double _timeLimit, CGBnBVisitor& _visitor) {
    spdlog::info("Running CGBnB with functions licences constraints");
    const auto dummyRange =
        _inst.demands
        | boost::adaptors::transformed([&](const Demand& _demand) {
              return 2 * _demand.bandwidth
                     * static_cast<double>(num_vertices(_inst.network));
          });
    const auto functionsRange =
        _inst.demands
        | boost::adaptors::transformed(
            [&](const SFC::Demand& _demand) { return _demand.functions; });
    const std::size_t nbFunctions = [&] {
        std::set<std::size_t> functions;
        for (const auto& demand : _inst.demands) {
            for (const auto function : demand.functions) {
                functions.insert(function);
            }
        }
        return functions.size();
    }();
    IloEnv env;
    OnDestroy onDestroy([&] { env.end(); });

    PathExtendedAlphaModel rmp(env, _inst.demands.size(), _nbLicenses,
        nbFunctions, functionsRange, dummyRange, _inst.getBandwidthRange(),
        _inst.getCpuRange(), _inst.getRAMRange(), _inst.getStorageRange());
    spdlog::info("Created model");
    IloCplex solver(rmp.getModel());
    solver.setParam(IloCplex::Param::TimeLimit, _timeLimit);
    solver.setParam(IloCplex::Param::Threads, 1);

    auto solveRMP = [&](const auto& _cols) {
        for (const auto& col : _cols) {
            rmp.addColumn(col);
        }
        spdlog::info("Added {} columns. Currently: {} columns, {} rows",
            _cols.size(), solver.getNcols(), solver.getNrows());
        auto retval = CppRO::ColumnGeneration::solve<PathExtendedAlphaModel,
            PathExtendedAlphaModel::DualValues>(rmp, solver);
#ifndef NDEBUG
        const auto duals = rmp.getDualValues(solver);
        for (const auto& cols : rmp.getColumns()) {
            for (const auto& col : cols) {
                const auto myRC = rmp.getReducedCost(col.obj, duals);
                const auto cplexRC = solver.getReducedCost(col.var);
                SFC_ASSERT(epsilon_equal<double>()(myRC, cplexRC));
            }
        }
#endif
        return retval;
    };
    oneapi::tbb::enumerable_thread_specific<
        Pricing<ShortestPathPerDemand, Instance>>
        pricings(_inst);
    oneapi::tbb::global_control gc(
        oneapi::tbb::global_control::max_allowed_parallelism, _nbThreads);
    auto genColFunc = [&](const auto& _dualValues)
        -> std::optional<std::vector<PathExtendedModel::ColumnType>> {
        const auto range =
            oneapi::tbb::blocked_range<std::vector<Demand>::const_iterator>(
                _inst.demands.cbegin(), _inst.demands.cend());
        const std::vector<PathExtendedModel::ColumnType> columns =
            oneapi::tbb::parallel_deterministic_reduce(
                range, std::vector<PathExtendedModel::ColumnType>{},
                [&](const auto& _demands,
                    std::vector<PathExtendedModel::ColumnType> _cols) {
                    for (const auto& demand : _demands) {
                        auto col = PathExtendedModel::ColumnType{
                            pricings.local()(demand, _dualValues), _inst,
                            demand};
                        if (rmp.isImprovingColumn(col, _dualValues)) {
                            _cols.emplace_back(std::move(col));
                        }
                    }
                    return _cols;
                },
                [](auto _c1, auto _c2) {
                    _c1.insert(_c1.end(), _c2.begin(), _c2.end());
                    return _c1;
                });
        if (columns.empty()) {
            return std::nullopt;
        }
        return columns;
    };
    _visitor(CGBnBVisitor::AlgorithmStarted{});
    CppRO::ColumnGeneration::Visitor::TimeLogger cgVisitor;
    try {
        if (_warmStart) {
            CppRO::ColumnGeneration::solve(
                ranges::views::transform(_warmStart->getServicePaths(),
                    _inst.demands,
                    [&](const auto& _sPath,
                        const auto& _demand) -> PathExtendedModel::ColumnType {
                        return {_sPath, _inst, _demand};
                    }),
                solveRMP, genColFunc, _visitor.cgVisitor);
        } else {
            CppRO::ColumnGeneration::solve(
                std::vector<PathExtendedModel::ColumnType>{}, solveRMP,
                genColFunc, _visitor.cgVisitor);
        }
    } catch (IloException& e) {
        std::cout << e.getMessage() << '\n';
    }
    _visitor(solver, CGBnBVisitor::CGConverged{});
    // Set model to integer
    solver.extract(rmp.getIntegerModel());
    if (_warmStart) {
        IloNumVarArray vars(env);
        IloNumArray values(env);
        for (const auto& cols : rmp.getColumns()) {
            vars.add(cols[0].var);
            values.add(1.0);
        }
        solver.addMIPStart(vars, values);
    }
    // solver.setParam(IloCplex::Param::RootAlgorithm, CPX_ALG_AUTOMATIC);
    const auto state = solver.solve();
    _visitor(CGBnBVisitor::BnBConverged{});
    solver.exportModel("RMP_ILP.lp");
    if (state == IloFalse) {
        return std::nullopt;
    }
    const auto getUsedPath =
        [&](const auto& _cols) -> std::optional<SFC::ServicePath> {
        for (const auto& col : _cols) {
            if (epsilon_equal<double>()(solver.getValue(col.var), 1.0)) {
                return col.obj.path;
            }
        }
        return std::nullopt;
    };

    std::vector<SFC::ServicePath> paths;
    for (const auto& cols : rmp.getColumns()) {
        if (auto path = getUsedPath(cols); path) {
            paths.emplace_back(*path);
        } else {
            return std::nullopt;
        }
    }
    return Solution{solver.getObjValue(), std::move(paths)};
}

std::optional<LazyPathExtendedAlphaModel::PathFunctionLinking>
LicenceExceededChecker::operator()(
    const LazyPathExtendedAlphaModel::PathFunctionLinking& _linking,
    const LazyPathExtendedAlphaModel::RawSolution& _sol) const {
    if (auto ite = rmp.getPathFuncConstraints().find(_linking);
        ite != rmp.getPathFuncConstraints().end()) {
        return std::nullopt;
    }
    for (const auto& [col, val] :
        ranges::views::zip(rmp.getColumns()[_linking.demandId],
            _sol.pathValues[_linking.demandId])) {
        const auto& sPath = col.obj.path;
        if (sPath.locations[_linking.j] == _linking.u
            && epsilon_greater<double>()(val,
                _sol.locationsValues[long(_linking.u)][long(_linking.f)])) {
            return _linking;
        }
    }
    return std::nullopt;
}

std::optional<Solution> columnAndRowGenerationBranchAndBound(
    const Instance& _inst, std::size_t _nbLicenses,
    const std::optional<Solution>& _warmStart, std::size_t _nbThreads,
    double _timeLimit, CRGBnBVisitor& _visitor) {
    oneapi::tbb::global_control gc(
        oneapi::tbb::global_control::max_allowed_parallelism, _nbThreads);
    const auto dummyRange =
        _inst.demands
        | boost::adaptors::transformed([&](const Demand& _demand) {
              return 2 * _demand.bandwidth
                     * static_cast<double>(num_vertices(_inst.network));
          });
    const auto functionsRange =
        _inst.demands
        | boost::adaptors::transformed(
            [&](const SFC::Demand& _demand) { return _demand.functions; });
    const std::size_t nbFunctions = [&] {
        std::set<std::size_t> functions;
        for (const auto& demand : _inst.demands) {
            for (const auto function : demand.functions) {
                functions.insert(function);
            }
        }
        return functions.size();
    }();
    IloEnv env;
    OnDestroy onDestroy([&] { env.end(); });
    LazyPathExtendedAlphaModel rmp(env, _inst.demands.size(), _nbLicenses,
        nbFunctions, dummyRange, _inst.linkCapacity, _inst.cpuCapacity,
        _inst.ramCapacity, _inst.storageCapacity);
    spdlog::info("Created model");
    // TODO: Find cleaner way to express all triplets
    std::vector<LazyPathExtendedAlphaModel::PathFunctionLinking>
        allPathFunctionLinkings;
    for (const auto& demand : _inst.demands) {
        for (const auto u :
            boost::make_iterator_range(vertices(_inst.network))) {
            for (std::size_t j = 0; j < demand.functions.size(); ++j) {
                allPathFunctionLinkings.emplace_back(
                    LazyPathExtendedAlphaModel::PathFunctionLinking{
                        demand.id, u, j, demand.functions[j]});
            }
        }
    }

    double bound = -std::numeric_limits<double>::infinity();
    IloCplex solver(rmp.getModel());
    solver.setParam(IloCplex::Param::Threads, 1);
    auto solveRMP = [&](const auto& _cols) {
        for (const auto& col : _cols) {
            rmp.addColumn(col);
        }
        spdlog::info("Added {} columns. Currently: {} columns, {} rows",
            _cols.size(), solver.getNcols(), solver.getNrows());
        auto retval = CppRO::ColumnGeneration::solve<LazyPathExtendedAlphaModel,
            LazyPathExtendedAlphaModel::DualValues>(rmp, solver);
        bound = solver.getObjValue();
        spdlog::info("Current objective value: {}", bound);
#ifndef NDEBUG
        const auto duals = rmp.getDualValues(solver);
        for (const auto& cols : rmp.getColumns()) {
            for (const auto& col : cols) {
                const auto myRC = rmp.getReducedCost(col.obj, duals);
                const auto cplexRC = solver.getReducedCost(col.var);
                SFC_ASSERT(epsilon_equal<double>()(myRC, cplexRC));
            }
        }
#endif
        return retval;
    };

    oneapi::tbb::enumerable_thread_specific<
        Pricing<ShortestPathPerDemand, Instance>>
        pricings(std::cref(_inst));

    auto genColFunc = [&](const auto& _dualValues)
        -> std::optional<std::vector<PathExtendedModel::ColumnType>> {
        const auto range =
            oneapi::tbb::blocked_range<std::vector<Demand>::const_iterator>(
                _inst.demands.cbegin(), _inst.demands.cend());
        const std::vector<PathExtendedModel::ColumnType> columns =
            oneapi::tbb::parallel_deterministic_reduce(
                range, std::vector<PathExtendedModel::ColumnType>{},
                [&](const auto& _demands,
                    std::vector<PathExtendedModel::ColumnType> _cols) {
                    for (const auto& demand : _demands) {
                        auto col = PathExtendedModel::ColumnType{
                            pricings.local()(demand, _dualValues), _inst,
                            demand};
                        if (rmp.isImprovingColumn(col, _dualValues)) {
                            _cols.emplace_back(std::move(col));
                        }
                    }
                    return _cols;
                },
                [](auto _c1, auto _c2) {
                    _c1.insert(_c1.end(), _c2.begin(), _c2.end());
                    return _c1;
                });
        if (columns.empty()) {
            return std::nullopt;
        }
        return columns;
    };

    oneapi::tbb::enumerable_thread_specific<LicenceExceededChecker> separator(
        [&]() { return LicenceExceededChecker(rmp); });
    auto generateRowsFunction =
        [&](const LazyPathExtendedAlphaModel::RawSolution& _sol)
        -> std::optional<
            std::vector<LazyPathExtendedAlphaModel::PathFunctionLinking>> {
        const auto range = oneapi::tbb::blocked_range<std::vector<
            LazyPathExtendedAlphaModel::PathFunctionLinking>::const_iterator>(
            allPathFunctionLinkings.cbegin(), allPathFunctionLinkings.cend());

        const auto rows = oneapi::tbb::parallel_deterministic_reduce(
            range,
            std::vector<LazyPathExtendedAlphaModel::PathFunctionLinking>{},
            [&](const auto& _linkings, auto _rows) {
                for (const auto& linking : _linkings) {
                    if (auto row = separator.local()(linking, _sol); row) {
                        _rows.emplace_back(*row);
                    }
                }
                return _rows;
            },
            [](auto _c1, auto _c2) {
                _c1.insert(_c1.end(), _c2.begin(), _c2.end());
                return _c1;
            });

        spdlog::info("Found {} rows", rows.size());
        if (rows.empty()) {
            return std::nullopt;
        }
        return rows;
    };
    // Given a set of rows, add them to the RMP and call the solver
    auto solveForRG = [&](const auto& _rows)
        -> std::optional<LazyPathExtendedAlphaModel::RawSolution> {
        spdlog::info("Added {} rows. Currently: {} columns, {} rows",
            _rows.size(), solver.getNcols(), solver.getNrows());
        for (const auto& row : _rows) {
            rmp.enablePathFunctionConstraint(row);
        }
        if (solver.solve() == IloFalse) {
            return std::nullopt;
        }
        bound = solver.getObjValue();
        spdlog::info("Current objective value: {}", bound);
        return rmp.getRawSolution(solver);
    };
    std::size_t nbColumns = rmp.getNbColumns();
    std::size_t nbRows = rmp.getPathFuncConstraints().size();
    _visitor(CRGBnBVisitor::AlgorithmStarted{});
    CppRO::ColumnGeneration::Visitor::TimeLogger cgVisitor;
    CppRO::RowGeneration::Visitor::TimeLogger rgVistor;
    try {
        while (true) {
            if (_warmStart) {
                CppRO::ColumnGeneration::solve(
                    ranges::views::transform(_warmStart->getServicePaths(),
                        _inst.demands,
                        [&](const auto& _sPath, const auto& _demand)
                            -> PathExtendedModel::ColumnType {
                            return {_sPath, _inst, _demand};
                        }),
                    solveRMP, genColFunc, cgVisitor);
            } else {
                CppRO::ColumnGeneration::solve(
                    std::vector<PathExtendedModel::ColumnType>{}, solveRMP,
                    genColFunc, cgVisitor);
            }
            CppRO::RowGeneration::solve(
                std::vector<LazyPathExtendedAlphaModel::PathFunctionLinking>{},
                solveForRG, generateRowsFunction, rgVistor);
            if (nbColumns == rmp.getNbColumns()
                && nbRows == rmp.getPathFuncConstraints().size()) {
                break;
            }
            nbColumns = rmp.getNbColumns();
            nbRows = rmp.getPathFuncConstraints().size();
        }
    } catch (IloException& e) {
        std::cout << e.getMessage() << '\n';
    }
    _visitor(bound, CRGBnBVisitor::CRGConverged{});
    // Set model to integer
    solver.extract(rmp.getIntegerModel());
    solver.addLazyConstraints(rmp.getLazyConstraints(allPathFunctionLinkings));
    solver.setParam(IloCplex::Param::TimeLimit, _timeLimit);
    if (_warmStart) {
        IloNumVarArray vars(env);
        IloNumArray values(env);
        for (const auto& cols : rmp.getColumns()) {
            vars.add(cols[0].var);
            values.add(1.0);
        }
        solver.addMIPStart(vars, values);
    }
    const auto state = solver.solve();
    _visitor(CRGBnBVisitor::BnBConverged{});
    solver.exportModel("RMP_ILP.lp");
    if (state == IloFalse) {
        return std::nullopt;
    }
    const auto getUsedPath =
        [&](const auto& _cols) -> std::optional<SFC::ServicePath> {
        for (const auto& col : _cols) {
            if (epsilon_equal<double>()(solver.getValue(col.var), 1.0)) {
                return col.obj.path;
            }
        }
        return std::nullopt;
    };

    std::vector<SFC::ServicePath> paths;
    for (const auto& cols : rmp.getColumns()) {
        if (auto path = getUsedPath(cols); path) {
            paths.emplace_back(*path);
        } else {
            return std::nullopt;
        }
    }
    _visitor.columnGenerationTime = cgVisitor.generationDuration;
    _visitor.columnMasterSolvingTime = cgVisitor.masterSolvingDuration;
    _visitor.rowGenerationTime = rgVistor.generationDuration;
    _visitor.rowMasterSolvingTime = rgVistor.masterSolvingDuration;
    return Solution{solver.getObjValue(), std::move(paths)};
}

std::optional<boost::multi_array<bool, 2>> getFunctionPlacementPerFunc(
    const SFC::Instance& _inst, std::size_t _nbLicenses,
    const std::vector<SFC::function_descriptor>& _funcs) {
    std::cout << "Solving for ";
    boost::multi_array<bool, 2> funcPlacement(boost::extents[static_cast<long>(
        num_vertices(_inst.network))][static_cast<long>(_inst.nbFunctions)]);
    std::fill_n(funcPlacement.data(), funcPlacement.num_elements(), false);
    SFC::LocalisationPerFunction loc(_inst, _nbLicenses);
    std::vector<double> cpuCapacities = _inst.cpuCapacity;
    std::vector<double> ramCapacities = _inst.ramCapacity;
    std::vector<double> storageCapacities = _inst.storageCapacity;
    for (const function_descriptor f : _funcs) {
        // std::cout << "Changing model...\n";
        loc.setFunction(f);
        loc.setCPUCapacities(cpuCapacities);
        loc.setRAMCapacities(ramCapacities);
        loc.setStorageCapacities(storageCapacities);
        std::cout << f << '\t' << std::flush;
        if (!loc.solve()) {
            return std::nullopt;
        }
        for (const auto u :
            boost::make_iterator_range(vertices(_inst.network))) {
            cpuCapacities[u] -= loc.getCPUUsedCapacities(u);
            ramCapacities[u] -= loc.getRAMUsedCapacities(u);
            storageCapacities[u] -= loc.getStorageUsedCapacities(u);
        }
        IloNumArray localisations = loc.getLocalisation();
        for (const auto u :
            boost::make_iterator_range(vertices(_inst.network))) {
            funcPlacement[static_cast<long>(u)][static_cast<long>(f)] =
                static_cast<bool>(localisations[static_cast<long>(u)]);
        }
    }
    return funcPlacement;
}

std::optional<boost::multi_array<bool, 2>> getFunctionPlacementMedoids(
    const SFC::Instance& _inst, std::size_t _nbLicenses,
    const std::vector<SFC::function_descriptor>& _funcs, std::mt19937& _gen) {
    const auto getAllDistancesMatrix = [](const auto& _network) {
        boost::multi_array<int, 2> retval(
            boost::extents[static_cast<long>(num_vertices(_network))]
                          [static_cast<long>(num_vertices(_network))]);
        std::fill_n(retval.data(), retval.num_elements(), 0);
        johnson_all_pairs_shortest_paths(_network, retval,
            boost::weight_map(
                boost::make_function_property_map<Network::edge_descriptor,
                    int>([](auto&&) -> int { return 1; })));
        return retval;
    };
    const auto distMat = getAllDistancesMatrix(_inst.network);
    auto currentCapas = _inst.cpuCapacity;

    boost::multi_array<bool, 2> funcPlacement(boost::extents[static_cast<long>(
        num_vertices(_inst.network))][static_cast<long>(_inst.nbFunctions)]);
    std::fill_n(funcPlacement.data(), funcPlacement.num_elements(), false);
    std::vector<double> charge(num_vertices(_inst.network), 0.0);
    for (const auto& f : _funcs) {
        std::fill(charge.begin(), charge.end(), 0.0);
        // The charge of each node correspond to the sum of the demands'
        // charge from and to this node (divided by 2)
        double totalCharge = 0.0;
        for (std::size_t i = 0; i < _inst.demands.size(); ++i) {
            for (std::size_t j = 0; j < _inst.demands[i].functions.size();
                 ++j) {
                if (_inst.demands[i].functions[j] == f) {
                    totalCharge += _inst.demands[i].cpuCharges[j];
                    const double halfCharge =
                        _inst.demands[i].cpuCharges[j] / 2.0;
                    charge[_inst.demands[i].s] += halfCharge;
                    charge[_inst.demands[i].t] += halfCharge;
                }
            }
        }
        std::vector<std::size_t> medoids(_nbLicenses);
        std::iota(medoids.begin(), medoids.end(), 0);
        std::shuffle(medoids.begin(), medoids.end(), _gen);
        if (!improveKMedoids(medoids, distMat, currentCapas, charge)) {
            return std::nullopt;
        }
        for (const auto& u : medoids) {
            funcPlacement[static_cast<long>(u)][static_cast<long>(f)] = true;
            currentCapas[static_cast<std::size_t>(u)] -=
                totalCharge / double(_nbLicenses);
        }
    }
    return funcPlacement;
}

} // namespace SFC

