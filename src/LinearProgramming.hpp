#ifndef LINEAR_PROGRAMMING
#define LINEAR_PROGRAMMING

#include "CppRO/cplex_utility.hpp"

template <typename Problem>
void solve(Problem& _problem) {
    _problem.updateStepSize();
}

template <typename Problem>
void updateStepSize(Problem& _problem) {
    _problem.updateStepSize();
}

template <typename Problem>
void solve(Problem& _problem, bool _verbose) {
    _problem.solve(_verbose);
}

template <typename Problem>
void updateLagrangianMultipliers(
    Problem& _problem, double _alpha, double _bestObjValue) {
    _problem.updateLagrangianMultipliers(_alpha, _bestObjValue);
}

template <typename Problem>
bool solveLagrangianRelaxation(Problem& _problem, double _alpha, double _bestUB,
    int _nbIteration, bool _verbose) {
    for (int i = 0; i < _nbIteration; ++i) {
        if (solve(_problem, _verbose)) {
            updateStepSize(_problem, _alpha, _bestUB);
            updateLagrangianMultipliers(_problem);
        } else {
            return false;
        }
    }
    return true;
}

template <typename Variable>
struct FixedVariable {
    Variable var;
    bool use;
};

template <typename Var>
struct Weighted {
    Var var;
    double value;
};

#endif
