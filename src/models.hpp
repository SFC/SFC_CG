#ifndef MODELS_HPP
#define MODELS_HPP

#include "CppRO/cplex_utility.hpp"
#include "LinearProgramming.hpp"
#include "SFC.hpp"
#include <CppRO/utility.hpp>
#include <algorithm>
#include <boost/multi_array.hpp>
#include <boost/range/combine.hpp>
#include <boost/range/numeric.hpp>
#include <fmt/core.h>
#include <ilconcert/iloexpression.h>
#include <ilconcert/ilolinear.h>
#include <ilconcert/ilosys.h>
#include <ilcplex/ilocplex.h>
#include <ilcplex/ilocplexi.h>
#include <numeric>
#include <type_traits>

namespace SFC {

std::vector<IloRange> getOnePathPerDemandConstraints(
    std::size_t _nbDemands, IloModel& _model);

template <typename Instance>
std::vector<IloRange> getOnePathPerDemandConstraints(
    const Instance& _inst, IloModel& _model) {
    std::vector<IloRange> onePathCons(_inst.demands.size());
    std::generate_n(onePathCons.begin(), _inst.demands.size(), [&]() {
        return IloAdd(_model, IloRange(_model.getEnv(), 1.0, IloInfinity));
    });
    return onePathCons;
}

template <typename Instance>
std::vector<IloRange> getLinkCapacityConstraints(
    const Instance& _inst, IloModel& _model) {
    std::vector<IloRange> linkCapaCons(num_edges(_inst.network));
    for (const auto ed : boost::make_iterator_range(edges(_inst.network))) {
        const auto e = _inst.network[ed].id;
        linkCapaCons[e] = IloAdd(_model,
            IloRange(_model.getEnv(), -IloInfinity, _inst.linkCapacity[e],
                ("linkCapaCons(" + std::to_string(_inst.network[ed].id) + ')')
                    .c_str()));
    }
    return linkCapaCons;
}

template <typename CapacityRange>
std::vector<IloRange> getCapacityConstraints(const CapacityRange& _range,
    IloModel& _model, const std::string& _capaName) {
    std::vector<IloRange> linkCapaCons(std::size(_range));
    std::ranges::transform(
        _range, linkCapaCons.begin(), [&, i = 0](double _val) mutable {
            return IloAdd(
                _model, IloRange(_model.getEnv(), -IloInfinity, _val,
                            fmt::format(fmt::runtime(_capaName), i++).c_str()));
        });
    return linkCapaCons;
}

template <typename Instance>
std::vector<IloRange> getCPUCapacityConstraints(
    const Instance& _inst, IloModel& _model) {
    std::vector<IloRange> cpuCapasCons(num_vertices(_inst.network));
    std::transform(vertices(_inst.network).first,
        vertices(_inst.network).second, cpuCapasCons.begin(), [&](auto&& _u) {
            return IloAdd(_model,
                IloRange(_model.getEnv(), -IloInfinity, _inst.cpuCapacity[_u],
                    ("cpuCapaCons(" + std::to_string(_u) + ')').c_str()));
        });
    return cpuCapasCons;
}

template <typename Instance>
std::vector<IloRange> getRAMCapacityConstraints(
    const Instance& _inst, IloModel& _model) {
    std::vector<IloRange> ramCapasCons(num_vertices(_inst.network));
    std::transform(vertices(_inst.network).first,
        vertices(_inst.network).second, ramCapasCons.begin(), [&](auto&& _u) {
            return IloAdd(_model,
                IloRange(_model.getEnv(), -IloInfinity, _inst.ramCapacity[_u],
                    ("ramCapaCons(" + std::to_string(_u) + ')').c_str()));
        });
    return ramCapasCons;
}

template <typename Instance>
std::vector<IloRange> getStorageCapacityConstraints(
    const Instance& _inst, IloModel& _model) {
    std::vector<IloRange> storageCapasCons(num_vertices(_inst.network));
    std::transform(vertices(_inst.network).first,
        vertices(_inst.network).second, storageCapasCons.begin(),
        [&](auto&& _u) {
            return IloAdd(_model,
                IloRange(_model.getEnv(), -IloInfinity,
                    _inst.storageCapacity[_u],
                    ("storageCapaCons(" + std::to_string(_u) + ')').c_str()));
        });
    return storageCapasCons;
}

std::vector<boost::multi_array<IloNumVar, 2>> buildFlowVars(
    const Instance& _inst, IloModel& _model);

std::vector<boost::multi_array<IloNumVar, 2>> buildLocationVars(
    const Instance& _inst, IloModel& _model);

IloObjective buildMinimizeBandwidthUsageObjective(const Instance& _inst,
    const std::vector<boost::multi_array<IloNumVar, 2>>& _flowVars,
    IloModel& _model);

std::vector<boost::multi_array<IloRange, 2>> buildFlowConservationConstraints(
    const Instance& _inst,
    const std::vector<boost::multi_array<IloNumVar, 2>>& _flowVars,
    const std::vector<boost::multi_array<IloNumVar, 2>>& _locationVars,
    IloModel& _model);

std::vector<IloRange> buildLinkCapacityConstraints(const Instance& _inst,
    const std::vector<boost::multi_array<IloNumVar, 2>>& _flowVars,
    IloModel& _model);

std::vector<IloRange> buildCPUCapacityConstraints(const Instance& _inst,
    const std::vector<boost::multi_array<IloNumVar, 2>>& _locationVars,
    IloModel& _model);

class ILP {
  public:
    explicit ILP(const Instance& _inst)
        : m_f(buildFlowVars(_inst, m_model))
        , m_a(buildLocationVars(_inst, m_model))
        , m_obj(buildMinimizeBandwidthUsageObjective(_inst, m_f, m_model))
        , m_flowConservationConstraints(
              buildFlowConservationConstraints(_inst, m_f, m_a, m_model))
        , m_linkCapacityConstraitns(
              buildLinkCapacityConstraints(_inst, m_f, m_model))
        , m_cpuCapacityConstraints(
              buildCPUCapacityConstraints(_inst, m_a, m_model))

    {}

  private:
    IloEnvWrapper m_env;
    IloModel m_model{m_env};

    std::vector<boost::multi_array<IloNumVar, 2>> m_f;
    std::vector<boost::multi_array<IloNumVar, 2>> m_a;

    IloObjective m_obj;

    std::vector<boost::multi_array<IloRange, 2>> m_flowConservationConstraints;
    std::vector<IloRange> m_linkCapacityConstraitns;
    std::vector<IloRange> m_cpuCapacityConstraints;
};

class PathExtendedModel {
  public:
    struct ColumnType {
        template <typename Instance, typename Demand>
        ColumnType(
            ServicePath _sPath, const Instance& _inst, const Demand& _demand)
            : path(std::move(_sPath))
            , obj(static_cast<double>(std::ssize(path.nPath))
                  * _demand.bandwidth) {
            for (std::size_t j = 0; j < path.locations.size(); ++j) {
                cpuCharges[path.locations[j]] += _demand.cpuCharges[j];
            }
            for (std::size_t j = 0; j < path.locations.size(); ++j) {
                ramCharges[path.locations[j]] += _demand.ramCharges[j];
            }
            for (std::size_t j = 0; j < path.locations.size(); ++j) {
                storageCharges[path.locations[j]] += _demand.storageCharges[j];
            }
            for (const auto ed : path.nPath) {
                linkCharges[_inst.network[ed].id] += _demand.bandwidth;
            }
        }
        ServicePath path;
        double obj;
        std::unordered_map<std::size_t, double> linkCharges;
        std::unordered_map<std::size_t, double> cpuCharges;
        std::unordered_map<std::size_t, double> ramCharges;
        std::unordered_map<std::size_t, double> storageCharges;
    };

  private:
    IloModel m_model;
    IloObjective m_obj;

    // Constraints
    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_cpuCapasCons;
    std::vector<IloRange> m_ramCapasCons;
    std::vector<IloRange> m_storageCapasCons;
    // Vars
    std::vector<IloNumVar> m_dummyPaths;
    std::vector<std::vector<ColumnPair<ColumnType>>> m_columns;

  public:
    template <typename DummyCostRange, typename BandwidthRange,
        typename CpuRange, typename RAMRange, typename StorageRange>
    explicit PathExtendedModel(IloEnv _env, std::size_t _nbDemands,
        const DummyCostRange& _dummyCostRange, const BandwidthRange& _bwRange,
        const CpuRange& _cpuRange, const RAMRange& _ramRange,
        const StorageRange& _storageRange)
        : m_model(_env)
        , m_obj(IloAdd(m_model, IloMinimize(_env)))
        , m_onePathCons(getOnePathPerDemandConstraints(_nbDemands, m_model))
        , m_linkCapasCons(getCapacityConstraints(
              _bwRange, m_model, "bwCapacityConstraint({})"))
        , m_cpuCapasCons(getCapacityConstraints(
              _cpuRange, m_model, "cpuCapacityConstraint({})"))
        , m_ramCapasCons(getCapacityConstraints(
              _ramRange, m_model, "ramCapacityConstraint({})"))
        , m_storageCapasCons(getCapacityConstraints(
              _storageRange, m_model, "storageCapacityConstraint({})"))
        , m_dummyPaths([&]() {
            std::vector<IloNumVar> retval(_nbDemands);
            std::ranges::transform(_dummyCostRange, m_onePathCons,
                retval.begin(),
                [&, i = 0](double _cost, const IloRange& _constraint) mutable {
                    return IloAdd(
                        m_model, IloNumVar(_constraint(1.0) + m_obj(_cost), 0.0,
                                     IloInfinity, IloNumVar::Type::Float,
                                     fmt::format("dummy({})", i++).c_str()));
                });
            return retval;
        }())
        , m_columns(_nbDemands)

    {}

    void addColumn(const PathExtendedModel::ColumnType& _col);
    void getNetworkUsage(const IloCplex& _sol) const;
    [[nodiscard]] IloModel getIntegerModel() const;

    [[nodiscard]] std::size_t getNbColumns() const { return m_columns.size(); }
    [[nodiscard]] const auto& getColumns() const { return m_columns; }
    [[nodiscard]] const IloModel& getModel() const { return m_model; }

    [[nodiscard]] std::size_t getNbDemands() const {
        return m_onePathCons.size();
    }
    [[nodiscard]] std::size_t getNbVertices() const {
        return m_cpuCapasCons.size();
    }

    struct DualValues {
        std::vector<IloNum> m_onePathDuals;
        std::vector<IloNum> m_linkCapasDuals;
        std::vector<IloNum> m_cpuCapasDuals;
        std::vector<IloNum> m_ramCapasDuals;
        std::vector<IloNum> m_storageCapasDuals;

        [[nodiscard]] double getObjectiveValue(std::size_t _demandId) const {
            return m_onePathDuals[_demandId];
        }

        template <typename Instance>
        [[nodiscard]] IloNum getReducedCost(
            const Instance& _inst, const CrossLayerLink& _link) const {
            const auto& demand = _inst.demands[_link.demandID];
            return demand.cpuCharges[_link.j] * m_cpuCapasDuals[_link.u]
                   + demand.ramCharges[_link.j] * m_ramCapasDuals[_link.u]
                   + demand.storageCharges[_link.j]
                         * m_storageCapasDuals[_link.u];
        }

        template <typename Instance>
        [[nodiscard]] IloNum getReducedCost(
            const Instance& _inst, const IntraLayerLink& _link) const {
            return _inst.demands[_link.demandID].bandwidth
                   * (1 + m_linkCapasDuals[_inst.network[_link.edge].id]);
        }
    };

    // Static Functions
    [[nodiscard]] static double getReducedCost(
        const ColumnType& _col, const DualValues& _dualValues);
    [[nodiscard]] static bool isImprovingColumn(
        const ColumnType& _col, const DualValues& _dualValues);
    [[nodiscard]] DualValues getDualValues(const IloCplex& _solver) const;
};

class RoutingExtendedModel {
  public:
    struct ColumnType {
        template <typename Instance>
        ColumnType(std::vector<ServicePath> _sPaths, const Instance& _inst)
            : paths(std::move(_sPaths))
            , obj(std::accumulate(paths.begin(), paths.end(), 0.0,
                  [&](const auto _acc, const auto& _path) {
                      return _acc
                             + static_cast<double>(std::ssize(_path.nPath))
                                   * _inst.demands[_path.demand].bandwidth;
                  })) {
            for (const auto& path : paths) {
                for (std::size_t j = 0; j < path.locations.size(); ++j) {
                    cpuCharges[path.locations[j]] +=
                        _inst.demands[path.demand].cpuCharges[j];
                }
                for (const auto ed : path.nPath) {
                    linkCharges[_inst.network[ed].id] +=
                        _inst.demands[path.demand].bandwidth;
                }
            }
        }
        std::vector<ServicePath> paths;
        double obj;
        std::unordered_map<std::size_t, double> linkCharges;
        std::unordered_map<std::size_t, double> cpuCharges;
        std::unordered_map<std::size_t, double> ramCharges;
        std::unordered_map<std::size_t, double> storageCharges;
    };

  private:
    IloModel m_model;
    IloObjective m_obj;

    // Constraints
    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasConstraints;
    std::vector<IloRange> m_cpuCapasConstraints;
    std::vector<IloRange> m_ramCapasConstraints;
    std::vector<IloRange> m_storageCapasConstraints;
    // Vars
    std::vector<IloNumVar> m_dummyPaths;
    std::vector<ColumnPair<ColumnType>> m_columns;

  public:
    template <typename DummyCostRange, typename BandwidthRange,
        typename CpuRange, typename RAMRange, typename StorageRange>
    explicit RoutingExtendedModel(IloEnv _env, std::size_t _nbDemands,
        const DummyCostRange& _dummyCostRange, const BandwidthRange& _bwRange,
        const CpuRange& _cpuRange, const RAMRange& _ramRange,
        const StorageRange& _storageRange)
        : m_model(_env)
        , m_obj(IloAdd(m_model, IloMinimize(_env)))
        , m_onePathCons(getOnePathPerDemandConstraints(_nbDemands, m_model))
        , m_linkCapasConstraints(getCapacityConstraints(
              _bwRange, m_model, "bwCapacityConstraint({})"))
        , m_cpuCapasConstraints(getCapacityConstraints(
              _cpuRange, m_model, "cpuCapacityConstraint({})"))
        , m_ramCapasConstraints(getCapacityConstraints(
              _ramRange, m_model, "ramCapacityConstraint({})"))
        , m_storageCapasConstraints(getCapacityConstraints(
              _storageRange, m_model, "storageCapacityConstraint({})"))
        , m_dummyPaths([&]() {
            std::vector<IloNumVar> retval(_nbDemands);
            std::ranges::transform(_dummyCostRange, m_onePathCons,
                retval.begin(),
                [&, i = 0](double _cost, const IloRange& _constraint) mutable {
                    return IloAdd(
                        m_model, IloNumVar(_constraint(1.0) + m_obj(_cost), 0.0,
                                     IloInfinity, IloNumVar::Type::Float,
                                     fmt::format("dummy({})", i++).c_str()));
                });
            return retval;
        }())

    {}

    void addColumn(const RoutingExtendedModel::ColumnType& _col);
    [[nodiscard]] IloModel getIntegerModel() const;

    [[nodiscard]] std::size_t getNbColumns() const { return m_columns.size(); }
    [[nodiscard]] const auto& getColumns() const { return m_columns; }
    [[nodiscard]] const IloModel& getModel() const { return m_model; }

    [[nodiscard]] std::size_t getNbVertices() const {
        return m_cpuCapasConstraints.size();
    }

    struct DualValues {
        std::vector<IloNum> m_onePathDuals;
        std::vector<IloNum> m_linkCapasDuals;
        std::vector<IloNum> m_cpuCapasDuals;
        std::vector<IloNum> m_ramCapasDuals;
        std::vector<IloNum> m_storageCapasDuals;

        template <typename Instance>
        [[nodiscard]] IloNum getReducedCost(
            const Instance& _inst, const CrossLayerLink& _link) const {
            return _inst.demands[_link.demandID].cpuCharges[_link.j]
                       * m_cpuCapasDuals[_link.u]
                   + _inst.demands[_link.demandID].ramCharges[_link.j]
                         * m_ramCapasDuals[_link.u]
                   + _inst.demands[_link.demandID].storageCharges[_link.j]
                         * m_storageCapasDuals[_link.u];
        }

        template <typename Instance>
        [[nodiscard]] IloNum getReducedCost(
            const Instance& _inst, const IntraLayerLink& _link) const {
            return _inst.demands[_link.demandID].bandwidth
                   * (1 + m_linkCapasDuals[_inst.network[_link.edge].id]);
        }
    };

    // Static Functions
    [[nodiscard]] static double getReducedCost(
        const ColumnType& _col, const DualValues& _dualValues);
    [[nodiscard]] static bool isImprovingColumn(
        const ColumnType& _col, const DualValues& _dualValues);

    [[nodiscard]] static double getReducedCost(
        const PathExtendedModel::ColumnType& _col,
        const DualValues& _dualValues);
    [[nodiscard]] static bool isImprovingColumn(
        const PathExtendedModel::ColumnType& _col,
        const DualValues& _dualValues);
    [[nodiscard]] DualValues getDualValues(const IloCplex& _solver) const;
};

class ShortestPathPerDemandLp {
  public:
    using Column = ServicePath;
    explicit ShortestPathPerDemandLp(const Instance& _inst);

    void setPricingID(std::size_t _demandID);
    void resetBounds();

    template <typename DualValues>
    void updateDual(const DualValues& _dualValues) {
        const auto& functions = m_inst->demands[m_demandID].functions;

        IloNumExpr expr(m_env, -_dualValues.m_onePathDuals[m_demandID]);
        for (const auto ed :
            boost::make_iterator_range(edges(m_inst->network))) {
            for (std::size_t j = 0; j <= functions.size(); ++j) {
                expr += m_f[long(m_inst->network[ed].id)][long(j)]
                        * _dualValues.getReducedCost(
                            IntraLayerLink{m_demandID, ed, j});
            }
        }
        for (const auto u :
            boost::make_iterator_range(vertices(m_inst->network))) {
            for (std::size_t j = 0; j < functions.size(); ++j) {
                expr += m_a[long(u)][long(j)]
                        * _dualValues.getReducedCost(
                            CrossLayerLink{m_demandID, u, j});
            }
        }
        m_obj.setExpr(expr);
        expr.end();
        m_solver.extract(m_model);
    }

    [[nodiscard]] ServicePath getColumn() const;
    bool solve();
    [[nodiscard]] double getObjValue() const;

  protected:
    const Instance* m_inst;

    std::size_t m_demandID = 0;
    std::size_t n;
    std::size_t m;
    std::size_t nbLayers;

    IloEnvWrapper m_env{};
    IloModel m_model{m_env};

    IloObjective m_obj;

    boost::multi_array<IloNumVar, 2> m_a;
    boost::multi_array<IloNumVar, 2> m_f;

    boost::multi_array<IloRange, 2> m_flowConsCons;
    std::vector<IloRange> m_linkCapaCons;
    std::vector<IloRange> m_cpuCapaCons;
    std::vector<IloRange> m_ramCapaCons;
    std::vector<IloRange> m_storageCapaCons;

    IloCplex m_solver;
};

class PathExtendedAlphaModel {
    SFC::PathExtendedModel m_baseModel;
    IloModel m_model;

    boost::multi_array<IloNumVar, 2> m_locationsVars;
    std::vector<IloRange> m_nbLicensesCons;
    boost::multi_array<std::vector<IloRange>, 2> m_pathFuncCons;

  public:
    template <typename DummyCostRange, typename BandwidthRange,
        typename CpuRange, typename RAMRange, typename StorageRange,
        typename DemandFunctionsRange>
    PathExtendedAlphaModel(IloEnv _env, std::size_t _nbDemands,
        std::size_t _nbLicenses, std::size_t _nbFunctions,
        const DemandFunctionsRange& _functionsRange,
        const DummyCostRange& _dummyCostRange, const BandwidthRange& _bwRange,
        const CpuRange& _cpuRange, const RAMRange& _ramRange,
        const StorageRange& _storageRange);

    [[nodiscard]] std::vector<ServicePath> getServicePaths() const;

    [[nodiscard]] std::size_t getNbColumns() const {
        return m_baseModel.getNbColumns();
    }

    [[nodiscard]] const auto& getPathFuncConstraints() const {
        return m_pathFuncCons;
    }

    struct DualValues {
        PathExtendedModel::DualValues baseDuals;
        boost::multi_array<std::vector<double>, 2> pathFuncDuals;

        [[nodiscard]] double getObjectiveValue(std::size_t _demandId) const {
            return baseDuals.getObjectiveValue(_demandId);
        }

        template <typename Instance>
        [[nodiscard]] IloNum getReducedCost(
            const Instance& _inst, const CrossLayerLink& _link) const {
            return baseDuals.getReducedCost(_inst, _link)
                   + pathFuncDuals[long(_link.demandID)][long(_link.u)]
                                  [_link.j];
        }

        template <typename Instance>
        [[nodiscard]] IloNum getReducedCost(
            const Instance& _inst, const IntraLayerLink& _link) const {
            return baseDuals.getReducedCost(_inst, _link);
        }
    };
    void addColumn(const PathExtendedModel::ColumnType& _col);
    [[nodiscard]] const auto& getModel() const { return m_model; }
    [[nodiscard]] IloModel getIntegerModel() const;
    [[nodiscard]] const auto& getColumns() const {
        return m_baseModel.getColumns();
    }

    // Static Functions
    [[nodiscard]] static double getReducedCost(
        const PathExtendedModel::ColumnType& _col,
        const DualValues& _dualValues);
    [[nodiscard]] static bool isImprovingColumn(
        const PathExtendedModel::ColumnType& _col,
        const DualValues& _dualValues);
    [[nodiscard]] DualValues getDualValues(const IloCplex& _solver) const;
};

template <typename DummyCostRange, typename BandwidthRange, typename CpuRange,
    typename RAMRange, typename StorageRange, typename DemandFunctionsRange>
PathExtendedAlphaModel::PathExtendedAlphaModel(IloEnv _env,
    std::size_t _nbDemands, std::size_t _nbLicenses, std::size_t _nbFunctions,
    const DemandFunctionsRange& _functionsRange,
    const DummyCostRange& _dummyCostRange, const BandwidthRange& _bwRange,
    const CpuRange& _cpuRange, const RAMRange& _ramRange,
    const StorageRange& _storageRange)
    : m_baseModel(_env, _nbDemands, _dummyCostRange, _bwRange, _cpuRange,
        _ramRange, _storageRange)
    , m_model(_env)
    , m_locationsVars([&]() {
        boost::multi_array<IloNumVar, 2> b(
            boost::extents[std::ssize(_cpuRange)][long(_nbFunctions)]);
        for (std::size_t f = 0; f < _nbFunctions; ++f) {
            for (std::size_t u = 0; u < std::size(_cpuRange); ++u) {
                b[long(u)][long(f)] = IloAdd(m_model,
                    IloNumVar(_env, 0.0, IloInfinity, IloNumVar::Type::Float,
                        fmt::format("b(u~{})(f~{})", u, f).c_str()));
            }
        }
        return b;
    }())
    , m_nbLicensesCons([&]() {
        std::vector<IloRange> funcLicensesCons(_nbFunctions);
        for (std::size_t f = 0; f < _nbFunctions; ++f) {
            const auto allNode_f =
                m_locationsVars[boost::indices[boost::multi_array_types::
                        index_range(0, std::ssize(_cpuRange))][long(f)]];
            funcLicensesCons[f] =
                IloAdd(m_model, IloRange(_env, -IloInfinity,
                                    std::accumulate(allNode_f.begin(),
                                        allNode_f.end(), IloExpr(_env)),
                                    static_cast<double>(_nbLicenses)));
            setIloName(
                funcLicensesCons[f], "funcLicensesCons(" + toString(f) + ")");
            // vars.end();
        }
        return funcLicensesCons;
    }())
    , m_pathFuncCons([&]() {
        boost::multi_array<std::vector<IloRange>, 2> pathFuncCons(
            boost::extents[long(_nbDemands)][std::ssize(_cpuRange)]);

        for (std::size_t i = 0; i < _nbDemands; ++i) {
            for (std::size_t u = 0; u < std::size(_cpuRange); ++u) {
                for (std::size_t j = 0; j < std::size(_functionsRange[long(i)]);
                     ++j) {
                    pathFuncCons[long(i)][long(u)].emplace_back(IloAdd(m_model,
                        IloRange(_env, -IloInfinity,
                            -m_locationsVars[long(u)]
                                            [long(_functionsRange[long(i)][j])],
                            0.0,
                            fmt::format(
                                "pathFuncCons(i~{}, u~{}, j~{})", i, j, u)
                                .c_str())));
                }
            }
        }
        return pathFuncCons;
    }())

{
    m_model.add(m_baseModel.getModel());
}

/**
 * Similar to PathExtendedAlphaModel but the path-function constraints are
 * generated on the fly
 */
class LazyPathExtendedAlphaModel {
  public:
    struct PathFunctionLinking {
        std::size_t demandId;
        std::size_t u;
        std::size_t j;
        std::size_t f;

        /**
         * Given an order PathFunctionLinking, return true if they are equal.
         * PathFunctionLinking::f  is ignored since it is always the same for a
         * given PathFunctionLinking::j
         */
        bool operator==(const PathFunctionLinking& _other) const {
            return demandId == _other.demandId && u == _other.u
                   && j == _other.j;
        }
    };

    /**
     * Stores values of each variables
     */
    struct RawSolution {
        boost::multi_array<double, 2> locationsValues;
        std::vector<std::vector<double>> pathValues;
    };

  private:
    SFC::PathExtendedModel m_baseModel;
    IloModel m_model;

    boost::multi_array<IloNumVar, 2> m_locationsVars;
    std::vector<IloRange> m_nbLicensesCons;
    struct TripletHash {
        std::size_t nbDemands;
        std::size_t nbVertices;

        std::size_t operator()(const PathFunctionLinking& _k) const {
            return _k.j + nbDemands * (_k.u + nbVertices * _k.demandId);
        }
    };
    TripletHash m_hasher;
    std::unordered_map<PathFunctionLinking, IloRange, TripletHash>
        m_pathFuncCons;

  public:
    template <typename DummyCostRange, typename BandwidthRange,
        typename CpuRange, typename RAMRange, typename StorageRange>
    LazyPathExtendedAlphaModel(IloEnv _env, std::size_t _nbDemands,
        std::size_t _nbLicenses, std::size_t _nbFunctions,
        const DummyCostRange& _dummyCostRange, const BandwidthRange& _bwRange,
        const CpuRange& _cpuRange, const RAMRange& _ramRange,
        const StorageRange& _storageRange);

    LazyPathExtendedAlphaModel(const LazyPathExtendedAlphaModel&) = delete;
    LazyPathExtendedAlphaModel& operator=(
        const LazyPathExtendedAlphaModel&) = delete;
    LazyPathExtendedAlphaModel(LazyPathExtendedAlphaModel&&) = default;
    LazyPathExtendedAlphaModel& operator=(
        LazyPathExtendedAlphaModel&&) = default;
    ~LazyPathExtendedAlphaModel() = default;

    [[nodiscard]] std::vector<ServicePath> getServicePaths() const;

    [[nodiscard]] std::size_t getNbColumns() const {
        return m_baseModel.getNbColumns();
    }

    [[nodiscard]] const auto& getPathFuncConstraints() const {
        return m_pathFuncCons;
    }

    [[nodiscard]] const auto& getLocationVars() const {
        return m_locationsVars;
    }

    [[nodiscard]] RawSolution getRawSolution(const IloCplex& _solver) const {
        RawSolution retval{
            boost::multi_array<double, 2>{
                boost::extents[static_cast<long>(m_locationsVars.shape()[0])]
                              [static_cast<long>(m_locationsVars.shape()[1])]},
            std::vector<std::vector<double>>(m_baseModel.getColumns().size())};

        std::transform(m_locationsVars.data(),
            m_locationsVars.data() + m_locationsVars.num_elements(),
            retval.locationsValues.data(),
            [&](const auto& _var) { return _solver.getValue(_var); });

        for (std::size_t k = 0; k < retval.pathValues.size(); ++k) {
            retval.pathValues[k].resize(m_baseModel.getColumns()[k].size());
            std::transform(m_baseModel.getColumns()[k].begin(),
                m_baseModel.getColumns()[k].end(), retval.pathValues[k].begin(),
                [&](const auto& _col) { return _solver.getValue(_col.var); });
        }

        return retval;
    }

    struct DualValues {
        PathExtendedModel::DualValues baseDuals;
        std::unordered_map<PathFunctionLinking, double, TripletHash>
            pathFuncDuals;

        [[nodiscard]] double getObjectiveValue(std::size_t _demandId) const {
            return baseDuals.getObjectiveValue(_demandId);
        }

        template <typename Instance>
        [[nodiscard]] IloNum getReducedCost(
            const Instance& _inst, const CrossLayerLink& _link) const {
            auto retval = baseDuals.getReducedCost(_inst, _link);
            if (auto val =
                    pathFuncDuals.find({_link.demandID, _link.u, _link.j, 0});
                val != pathFuncDuals.end()) {
                retval += val->second;
            }
            return retval;
        }

        template <typename Instance>
        [[nodiscard]] IloNum getReducedCost(
            const Instance& _inst, const IntraLayerLink& _link) const {
            return baseDuals.getReducedCost(_inst, _link);
        }
    };
    void addColumn(const PathExtendedModel::ColumnType& _col);
    [[nodiscard]] const auto& getModel() const { return m_model; }
    [[nodiscard]] IloModel getIntegerModel() const;
    [[nodiscard]] const auto& getColumns() const {
        return m_baseModel.getColumns();
    }

    IloRange buildPathFunctionConstraint(
        const PathFunctionLinking& _linking) const;
    void enablePathFunctionConstraint(const PathFunctionLinking& _linking);
    IloRangeArray getLazyConstraints(
        const std::vector<LazyPathExtendedAlphaModel::PathFunctionLinking>&
            _allPathFunctionLinkings) const;
    // Static Functions
    [[nodiscard]] static double getReducedCost(
        const PathExtendedModel::ColumnType& _col,
        const DualValues& _dualValues);
    [[nodiscard]] static bool isImprovingColumn(
        const PathExtendedModel::ColumnType& _col,
        const DualValues& _dualValues);
    [[nodiscard]] DualValues getDualValues(const IloCplex& _solver) const;
};

template <typename DummyCostRange, typename BandwidthRange, typename CpuRange,
    typename RAMRange, typename StorageRange>
LazyPathExtendedAlphaModel::LazyPathExtendedAlphaModel(IloEnv _env,
    std::size_t _nbDemands, std::size_t _nbLicenses, std::size_t _nbFunctions,
    const DummyCostRange& _dummyCostRange, const BandwidthRange& _bwRange,
    const CpuRange& _cpuRange, const RAMRange& _ramRange,
    const StorageRange& _storageRange)
    : m_baseModel(_env, _nbDemands, _dummyCostRange, _bwRange, _cpuRange,
        _ramRange, _storageRange)
    , m_model(_env)
    , m_locationsVars([&]() {
        boost::multi_array<IloNumVar, 2> b(
            boost::extents[std::ssize(_cpuRange)][long(_nbFunctions)]);
        for (std::size_t f = 0; f < _nbFunctions; ++f) {
            for (std::size_t u = 0; u < std::size(_cpuRange); ++u) {
                b[long(u)][long(f)] = IloAdd(m_model,
                    IloNumVar(_env, 0.0, IloInfinity, IloNumVar::Type::Float,
                        fmt::format("b(u~{})(f~{})", u, f).c_str()));
            }
        }
        return b;
    }())
    , m_nbLicensesCons([&]() {
        std::vector<IloRange> funcLicensesCons(_nbFunctions);
        for (std::size_t f = 0; f < _nbFunctions; ++f) {
            const auto allNode_f =
                m_locationsVars[boost::indices[boost::multi_array_types::
                        index_range(0, std::ssize(_cpuRange))][long(f)]];
            funcLicensesCons[f] =
                IloAdd(m_model, IloRange(_env, -IloInfinity,
                                    std::accumulate(allNode_f.begin(),
                                        allNode_f.end(), IloExpr(_env)),
                                    static_cast<double>(_nbLicenses)));
            setIloName(
                funcLicensesCons[f], "funcLicensesCons(" + toString(f) + ")");
            // vars.end();
        }
        return funcLicensesCons;
    }())
    , m_hasher(TripletHash{_nbDemands, std::size(_cpuRange)})
    , m_pathFuncCons(1, m_hasher)

{
    m_model.add(m_baseModel.getModel());
}

class LocalisationPerFunction {
    [[nodiscard]] IloInt getIndexX(const Network::vertex_descriptor _u,
        std::size_t _i, std::size_t _j) const;
    [[nodiscard]] IloInt getIndexB(const Network::vertex_descriptor _u,
        const function_descriptor _f) const;
    const Instance& m_instance;
    function_descriptor m_function{
        std::numeric_limits<function_descriptor>::max()};

    IloEnvWrapper m_env{};
    IloModel m_model;

    IloNumVarArray m_x;
    IloNumVarArray m_b;

    IloObjective m_obj;

    IloRangeArray m_oneNodePerDemands;
    IloRangeArray m_cpuCapacities;
    IloRangeArray m_ramCapacities;
    IloRangeArray m_storageCapacities;
    IloRangeArray m_nbLicenses;
    IloRangeArray m_demandNodeLinkage;

    IloCplex m_solver;

  public:
    LocalisationPerFunction(
        const Instance& _instance, std::size_t _nbFunctions);
    bool solve();
    [[nodiscard]] IloNumArray getLocalisation() const;
    double getCPUUsedCapacities(const Network::vertex_descriptor& _u);
    double getRAMUsedCapacities(const Network::vertex_descriptor& _u);
    double getStorageUsedCapacities(const Network::vertex_descriptor& _u);
    void setCPUCapacities(const std::vector<double>& _cpuCapacities);
    void setRAMCapacities(const std::vector<double>& _ramCapacities);
    void setStorageCapacities(const std::vector<double>& _storageCapacities);
    void setFunction(const function_descriptor _f);
};

} // namespace SFC

#endif
