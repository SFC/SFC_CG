#ifndef SFC_PARTIAL_HPP
#define SFC_PARTIAL_HPP

#include "SFC.hpp"
#include "json.hpp"

#include "ColumnGeneration.hpp"
#include "LinearProgramming.hpp" 
#include "boost/graph/adjacency_matrix.hpp"
#include "boost/graph/graph_traits.hpp"
#include "boost/graph/graph_utility.hpp"
#include "boost/multi_array.hpp"
#include <boost/dynamic_bitset.hpp>

namespace SFC::Partial {

using ::operator<<;
struct Function {
    function_descriptor function;
};

using OrderGraph = boost::adjacency_matrix<boost::directedS, Function>;

class Chain {
  public:
    Chain(std::vector<function_descriptor> _functions,
        std::vector<std::vector<function_descriptor>> _order);

    template <typename InputIterator>
    Chain(std::vector<function_descriptor> _functions, InputIterator _first,
        InputIterator _last)
        : m_functions(std::move(_functions))
        , m_precedingFunctions(m_functions.size()) {
        std::for_each(_first, _last, [&](auto&& _fPair) {
            m_precedingFunctions[_fPair.second].push_back(_fPair.first);
        });
    }

    ~Chain() = default;
    Chain(const Chain&) = default;
    Chain& operator=(const Chain&) = default;
    Chain(Chain&&) = default;
    Chain& operator=(Chain&&) = default;

    function_descriptor getFunction(int _layer, int _index) const;
    function_descriptor getFunction(int _index) const;

    const std::vector<function_descriptor>& getPrecedingFunctions(
        int _fIndex) const {
        return m_precedingFunctions[_fIndex];
    }

    std::vector<std::pair<function_descriptor, function_descriptor>>
    getExplicitOrder() const noexcept;

    int size() const { return m_functions.size(); }

  private:
    std::vector<function_descriptor> m_functions;
    std::vector<std::vector<function_descriptor>> m_precedingFunctions;
    friend bool operator<(const Chain& _lhs, const Chain& _rhs) noexcept;
    friend std::ostream& operator<<(
        std::ostream& _out, const Chain& _rhs) noexcept;
};

bool operator<(const Chain& _lhs, const Chain& _rhs) noexcept;
std::ostream& operator<<(std::ostream& _out, const Chain& _rhs) noexcept;

struct ServicePath {
    ServicePath() = default;
    ~ServicePath() = default;

    ServicePath(const ServicePath&) = default;
    ServicePath& operator=(const ServicePath&) = default;
    ServicePath(ServicePath&&) noexcept = default;
    ServicePath& operator=(ServicePath&&) noexcept = default;
    ServicePath(Path _nPath, std::vector<Node> _locations,
        std::vector<function_descriptor> _chain, demand_descriptor _demand);

    Path nPath;
    std::vector<::Node> locations;
    std::vector<function_descriptor> function_indexes;
    demand_descriptor demand;
};

struct RelaxedSolution {
    std::vector<std::vector<Weighted<ServicePath>>> paths;
};
std::ostream& operator<<(std::ostream& _out, const ServicePath& _sPath);
bool operator==(const ServicePath& _lhs, const ServicePath& _rhs);

struct CrossLayerLink {
    demand_descriptor demandID;
    Node u;
    int j;
    int function_index; /// The index of the possible function
};

struct IntraLayerLink {
    demand_descriptor demandID;
    Edge edge;
    int j;
};

bool operator==(const CrossLayerLink& _lhs, const CrossLayerLink& _rhs);
bool operator==(const IntraLayerLink& _lhs, const IntraLayerLink& _rhs);
bool operator<(const CrossLayerLink& _lhs, const CrossLayerLink& _rhs);
bool operator<(const IntraLayerLink& _lhs, const IntraLayerLink& _rhs);

using Demand = SFC::Demand<Chain>;

bool isValid(const ServicePath& _sPath, const Demand& _demand);

std::ostream& operator<<(std::ostream& _out, const Demand& _d);

struct Instance {
    Instance(DiGraph _network, std::vector<int> _nodeCapa,
        std::vector<Demand> _demands, std::vector<double> _funcCharge);
    Instance(Network _network, std::vector<Demand> _demands,
        std::vector<double> _funcCharge);

    SFC::Network network;
    std::vector<double> funcCharge;
    std::vector<Demand> demands;
    int maxChainSize;
    double getNbCores(function_descriptor _f, double _bandwidth) const;
    double getTotalNbCores() const;
};

struct Node {
    int index;
    ::Node original;
    int layer;
};

std::ostream& operator<<(std::ostream& _out, const Node& _n);

struct Link {
    int index;
    double weight;
    int layer;
    int function_index;
    Edge original;
    bool isIntraLayer() const { return function_index == -1; }
};

bool operator==(const Link& _link, const IntraLayerLink& _illink);
bool operator==(const IntraLayerLink& _illink, const Link& _link);
bool operator==(const Link& _link, const CrossLayerLink& _clink);
bool operator==(const CrossLayerLink& _clink, const Link& _link);

template <typename Variable>
bool operator==(
    const FixedVariable<Variable>& _lhs, const FixedVariable<Variable>& _rhs) {
    return std::make_tuple(_lhs.var, _lhs.use)
           == std::make_tuple(_rhs.var, _rhs.use);
}

template <typename Variable>
bool operator<(
    const FixedVariable<Variable>& _lhs, const FixedVariable<Variable>& _rhs) {
    return std::make_tuple(_lhs.var, _lhs.use)
           < std::make_tuple(_rhs.var, _rhs.use);
}

using FixedIntraLayerLink = FixedVariable<IntraLayerLink>;
using FixedCrossLayerLink = FixedVariable<CrossLayerLink>;

bool contains(const ServicePath& _sPath, const IntraLayerLink& _link);
bool contains(const ServicePath& _sPath, const CrossLayerLink& _link);

template <typename Variable>
struct FixedSetVariable {
    std::map<Variable, bool> vars;
};

struct FunctionLocation {
    int function;
    int node;
};

bool operator==(const FunctionLocation& _lhs, const FunctionLocation& _rhs);
bool operator<(const FunctionLocation& _lhs, const FunctionLocation& _rhs);

using FixedLocationFunctions = FixedSetVariable<FunctionLocation>;
bool contains(const ServicePath& _sPath, const Demand& _demand,
    const FixedLocationFunctions& _funcLoc);

bool operator==(const Link& _lhs, const Link& _rhs);

std::ostream& operator<<(std::ostream& _out, const Link& _l);
std::ostream& operator<<(std::ostream& _out, const IntraLayerLink& _link);

using LayeredGraph = boost::adjacency_list<boost::vecS, boost::vecS,
    boost::directedS, Node, Link, boost::no_property, boost::vecS>;

LayeredGraph getLayeredGraph(const Instance& _inst);

template <typename Graph>
ServicePath getServicePath(const Demand& _demand,
    const std::vector<boost::graph_traits<LayeredGraph>::edge_descriptor> _path,
    const std::vector<function_descriptor>& _chain, const Graph& _g) {
    ServicePath retval{{_demand.s}, {}, _chain, _demand.id};

    assert(_path.size() > 0);
    for (auto ite = _path.rbegin(), end = _path.rend(); ite != end; ++ite) {
        const auto ed = *ite;
        const auto v = target(ed, _g);

        if (_g[ed].isIntraLayer()) {
            retval.nPath.push_back(_g[v].original);
        } else {
            retval.locations.push_back(_g[v].original);
        }
    }
    assert(retval.nPath.front() == _demand.s);
    assert(retval.nPath.back() == _demand.t);
    return retval;
}

class Solution {
  public:
    Solution(const Instance& _inst,
        double _objValue = std::numeric_limits<double>::infinity(),
        double _lowerBound = std::numeric_limits<double>::infinity(),
        std::vector<ServicePath> _sPaths = {})
        : inst(&_inst)
        , objValue(_objValue)
        , lowerBound(_lowerBound)
        , sPaths(_sPaths) {}

    // Solution(const Instance& _inst, const nlohmann::json& _json)
    //     : inst(&_inst)
    //     , objValue(_json["objValue"])
    //     , lowerBound(_json["lowerBound"])
    //     , sPaths([&](){
    //         std::vector<ServicePath> retval;
    //         for(const auto& path : _json["paths"]) {

    //         }
    //     }()) {}

    ~Solution() = default;
    Solution(const Solution&) = default;
    Solution& operator=(const Solution&) = default;
    Solution(Solution&&) noexcept = default;
    Solution& operator=(Solution&&) noexcept = default;

    void setPaths(std::vector<ServicePath> _sPaths) { sPaths = _sPaths; }

    void save(
        std::ostream& _out, nlohmann::json _initInfo = nlohmann::json()) const {
        _initInfo["instanceName"] = inst->network[boost::graph_bundle];
        _initInfo["objValue"] = objValue;
        _initInfo["lowerBound"] = lowerBound;
        _initInfo["paths"] = nlohmann::json::array();

        for (const auto& [nPath, locations, function_indexes, demandID] :
            sPaths) {
            _initInfo["paths"].push_back(
                {{{"nPath", nPath}, {"locations", locations},
                    {"function_indexes", function_indexes},
                    {"demandID", demandID}}});
        }
        _out << std::setprecision(6) << _initInfo.dump(-1);
    }

    const std::vector<ServicePath>& getServicePaths() const { return sPaths; }
    const Instance& getInstance() const { return *inst; }
    double getObjValue() const { return objValue; }
    double getLowerBound() const { return lowerBound; }
    void setLowerBound(double _lowerBound) { lowerBound = _lowerBound; }
    void setObjValue(double _objValue) { objValue = _objValue; }
    double getEpsilon() const { return (objValue - lowerBound) / lowerBound; }

  private:
    const Instance* inst;
    double objValue;
    double lowerBound;
    std::vector<ServicePath> sPaths;
};

SFC::Partial::Instance loadInstance(std::istream& _in);

using ::operator<<;

} // namespace SFC::Partial

#endif
