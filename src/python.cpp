#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "SFC.hpp"
#include "algorithm.hpp"

PYBIND11_MODULE(_core, mod) {
    mod.doc() = "SFC module";
    mod.def(
        "crgAndBranchAndBound",
        [&](const SFC::Instance& _inst, std::size_t _nbLicences,
            std::size_t _nbThreads, double _timeLimit, int _seed,
            SFC::CRGBnBVisitor& _visitor) {
            std::seed_seq seed{{_seed}};
            std::mt19937 gen(seed);
            return SFC::columnAndRowGenerationBranchAndBound(_inst, _nbLicences,
                SFC::twoPhaseHeuristic(_inst, _nbLicences, gen, _nbThreads),
                _nbThreads, _timeLimit, _visitor);
        },
        pybind11::arg("instance"), pybind11::arg("nb_licenses"),
        pybind11::arg("nb_threads"), pybind11::arg("time_limit"),
        pybind11::arg("seed"), pybind11::arg("visitor"),
        pybind11::doc(
            "Solve the problem with column and row generation followed by "
            "branch-and-bound"));
    mod.def(
        "cgAndBranchAndBound",
        [&](const SFC::Instance& _inst, std::size_t _nbLicences,
            std::size_t _nbThreads, double _timeLimit, int _seed,
            SFC::CGBnBVisitor& _visitor) {
            std::seed_seq seed{{_seed}};
            std::mt19937 gen(seed);
            return SFC::columnGenerationBranchAndBound(_inst, _nbLicences,
                SFC::twoPhaseHeuristic(_inst, _nbLicences, gen, _nbThreads),
                _nbThreads, _timeLimit, _visitor);
        },
        pybind11::arg("instance"), pybind11::arg("nb_licenses"),
        pybind11::arg("nb_threads"), pybind11::arg("time_limit"),
        pybind11::arg("seed"), pybind11::arg("visitor"),
        pybind11::doc("Solve the problem with column generation followed by "
                      "branch-and-bound"));

    pybind11::class_<SFC::Solution>(mod, "Solution")
        .def("getServicePaths", &SFC::Solution::getServicePaths);

    pybind11::class_<SFC::ServicePath>(mod, "ServicePath")
        .def_readwrite("path", &SFC::ServicePath::nPath)
        .def_readwrite("locations", &SFC::ServicePath::locations)
        .def_readwrite("id", &SFC::ServicePath::demand);

    pybind11::class_<SFC::Instance>(mod, "Instance")
        .def(
            pybind11::init<>(
                [](const SFC::Network& _graph,
                    const std::vector<double>& _linkCapacities,
                    const std::vector<double>& _linkDelays,
                    const std::vector<double>& _cpuCapacities,
                    const std::vector<double>& _ramCapacities,
                    const std::vector<double>& _storageCapacities,
                    const std::vector<SFC::Demand>& _demands) -> SFC::Instance {
                    return SFC::Instance{_graph, _linkCapacities, _linkDelays,
                        _cpuCapacities, _ramCapacities, _storageCapacities,
                        _demands};
                }),
            pybind11::arg("network"), pybind11::arg("link_capacities"),
            pybind11::arg("link_delays"), pybind11::arg("cpu_capacities"),
            pybind11::arg("ram_capacities"),
            pybind11::arg("storage_capacities"), pybind11::arg("demands"))
        .def_readwrite("network", &SFC::Instance::network)
        .def_readwrite("demands", &SFC::Instance::demands)
        .def_readwrite("maxChainSize", &SFC::Instance::maxChainSize);

    mod.def("loadInstance", &SFC::loadInstance);

    pybind11::class_<SFC::CGBnBVisitor>(mod, "CGBnBVisitor")
        .def(pybind11::init<>())
        .def_readwrite("cgVistor", &SFC::CGBnBVisitor::cgVisitor);
    pybind11::class_<SFC::CRGBnBVisitor>(mod, "CRGBnBVisitor")
        .def(pybind11::init<>());

    pybind11::class_<SFC::Network>(mod, "Network")
        .def(pybind11::init<>(
                 [](const std::vector<std::pair<std::size_t, std::size_t>>&
                         _edgeList,
                     std::size_t _order) {
                     SFC::Network g(_order);
                     for (std::size_t e = 0; e < _edgeList.size(); ++e) {
                         const auto& [ed, b] = add_edge(
                             _edgeList[e].first, _edgeList[e].second, g);
                         g[ed].id = e;
                     }
                     return g;
                 }),
            pybind11::arg("edge_list"), pybind11::arg("order"))
        .def("order",
            [](const SFC::Network* _this) { return num_vertices(*_this); })
        .def("size",
            [](const SFC::Network* _this) { return num_edges(*_this); });

    pybind11::class_<SFC::Demand>(mod, "Demand")
        .def(pybind11::init<std::size_t, std::size_t, std::size_t, double,
                 std::vector<std::size_t>, std::vector<double>,
                 std::vector<double>, std::vector<double>, double>(),
            pybind11::arg("id"), pybind11::arg("source"), pybind11::arg("dest"),
            pybind11::arg("bandwidth"), pybind11::arg("functions"),
            pybind11::arg("cpu_charges"), pybind11::arg("ram_charges"),
            pybind11::arg("storage_charges"), pybind11::arg("delay"));
}
