#include "models.hpp"
#include <algorithm>
#include <boost/graph/bellman_ford_shortest_paths.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graph_utility.hpp>
#include <limits>
#include <numeric>

namespace SFC {

std::vector<IloRange> getOnePathPerDemandConstraints(
    std::size_t _nbDemands, IloModel& _model) {
    std::vector<IloRange> onePathCons(_nbDemands);
    std::generate_n(onePathCons.begin(), _nbDemands, [&]() {
        return IloAdd(_model, IloRange(_model.getEnv(), 1.0, IloInfinity));
    });
    return onePathCons;
}

// NOLINTBEGIN
ShortestPathPerDemandLp::ShortestPathPerDemandLp(const Instance& _inst)
    : m_inst(&_inst)
    , n(num_vertices(m_inst->network))
    , m(num_edges(m_inst->network))
    , nbLayers(m_inst->maxChainSize + 1)
    , m_model(m_env)
    , m_obj(IloAdd(m_model, IloObjective(m_env)))
    , m_a([&]() {
        boost::multi_array<IloNumVar, 2> a(boost::extents[long(
            num_vertices(m_inst->network))][long(nbLayers - 1)]);
        for (const auto u :
            boost::make_iterator_range(vertices(m_inst->network))) {
            for (std::size_t j = 0; j < nbLayers - 1; ++j) {
                a[long(u)][long(j)] =
                    IloAdd(m_model, IloNumVar(m_env, 0, 1, ILOBOOL));
            }
        }
        return a;
    }())
    , m_f([&]() {
        boost::multi_array<IloNumVar, 2> f(
            boost::extents[long(m)][long(nbLayers)]);
        std::generate_n(f.data(), f.num_elements(),
            [&]() { return IloAdd(m_model, IloNumVar(m_env, 0, 1, ILOBOOL)); });
        return f;
    }())
    , m_flowConsCons([&]() {
        boost::multi_array<IloRange, 2> flowConsCons(
            boost::extents[long(n)][long(nbLayers)]);
        for (const auto u :
            boost::make_iterator_range(vertices(m_inst->network))) {
            for (std::size_t j = 0; j < nbLayers; ++j) {
                IloExpr expr(m_env);
                for (const auto ed :
                    boost::make_iterator_range(out_edges(u, m_inst->network))) {
                    expr += m_f[long(m_inst->network[ed].id)][long(j)];
                }
                for (const auto ed :
                    boost::make_iterator_range(in_edges(u, m_inst->network))) {
                    expr += -m_f[long(m_inst->network[ed].id)][long(j)];
                }
                if (j > 0) {
                    expr += -m_a[long(u)][long(j - 1)];
                }
                if (j < nbLayers - 1) {
                    expr += m_a[long(u)][long(j)];
                }
                flowConsCons[long(u)][long(j)] =
                    IloAdd(m_model, IloRange(m_env, 0.0, expr, 0.0));
                expr.end();
            }
        }
        return flowConsCons;
    }())
    , m_linkCapaCons(getLinkCapacityConstraints(*m_inst, m_model))
    , m_cpuCapaCons(getCPUCapacityConstraints(*m_inst, m_model))
    , m_ramCapaCons(getRAMCapacityConstraints(*m_inst, m_model))
    , m_storageCapaCons(getStorageCapacityConstraints(*m_inst, m_model))
    , m_solver([&]() {
        IloCplex solver(m_env);
        solver.setParam(IloCplex::Param::Threads, 1);
        solver.setOut(solver.getEnv().getNullStream());
        solver.setWarning(solver.getEnv().getNullStream());
        return solver;
    }()) {}
// NOLINTEND

bool ShortestPathPerDemandLp::solve() { return m_solver.solve() != 0; }

double ShortestPathPerDemandLp::getObjValue() const {
    return m_solver.getObjValue();
}

void ShortestPathPerDemandLp::resetBounds() {
    m_solver.clearModel();
    const auto& [id, s, t, d, functions, cpuCharges, ramCharges, storageCharges,
        delay] = m_inst->demands[m_demandID];
    m_flowConsCons[long(s)][0].setBounds(0.0, 0.0);
    m_flowConsCons[long(t)][long(functions.size())].setBounds(0.0, 0.0);
}

void ShortestPathPerDemandLp::setPricingID(const std::size_t _demandID) {
    resetBounds();
    m_demandID = _demandID;
    const auto& [id, s, t, d, functions, cpuCharges, ramCharges, storageCharges,
        delay] = m_inst->demands[m_demandID];
    m_flowConsCons[long(s)][0].setBounds(1.0, 1.0);
    m_flowConsCons[long(t)][long(functions.size())].setBounds(-1.0, -1.0);
    IloExpr expr(m_env);
    for (const auto ed : boost::make_iterator_range(edges(m_inst->network))) {
        expr.clear();
        for (std::size_t j = 0; j <= functions.size(); ++j) {
            expr += m_f[long(m_inst->network[ed].id)][long(j)] * d;
        }
        m_linkCapaCons[m_inst->network[ed].id].setExpr(expr);
    }
    for (const auto u : boost::make_iterator_range(vertices(m_inst->network))) {
        expr.clear();
        for (std::size_t j = 0; j < functions.size(); ++j) {
            expr += m_a[long(u)][long(j)]
                    * m_inst->demands[m_demandID].cpuCharges[j];
        }
        m_cpuCapaCons[u].setExpr(expr);
    }
    for (const auto u : boost::make_iterator_range(vertices(m_inst->network))) {
        expr.clear();
        for (std::size_t j = 0; j < functions.size(); ++j) {
            expr += m_a[long(u)][long(j)]
                    * m_inst->demands[m_demandID].ramCharges[j];
        }
        m_ramCapaCons[u].setExpr(expr);
    }
    for (const auto u : boost::make_iterator_range(vertices(m_inst->network))) {
        expr.clear();
        for (std::size_t j = 0; j < functions.size(); ++j) {
            expr += m_a[long(u)][long(j)]
                    * m_inst->demands[m_demandID].storageCharges[j];
        }
        m_storageCapaCons[u].setExpr(expr);
    }
    expr.end();
}

ServicePath ShortestPathPerDemandLp::getColumn() const {
    const auto& funcs = m_inst->demands[m_demandID].functions;
    const auto& t = m_inst->demands[m_demandID].t;
    auto u = m_inst->demands[m_demandID].s;
    ServicePath retval{{}, {}, m_demandID};
    std::size_t j = 0;
    while (u != t || j < funcs.size()) {
        if (j < funcs.size()
            && epsilon_equal<double>()(
                m_solver.getValue(m_a[long(u)][long(j)]), IloTrue)) {
            retval.locations.push_back(u);
            ++j;
        } else {
            const auto& [first, last] = out_edges(u, m_inst->network);
            auto ite = std::find_if(first, last, [&](const auto _ed) {
                return epsilon_equal<double>()(
                    m_solver.getValue(
                        m_f[long(m_inst->network[_ed].id)][long(j)]),
                    IloTrue);
            });
            retval.nPath.push_back(*ite);
            u = target(*ite, m_inst->network);
        }
    }
    SFC_ASSERT(target(retval.nPath.back(), m_inst->network) == t);
    return retval;
}

std::vector<boost::multi_array<IloNumVar, 2>> buildFlowVars(
    const Instance& _inst, IloModel& _model) {
    std::vector<boost::multi_array<IloNumVar, 2>> retval(_inst.demands.size());
    for (std::size_t i = 0; i < _inst.demands.size(); ++i) {
        for (const auto ed : boost::make_iterator_range(edges(_inst.network))) {
            retval[i] = boost::multi_array<IloNumVar, 2>(boost::extents[long(
                num_edges(_inst.network))][long(_inst.maxChainSize + 1)]);
            for (std::size_t j = 0; j <= _inst.demands[i].functions.size();
                 ++j) {
                retval[i][long(_inst.network[ed].id)][long(j)] = IloAdd(
                    _model, IloNumVar(_model.getEnv(), 0.0, 1.0, ILOBOOL));
            }
        }
    }
    return retval;
}

std::vector<boost::multi_array<IloNumVar, 2>> buildLocationVars(
    const Instance& _inst, IloModel& _model) {
    std::vector<boost::multi_array<IloNumVar, 2>> retval(_inst.demands.size());
    for (std::size_t i = 0; i < _inst.demands.size(); ++i) {
        retval[i] = boost::multi_array<IloRange, 2>(
            boost::extents[long(num_vertices(_inst.network))]
                          [long(_inst.demands[i].functions.size())]);
        for (const auto u :
            boost::make_iterator_range(vertices(_inst.network))) {
            for (std::size_t j = 0; j < _inst.demands[i].functions.size();
                 ++j) {
                retval[i][long(u)][long(j)] = IloAdd(
                    _model, IloNumVar(_model.getEnv(), 0.0, 1.0, ILOBOOL));
            }
        }
    }
    return retval;
}

IloObjective buildMinimizeBandwidthUsageObjective(const Instance& _inst,
    const std::vector<boost::multi_array<IloNumVar, 2>>& _flowVars,
    IloModel& _model) {
    IloExpr expr(_model.getEnv());
    for (const auto ed : boost::make_iterator_range(edges(_inst.network))) {
        for (std::size_t i = 0; i < _inst.demands.size(); ++i) {
            for (std::size_t j = 0; j <= _inst.demands[i].functions.size();
                 ++j) {
                expr += _inst.demands[i].bandwidth
                        * _flowVars[i][long(_inst.network[ed].id)][long(j)];
            }
        }
    }
    const IloObjective retval = IloObjective(_model.getEnv(), expr);
    expr.end();
    return IloAdd(_model, retval);
}

std::vector<boost::multi_array<IloRange, 2>> buildFlowConservationConstraints(
    const Instance& _inst,
    const std::vector<boost::multi_array<IloNumVar, 2>>& _flowVars,
    const std::vector<boost::multi_array<IloNumVar, 2>>& _locationVars,
    IloModel& _model) {
    std::vector<boost::multi_array<IloRange, 2>> retval(_inst.demands.size());
    IloExpr expr(_model.getEnv());
    for (const auto i : boost::irange(_inst.demands.size())) {
        retval[i] = boost::multi_array<IloRange, 2>(
            boost::extents[long(num_vertices(_inst.network))]
                          [long(_inst.demands[i].functions.size())]);
        for (const auto& [u, j] :
            boost::combine(boost::irange(num_vertices(_inst.network)),
                boost::irange(_inst.demands[i].functions.size()))) {
            expr.clear();
            // Outgoing flows
            boost::accumulate(out_edges(u, _inst.network), expr,
                [&, f = j](auto _acc, auto _ed) {
                    return _acc
                           + _flowVars[i][long(_inst.network[_ed].id)][long(f)];
                });
            // Incoming flows
            boost::accumulate(in_edges(u, _inst.network), expr,
                [&, f = j](auto _acc, auto _ed) {
                    return _acc
                           - _flowVars[i][long(_inst.network[_ed].id)][long(f)];
                });
            // Next layer
            if (j < _inst.demands[i].functions.size()) {
                expr += _locationVars[i][long(u)][long(j)];
            }
            // Previous layer
            if (j >= 1) {
                expr += -_locationVars[i][long(u)][long(j - 1)];
            }
            retval[i][long(u)][long(j)] = IloAdd(_model, expr == 0.0);
        }
    }
    for (const auto& [id, s, t, d, functions, cpuCharges, ramCharges,
             storageCharges, delay] : _inst.demands) {
        retval[id][long(s)][0].setBounds(1.0, 1.0);
        retval[id][long(t)][long(functions.size())].setBounds(-1.0, -1.0);
    }
    expr.end();
    return retval;
}

std::vector<IloRange> buildLinkCapacityConstraints(const Instance& _inst,
    const std::vector<boost::multi_array<IloNumVar, 2>>& _flowVars,
    IloModel& _model) {
    std::vector<IloRange> linkCapaConstraints(num_edges(_inst.network));
    IloExpr expr(_model.getEnv());
    for (const auto ed : boost::make_iterator_range(edges(_inst.network))) {
        expr.clear();
        for (std::size_t i = 0; i < _inst.demands.size(); ++i) {
            for (std::size_t j = 0; j <= _inst.demands[i].functions.size();
                 ++j) {
                expr += _inst.demands[i].bandwidth
                        * _flowVars[i][long(_inst.network[ed].id)][long(j)];
            }
        }
        linkCapaConstraints[_inst.network[ed].id] =
            IloAdd(_model, expr <= _inst.linkCapacity[_inst.network[ed].id]);
    }
    expr.end();
    return linkCapaConstraints;
}

std::vector<IloRange> buildCPUCapacityConstraints(const Instance& _inst,
    const std::vector<boost::multi_array<IloNumVar, 2>>& _locationVars,
    IloModel& _model) {
    std::vector<IloRange> cpuCapaConstraints(num_vertices(_inst.network));
    IloExpr expr(_model.getEnv());
    for (const auto u : boost::make_iterator_range(vertices(_inst.network))) {
        expr.clear();
        for (std::size_t i = 0; i < _inst.demands.size(); ++i) {
            for (std::size_t j = 0; j < _inst.demands[i].functions.size();
                 ++j) {
                expr += _inst.demands[i].cpuCharges[j]
                        * _locationVars[i][long(u)][long(j)];
            }
        }
        cpuCapaConstraints[u] = IloAdd(_model, expr <= _inst.cpuCapacity[u]);
    }
    expr.end();
    return cpuCapaConstraints;
}

// #####################################################################
// #####################################################################
// #####################################################################

IloModel PathExtendedModel::getIntegerModel() const {
    IloModel result(m_model);
    for (const auto& cols : m_columns) {
        for (const auto& col : cols) {
            result.add(
                IloConversion(m_model.getEnv(), col.var, IloNumVar::Bool));
        }
    }
    for (const auto& dummy : m_dummyPaths) {
        result.add(dummy == 0);
    }
    return result;
}

void PathExtendedModel::addColumn(const PathExtendedModel::ColumnType& _col) {
    const auto& [nPath, locations, demandID] = _col.path;
    IloNumColumn col = m_onePathCons[demandID](1.0) + m_obj(_col.obj);
    col += std::accumulate(_col.linkCharges.begin(), _col.linkCharges.end(),
        IloNumColumn(m_model.getEnv()),
        [&](IloNumColumn _acc, const std::pair<std::size_t, double>& _edgeBw) {
            return _acc += m_linkCapasCons[_edgeBw.first](_edgeBw.second);
        });
    col += std::accumulate(_col.cpuCharges.begin(), _col.cpuCharges.end(),
        IloNumColumn(m_model.getEnv()),
        [&](IloNumColumn _acc,
            const std::pair<std::size_t, double>& _cpuCharge) {
            return _acc += m_cpuCapasCons[_cpuCharge.first](_cpuCharge.second);
        });
    col += std::accumulate(_col.ramCharges.begin(), _col.ramCharges.end(),
        IloNumColumn(m_model.getEnv()),
        [&](IloNumColumn _acc,
            const std::pair<std::size_t, double>& _ramCharge) {
            return _acc += m_ramCapasCons[_ramCharge.first](_ramCharge.second);
        });
    col += std::accumulate(_col.storageCharges.begin(),
        _col.storageCharges.end(), IloNumColumn(m_model.getEnv()),
        [&](IloNumColumn _acc,
            const std::pair<std::size_t, double>& _storageCharge) {
            return _acc += m_storageCapasCons[_storageCharge.first](
                       _storageCharge.second);
        });
    m_columns[demandID].emplace_back(
        IloNumVar(col, 0.0, IloInfinity, IloNumVar::Type::Float,
            fmt::format(
                "path(k~{})(i~{})", demandID, m_columns[demandID].size())
                .c_str()),
        _col);
    IloAdd(m_model, m_columns[demandID].back().var);
}

double PathExtendedModel::getReducedCost(
    const PathExtendedModel::ColumnType& _col, const DualValues& _dualValues) {
    const auto& [nPath, locations, demandID] = _col.path;
    return -_dualValues.m_onePathDuals[demandID]
           + std::accumulate(_col.linkCharges.begin(), _col.linkCharges.end(),
               0.0,
               [&](const auto& _acc, const auto& _edgeBw) {
                   return _acc
                          + _edgeBw.second
                                * (1
                                    + _dualValues
                                          .m_linkCapasDuals[_edgeBw.first]);
               })
           + std::accumulate(_col.cpuCharges.begin(), _col.cpuCharges.end(),
               0.0,
               [&](const auto& _acc, const auto& _cpuCharge) {
                   return _acc
                          + _dualValues.m_cpuCapasDuals[_cpuCharge.first]
                                * _cpuCharge.second;
               })
           + std::accumulate(_col.ramCharges.begin(), _col.ramCharges.end(),
               0.0,
               [&](const auto& _acc, const auto& _ramCharge) {
                   return _acc
                          + _dualValues.m_ramCapasDuals[_ramCharge.first]
                                * _ramCharge.second;
               })
           + std::accumulate(_col.storageCharges.begin(),
               _col.storageCharges.end(), 0.0,
               [&](const auto& _acc, const auto& _storageCharge) {
                   return _acc
                          + _dualValues
                                    .m_storageCapasDuals[_storageCharge.first]
                                * _storageCharge.second;
               });
}

PathExtendedModel::DualValues PathExtendedModel::getDualValues(
    const IloCplex& _solver) const {
    PathExtendedModel::DualValues retval;

    retval.m_linkCapasDuals.resize(m_linkCapasCons.size());
    retval.m_cpuCapasDuals.resize(m_cpuCapasCons.size());
    retval.m_ramCapasDuals.resize(m_ramCapasCons.size());
    retval.m_storageCapasDuals.resize(m_storageCapasCons.size());
    retval.m_onePathDuals.resize(m_onePathCons.size());

    std::transform(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        retval.m_linkCapasDuals.begin(),
        [&](const IloRange& _cons) { return -_solver.getDual(_cons); });
    SFC_ASSERT(std::all_of(retval.m_linkCapasDuals.begin(),
        retval.m_linkCapasDuals.end(),
        [](double _val) { return !epsilon_less<double>()(_val, 0.0); }));

    std::transform(m_cpuCapasCons.begin(), m_cpuCapasCons.end(),
        retval.m_cpuCapasDuals.begin(),
        [&](const IloRange& _cons) { return -_solver.getDual(_cons); });
    SFC_ASSERT(std::all_of(retval.m_cpuCapasDuals.begin(),
        retval.m_cpuCapasDuals.end(),
        [](double _val) { return !epsilon_less<double>()(_val, 0.0); }));

    std::transform(m_storageCapasCons.begin(), m_storageCapasCons.end(),
        retval.m_storageCapasDuals.begin(),
        [&](const IloRange& _cons) { return -_solver.getDual(_cons); });
    SFC_ASSERT(std::all_of(retval.m_storageCapasDuals.begin(),
        retval.m_storageCapasDuals.end(),
        [](double _val) { return !epsilon_less<double>()(_val, 0.0); }));

    std::transform(m_ramCapasCons.begin(), m_ramCapasCons.end(),
        retval.m_ramCapasDuals.begin(),
        [&](const IloRange& _cons) { return -_solver.getDual(_cons); });
    SFC_ASSERT(std::all_of(retval.m_ramCapasDuals.begin(),
        retval.m_ramCapasDuals.end(),
        [](double _val) { return !epsilon_less<double>()(_val, 0.0); }));

    std::transform(m_onePathCons.begin(), m_onePathCons.end(),
        retval.m_onePathDuals.begin(),
        [&](const IloRange& _cons) { return _solver.getDual(_cons); });
    SFC_ASSERT(
        std::all_of(retval.m_onePathDuals.begin(), retval.m_onePathDuals.end(),
            [](double _val) { return !epsilon_less<double>()(_val, 0.0); }));
    return retval;
}

bool PathExtendedModel::isImprovingColumn(
    const ColumnType& _col, const DualValues& _dualValues) {
    return epsilon_less<double>()(getReducedCost(_col, _dualValues), 0.0);
}

IloModel RoutingExtendedModel::getIntegerModel() const {
    IloModel result(m_model);
    for (const auto& col : m_columns) {
        result.add(IloConversion(m_model.getEnv(), col.var, IloNumVar::Bool));
    }
    for (const auto& dummy : m_dummyPaths) {
        result.add(dummy == 0);
    }
    return result;
}

void RoutingExtendedModel::addColumn(
    const RoutingExtendedModel::ColumnType& _col) {
    IloNumColumn col = m_obj(_col.obj);
    col += std::accumulate(_col.paths.begin(), _col.paths.end(), col,
        [&](IloNumColumn _acc, const auto& _sPath) {
            return _acc += m_onePathCons[_sPath.demand](1.0);
        });
    col += std::accumulate(_col.linkCharges.begin(), _col.linkCharges.end(),
        IloNumColumn(m_model.getEnv()),
        [&](IloNumColumn _acc, const std::pair<std::size_t, double>& _edgeBw) {
            return _acc +=
                   m_linkCapasConstraints[_edgeBw.first](_edgeBw.second);
        });
    col += std::accumulate(_col.cpuCharges.begin(), _col.cpuCharges.end(),
        IloNumColumn(m_model.getEnv()),
        [&](IloNumColumn _acc,
            const std::pair<std::size_t, double>& _cpuCharge) {
            return _acc +=
                   m_cpuCapasConstraints[_cpuCharge.first](_cpuCharge.second);
        });
    col += std::accumulate(_col.ramCharges.begin(), _col.ramCharges.end(),
        IloNumColumn(m_model.getEnv()),
        [&](IloNumColumn _acc,
            const std::pair<std::size_t, double>& _ramCharge) {
            return _acc +=
                   m_ramCapasConstraints[_ramCharge.first](_ramCharge.second);
        });
    col += std::accumulate(_col.storageCharges.begin(),
        _col.storageCharges.end(), IloNumColumn(m_model.getEnv()),
        [&](IloNumColumn _acc,
            const std::pair<std::size_t, double>& _storageCharge) {
            return _acc += m_storageCapasConstraints[_storageCharge.first](
                       _storageCharge.second);
        });
    m_columns.emplace_back(
        IloNumVar(col, 0.0, IloInfinity, IloNumVar::Type::Float,
            fmt::format("routing(i~{})", m_columns.size()).c_str()),
        _col);
    IloAdd(m_model, m_columns.back().var);
}

double RoutingExtendedModel::getReducedCost(
    const RoutingExtendedModel::ColumnType& _col,
    const DualValues& _dualValues) {
    return std::accumulate(_col.paths.begin(), _col.paths.end(), 0.0,
               [&](const auto _acc, const auto& _sPath) {
                   return _acc + -_dualValues.m_onePathDuals[_sPath.demand];
               })
           + std::accumulate(_col.linkCharges.begin(), _col.linkCharges.end(),
               0.0,
               [&](const auto& _acc, const auto& _edgeBw) {
                   return _acc
                          + _edgeBw.second
                                * (1
                                    + _dualValues
                                          .m_linkCapasDuals[_edgeBw.first]);
               })
           + std::accumulate(_col.cpuCharges.begin(), _col.cpuCharges.end(),
               0.0,
               [&](const auto& _acc, const auto& _cpuCharge) {
                   return _acc
                          + _dualValues.m_cpuCapasDuals[_cpuCharge.first]
                                * _cpuCharge.second;
               })
           + std::accumulate(_col.ramCharges.begin(), _col.ramCharges.end(),
               0.0,
               [&](const auto& _acc, const auto& _ramCharge) {
                   return _acc
                          + _dualValues.m_ramCapasDuals[_ramCharge.first]
                                * _ramCharge.second;
               })
           + std::accumulate(_col.storageCharges.begin(),
               _col.storageCharges.end(), 0.0,
               [&](const auto& _acc, const auto& _storageCharge) {
                   return _acc
                          + _dualValues
                                    .m_storageCapasDuals[_storageCharge.first]
                                * _storageCharge.second;
               });
}

double RoutingExtendedModel::getReducedCost(
    const PathExtendedModel::ColumnType& _col, const DualValues& _dualValues) {
    return -_dualValues.m_onePathDuals[_col.path.demand]
           + +std::accumulate(_col.linkCharges.begin(), _col.linkCharges.end(),
               0.0,
               [&](const auto& _acc, const auto& _edgeBw) {
                   return _acc
                          + _edgeBw.second
                                * (1
                                    + _dualValues
                                          .m_linkCapasDuals[_edgeBw.first]);
               })
           + std::accumulate(_col.cpuCharges.begin(), _col.cpuCharges.end(),
               0.0,
               [&](const auto& _acc, const auto& _cpuCharge) {
                   return _acc
                          + _dualValues.m_cpuCapasDuals[_cpuCharge.first]
                                * _cpuCharge.second;
               })
           + std::accumulate(_col.ramCharges.begin(), _col.ramCharges.end(),
               0.0,
               [&](const auto& _acc, const auto& _ramCharge) {
                   return _acc
                          + _dualValues.m_ramCapasDuals[_ramCharge.first]
                                * _ramCharge.second;
               })
           + std::accumulate(_col.storageCharges.begin(),
               _col.storageCharges.end(), 0.0,
               [&](const auto& _acc, const auto& _storageCharge) {
                   return _acc
                          + _dualValues
                                    .m_storageCapasDuals[_storageCharge.first]
                                * _storageCharge.second;
               });
}

RoutingExtendedModel::DualValues RoutingExtendedModel::getDualValues(
    const IloCplex& _solver) const {
    RoutingExtendedModel::DualValues retval;

    retval.m_linkCapasDuals.resize(m_linkCapasConstraints.size());
    retval.m_cpuCapasDuals.resize(m_cpuCapasConstraints.size());
    retval.m_ramCapasDuals.resize(m_ramCapasConstraints.size());
    retval.m_storageCapasDuals.resize(m_storageCapasConstraints.size());
    retval.m_onePathDuals.resize(m_onePathCons.size());

    std::transform(m_linkCapasConstraints.begin(), m_linkCapasConstraints.end(),
        retval.m_linkCapasDuals.begin(),
        [&](const IloRange& _cons) { return -_solver.getDual(_cons); });
    SFC_ASSERT(std::all_of(retval.m_linkCapasDuals.begin(),
        retval.m_linkCapasDuals.end(),
        [](double _val) { return !epsilon_less<double>()(_val, 0.0); }));

    std::transform(m_cpuCapasConstraints.begin(), m_cpuCapasConstraints.end(),
        retval.m_cpuCapasDuals.begin(),
        [&](const IloRange& _cons) { return -_solver.getDual(_cons); });
    SFC_ASSERT(std::all_of(retval.m_cpuCapasDuals.begin(),
        retval.m_cpuCapasDuals.end(),
        [](double _val) { return !epsilon_less<double>()(_val, 0.0); }));

    std::transform(m_ramCapasConstraints.begin(), m_ramCapasConstraints.end(),
        retval.m_ramCapasDuals.begin(),
        [&](const IloRange& _cons) { return -_solver.getDual(_cons); });
    SFC_ASSERT(std::all_of(retval.m_ramCapasDuals.begin(),
        retval.m_ramCapasDuals.end(),
        [](double _val) { return !epsilon_less<double>()(_val, 0.0); }));

    std::transform(m_storageCapasConstraints.begin(),
        m_storageCapasConstraints.end(), retval.m_storageCapasDuals.begin(),
        [&](const IloRange& _cons) { return -_solver.getDual(_cons); });
    SFC_ASSERT(std::all_of(retval.m_storageCapasDuals.begin(),
        retval.m_storageCapasDuals.end(),
        [](double _val) { return !epsilon_less<double>()(_val, 0.0); }));

    std::transform(m_onePathCons.begin(), m_onePathCons.end(),
        retval.m_onePathDuals.begin(),
        [&](const IloRange& _cons) { return _solver.getDual(_cons); });
    SFC_ASSERT(
        std::all_of(retval.m_onePathDuals.begin(), retval.m_onePathDuals.end(),
            [](double _val) { return !epsilon_less<double>()(_val, 0.0); }));
    return retval;
}

bool RoutingExtendedModel::isImprovingColumn(
    const ColumnType& _col, const DualValues& _dualValues) {
    return epsilon_less<double>()(getReducedCost(_col, _dualValues), 0.0);
}

bool RoutingExtendedModel::isImprovingColumn(
    const PathExtendedModel::ColumnType& _col, const DualValues& _dualValues) {
    return epsilon_less<double>()(getReducedCost(_col, _dualValues), 0.0);
}

void PathExtendedAlphaModel::addColumn(
    const PathExtendedModel::ColumnType& _col) {
    const auto& [nPath, locations, demandID] = _col.path;
    m_baseModel.addColumn(_col);
    for (std::size_t j = 0; j < locations.size(); ++j) {
        const auto u = locations[j];
        m_pathFuncCons[long(demandID)][long(u)][j].setLinearCoef(
            m_baseModel.getColumns()[demandID].back().var, 1.0);
    }
}

IloModel PathExtendedAlphaModel::getIntegerModel() const {
    IloModel result = m_baseModel.getIntegerModel();
    result.add(m_model);
    for (const auto& locationVar : m_locationsVars) {
        for (const auto& var : locationVar) {
            result.add(IloConversion(result.getEnv(), var, IloNumVar::Bool));
        }
    }
    return result;
}

[[nodiscard]] PathExtendedAlphaModel::DualValues
PathExtendedAlphaModel::getDualValues(const IloCplex& _solver) const {
    PathExtendedAlphaModel::DualValues retval;
    retval.baseDuals = m_baseModel.getDualValues(_solver);
    retval.pathFuncDuals.resize(boost::extents[long(
        m_pathFuncCons.shape()[0])][long(m_pathFuncCons.shape()[1])]);
    for (std::size_t i = 0; i < m_pathFuncCons.shape()[0]; ++i) {
        for (std::size_t u = 0; u < m_pathFuncCons.shape()[1]; ++u) {
            retval.pathFuncDuals[long(i)][long(u)].resize(
                m_pathFuncCons[long(i)][long(u)].size());
            for (std::size_t j = 0;
                 j < std::size(m_pathFuncCons[long(i)][long(u)]); ++j) {
                retval.pathFuncDuals[long(i)][long(u)][j] =
                    -_solver.getDual(m_pathFuncCons[long(i)][long(u)][j]);
            }
        }
    }
    SFC_ASSERT(std::ranges::all_of(
        std::span{retval.pathFuncDuals.data(), retval.pathFuncDuals.size()},
        [](const std::vector<double>& _duals) {
            return std::all_of(_duals.begin(), _duals.end(),
                [](double _val) { return !epsilon_less<double>()(_val, 0.0); });
        }));
    return retval;
}

double PathExtendedAlphaModel::getReducedCost(
    const PathExtendedModel::ColumnType& _col,
    const PathExtendedAlphaModel::DualValues& _dualValues) {
    const auto& [nPath, locations, demandID] = _col.path;
    double retval =
        PathExtendedModel::getReducedCost(_col, _dualValues.baseDuals);
    for (std::size_t j = 0; j < locations.size(); ++j) {
        retval +=
            _dualValues.pathFuncDuals[long(demandID)][long(locations[j])][j];
    }
    return retval;
}

bool PathExtendedAlphaModel::isImprovingColumn(
    const PathExtendedModel::ColumnType& _col, const DualValues& _dualValues) {
    return epsilon_less<double>()(getReducedCost(_col, _dualValues), 0.0);
}

void LazyPathExtendedAlphaModel::addColumn(
    const PathExtendedModel::ColumnType& _col) {
    const auto& [nPath, locations, demandID] = _col.path;
    m_baseModel.addColumn(_col);
    for (std::size_t j = 0; j < locations.size(); ++j) {
        const auto u = locations[j];
        if (auto ite = m_pathFuncCons.find({demandID, u, j, 0});
            ite != m_pathFuncCons.end()) {
            ite->second.setLinearCoef(
                m_baseModel.getColumns()[demandID].back().var, 1.0);
        }
    }
}

IloModel LazyPathExtendedAlphaModel::getIntegerModel() const {
    IloModel result = m_baseModel.getIntegerModel();
    result.add(m_model);
    for (const auto& locationVar : m_locationsVars) {
        for (const auto& var : locationVar) {
            result.add(IloConversion(result.getEnv(), var, IloNumVar::Bool));
        }
    }
    return result;
}

IloRangeArray LazyPathExtendedAlphaModel::getLazyConstraints(
    const std::vector<LazyPathExtendedAlphaModel::PathFunctionLinking>&
        _allPathFunctionLinkings) const {
    IloRangeArray retval(m_model.getEnv());
    for (const auto& _linking : _allPathFunctionLinkings) {
        retval.add(buildPathFunctionConstraint(_linking));
    }
    return retval;
}

[[nodiscard]] LazyPathExtendedAlphaModel::DualValues
LazyPathExtendedAlphaModel::getDualValues(const IloCplex& _solver) const {
    LazyPathExtendedAlphaModel::DualValues retval = {
        m_baseModel.getDualValues(_solver),
        std::unordered_map<PathFunctionLinking, double, TripletHash>{
            1, m_hasher}};
    for (const auto& [triplet, constraint] : m_pathFuncCons) {
        retval.pathFuncDuals[triplet] = -_solver.getDual(constraint);
    }
    SFC_ASSERT(std::all_of(retval.pathFuncDuals.begin(),
        retval.pathFuncDuals.end(), [](const auto& _pair) {
            return !epsilon_less<double>()(_pair.second, 0.0);
        }));
    return retval;
}

double LazyPathExtendedAlphaModel::getReducedCost(
    const PathExtendedModel::ColumnType& _col,
    const LazyPathExtendedAlphaModel::DualValues& _dualValues) {
    const auto& [nPath, locations, demandID] = _col.path;
    double retval =
        PathExtendedModel::getReducedCost(_col, _dualValues.baseDuals);
    for (std::size_t j = 0; j < locations.size(); ++j) {
        if (auto ite =
                _dualValues.pathFuncDuals.find({demandID, locations[j], j, 0});
            ite != _dualValues.pathFuncDuals.end()) {
            retval += ite->second;
        }
    }
    return retval;
}

bool LazyPathExtendedAlphaModel::isImprovingColumn(
    const PathExtendedModel::ColumnType& _col, const DualValues& _dualValues) {
    return epsilon_less<double>()(getReducedCost(_col, _dualValues), 0.0);
}

IloRange LazyPathExtendedAlphaModel::buildPathFunctionConstraint(
    const LazyPathExtendedAlphaModel::PathFunctionLinking& _linking) const {
    IloExpr expr = -m_locationsVars[long(_linking.u)][long(_linking.f)];
    for (const auto& col : m_baseModel.getColumns()[_linking.demandId]) {
        const auto& sPath = col.obj.path;
        if (sPath.locations[_linking.j] == _linking.u) {
            expr += col.var;
        }
    }
    return {m_model.getEnv(), -IloInfinity, expr, 0.0,
        fmt::format("pathFuncCons(i~{}, u~{}, j~{})", _linking.demandId,
            _linking.j, _linking.u)
            .c_str()};
}

void LazyPathExtendedAlphaModel::enablePathFunctionConstraint(
    const LazyPathExtendedAlphaModel::PathFunctionLinking& _linking) {
    m_pathFuncCons[_linking] =
        IloAdd(m_model, buildPathFunctionConstraint(_linking));
}

LocalisationPerFunction::LocalisationPerFunction(
    const Instance& _instance, std::size_t _nbLicenses)
    : m_instance(_instance)
    , m_model(m_env)
    , m_x([&]() {
        IloNumVarArray x(
            m_env, static_cast<IloInt>(num_vertices(m_instance.network)
                                       * m_instance.demands.size()
                                       * m_instance.maxChainSize));
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance.network))) {
            for (std::size_t i = 0; i < m_instance.demands.size(); ++i) {
                for (std::size_t j = 0;
                     j < m_instance.demands[i].functions.size(); ++j) {
                    x[getIndexX(u, i, j)] =
                        IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                    setIloName(x[getIndexX(u, i, j)],
                        "x" + toString(std::make_pair(i, j)));
                }
            }
        }
        return x;
    }())
    , m_b([&]() {
        IloNumVarArray b(m_env,
            static_cast<IloInt>(
                num_vertices(m_instance.network) * m_instance.nbFunctions));
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance.network))) {
            for (std::size_t f = 0; f < m_instance.nbFunctions; ++f) {
                b[getIndexB(u, f)] =
                    IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                setIloName(
                    b[getIndexB(u, f)], "b" + toString(std::make_pair(u, f)));
            }
        }
        return b;
    }())
    , m_obj([&]() {
        boost::multi_array<int, 2> distMat(
            boost::extents[static_cast<long>(num_vertices(m_instance.network))]
                          [static_cast<long>(
                              num_vertices(m_instance.network))]);
        std::fill_n(distMat.data(), distMat.num_elements(), 0);
        const auto oneHop =
            boost::make_function_property_map<Network::edge_descriptor, int>(
                [](auto&&) -> int { return 1; });
        johnson_all_pairs_shortest_paths(
            m_instance.network, distMat, boost::weight_map(oneHop));
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance.network))) {
            for (std::size_t i = 0; i < _instance.demands.size(); ++i) {
                for (std::size_t j = 0;
                     j < m_instance.demands[i].functions.size(); ++j) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                    vals.add(distMat[static_cast<long>(m_instance.demands[i].t)]
                                    [static_cast<long>(u)]
                             + distMat[static_cast<long>(u)][static_cast<long>(
                                 m_instance.demands[u].t)]);
                }
            }
        }
        auto obj = IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
        vars.end();
        vals.end();
        return obj;
    }())
    , m_oneNodePerDemands([&]() {
        IloRangeArray oneNodePerDemands(
            m_env, static_cast<IloInt>(
                       m_instance.demands.size() * m_instance.maxChainSize));
        for (std::size_t i = 0; i < m_instance.demands.size(); ++i) {
            for (std::size_t j = 0; j < m_instance.demands[i].functions.size();
                 ++j) {
                IloNumVarArray vars(m_env);
                for (const auto u :
                    boost::make_iterator_range(vertices(m_instance.network))) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                }
                oneNodePerDemands[static_cast<IloInt>(
                    m_instance.demands.size() * j + i)] =
                    IloAdd(m_model, IloRange(m_env, 0.0, IloSum(vars), 0.0));
                vars.end();
            }
        }
        return oneNodePerDemands;
    }())
    , m_cpuCapacities([&]() {
        IloRangeArray cpuCapacities(
            m_env, static_cast<IloInt>(num_vertices(m_instance.network)));
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance.network))) {
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (std::size_t i = 0; i < m_instance.demands.size(); ++i) {
                for (std::size_t j = 0;
                     j < m_instance.demands[i].functions.size(); ++j) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                    vals.add(m_instance.demands[i].cpuCharges[j]);
                }
            }
            cpuCapacities[static_cast<IloInt>(u)] =
                IloAdd(m_model, IloRange(m_env, 0.0, IloScalProd(vars, vals),
                                    m_instance.cpuCapacity[u]));
            vars.end();
            vals.end();
        }
        return cpuCapacities;
    }())
    , m_ramCapacities([&]() {
        IloRangeArray ramCapacities(
            m_env, static_cast<IloInt>(num_vertices(m_instance.network)));
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance.network))) {
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (std::size_t i = 0; i < m_instance.demands.size(); ++i) {
                for (std::size_t j = 0;
                     j < m_instance.demands[i].functions.size(); ++j) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                    vals.add(m_instance.demands[i].ramCharges[j]);
                }
            }
            ramCapacities[static_cast<IloInt>(u)] =
                IloAdd(m_model, IloRange(m_env, 0.0, IloScalProd(vars, vals),
                                    m_instance.ramCapacity[u]));
            vars.end();
            vals.end();
        }
        return ramCapacities;
    }())
    , m_storageCapacities([&]() {
        IloRangeArray storageCapacities(
            m_env, static_cast<IloInt>(num_vertices(m_instance.network)));
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance.network))) {
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (std::size_t i = 0; i < m_instance.demands.size(); ++i) {
                for (std::size_t j = 0;
                     j < m_instance.demands[i].functions.size(); ++j) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                    vals.add(m_instance.demands[i].storageCharges[j]);
                }
            }
            storageCapacities[static_cast<IloInt>(u)] =
                IloAdd(m_model, IloRange(m_env, 0.0, IloScalProd(vars, vals),
                                    m_instance.storageCapacity[u]));
            vars.end();
            vals.end();
        }
        return storageCapacities;
    }())
    , m_nbLicenses([&]() {
        IloRangeArray nbLicenses(
            m_env, static_cast<IloInt>(m_instance.nbFunctions));
        for (std::size_t f = 0; f < m_instance.nbFunctions; ++f) {
            IloNumVarArray vars(m_env);
            for (const auto u :
                boost::make_iterator_range(vertices(m_instance.network))) {
                vars.add(m_b[getIndexB(u, f)]);
            }
            nbLicenses[static_cast<IloInt>(f)] = IloAdd(
                m_model, IloRange(m_env, static_cast<IloNum>(_nbLicenses),
                             IloSum(vars), static_cast<IloNum>(_nbLicenses)));
            vars.end();
        }
        return nbLicenses;
    }())
    , m_demandNodeLinkage([&]() {
        IloRangeArray demandNodeLinkage(
            m_env, static_cast<IloInt>(num_vertices(m_instance.network)
                                       * m_instance.demands.size()
                                       * m_instance.maxChainSize));
        for (const auto u :
            boost::make_iterator_range(vertices(m_instance.network))) {
            for (std::size_t i = 0; i < m_instance.demands.size(); ++i) {
                for (std::size_t j = 0;
                     j < m_instance.demands[i].functions.size(); ++j) {
                    const auto f = m_instance.demands[i].functions[j];
                    demandNodeLinkage[getIndexX(u, i, j)] = IloAdd(m_model,
                        IloRange(m_env, 0.0,
                            m_b[getIndexB(u, f)] - m_x[getIndexX(u, i, j)],
                            IloInfinity));
                }
            }
        }
        return demandNodeLinkage;
    }())
    , m_solver([&]() {
        IloCplex solver(m_model);
        for (IloInt i = 0; i < m_b.getSize(); ++i) {
            solver.setPriority(m_b[i], 2);
        }
        return solver;
    }()) {}

bool LocalisationPerFunction::solve() { return m_solver.solve(); }

double LocalisationPerFunction::getCPUUsedCapacities(
    const Network::vertex_descriptor& _u) {
    IloNumArray x(m_env);
    m_solver.getValues(x, m_x);

    double usedCapa = 0.0;
    for (std::size_t i = 0; i < m_instance.demands.size(); ++i) {
        for (std::size_t j = 0; j < m_instance.demands[i].functions.size();
             ++j) {
            if (epsilon_equal<double>()(x[getIndexX(_u, i, j)], IloTrue)) {
                usedCapa += m_instance.demands[i].cpuCharges[j];
            }
        }
    }
    x.end();
    return usedCapa;
}

double LocalisationPerFunction::getRAMUsedCapacities(
    const Network::vertex_descriptor& _u) {
    IloNumArray x(m_env);
    m_solver.getValues(x, m_x);

    double usedCapa = 0.0;
    for (std::size_t i = 0; i < m_instance.demands.size(); ++i) {
        for (std::size_t j = 0; j < m_instance.demands[i].functions.size();
             ++j) {
            if (epsilon_equal<double>()(x[getIndexX(_u, i, j)], IloTrue)) {
                usedCapa += m_instance.demands[i].ramCharges[j];
            }
        }
    }
    x.end();
    return usedCapa;
}

double LocalisationPerFunction::getStorageUsedCapacities(
    const Network::vertex_descriptor& _u) {
    IloNumArray x(m_env);
    m_solver.getValues(x, m_x);

    double usedCapa = 0.0;
    for (std::size_t i = 0; i < m_instance.demands.size(); ++i) {
        for (std::size_t j = 0; j < m_instance.demands[i].functions.size();
             ++j) {
            if (epsilon_equal<double>()(x[getIndexX(_u, i, j)], IloTrue)) {
                usedCapa += m_instance.demands[i].storageCharges[j];
            }
        }
    }
    x.end();
    return usedCapa;
}

void LocalisationPerFunction::setFunction(const function_descriptor _f) {
    m_function = _f;
    for (std::size_t i = 0; i < m_instance.demands.size(); ++i) {
        for (std::size_t j = 0; j < m_instance.demands[i].functions.size();
             ++j) {
            const int value =
                m_instance.demands[i].functions[j] == m_function ? 1.0 : 0.0;
            m_oneNodePerDemands[static_cast<long>(
                                    m_instance.demands.size() * j + i)]
                .setBounds(value, value);
        }
    }
}

/**
Return an boolean array of size n representing the location of the
function on the network b[u] = 1 iff f is installed of u
*/
IloNumArray LocalisationPerFunction::getLocalisation() const {
    IloNumArray bVal(m_env);
    IloNumArray bResult(m_env);

    m_solver.getValues(bVal, m_b);
    for (const auto u :
        boost::make_iterator_range(vertices(m_instance.network))) {
        bResult.add(bVal[getIndexB(u, m_function)]);
    }
    bVal.end();
    return bResult;
}

IloInt LocalisationPerFunction::getIndexX(const Network::vertex_descriptor _u,
    const std::size_t _i, const std::size_t _j) const {
    SFC_ASSERT(_u < num_vertices(m_instance.network));
    SFC_ASSERT(_i < m_instance.demands.size());
    SFC_ASSERT(_j < m_instance.maxChainSize);
    return static_cast<long>(
        num_vertices(m_instance.network) * m_instance.demands.size() * _j
        + num_vertices(m_instance.network) * _i + _u);
}

IloInt LocalisationPerFunction::getIndexB(
    const Network::vertex_descriptor _u, const function_descriptor _f) const {
    SFC_ASSERT(_f < m_instance.nbFunctions);
    SFC_ASSERT(_u < num_vertices(m_instance.network));
    return static_cast<long>(num_vertices(m_instance.network) * _f + _u);
}

void LocalisationPerFunction::setCPUCapacities(
    const std::vector<double>& _cpuCapacities) {
    for (std::size_t u = 0; u < _cpuCapacities.size(); ++u) {
        m_cpuCapacities[static_cast<long>(u)].setUB(_cpuCapacities[u]);
    }
}

void LocalisationPerFunction::setRAMCapacities(
    const std::vector<double>& _ramCapacities) {
    for (std::size_t u = 0; u < _ramCapacities.size(); ++u) {
        m_ramCapacities[static_cast<long>(u)].setUB(_ramCapacities[u]);
    }
}

void LocalisationPerFunction::setStorageCapacities(
    const std::vector<double>& _storageCapacities) {
    for (std::size_t u = 0; u < _storageCapacities.size(); ++u) {
        m_storageCapacities[static_cast<long>(u)].setUB(_storageCapacities[u]);
    }
}

} // namespace SFC
