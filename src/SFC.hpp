#ifndef SFC_HPP
#define SFC_HPP

#include <CppRO/cplex_utility.hpp>
#include <CppRO/utility.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/johnson_all_pairs_shortest.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/multi_array.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <fstream>
#include <iostream>
#include <numeric>
#include <range/v3/view/transform.hpp>
#include <tuple>
#include <utility>
#include <vector>

// NOLINTBEGIN(cppcoreguidlines-macro-usage)
#ifndef DISABLE_SFC_ASSERT
#define SFC_ASSERT(x) assert(x)
#else
#define SFC_ASSERT(x) (void)0
#endif
// NOLINTEND(cppcoreguidlines-macro-usage)

namespace SFC {

template <typename Graph>
using Path = std::vector<typename Graph::edge_descriptor>;

struct Link {
    std::size_t id{std::numeric_limits<std::size_t>::max()};
};

struct Router {
    std::size_t id{std::numeric_limits<std::size_t>::max()};
};
using Network = boost::adjacency_list<boost::vecS, boost::vecS,
    boost::bidirectionalS, Router, Link, std::string, boost::vecS>;

template <typename FunctionType>
struct OnDestroy {
    FunctionType f;
    explicit OnDestroy(FunctionType&& _f)
        : f(_f) {}
    OnDestroy(const OnDestroy&) = delete;
    OnDestroy& operator=(const OnDestroy&) = delete;
    OnDestroy(OnDestroy&&) noexcept = default;
    OnDestroy& operator=(OnDestroy&&) noexcept = default;
    ~OnDestroy() { f(); }
};

/**
 * Given a graph and a number of layers n, LayeredGraph copies the given graph
 * n times and each layer is connected to the next one by each vertex.
 */
template <typename OriginalGraphType>
class LayeredGraph {
  public:
    struct Arc {
        enum class Type { IntraLayer, InterLayer };
        std::size_t id;
        std::variant<
            typename boost::graph_traits<OriginalGraphType>::edge_descriptor,
            typename boost::graph_traits<OriginalGraphType>::vertex_descriptor>
            originalDescriptor;
    };

    using Graph =
        boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS,
            boost::property<boost::vertex_index_t, std::size_t>, Arc,
            boost::no_property, boost::vecS>;

  private:
    std::size_t m_nbLayers;
    std::size_t m_originalOrder;
    Graph m_graph;

  public:
    /**
     * Given a graph and a number of layers, builds the equivalent layered graph
     * \param _graph A graph
     * \param _nbLayers Total number of layers
     */
    LayeredGraph(const OriginalGraphType& _graph, std::size_t _nbLayers);

    /**
     * Given a vertex descriptor of the layered graph, returns the original
     * vertex descriptor
     * \param _vd vertex descriptor of the layered graph
     */
    [[nodiscard]] auto getVertexDescriptor(std::size_t _vd) const {
        return _vd % (num_vertices(m_graph) / m_nbLayers);
    }

    /**
     * Given a vertex descriptor of the layered graph, return the layer it
     * belongs to. \ param _vd vertex descriptor of the layered graph
     */
    [[nodiscard]] auto getLayer(std::size_t _vd) const {
        return _vd / m_originalOrder;
    }

    [[nodiscard]] auto getOriginalOrder() const { return m_originalOrder; }

    /**
     * Given a layer and an original vertex_descriptor, return the corresponding
     * vertex_descriptor in the layered graph
     **/
    [[nodiscard]] auto getVertexDescriptor(
        std::size_t _layer, std::size_t _vd) const {
        return _layer * m_originalOrder + _vd;
    }

    /**
     * Returns the graph
     */
    [[nodiscard]] const auto& getGraph() const { return m_graph; }

    /**
     * Returns the number of layers of the graph
     */
    [[nodiscard]] const auto& getNbLayers() const { return m_nbLayers; }
};

template <typename OriginalGraphType>
LayeredGraph<OriginalGraphType>::LayeredGraph(
    const OriginalGraphType& _graph, std::size_t _nbLayers)
    : m_nbLayers(_nbLayers)
    , m_originalOrder(num_vertices(_graph))
    , m_graph([&] {
        std::vector<std::pair<std::size_t, std::size_t>> edgeList;
        std::vector<Arc> edgeProperties;
        // Intra-layer links
        for (const auto ed : boost::make_iterator_range(edges(_graph))) {
            for (std::size_t layer = 0; layer < _nbLayers; ++layer) {
                edgeList.emplace_back(
                    getVertexDescriptor(layer, source(ed, _graph)),
                    getVertexDescriptor(layer, target(ed, _graph)));
                edgeProperties.emplace_back(Arc{edgeProperties.size(), ed});
            }
        }
        // Inter-layer links
        for (const auto vd : boost::make_iterator_range(vertices(_graph))) {
            for (std::size_t layer = 0; layer < _nbLayers - 1; ++layer) {
                edgeList.emplace_back(getVertexDescriptor(layer, vd),
                    getVertexDescriptor(layer + 1, vd));
                edgeProperties.emplace_back(Arc{edgeProperties.size(), vd});
            }
        }
        return Graph{edgeList.begin(), edgeList.end(), edgeProperties.begin(),
            num_vertices(_graph) * m_nbLayers};
    }())

{}

using function_descriptor = std::size_t;
using demand_descriptor = std::size_t;

struct ServicePath {
    Path<Network> nPath{};
    std::vector<Network::vertex_descriptor> locations{};
    demand_descriptor demand{};
    friend std::ostream& operator<<(
        std::ostream& _out, const ServicePath& _sPath);
    friend bool operator==(const ServicePath& _sp1, const ServicePath& _sp2);
};

/**
 * Demand describes a demand between two nodes with a given charge and a
 * maximum delay
 */
struct Demand {
    std::size_t id;
    Network::vertex_descriptor s;
    Network::vertex_descriptor t;
    double bandwidth;
    std::vector<function_descriptor> functions;
    std::vector<double> cpuCharges;
    std::vector<double> ramCharges;
    std::vector<double> storageCharges;
    double maxDelay;
};

template <typename Demand>
std::size_t getMaximumChainSize(const std::vector<Demand>& _demands) {
    return std::max_element(_demands.begin(), _demands.end(),
        [&](auto&& _d1, auto&& _d2) {
            return _d1.functions.size() < _d2.functions.size();
        })
        ->functions.size();
}

/**
 * Load the node capacities from a file
 * Capacities can be scaled using the percent parameters
 */
std::vector<double> loadNodeCapa(
    const std::string& _filename, double _percent = 1.0);

std::vector<Network::vertex_descriptor> loadNFV(
    const std::string& _filename, std::size_t _nbSDNs, std::size_t _type);

std::pair<std::vector<Demand>, std::vector<double>> loadDemands(
    const std::string& _filename, double _percent);

std::vector<double> loadFunctions(const std::string& _filename);

struct CrossLayerLink {
    demand_descriptor demandID;
    Network::vertex_descriptor u;
    std::size_t j;
};

struct IntraLayerLink {
    demand_descriptor demandID;
    Network::edge_descriptor edge;
    std::size_t j;
};

template <typename Graph, typename PredecessorMap>
Path<Graph> getPathFromPredecessors(typename Graph::vertex_descriptor _source,
    typename Graph::vertex_descriptor _dest, const Graph& _graph,
    const PredecessorMap& _pred) {
    Path<Graph> path{get(_pred, _dest)};
    while (source(path.back(), _graph) != _source) {
        path.push_back(get(_pred, source(path.back(), _graph)));
    }
    std::reverse(path.begin(), path.end());
    return path;
}

struct Instance {
    Instance(const Network& _network, std::vector<double> _linkCapa,
        std::vector<double> _linkDelay, std::vector<double> _cpuCapa,
        std::vector<double> _ramCapa, std::vector<double> _storageCapa,
        std::vector<Demand> _demands);
    Network network;
    std::vector<double> linkCapacity;
    std::vector<double> linkDelay;
    std::vector<double> cpuCapacity;
    std::vector<double> ramCapacity;
    std::vector<double> storageCapacity;
    std::vector<Demand> demands;
    std::size_t maxChainSize;
    std::size_t nbFunctions;

    [[nodiscard]] double getNbCores(demand_descriptor _demandID) const;
    [[nodiscard]] const auto& getBandwidthRange() const { return linkCapacity; }
    [[nodiscard]] const auto& getCpuRange() const { return cpuCapacity; }
    [[nodiscard]] const auto& getRAMRange() const { return ramCapacity; }
    [[nodiscard]] const auto& getStorageRange() const { return cpuCapacity; }
};

Instance loadInstance(const std::string& _filename);

template <typename ServicePath, typename Instance>
std::vector<double> getNodeUsage(
    const std::vector<ServicePath>& _sPaths, const Instance& _inst) {
    std::vector<double> nodeUsage(num_vertices(_inst.network), 0);
    for (const auto& sPath : _sPaths) {
        for (std::size_t j = 0; j < sPath.locations.size(); ++j) {
            nodeUsage[sPath.locations[j]] +=
                std::ceil(_inst.nbCores[long(sPath.demand)][long(j)]);
        }
    }
    return nodeUsage;
}

/**
 * Returns the bandwidth usage of each when the given service paths are
 * used
 */
template <typename ServicePath, typename Instance>
std::vector<double> getLinkUsage(
    const std::vector<ServicePath>& _sPaths, const Instance& _inst) {
    std::vector<double> linkUsage(num_edges(_inst.network), 0);
    for (const auto& sPath : _sPaths) {
        for (auto iteU = sPath.nPath.begin(), iteV = std::next(iteU);
             iteV != sPath.nPath.end(); ++iteU, ++iteV) {
            linkUsage[_inst.network[edge(*iteU, *iteV, _inst.network).first]
                          .id] += _inst.demands[sPath.demand].bandwidth;
        }
    }
    return linkUsage;
}

template <typename DemandIterator, typename ServicePathIterator>
bool checkProvisionning(DemandIterator _first1, DemandIterator _last1,
    ServicePathIterator _first2) {
    for (; _first1 != _last1; ++_first1, ++_first2) {
        if (!isValid(*_first2, *_first1)) {
            return false;
        }
    }
    return true;
}

/**
 * Checks that
 * - the path isn't empy
 * - the path starts at the source
 * - the path ends at the destination
 * - the service id corresponds to the demand
 */
bool isValid(const ServicePath& _sPath, const Demand& _demand);

struct Solution {
    double objValue;
    std::vector<ServicePath> m_paths;

    [[nodiscard]] const std::vector<ServicePath>&
    getServicePaths() const noexcept {
        return m_paths;
    }
};

void showNetworkUsage(
    const Instance& _inst, const std::vector<ServicePath>& _sPaths);

/**
 * Given an instance, return the functions sorted in ascending order on the
 * order of needed cores
 */
std::vector<function_descriptor> getFunctionsAscendingCores(
    const SFC::Instance& _inst);

/**
 * Given an instance, return the functions sorted in descending order on the
 * order of needed cores
 */
std::vector<function_descriptor> getFunctionsDescendingCores(
    const SFC::Instance& _inst);

Instance loadInstance(const std::string& _filename);

boost::multi_array<bool, 2> getFunctionsLocations(
    const Instance& _instance, const std::vector<ServicePath>& _sPath);

template <typename Network>
ServicePath getServicePath(
    const Path<typename LayeredGraph<Network>::Graph>& _layeredPath,
    const LayeredGraph<Network>& _layeredGraph, demand_descriptor _demandId) {
    ServicePath retval;
    retval.demand = _demandId;
    for (const auto ed : _layeredPath) {
        std::visit(
            [&](auto&& _val) {
                using T = std::decay_t<decltype(_val)>;
                if constexpr (std::is_same_v<
                                  typename Network::vertex_descriptor, T>) {
                    retval.locations.emplace_back(_val);
                }
                if constexpr (std::is_same_v<typename Network::edge_descriptor,
                                  T>) {
                    retval.nPath.emplace_back(_val);
                }
            },
            _layeredGraph.getGraph()[ed].originalDescriptor);
    }
    return retval;
}

} // namespace SFC

template <typename Graph>
std::vector<typename Graph::edge_descriptor> getEdgeCut(const Graph& _graph,
    const std::vector<double>& _weight,
    const typename Graph::vertex_descriptor& _s,
    const typename Graph::vertex_descriptor& _t) {
    // Create duplicate graph
    struct TempLink {
        std::size_t id;
    };
    using DupGraph = boost::adjacency_list<boost::vecS, boost::vecS,
        boost::directedS, boost::no_property, TempLink>;
    DupGraph augGraph(num_vertices(_graph));
    std::vector<typename DupGraph::edge_descriptor> reverse_edges(
        2 * num_edges(_graph));
    std::vector<double> weights(_weight);
    std::vector<double> resCapa(2 * num_edges(_graph));

    // Copy edges
    std::size_t i = 0;
    for (const auto ed : boost::make_iterator_range(edges(_graph))) {
        reverse_edges[i + num_edges(_graph)] =
            add_edge(source(ed, _graph), target(ed, _graph), {i}, augGraph)
                .first;
        i++;
    }
    // Add reverse edges
    for (const auto ed : boost::make_iterator_range(edges(_graph))) {
        reverse_edges[i - num_edges(_graph)] =
            add_edge(target(ed, _graph), source(ed, _graph), {i}, augGraph)
                .first;
        weights.emplace_back(0.0);
        i++;
    }
    const auto getAugEdgeGrapProp = [&](auto&& _vector) {
        return boost::make_iterator_property_map(
            _vector.begin(), get(&TempLink::id, augGraph));
    };
#ifndef NDEBUG
    for (const auto ed : boost::make_iterator_range(edges(augGraph))) {
        assert(source(ed, augGraph)
               == target(reverse_edges[augGraph[ed].id], augGraph));
        assert(target(ed, augGraph)
               == source(reverse_edges[augGraph[ed].id], augGraph));
    }
#endif

    const auto cutSize = boost::push_relabel_max_flow(augGraph, _s, _t,
        boost::capacity_map(getAugEdgeGrapProp(weights))
            .reverse_edge_map(getAugEdgeGrapProp(reverse_edges))
            .residual_capacity_map(getAugEdgeGrapProp(resCapa)));
    (void)cutSize;
    struct Filter {
        const std::vector<double>* capa;
        const Graph* network;
        bool operator()(const std::size_t /*unused*/) const {
            return true;
            /*
            for (const auto ed :
                boost::make_iterator_range(in_edges(u, *network))) {
                if ((*capa)[get(boost::edge_index, (*network), ed)] >
            1e-6) { return true;
                }
            }
            for (const auto ed :
                boost::make_iterator_range(out_edges(u, *network))) {
                if ((*capa)[get(boost::edge_index, *network, ed)] >
            1e-6) { return true;
                }
            }
            return false;
            */
        }

        bool operator()(const typename Graph::edge_descriptor _ed) const {
            return (*capa)[get(boost::edge_index, *network, _ed)] > 1e-6;
        }
    };
    const auto graphFilter = Filter{&resCapa, &_graph};
    std::vector<bool> preds(num_edges(_graph));
    for (const auto ed : boost::make_iterator_range(edges(_graph))) {
        preds[get(boost::edge_index, _graph, ed)] = graphFilter(ed);
    }
    boost::filtered_graph<Graph, Filter, Filter> filterGraph(
        _graph, graphFilter, graphFilter);
    std::vector<boost::default_color_type> colors(
        num_vertices(filterGraph), boost::default_color_type::white_color);

    const auto getNodePropMap = [&](auto&& _vector) {
        return boost::make_iterator_property_map(
            _vector.begin(), get(boost::vertex_index, filterGraph));
    };

    auto color_map = getNodePropMap(colors);
    auto vis = boost::dfs_visitor<boost::null_visitor>();
    boost::depth_first_visit(
        filterGraph, _s, vis, color_map, [](auto&&, auto&&) { return false; });
    assert(colors[_s] == boost::default_color_type::black_color);

    std::vector<typename Graph::edge_descriptor> cut;
    for (const auto ed : boost::make_iterator_range(edges(_graph))) {
        const auto [u, v] = incident(ed, _graph);
        if (colors[u] == boost::default_color_type::black_color
            && colors[v] == boost::default_color_type::white_color) {
            cut.emplace_back(ed);
        }
    }
    return cut;
}

std::size_t getTotalNeededCores();

#endif
