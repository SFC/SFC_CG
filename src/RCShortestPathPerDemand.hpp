#ifndef RC_SHORTESTPATH_PER_DEMAND_HPP
#define RC_SHORTESTPATH_PER_DEMAND_HPP

#include "CppRO/cplex_utility.hpp"
#include "Partial.hpp"
#include "TypeTraits.hpp"
#include "utility.hpp"
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/r_c_shortest_paths.hpp>
#include <boost/multi_array.hpp>

namespace SFC::Partial {

class ResourceContainer {
    friend bool dominance(
        const ResourceContainer& _rc1, const ResourceContainer& _rc2);
    friend bool operator<(
        const ResourceContainer& _rc1, const ResourceContainer& _rc2);

  public:
    /**
     * Create a container for a demand with _chainSize functions
     */
    ResourceContainer(int _chainSize)
        : usedFunction(_chainSize) {}

    /**
     * Create a container from an old one with additional weight
     */
    ResourceContainer(const ResourceContainer& _old, double _additionalWeigt)
        : ResourceContainer(_old) {
        totalWeight += _additionalWeigt;
        nbHops++;
    }

    /**
     * Create a container from an old one with additional weight and a new
     * function done
     */
    ResourceContainer(const ResourceContainer& _old, double _additionalWeigt,
        int _functionIndex)
        : ResourceContainer(_old) {
        function_indexes.push_back(_functionIndex);
        usedFunction[_functionIndex] = true;
        totalWeight += _additionalWeigt;
    }

    ~ResourceContainer() = default;
    ResourceContainer(const ResourceContainer&) = default;
    ResourceContainer& operator=(const ResourceContainer&) = default;
    ResourceContainer(ResourceContainer&&) = default;
    ResourceContainer& operator=(ResourceContainer&&) = default;

    /**
     *
     */
    bool canExtendFunction(const Demand& _demand, int _functionIndex) const {
        if (usedFunction[_functionIndex]) { /// Cannot executed twice the same
                                            /// function
            return false;
        }
        const auto& predFunctions =
            _demand.functions.getPrecedingFunctions(_functionIndex);
        if (std::any_of(predFunctions.begin(), predFunctions.end(),
                [&](auto&& _i) { return !usedFunction[_i]; })) {
            return false;
        }
        return true;
    }

    double getWeight() const { return totalWeight; }

    const std::vector<function_descriptor>& getFunctionIndexes() const {
        return function_indexes;
    }

  private:
    boost::dynamic_bitset<> usedFunction;
    std::vector<function_descriptor> function_indexes{};
    double totalWeight = 0.0;
    int nbHops = 0;
};

bool dominance(const ResourceContainer& _rc1, const ResourceContainer& _rc2);
bool operator<(const ResourceContainer& _rc1, const ResourceContainer& _rc2);

struct RCVisitor {
    double maxWeight;
    struct MaximumWeightException {};

    template <typename Label, typename Graph>
    void on_label_popped(const Label& l, const Graph& /*g*/) {
        if (epsilon_greater<double>()(
                l.cumulated_resource_consumption.getWeight(), maxWeight)) {
            std::cout << l.cumulated_resource_consumption.getWeight() << " > "
                      << maxWeight << '\n';
            throw MaximumWeightException();
        }
    }

    template <typename Label, typename Graph>
    void on_label_feasible(const Label& /*l*/, const Graph& /*g*/) {}
    template <typename Label, typename Graph>
    void on_label_not_feasible(const Label& /*l*/, const Graph& /*g*/) {}
    template <typename Label, typename Graph>
    void on_label_dominated(const Label& /*l*/, const Graph& /*g*/) {}
    template <typename Label, typename Graph>
    void on_label_not_dominated(const Label& /*l*/, const Graph& /*g*/) {}

    template <typename Labels, typename Graph>
    bool on_enter_loop(const Labels& /*l*/, const Graph& /*g*/) {
        return true;
    }
};
/**
 * Create a filtered view of the layered graph for a particular demand
 */
struct DemandFilter {
    const Demand* m_demand;
    const LayeredGraph* m_layeredGraph;
    using edge_descriptor = boost::graph_traits<LayeredGraph>::edge_descriptor;
    using vertex_descriptor =
        boost::graph_traits<LayeredGraph>::vertex_descriptor;

    bool operator()(vertex_descriptor _u) const {
        if ((*m_layeredGraph)[_u].layer > m_demand->functions.size()) {
            return false;
        }
        return true;
    }

    bool operator()(edge_descriptor _ed) const {
        /// we cannot extend a non existing function
        if ((*m_layeredGraph)[_ed].function_index
            >= m_demand->functions.size()) {
            return false;
        }
        return true;
    }
};

class RCShortestPathPerDemand {
  public:
    using Column = ServicePath;

    explicit RCShortestPathPerDemand(const Instance& _inst);
    RCShortestPathPerDemand(const RCShortestPathPerDemand&) = default;
    RCShortestPathPerDemand(RCShortestPathPerDemand&&) = default;
    RCShortestPathPerDemand& operator=(
        const RCShortestPathPerDemand&) = default;
    RCShortestPathPerDemand& operator=(RCShortestPathPerDemand&&) = default;
    ~RCShortestPathPerDemand() noexcept = default;

    void setPricingID(demand_descriptor _demandID);
    void setPricingID(const Demand& _demand);
    void setPricingID(Demand&& _demand) = delete;

    template <typename GetWeightFunction>
    void updateWeight(GetWeightFunction _func) {
        for (const auto ed :
            boost::make_iterator_range(edges(m_layeredGraph))) {
            m_layeredGraph[ed].weight = _func(ed, m_layeredGraph);

            // Check that weight are positive
            assert([&]() {
                if (epsilon_less<double>()(m_layeredGraph[ed].weight, 0)) {
                    std::cout
                        << m_layeredGraph[source(ed, m_layeredGraph)] << ",  "
                        << m_layeredGraph[target(ed, m_layeredGraph)] << " => "
                        << m_layeredGraph[ed] << '\n';
                    return false;
                }
                return true;
            }());
        }
    }

    template <typename DualValues>
    void updateDual(const DualValues& _dualValues) {
        m_objValue = _dualValues.getReducedCost(*m_demand);
        for (const auto ed :
            boost::make_iterator_range(edges(m_layeredGraph))) {
            if (m_layeredGraph[ed].isIntraLayer()) {
                const auto [u, v] = incident(ed, m_layeredGraph);
                m_layeredGraph[ed].weight = _dualValues.getReducedCost(
                    Partial::IntraLayerLink{m_demand->id,
                        {m_layeredGraph[u].original,
                            m_layeredGraph[v].original},
                        m_layeredGraph[ed].layer});
            } else {
                const auto [u, v] = incident(ed, m_layeredGraph);
                m_layeredGraph[ed].weight =
                    _dualValues.getReducedCost(CrossLayerLink{m_demand->id,
                        m_layeredGraph[u].original, m_layeredGraph[u].layer,
                        m_layeredGraph[ed].function_index});
            }

            // Check that weight are positive
            assert([&]() {
                if (epsilon_less<double>()(m_layeredGraph[ed].weight, 0)) {
                    std::cout
                        << m_layeredGraph[source(ed, m_layeredGraph)] << ",  "
                        << m_layeredGraph[target(ed, m_layeredGraph)] << " => "
                        << m_layeredGraph[ed] << '\n';
                    return false;
                }
                return true;
            }());
        }
    }

    bool solve();
    double getObjValue() const;
    ServicePath getColumn() const;
    std::vector<ServicePath> getAllColumn() const;

  protected:
    const Instance* m_inst;
    int n;
    int m;
    int nbLayers;
    const Demand* m_demand{nullptr};
    LayeredGraph m_layeredGraph;
    DemandFilter m_demandFilter{nullptr, &m_layeredGraph};
    std::vector<boost::graph_traits<LayeredGraph>::edge_descriptor> m_paths{};
    ResourceContainer m_containers;
    double m_objValue{-std::numeric_limits<double>::max()};
    std::allocator<
        boost::r_c_shortest_paths_label<LayeredGraph, ResourceContainer>>
        m_allocator;
};

class PartialShortestPath_LP {

  public:
    using Column = ServicePath;

    explicit PartialShortestPath_LP(const Instance& _inst);
    PartialShortestPath_LP(const PartialShortestPath_LP&);
    PartialShortestPath_LP(PartialShortestPath_LP&&);
    PartialShortestPath_LP& operator=(const PartialShortestPath_LP&);
    PartialShortestPath_LP& operator=(PartialShortestPath_LP&&);
    ~PartialShortestPath_LP() { m_env.end(); }

    void setPricingID(demand_descriptor _demandID) {
        m_demand = &m_inst->demands[_demandID];
    }

    template <typename DualValues>
    void updateDual(const DualValues& _dualValues) {
        m_obj.setConstant(_dualValues.getReducedCost(*m_demand));
        IloExpr expr(m_env);
        // Update phi obj coeff
        for (const auto ed :
            boost::make_iterator_range(edges(m_inst->network))) {
            const auto [u, v] = incident(ed, m_inst->network);
            for (int j = 0; j < m_demand->functions.size() + 1; ++j) {
                expr += m_phi[m_inst->network[ed].id][j]
                        * _dualValues.getReducedCost(
                            Partial::IntraLayerLink{m_demand->id, {u, v}, j});
            }
        }

        // Update alpha
        for (const auto u :
            boost::make_iterator_range(vertices(m_inst->network))) {
            for (int i = 0; i < m_demand->functions.size(); ++i) {
                for (int j = 0; j < m_demand->functions.size(); ++j) {
                    expr += m_alpha[u][i][j]
                            * _dualValues.getReducedCost(
                                CrossLayerLink{m_demand->id, u, j, i});
                }
            }
        }
        m_obj.setExpr(expr);
        expr.end();
    }

    bool solve();
    double getObjValue() const;
    ServicePath getColumn() const;

  protected:
    const Instance* m_inst;
    const Demand* m_demand{nullptr};

    IloEnvWrapper m_env{};
    IloModel m_model{m_env};
    IloObjective m_obj{IloMinimize(m_env)};

    boost::multi_array<IloNumVar, 2> m_phi;
    boost::multi_array<IloNumVar, 3> m_alpha;

    boost::multi_array<IloRange, 2> m_flowConservationConstraints;
    boost::multi_array<IloRange, 2> m_precedingConstraints;
    std::vector<IloRange> m_uniqueFunctionsConstraints;

    IloCplex m_solver{m_model};
};

template <typename Graph>
bool extension(const Demand& _demand, const Graph& _g,
    ResourceContainer& _newContainer, const ResourceContainer& _oldContainer,
    typename boost::graph_traits<Graph>::edge_descriptor _ed) {
    // Intra layer links are straight forward: propagate and add the weight of
    // the link
    if (_g[_ed].isIntraLayer()) {
        _newContainer = ResourceContainer(_oldContainer, _g[_ed].weight);
        return true;
    } else {
        // Cross layer link are more complicated
        // First, we check that we are not above the ending layer
        if (!_oldContainer.canExtendFunction(_demand, _g[_ed].function_index)) {
            return false;
        }
        _newContainer = ResourceContainer(
            _oldContainer, _g[_ed].weight, _g[_ed].function_index);
        return true;
    }
}

class RCShortestPathBlock {
  public:
    struct ResourceContainer {
        boost::dynamic_bitset<> usedFunction;
        std::vector<function_descriptor> function_indexes;
        double totalWeight;
        int nbHops;
    };

    using Column = std::vector<ServicePath>;

    explicit RCShortestPathBlock(const Instance& _inst);
    RCShortestPathBlock(const RCShortestPathBlock&) = default;
    RCShortestPathBlock(RCShortestPathBlock&&) = default;
    RCShortestPathBlock& operator=(const RCShortestPathBlock&) = default;
    RCShortestPathBlock& operator=(RCShortestPathBlock&&) = default;
    ~RCShortestPathBlock() noexcept = default;

    template <typename DualValues>
    void updateWeights(const DualValues& _dualValues, const Demand& _demand) {
        for (const auto ed :
            boost::make_iterator_range(edges(m_layeredGraph))) {
            if (m_layeredGraph[ed].isIntraLayer()) {
                const auto [u, v] = incident(ed, m_layeredGraph);
                m_layeredGraph[ed].weight = _dualValues.getReducedCost(
                    Partial::IntraLayerLink{_demand.id,
                        {m_layeredGraph[u].original,
                            m_layeredGraph[v].original},
                        m_layeredGraph[ed].layer});
            } else {
                const auto u = source(ed, m_layeredGraph);
                m_layeredGraph[ed].weight =
                    _dualValues.getReducedCost(CrossLayerLink{_demand.id,
                        m_layeredGraph[u].original, m_layeredGraph[u].layer,
                        m_layeredGraph[ed].function_index});
            }
            assert([&]() {
                if (epsilon_less<double>()(m_layeredGraph[ed].weight, 0)) {
                    std::cout
                        << m_layeredGraph[source(ed, m_layeredGraph)] << ",  "
                        << m_layeredGraph[target(ed, m_layeredGraph)] << " => "
                        << m_layeredGraph[ed] << '\n';
                    return false;
                }
                return true;
            }());
        }
    }

    bool extension(const Demand& _demand, const LayeredGraph& g,
        ResourceContainer& new_cont, const ResourceContainer& old_cont,
        boost::graph_traits<LayeredGraph>::edge_descriptor ed);
    static bool dominance(
        const ResourceContainer& _rc1, const ResourceContainer& _rc2);

    template <typename DualValues>
    bool solve(const DualValues& _dualValues) {
        m_objValue =
            _dualValues.getReducedCost(typename DualValues::OneBlockTag{});
        for (const auto& demand : m_inst->demands) {
            updateWeights(_dualValues, demand);
            m_paths[demand.id].clear();
            const int dest = n * (demand.functions.size()) + demand.t;
            r_c_shortest_paths(
                m_layeredGraph, get(&Node::index, m_layeredGraph),
                get(&Link::index, m_layeredGraph), demand.s, dest,
                m_paths[demand.id], m_containers[demand.id],
                {boost::dynamic_bitset<>(demand.functions.size()), {}, 0.0, 0},
                [&](auto&&... args) { // extension predicate
                    return extension(demand, args...);
                },
                [&](auto&&... args) { // dominance predicate
                    return dominance(args...);
                });
            m_objValue += m_containers[demand.id].totalWeight;
            if (m_paths[demand.id].empty()) {
                return false;
            }
        }
        std::cout << "Found column with rc = " << m_objValue << '\n';
        return true;
    }

    double getObjValue() const { return m_objValue; }
    std::vector<ServicePath> getColumn() const;

  protected:
    const Instance* m_inst;
    int n;
    int m;
    int nbLayers;
    LayeredGraph m_layeredGraph;
    std::vector<std::vector<boost::graph_traits<LayeredGraph>::edge_descriptor>>
        m_paths;
    std::vector<ResourceContainer> m_containers;
    double m_objValue{-std::numeric_limits<double>::max()};
};

bool operator<(const RCShortestPathBlock::ResourceContainer& _rc1,
    const RCShortestPathBlock::ResourceContainer& _rc2);
bool operator==(const RCShortestPathBlock::ResourceContainer& _rc1,
    const RCShortestPathBlock::ResourceContainer& _rc2);

namespace Fixed {
class RCShortestPathPerDemand : public SFC::Partial::RCShortestPathPerDemand {
  public:
    RCShortestPathPerDemand(
        const Instance& _inst, boost::multi_array<bool, 2> _locations);
    ~RCShortestPathPerDemand() = default;

    RCShortestPathPerDemand(const RCShortestPathPerDemand&) = default;
    RCShortestPathPerDemand& operator=(
        const RCShortestPathPerDemand&) = default;
    RCShortestPathPerDemand(RCShortestPathPerDemand&&) = default;
    RCShortestPathPerDemand& operator=(RCShortestPathPerDemand&&) = default;

    bool extension(const LayeredGraph& g, ResourceContainer& new_cont,
        const ResourceContainer& old_cont,
        boost::graph_traits<LayeredGraph>::edge_descriptor ed);
    bool solve();

  private:
    boost::multi_array<bool, 2> m_locations;
};

class RCShortestPathBlock : public SFC::Partial::RCShortestPathBlock {
  public:
    RCShortestPathBlock(
        const Instance& _inst, boost::multi_array<bool, 2> _locations);
    ~RCShortestPathBlock() = default;

    RCShortestPathBlock(const RCShortestPathBlock&) = default;
    RCShortestPathBlock& operator=(const RCShortestPathBlock&) = default;
    RCShortestPathBlock(RCShortestPathBlock&&) = default;
    RCShortestPathBlock& operator=(RCShortestPathBlock&&) = default;

    bool extension(const Demand& _demand, const LayeredGraph& g,
        ResourceContainer& new_cont, const ResourceContainer& old_cont,
        boost::graph_traits<LayeredGraph>::edge_descriptor ed);

    template <typename DualValues>
    bool solve(const DualValues& _dualValues) {
        m_objValue =
            _dualValues.getReducedCost(typename DualValues::OneBlockTag{});
        for (const auto& demand : m_inst->demands) {
            updateWeights(_dualValues, demand);
            m_paths[demand.id].clear();
            const int dest = n * (demand.functions.size()) + demand.t;
            r_c_shortest_paths(
                m_layeredGraph, get(&Node::index, m_layeredGraph),
                get(&Link::index, m_layeredGraph), demand.s, dest,
                m_paths[demand.id], m_containers[demand.id],
                {boost::dynamic_bitset<>(demand.functions.size()), {}, 0.0, 0},
                [&](auto&&... args) { // extension predicate
                    return SFC::Partial::Fixed::RCShortestPathBlock::extension(
                        demand, args...);
                },
                [&](auto&&... args) { // dominance predicate
                    return dominance(args...);
                });
            m_objValue += m_containers[demand.id].totalWeight;
            if (m_paths[demand.id].empty()) {
                return false;
            }
        }

        return true;
    }

  private:
    boost::multi_array<bool, 2> m_locations;
};

} // namespace Fixed

class FixedLinks {
  public:
    FixedLinks() = default;
    ~FixedLinks() = default;

    FixedLinks(const FixedLinks&) = default;
    FixedLinks& operator=(const FixedLinks&) = default;
    FixedLinks(FixedLinks&&) noexcept = default;
    FixedLinks& operator=(FixedLinks&&) noexcept = default;

    void addFixedVariable(const FixedCrossLayerLink& _link) {
        m_fixedCrossLinks.emplace_back(_link);
    }

    void addFixedVariable(const FixedIntraLayerLink& _link) {
        assert(
            std::find(m_fixedIntraLinks.begin(), m_fixedIntraLinks.end(), _link)
            == m_fixedIntraLinks.end());
        /*std::cout << "Fixing link (" << _link.var.edge.first << ", "
                  << _link.var.edge.second << ") at layer " << _link.var.j
                  << " for demand " << _link.var.demandID << " to " << _link.use
                  << '\n';*/
        m_fixedIntraLinks.emplace_back(_link);
    }

    void removeFixedVariable(const FixedCrossLayerLink& _link) {
        m_fixedCrossLinks.erase(std::remove(m_fixedCrossLinks.begin(),
                                    m_fixedCrossLinks.end(), _link),
            m_fixedCrossLinks.end());
    }

    void removeFixedVariable(const FixedIntraLayerLink& _link) {
        m_fixedIntraLinks.erase(std::remove(m_fixedIntraLinks.begin(),
                                    m_fixedIntraLinks.end(), _link),
            m_fixedIntraLinks.end());
    }

    bool isForbidden(const Link& _link) const noexcept {
        if (_link.isIntraLayer()) {
            return std::find_if(m_fixedIntraLinks.begin(),
                       m_fixedIntraLinks.end(),
                       [&](auto&& _fixedVar) {
                           return _fixedVar.use == false
                                  && _link == _fixedVar.var;
                       })
                   != m_fixedIntraLinks.end();
        } else {
            return std::find_if(m_fixedCrossLinks.begin(),
                       m_fixedCrossLinks.end(),
                       [&](auto&& _fixedVar) {
                           return _fixedVar.use == false
                                  && _link == _fixedVar.var;
                       })
                   != m_fixedCrossLinks.end();
        }
    }

    bool isMandatory(const Link& _link) const noexcept {
        if (_link.isIntraLayer()) {
            return std::find_if(m_fixedIntraLinks.begin(),
                       m_fixedIntraLinks.end(),
                       [&](auto&& _fixedVar) {
                           return _fixedVar.use == true
                                  && _link == _fixedVar.var;
                       })
                   != m_fixedIntraLinks.end();
        } else {
            return std::find_if(m_fixedCrossLinks.begin(),
                       m_fixedCrossLinks.end(),
                       [&](auto&& _fixedVar) {
                           return _fixedVar.use == true
                                  && _link == _fixedVar.var;
                       })
                   != m_fixedCrossLinks.end();
        }
    }

    void clear() {
        m_fixedCrossLinks.clear();
        m_fixedIntraLinks.clear();
    }

    /**
     * Check if all mandatory links are contained in _eds
     */
    bool containAllMandatory(
        const std::vector<boost::graph_traits<LayeredGraph>::edge_descriptor>
            _eds,
        const LayeredGraph& _graph) const {
        const auto checkIfIn = [&](auto&& _fixedVar) {
            if (_fixedVar.use) {
                return std::find_if(_eds.begin(), _eds.end(), [&](auto&& ed) {
                    return _graph[ed] == _fixedVar.var;
                }) != _eds.end();
            }
            return true; // Forbidden link, we don't care
        };
        return std::all_of(m_fixedCrossLinks.begin(), m_fixedCrossLinks.end(),
                   checkIfIn)
               && std::all_of(m_fixedIntraLinks.begin(),
                   m_fixedIntraLinks.end(), checkIfIn);
    }

  private:
    std::vector<FixedCrossLayerLink> m_fixedCrossLinks;
    std::vector<FixedIntraLayerLink> m_fixedIntraLinks;
};

class RCShortestPathPerDemandRestricted {
  public:
    struct ResourceContainer {
        boost::dynamic_bitset<> usedFunction;
        std::vector<function_descriptor> function_indexes;
        double totalWeight;
        int nbHops;
        std::vector<boost::graph_traits<LayeredGraph>::edge_descriptor>
            mandatory_links;
    };

    using Column = ServicePath;

    explicit RCShortestPathPerDemandRestricted(
        const Instance& _inst, const FixedLinks& _links);
    RCShortestPathPerDemandRestricted(
        const RCShortestPathPerDemandRestricted&) = default;
    RCShortestPathPerDemandRestricted(
        RCShortestPathPerDemandRestricted&&) = default;
    RCShortestPathPerDemandRestricted& operator=(
        const RCShortestPathPerDemandRestricted&) = default;
    RCShortestPathPerDemandRestricted& operator=(
        RCShortestPathPerDemandRestricted&&) = default;
    ~RCShortestPathPerDemandRestricted() noexcept = default;

    void setPricingID(demand_descriptor _demandID);
    void setPricingID(const Demand& _demand);
    void setPricingID(Demand&& _demand) = delete;

    template <typename DualValues>
    void updateDual(const DualValues& _dualValues) {
        m_objValue = _dualValues.getReducedCost(*m_demand);

        for (const auto ed :
            boost::make_iterator_range(edges(m_layeredGraph))) {
            if (m_layeredGraph[ed].isIntraLayer()) {
                const auto [u, v] = incident(ed, m_layeredGraph);
                m_layeredGraph[ed].weight = _dualValues.getReducedCost(
                    Partial::IntraLayerLink{m_demand->id,
                        {m_layeredGraph[u].original,
                            m_layeredGraph[v].original},
                        m_layeredGraph[ed].layer});
            } else {
                const auto [u, v] = incident(ed, m_layeredGraph);
                m_layeredGraph[ed].weight =
                    _dualValues.getReducedCost(CrossLayerLink{m_demand->id,
                        m_layeredGraph[u].original, m_layeredGraph[u].layer,
                        m_layeredGraph[ed].function_index});
            }
            /*assert([&]() {
                if (epsilon_less<double>()(m_layeredGraph[ed].weight, 0)) {
                    std::cout
                        << m_layeredGraph[source(ed, m_layeredGraph)] << ",  "
                        << m_layeredGraph[target(ed, m_layeredGraph)] << " => "
                        << m_layeredGraph[ed] << '\n';
                    return false;
                }
                return true;
            }());*/
        }
    }

    bool extension(const LayeredGraph& g, ResourceContainer& new_cont,
        const ResourceContainer& old_cont,
        boost::graph_traits<LayeredGraph>::edge_descriptor ed);
    static bool dominance(
        const ResourceContainer& _rc1, const ResourceContainer& _rc2);

    bool solve();
    double getObjValue() const;
    ServicePath getColumn() const;
    std::vector<ServicePath> getAllColumn() const;

  private:
    const Instance* m_inst;
    const FixedLinks* m_fixedLinks;

    int n;
    int m;
    int nbLayers;
    const Demand* m_demand{nullptr};
    LayeredGraph m_layeredGraph;
    std::vector<boost::graph_traits<LayeredGraph>::edge_descriptor> m_paths;
    ResourceContainer m_containers;
    double m_objValue{-std::numeric_limits<double>::max()};
};

bool operator<(const RCShortestPathPerDemandRestricted::ResourceContainer& _rc1,
    const RCShortestPathPerDemandRestricted::ResourceContainer& _rc2);
bool operator==(
    const RCShortestPathPerDemandRestricted::ResourceContainer& _rc1,
    const RCShortestPathPerDemandRestricted::ResourceContainer& _rc2);

} // namespace SFC::Partial
#endif
