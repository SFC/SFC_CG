cmake_minimum_required(VERSION 3.10)
project (SFC::Multicast VERSION 1.0.0 LANGUAGES CXX)
set(CMAKE_MODULE_PATH
    ${CMAKE_MODULE_PATH}
    ${PROJECT_SOURCE_DIR}/cmake
    ${PROJECT_SOURCE_DIR}/externals/sanitizers-cmake/cmake)

find_package(Sanitizers)
find_package(CppRo REQUIRED)

add_library(Multicast ${PROJECT_SOURCE_DIR}/src/Multicast.cpp)
target_include_directories(Multicast
    PUBLIC
    $<INSTALL_INTERFACE:include>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)
target_link_libraries(Multicast 
    PUBLIC
    CppRo::CppRo SFC)
target_compile_features(Multicast PUBLIC cxx_std_17)
target_compile_options(Multicast
    PUBLIC
    -Werror
    -Wall
    -fpermissive
    -Wextra
    -Wshadow
    -Wnon-virtual-dtor
    -pedantic
    -Wno-c++11-compat
    -DIL_STD
    -march=native
    -Wno-c++98-compat
    -Wno-sign-compare
    -Weverything
    -Wno-sign-compare
    -Wno-sign-conversion
    -Wno-c++98-compat-pedantic
    -Wno-error=padded
    -Wno-error=unused-variable
    -Wno-shorten-64-to-32
    -Wno-padded)
IF(CMAKE_BUILD_TYPE MATCHES DEBUG)
    add_sanitizers(SFC)
ENDIF(CMAKE_BUILD_TYPE MATCHES DEBUG)