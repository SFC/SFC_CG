#ifndef MULTICAST_HPP
#define MULTICAST_HPP

#include "SFC.hpp"
#include "SteinerTree.hpp"

#include <boost/graph/copy.hpp>
#include <boost/graph/depth_first_search.hpp>

namespace SFC::Multicast {

template <typename NetworkService>
class Demand {
  public:
    Demand();
    ~Demand() = default;
    Demand(const Demand&) = default;
    Demand& operator=(const Demand&) = default;
    Demand(Demand&&) noexcept = default;
    Demand& operator=(Demand&&) noexcept = default;

    int id;
    ::Node source;
    std::vector<::Node> destinations;
    NetworkService chain;
};

template <typename NetworkService>
class SteinerTree_LP {
  public:
    SteinerTree_LP(const Instance& _inst)
        : m_inst(&_inst)
        , n(num_vertices(m_inst->network))
        , m(num_edges(m_inst->network))
        , nbLayers(m_inst->maxChainSize + 1)
        , m_steinerGraph([&]() {
            DiGraph retval(num_vertices(m_inst->network) + 1);
            copy_graph(m_inst->network, retval);
            for (Node u = 0; u < num_vertices(m_inst->network); ++u) {
                add_edge(num_vertices(m_inst->network), u,
                    DiGraph::edge_property_type(
                        0.0, boost::property<boost::edge_index_t, int>(num_edges(m_inst->network) + u)),
                    retval);
            }
            return retval;
        }())
        , m_steiner(m_steinerGraph)
        , m_layeredGraph([&]() {
            DiGraph retval(n * nbLayers);
            for (const auto u : boost::make_iterator_range(vertices(m_inst->network))) {
                if (m_inst->network[u].isNFV) {
                    for (int j = 0; j < nbLayers - 1; ++j) {
                        add_edge(n * j + u, n * (j + 1) + u, retval);
                    }
                }
            }
            for (int j = 0; j < nbLayers; ++j) {
                for (auto [ite, end] = edges(m_inst->network); ite != end; ++ite) {
                    add_edge(
                        n * j + source(*ite, m_inst->network), n * j + target(*ite, m_inst->network), retval);
                }
            }
            return retval;
        }())
        , m_distance(num_vertices(m_layeredGraph))
        , m_predecessors(num_vertices(m_layeredGraph)) {}

    ~SteinerTree_LP() = default;
    SteinerTree_LP(const SteinerTree_LP&) = default;
    SteinerTree_LP& operator=(const SteinerTree_LP&) = default;
    SteinerTree_LP(SteinerTree_LP&&) noexcept = default;
    SteinerTree_LP& operator=(SteinerTree_LP&&) noexcept = default;

    void setPricingID(const Demand<NetworkService>* _demand) { m_demand = _demand; }

    template <typename DualValue>
    void updateDual(const DualValue& _dualValues) {
        // Inter layer weight
        const auto& weight_map = get(boost::edge_weight, m_layeredGraph);
        const auto& index_map = get(boost::edge_index, m_layeredGraph);

        // Inter layer weight
        for (const auto u : boost::make_iterator_range(vertices(m_inst->network))) {
            if (m_inst->network[u].isNFV) {
                for (int j = 0; j < nbLayers - 1; ++j) {
                    put(weight_map, edge(n * j + u, n * (j + 1) + u, m_layeredGraph).first,
                        _dualValues.getReducedCost(CrossLayerLink{m_demand->id, u, j}));
                }
            }
        }

        // Same layer weight
        for (int j = 0; j < nbLayers; ++j) {
            for (auto ed : boost::make_iterator_range(edges(m_inst->network))) {
                const auto [u, v] = incident(ed, m_inst->network);
                put(weight_map, edge(n * j + u, n * j + v, m_layeredGraph).first,
                    _dualValues.getReducedCost(IntraLayerLink{m_demand->id, {u, v}, j}));
            }
        }
    }

    bool solve() { return false; }

  private:
    const Instance* m_inst;
    int n;
    int m;
    int nbLayers;
    const Demand<NetworkService>* m_demand{nullptr};
    DiGraph m_steinerGraph;
    SteinerTree<DiGraph> m_steiner;
    DiGraph m_layeredGraph;
    std::vector<double> m_distance;
    std::vector<Node> m_predecessors;
    bool isNegative{false};
};

} // namespace SFC::Multicast
#endif
