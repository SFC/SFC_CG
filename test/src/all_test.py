import os
import run_json

sol = run_json.run(os.path.dirname(__file__) + "/test1.json")
assert sol is not None
assert len(sol.getServicePaths()) == 5

sol = run_json.run(os.path.dirname(__file__) + "/test2.json")
assert sol is not None
assert len(sol.getServicePaths()) == 5

sol = run_json.run(os.path.dirname(__file__) + "/test3.json")
assert sol is None

sol = run_json.run(os.path.dirname(__file__) + "/test4.json")
assert sol is None
