import json
import argparse
import networkx as nx
import sfc


def run(filename: str):
    instance_dict = json.load(open(filename))
    functions_charge = instance_dict['functions_charges']
    print(f"Functions charges: {functions_charge}")
    g = nx.Graph((edge['src'], edge['dst'], {'bandwidth': edge['bandwidth'], 'delay': edge['delay']})
                 for edge in instance_dict['edge_list'])
    g = nx.DiGraph(g)
    edge_list = list(g.edges())
    print(f"Edges: {edge_list}")
    link_capacities = [g[u][v]['bandwidth'] for u, v in edge_list]
    link_delays = [g[u][v]['delay'] for u, v in edge_list]
    print(f"Capacities: {link_capacities}")
    order = int(instance_dict['order'])
    print("# vertices: ", order)
    network = sfc.DiGraph(edge_list=edge_list, link_capacities=link_capacities,
                          order=order)
    print(f"Demands: {instance_dict['demands']}")
    demands = [sfc.Demand(i, demand['src'], demand['dst'], demand['bandwidth'],
                          demand['functions'], demand['delay']) for i, demand in enumerate(instance_dict['demands'])]
    # All nodes have 100 capacity
    node_capacities = instance_dict['nodeCapacity']
    # Create the instance and run the algorithm
    inst = sfc.Instance(network=network, node_capacities=node_capacities,
                        link_delays=link_delays, demands=demands,
                        functions_charge=functions_charge)
    nb_threads = 1
    return sfc.cgAndBranchAndBound(inst, nb_threads)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()
    run(args.filename)
