import networkx as nx
import sfc

if __name__ == "__main__":
    NB_FUNCTIONS = 3

    g = nx.Graph()
    g.add_edges_from([(0, 1), (0, 2), (0, 3), (2, 3)])

    g = nx.DiGraph(g)

    link_capacities = [10] * g.size()
    link_delays = [10] * g.size()

    network = sfc.DiGraph(edge_list=list(g.edges()),
                          link_capacities=[10] * g.size(), order=g.order())

    demands = [
        sfc.Demand(0, 0, 1, 1.0, [0, 1, 2], 10),
        sfc.Demand(1, 0, 1, 1.0, [0, 1], 10),
        sfc.Demand(2, 1, 2, 1.0, [0, 1], 10),
        sfc.Demand(3, 1, 3, 1.0, [0, 1], 10),
        sfc.Demand(4, 2, 3, 1.0, [0, 1], 10)
    ]

    functions_charge = [10] * NB_FUNCTIONS
    node_capacities = [30, 0, 0, 0]

    inst = sfc.Instance(network=network, node_capacities=node_capacities,
                        link_delays=link_delays, demands=demands,
                        functions_charge=functions_charge)

    sol = sfc.cgAndBranchAndBound(inst, 1)
    if not sol:
        print("No solution found")
    else:
        for sPath in sol.getServicePaths():
            print(
                f"Path for demand {sPath.id}: {sPath.path}, NFVs at {sPath.locations}")
