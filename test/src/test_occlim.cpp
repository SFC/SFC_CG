#include "SFC.hpp"
#include "catch2/catch.hpp"
#include <algorithm.hpp>
#include <numeric>
#include <random>

// NOLINTBEGIN(*)

SCENARIO("The medoid-based heuristic can find function placement", "[medoid]") {
    GIVEN(" The internet2 occlim instance ") {
        auto instance =
            SFC::loadInstance("data/instances/occlim/internet2.json");
        std::set<std::size_t> functions;
        for (const auto& demand : instance.demands) {
            for (const auto function : demand.functions) {
                functions.insert(function);
            }
        }

        WHEN(" using the heuristics ") {
            const std::size_t nbLicenses = GENERATE(range(1u, 11u));
            std::seed_seq seed{{0}};
            std::mt19937 gen(seed);
            const auto locations = SFC::getFunctionPlacementMedoids(instance,
                nbLicenses, {functions.begin(), functions.end()}, gen);
            THEN(" functions are placed somewhere ") {
                if (locations.has_value()) {
                    REQUIRE(locations->shape()[0]
                            == num_vertices(instance.network));
                    REQUIRE(locations->shape()[1] == functions.size());
                    const auto checkNumberLicencesUsed =
                        [&](const auto& _locations, const auto& _function) {
                            std::size_t nb = 0;
                            for (std::size_t u = 0;
                                 u < num_vertices(instance.network); ++u) {
                                nb += _locations[static_cast<long>(u)]
                                                [static_cast<long>(_function)];
                            }
                            return nb;
                        };
                    for (std::size_t f = 0; f < functions.size(); ++f) {
                        REQUIRE(checkNumberLicencesUsed(*locations, f)
                                == nbLicenses);
                    }
                    auto sol = SFC::columnGenerationBranchAndBound(
                        instance, *locations, 2);
                    if (sol) {
                        auto realLocations = getFunctionsLocations(
                            instance, sol->getServicePaths());
                        for (std::size_t f = 0; f < functions.size(); ++f) {
                            REQUIRE(checkNumberLicencesUsed(realLocations, f)
                                    <= nbLicenses);
                        }
                    }
                }
            }
        }
    }
}

// NOLINTEND(*)
