import csv
import sys
import matplotlib
from math import sqrt
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import math
import metrics
import sys
sys.path.append("../../../MyPython")
from utility import latexify
import collections

def latexify(fig_width=None, fig_height=None, columns=1, fontsize=20):
	assert(columns in [1,2])
	fig_widths = [3.39, 6.9]
	if fig_width is None:
	    fig_width = fig_widths[columns-1] # width in inches

	if fig_height is None:
	    fig_height = fig_width*((sqrt(5)-1.0)/2.0) # height in inches

	MAX_HEIGHT_INCHES = 8.0
	if fig_height > MAX_HEIGHT_INCHES:
	    print("WARNING: fig_height too large:" + fig_height + 
	          "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
	    fig_height = MAX_HEIGHT_INCHES

	params = {
		'backend': 'pdf',
      # 'text.latex.preamble': ['\usepackage{gensymb}'],
      'axes.labelsize': 10, # fontsize for x and y labels (was 10)
      'axes.titlesize': 10,
      'font.size': fontsize, # was 10
      'legend.fontsize': 10, # was 10
      'xtick.labelsize': 10,
      'ytick.labelsize': 10,
      # 'text.usetex': True,
      'figure.figsize': [fig_width, fig_height],
      'font.family': 'serif', 
	}
	

	matplotlib.rcParams.update(params)
latexify()

orders = {
	"atlanta": 15,
	"internet2": 10,
	"germany50": 50
}

labels = {
	"heur_pf":"PFL-CG", 
	"ILP": "ILP"
}

funcName = ["NAT", "FW", "TM", "WOC", "IDPS", "VOC"]

def bandwidth_VS_replication(name, models=["heur"], percent=1, funcCoef=4, cheat=False, gap=False):
	listXlb = []
	listLB = []

	plt.clf()
	# Get lower bound values
	for replic in xrange(1, orders[name]+1):
		# print name, " -> ", replic, 
		_, lowerBound, _, _, _, _, _, _, _, _ = metrics.getCGMetrics("./results/occlim/{}_{}_{:.6f}_{}_{}.res".format(name, "lowerBound", percent, funcCoef, replic))
		if lowerBound != -1:
			listLB.append(lowerBound)
			listXlb.append(replic)
	print "LB"
	for x, y in zip(listXlb, listLB):
		print x, y
	plt.plot(listXlb, listLB, label="LB")			
		
	# Get model values
	for model in models:
		listInt = []
		listFrac = []
		listX = []
		for replic in xrange(1, orders[name]+1):
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains = None, None, None, None, None, None, None, None
			if model == "ILP":
				intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = metrics.getILPMetrics("./results/occlim/{}_{}_{:.6f}_{}_{}.res".format(name, model, percent, funcCoef, replic))
			else:
				intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = metrics.getCGMetrics("./results/occlim/{}_{}_{:.6f}_{}_{}.res".format(name, model, percent, funcCoef, replic))
			if intObj != -1:
				listInt.append(intObj)
				listFrac.append(intObj - fracObj)
				listX.append(replic)
		if cheat:
			minVal = max(listInt)
			for i in xrange(len(listInt)):
				if listInt[i] > minVal:
					listInt[i] = minVal
				else:
					minVal = listInt[i]
		print model
		for x, y in zip(listX, listInt):
			print x, y
		if gap:
			plt.errorbar(x=listX, y=listInt, yerr=(listFrac, [0 for _ in range(len(listFrac))]), label=labels.get(model, model))
		else:
			plt.plot(listX, listInt, label=labels.get(model, model))
	

	plt.xlabel("# of licences")
	plt.xlim([0, orders[name]])
	plt.ylabel("Bandwidth")
	plt.subplots_adjust(left=0.18, bottom=0.18, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	plt.grid()
	plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
	filename = "./figs/occlim/{}_{}_ILPValue_cheat.pdf".format(name, models) if cheat else "./figs/occlim/{}_{}_ILPValue.pdf".format(name, models)
	print "Saved to", filename
	plt.savefig(filename, bbox_inches='tight')

def time_VS_replication(name, model="heur", percent=1, funcCoef=4):
	listY = []
	listX = []

	for replic in xrange(1, orders[name]+1):
		try:
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = metrics.getCGMetrics("./results/occlim/{}_{}_{:.6f}_{}_{}.res".format(name, model, percent, funcCoef, replic))
			if intObj != -1:
				listY.append(WallTime)
				listX.append(replic)
				print name, " -> ", listX[-1], listY[-1]
		except Exception as e:
			print e
			pass
	# plt.scatter(nbNFV, listFrac)
	plt.clf()
	plt.scatter(listX, listY, color="red")
	# X
	plt.xlabel("# of licences")
	plt.xlim([0, orders[name]])
	# Y
	plt.ylabel("Time (ms)")
	plt.yscale('log')
	plt.grid()
	plt.subplots_adjust(left=0.18, bottom=0.18, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	plt.savefig("./figs/occlim/{}_{}_WallTime.pdf".format(name, model), bbox_inches='tight')

def delay_VS_replication(name, model="heur", percent=1, funcCoef=4):
	listY = []
	listX = []

	for replic in xrange(1, orders[name]+1):
		try:
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = metrics.getCGMetrics("./results/occlim/{}_{}_{:.6f}_{}_{}.res".format(name, model, percent, funcCoef, replic))
			if intObj != -1:
				listY.append([(len(path)-1) for path in paths])
				listX.append(replic)
				# print name, " -> ", listX[-1], listY[-1]
		except Exception as e:
			print e
			pass
	plt.clf()
	# print listX, listY
	plt.boxplot(listY, positions=listX, widths=.8, sym='')#, whis=[10, 90])
	# X
	plt.xlabel("# of licences")
	plt.xlim([0, orders[name]+1])
	# Y
	plt.ylabel("# of hops")
	plt.ylim([0, math.ceil(plt.ylim()[1]+1)])

	plt.subplots_adjust(left=0.12, bottom=0.18, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	plt.savefig("./figs/occlim/{}_{}_delay.pdf".format(name, model), bbox_inches='tight')

def graphFuncLocations(name, replic, models=["heur_pf"], percent=1, funcCoef=4):
	for model in models:
		intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = metrics.getCGMetrics("./results/occlim/{}_{}_{:.6f}_{}_{}.res".format(name, model, percent, funcCoef, replic))
		if intObj != -1:
			totalF = [0 for f in range(6)]
			b = [[0 for f in range(6)] for u in range(orders[name])]
			for c, locationsDict in chains.iteritems():
				for loc, nb in locationsDict.iteritems():
					for i in range(len(loc)):
						b[loc[i]][c[i]] += 1 
						totalF[c[i]] += 1
			# Show node for each function			
			for f in range(6):
				print "f{} ->".format(f),
				for u in range(orders[name]):
					if b[u][f]:
						print u,
				print ""
			# Show function on each node
			values = {}
			for u in range(orders[name]):
				for f in range(6):
					values[u, f] = b[u][f] / float(totalF[f])
				print [values[u, f] for f in range(6)]
			maxValue = max([max([values[u, f] for u in range(orders[name])]) for f in range(6)])
			print "Max value is: ", maxValue
			height = {}
			for u in range(orders[name]):
				for f in range(6):
					height[u, f] = int(20 * values[u, f] / float(maxValue))
			for u in range(orders[name]):
				string = '<?xml version="1.0" encoding="utf-8"?>\n\
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="60" height="20" style="background: white">\n\
<rect x="0" y="{}" width="10" height="{}" style="fill:red" />\n\
<rect x="10" y="{}" width="10" height="{}" style="fill:black" />\n\
<rect x="20" y="{}" width="10" height="{}" style="fill:green" />\n\
<rect x="30" y="{}" width="10" height="{}" style="fill:yellow" />\n\
<rect x="40" y="{}" width="10" height="{}" style="fill:blue" />\n\
<rect x="50" y="{}" width="10" height="{}" style="fill:grey" />\n\
<rect x="0"  y="0" width="60" height="20" style="fill-opacity:0; stroke:black; stroke-width:1;"/>\n\
</svg>\n'.format(
	20 - height[u, 0], height[u, 0],
	20 - height[u, 1], height[u, 1],
	20 - height[u, 2], height[u, 2],
	20 - height[u, 3], height[u, 3],
	20 - height[u, 4], height[u, 4],
	20 - height[u, 5], height[u, 5])
				with open("figs/occlim/placement/{}/{}_{}_{}.svg".format(name, name, u, model), "w") as file:
					file.write(string)
				print '{} [shape=plaintext, label="", image="{}_{}.svg"]'.format(u, name, u)
		else:
			print "No solution found!"



