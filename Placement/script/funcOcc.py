import metrics
import collections
import sys

intObjCG, fracObjCG, nbColumnCG, CPUTimeCG, WallTimeCG, nbNFV, pathsCG, chainsCG = metrics.getCGMetrics("./results/{}_path_1.000000_4.res".format(sys.argv[1]))
# print chainsCG.keys()
a = [collections.defaultdict(int) for _ in range(6)]
for c, locs in chainsCG.items():
	for loc in locs.keys():
		i = 0
		for node in loc:
			a[c[i]][node] += 1
			i += 1
print a
print "Average # func occ:", sum([len(p) for p in a])/len(a)