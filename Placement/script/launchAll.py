from subprocess import call
import sys

orders = {"atlanta": 15, "internet2": 10, "germany50": 50}

name = sys.argv[1]
model = sys.argv[2]
for n in range(1, orders[name]+1):
	with open("{}_{}.txt".format(name, n)) as f:
		names = f.read().splitlines()

		for instName in names:
			print "./LP {}".format(instName)
			call("./LP {}".format(instName), shell=True)
			break
