import csv
import sys
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt

orders = {"atlanta": 15, "internet2": 10}

def getMetrics(filename):
	print "Getting metrics for ", filename
	with open("./results/" + filename, 'r') as csvfile:
		dataReader = csv.reader(csvfile, delimiter='\t')
		row = dataReader.next()
		intObj = float(row[0])
		fracObj = float(row[1])

		row = dataReader.next()
		nbColumn = int(row[0])
		row = dataReader.next() 
		CPUTime = float(row[0])
		WallTime = float(row[1])

	return intObj, fracObj, nbColumn, CPUTime, WallTime

def plotILPValue(name, model, percent=1, funcCoef=4):
	listInt = []
	listFrac = []
	nbNFV = []

	for n in range(1, orders[name]+1):
		try:
			with open("{}_{}.txt".format(name, n)) as f:
				names = f.read().splitlines()
			print names
		except:
			print "No file", name
		for instName in names:
			try:
				intObj, fracObj, _, _, _ = getMetrics("{}_{}_{:.6f}_{}.res".format(instName, model, percent, funcCoef))
				if intObj != -1:
					listInt.append(intObj)
					listFrac.append(fracObj)
					nbNFV.append(n)
			except:
				pass
	# plt.scatter(nbNFV, listFrac)
	plt.clf()
	plt.scatter(nbNFV, listInt)
	plt.xlabel("# of NFV Nodes")
	plt.ylabel("Bandwidth")
	plt.savefig("./figs/"+name+"_"+model+"_ILPValue.pdf")

def plotNbCol(name, model, percent=1, funcCoef=4):
	listY = []
	listX = []

	for n in range(1, orders[name]+1):
		try:
			with open("{}_{}.txt".format(name, n)) as f:
				names = f.read().splitlines()
			print names
		except:
			print "No file", name
		for instName in names:
			try:
				intObj, _, nbColumn, _, _ = getMetrics("{}_{}_{:.6f}_{}.res".format(instName, model, percent, funcCoef))
				if intObj != -1:
					listY.append(nbColumn)
					listX.append(n)
			except:
				pass
	# plt.scatter(listX, listFrac)
	plt.clf()
	plt.scatter(listX, listY)
	plt.xlabel("# of NFV Nodes")
	plt.ylabel("# of Columns")
	plt.savefig("./figs/"+name+"_"+model+"_nbCol.pdf")

def plotCPUTime(name, model, percent=1, funcCoef=4):
	listInt = []
	listFrac = []
	nbNFV = []

	for n in range(1, orders[name]+1):
		try:
			with open("{}_{}.txt".format(name, n)) as f:
				names = f.read().splitlines()
			print names
		except:
			print "No file", name
		for instName in names:
			try:
				intObj, _, _, CPUTime, _ = getMetrics("{}_{}_{:.6f}_{}.res".format(instName, model, percent, funcCoef))
				if intObj != -1:
					listInt.append(CPUTime)
					nbNFV.append(n)
			except:
				pass
	# plt.scatter(nbNFV, listFrac)
	plt.clf()
	plt.scatter(nbNFV, listInt)
	plt.xlabel("# of NFV Nodes")
	plt.ylabel("Time (ms)")
	plt.savefig("./figs/"+name+"_"+model+"_CPUTime.pdf")
	plt.

def plotWallTime(name, model, percent=1, funcCoef=4):
	listInt = []
	listFrac = []
	nbNFV = []

	for n in range(1, orders[name]+1):
		try:
			with open("{}_{}.txt".format(name, n)) as f:
				names = f.read().splitlines()
			print names
		except:
			print "No file", name
		for instName in names:
			try:
				intObj, _, _, _, WallTime = getMetrics("{}_{}_{:.6f}_{}.res".format(instName, model, percent, funcCoef))
				if intObj != -1:
					listInt.append(WallTime)
					nbNFV.append(n)
			except:
				pass
	# plt.scatter(nbNFV, listFrac)
	plt.clf()
	plt.scatter(nbNFV, listInt)
	plt.xlabel("# of NFV Nodes")
	plt.ylabel("Time (ms)")
	plt.savefig("./figs/"+name+"_"+model+"_WallTime.pdf")

if __name__ == "__main__":
	if len(sys.argv) < 3:
		print "Not enough arguments..."
		sys.exit()
	name = sys.argv[1]
	model = sys.argv[2]

	plotILPValue(name, model)
	plotNbCol(name, model)
	plotCPUTime(name, model)
	plotWallTime(name, model)