"""
Print all commands for the replication experiments for a given network or all networks
Arguments:
	1st: Network name. If equal to "all", experiments will be run on all networks 
"""
import sys


orders = {
	"internet2": 10,
	"atlanta": 15,
	"germany50": 50
}
 
names = [sys.argv[1]] if sys.argv[1] != "all" else orders.keys()
models = [sys.argv[2]] if sys.argv[2] != "all" else ["pathThread", "pathThreadBF"]
percent = sys.argv[3] 

if sys.argv[4] == "allDemands":
	for name in names:
	  for model in models:
			for nbNFV in reversed(xrange(1, orders[name]+1)):
				print "{}_emb_{} -m {} -d {}".format(name, nbNFV, model, percent)
elif sys.argv[4] == "percentDemands":
	for name in names:
		for model in models:
			for percentDemands in xrange(10, 100+1, 10):
				print "{}_emb_{} -m {} -d {} -p {}".format(name, orders[name], model, percent, percentDemands)