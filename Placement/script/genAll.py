from subprocess import call
import sys

orders = {"atlanta": 15, "internet2": 10, "germany50": 50, "ta2": 65}

if len(sys.argv) < 5:
	print "Missing arguments {name} {nb} {genType} {ordering}"
	sys.exit(-1)

name = sys.argv[1]
nb = int(sys.argv[2])
genType = sys.argv[3]
ordering = sys.argv[4]

commands = ["./LP_GEN {} {} 1 4 {} {}".format(genType, name, n, ordering) for n in range(1, orders[name] + 1) for _ in range(nb)]
print commands
for command in commands:
	print command
	call(command, shell=True)