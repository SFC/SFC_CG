import csv
import sys
import matplotlib
from math import sqrt
import argparse
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import math
import numpy as np

import sys
sys.path.append("../../../MyPython")

load("metrics.sage")

def latexify(fig_width=None, fig_height=None, columns=1, fontsize=20):
	assert(columns in [1,2])
	fig_widths = [3.39, 6.9]
	if fig_width is None:
	    fig_width = fig_widths[columns-1] # width in inches

	if fig_height is None:
	    fig_height = fig_width*((sqrt(5)-1.0)/2.0) # height in inches

	MAX_HEIGHT_INCHES = 8.0
	if fig_height > MAX_HEIGHT_INCHES:
	    print("WARNING: fig_height too large:" + fig_height + 
	          "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
	    fig_height = MAX_HEIGHT_INCHES

	params = {'backend': 'pdf',
	          # 'text.latex.preamble': ['\usepackage{gensymb}'],
	          'axes.labelsize': 10, # fontsize for x and y labels (was 10)
	          'axes.titlesize': 10,
	          'font.size': fontsize, # was 10
	          'legend.fontsize': 10, # was 10
	          'xtick.labelsize': 10,
	          'ytick.labelsize': 10,
	          # 'text.usetex': True,
	          'figure.figsize': [fig_width, fig_height],
	          'font.family': 'serif', 
	}
	

	matplotlib.rcParams.update(params)

TOPO_ORDERS = {
	"atlanta": 15,
	"internet2": 10,
	"germany50": 50,
	"ta2": 65
}

def plotILPValue(name, model, percent=1):
	listInt = []
	listFrac = []
	listX = []

	for instName in ["{}_emb_{}".format(name, n) for n in range(1, TOPO_ORDERS[name]+1)]:
		try:
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = getCGMetrics("./results/{}_{}_{:.6f}.res".format(instName, model, percent))
			if intObj != -1:
				print name, " -> ", intObj, fracObj, nbNFV
				listInt.append(intObj)
				listFrac.append(fracObj)
				listX.append(nbNFV)
		except Exception as e:
			print e
			pass
	# plt.scatter(nbNFV, listFrac)
	plt.clf()
	plt.scatter(listX, listInt)
	plt.xlabel("# of NFV Nodes")
	plt.xlim([0, TOPO_ORDERS[name]])
	plt.ylabel("Bandwidth")
	plt.savefig("./figures/emb/"+name+"_"+model+"_ILPValue.pdf", bbox_inches='tight')

def plotRatio(name, model, percent=1):
	listInt = []
	listX = []

	for instName in ["{}_emb_{}".format(name, n) for n in range(1, TOPO_ORDERS[name]+1)]:
		try:
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = getCGMetrics("./results/{}_{}_{:.6f}.res".format(instName, model, percent))
			if intObj != -1:
				print name, " -> ", intObj, fracObj, nbNFV
				listInt.append(intObj / fracObj)
				listX.append(nbNFV)
		except Exception as e:
			print e
			pass
	plt.clf()
	plt.scatter(listX, listInt)
	plt.axhline(1.0)
	plt.xlabel("# of NFV Nodes")
	plt.xlim([0, TOPO_ORDERS[name]])
	plt.ylabel("Ratio ILP/LP")
	plt.savefig("./figures/emb/"+name+"_"+model+"_ratio.pdf", bbox_inches='tight')

def plotNbCol(name, model, percent=1):
	listY = []
	listX = []

	for instName in ["{}_emb_{}".format(name, n) for n in range(1, TOPO_ORDERS[name]+1)]:
		try:
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = getCGMetrics("./results/{}_{}_{:.6f}.res".format(instName, model, percent))
			print name, " -> ", intObj, nbColumn, nbNFV
			if intObj != -1:
				listY.append(nbColumn)
				listX.append(nbNFV)
		except Exception as e:
			print e
			pass
	# plt.scatter(listX, listFrac)
	plt.clf()
	plt.scatter(listX, listY)
	plt.xlabel("# of NFV Nodes")
	plt.xlim([0, TOPO_ORDERS[name]])
	plt.ylabel("# of Columns")
	plt.savefig("./figures/emb/"+name+"_"+model+"_nbCol.pdf", bbox_inches='tight')

def plotCPUTime(name, model, percent=1):
	listInt = []
	listFrac = []
	listX = []

	for instName in ["{}_emb_{}".format(name, n) for n in range(1, TOPO_ORDERS[name]+1)]:
		try:
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = getCGMetrics("./results/{}_{}_{:.6f}.res".format(instName, model, percent))
			if intObj != -1:
				print name, " -> ", intObj, CPUTime, nbNFV
				listInt.append(CPUTime / 1000.0)
				listX.append(nbNFV)
		except Exception as e:
			print e
			pass
	# plt.scatter(nbNFV, listFrac)
	plt.clf()
	plt.scatter(listX, listInt)
	plt.xlabel("# of NFV Nodes")
	plt.xlim([0, TOPO_ORDERS[name]])
	plt.ylabel("Time (s)")
	plt.savefig("./figures/emb/"+name+"_"+model+"_CPUTime.pdf", bbox_inches='tight')

def plotDelay(name, model, percent=1, avgDelay=1.8):
	listInt = []
	listFrac = []
	listX = []

	for instName in ["{}_emb_{}".format(name, n) for n in range(1, TOPO_ORDERS[name]+1)]:
		try:
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = getCGMetrics("./results/{}_{}_{:.6f}.res".format(instName, model, percent))
			if intObj != -1:
				print name, " -> ", intObj, CPUTime, nbNFV
				listInt.append([(len(path)-1) for path in paths])
				listX.append(nbNFV)
		except Exception as e:
			print e
			pass
	plt.clf()
	plt.boxplot(listInt, positions=listX, widths=float(0.8), sym='')#, whis=[10, 90])
	plt.xlabel("# of NFV Nodes")
	if name == "germany50":
		plt.xticks([1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50], [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50])
	plt.xlim([0, TOPO_ORDERS[name]+1])
	plt.ylabel("# of hops")
	plt.ylim([0, math.ceil(plt.ylim()[1]+1)])
	plt.savefig("./figures/emb/"+name+"_"+model+"_delay.pdf", bbox_inches='tight')

def plotNbHopsAlgotel(name, model, percent=1, avgDelay=1.8):
	listInt = []
	listX = []

	if name == ["germany50", "ta2"] :
		nRange = range(1, TOPO_ORDERS[name]+1, 2) + [50]
	else:
		nRange = range(1, TOPO_ORDERS[name]+1, 1)

	for n in nRange:
		instName = "{}_emb_{}".format(name, n)
		try:
			intObj, _, _, CPUTime, _, nbNFV, paths, _, _, _ = getCGMetrics("./results/{}_{}_{:.6f}.res".format(instName, model, percent))
			
			if intObj != -1:
				print name, " -> ", intObj, CPUTime, n
				nbHops = [(len(path)-1) for path in paths]
				listInt.append(nbHops)
				listX.append(n)
		except Exception as e:
			print e
			pass
	plt.clf()
	plt.boxplot(listInt, positions=listX, widths=float(0.8), sym='', whis=[25, 75])

	plt.xlabel("# of NFV Nodes")
	if name == "germany50":
		plt.xticks([1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50], [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50])
	plt.xlim([0, TOPO_ORDERS[name]+1])

	plt.ylabel("# of hops")
	plt.ylim([0, math.ceil(plt.ylim()[1]+1)])
	plt.savefig("./figures/emb/"+name+"_"+model+"_delayAlgotel.pdf", bbox_inches='tight')

def plotNbHopsAlgotelAsScatter(name, model, percent=1, avgDelay=1.8):
	listInt = []
	listX = []

	if name == ["germany50", "ta2"] :
		nRange = range(1, TOPO_ORDERS[name]+1, 2) + [50]
	else:
		nRange = range(1, TOPO_ORDERS[name]+1, 1)

	for n in nRange:
		instName = "{}_emb_{}".format(name, n)
		try:
			intObj, _, _, CPUTime, _, nbNFV, paths, _, _, _ = getCGMetrics("./results/{}_{}_{:.6f}.res".format(instName, model, percent))
			
			if intObj != -1:
				print name, " -> ", intObj, CPUTime, n
				for nbHops in [(len(path)-1) for path in paths]:
					listInt.append(nbHops)
					listX.append(n)
		except Exception as e:
			print e
			pass

	plt.clf()
	plt.scatter(listInt, listX)

	plt.xlabel("# of NFV Nodes")
	plt.xlim([0, TOPO_ORDERS[name]+1])
	plt.ylabel("# of hops")
	plt.ylim([0, math.ceil(plt.ylim()[1]+1)])

	plt.savefig("./figures/emb/"+name+"_"+model+"_delayAlgotel_scatter.pdf", bbox_inches='tight')

def plotNbHopsAlgotelMedFrstThrd(name, model, percent=1, avgDelay=1.8):
	listX = []

	listMin = []
	listMax = []
	listMedian = []
	listFirst = []
	listThird = []

	nRange = range(1, TOPO_ORDERS[name]+1, 1)
	for n in nRange:
		instName = "{}_emb_{}".format(name, n)
		try:
			intObj, _, _, CPUTime, _, nbNFV, paths, _, _, _ = getCGMetrics("./results/{}_{}_{:.6f}.res".format(instName, model, percent))
			
			if intObj != -1:
				listX.append(n)
				setOfNbHops = [(len(path)-1) for path in paths]
				mn, frst, med, thrd, mx = np.percentile(setOfNbHops, [0, 25, 50, 75, 100])
				print name, " -> ", intObj, mn, frst, med, thrd, mx
				listMin.append(mn)
				listMax.append(mx)
				listMedian.append(med)
				listFirst.append(frst)
				listThird.append(thrd)

		except Exception as e:
			print e
			pass

	plt.clf()
	# plt.plot(listX, listMin, label="Min.", linestyle="-", color="black")
	plt.plot(listX, listFirst, label="1st quart.", linestyle="-.", color="blue")
	plt.plot(listX, listMedian, label="Median", linestyle="-", color="red")
	plt.plot(listX, listThird, label="3rd quart.", linestyle="-.", color="blue")
	# plt.plot(listX, listMax, label="Max.", linestyle="-", color="black")


	plt.xlabel("# of NFV Nodes")
	plt.xlim([0, TOPO_ORDERS[name]])
	plt.ylabel("# of hops")
	plt.ylim([0, math.ceil(plt.ylim()[1]+1)])
	plt.legend(loc="lower center", ncol=2)
	plt.savefig("./figures/emb/"+name+"_"+model+"_delayAlgotel_mft.pdf", bbox_inches='tight')

def plotMultiplicativeStrech(instanceName, model, percent=1):
	g = loadNetwork("./instances/" + instanceName + "_topo.txt")
	xRange = range(1, g.order()+1) if instanceName != "germany50" else range(1, 50, 2) + [50]
	yValue = [getMultplicativeStrech(instanceName="{instanceName}_emb_{nbVNFs}".format(instanceName=instanceName, nbVNFs=nbVNFs),
		model=model, percent=percent) for nbVNFs in xRange]
	plt.clf()
	plt.boxplot(yValue, positions=xRange, widths=float(0.8), sym='', whis=[10, 90])
	
	plt.xlabel("# of NFV Nodes")
	if instanceName == "germany50":
		plt.xticks([1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50], [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50])
	plt.xlim([0, TOPO_ORDERS[instanceName]+1])
	plt.ylabel("Stretch")
	plt.ylim([0, math.ceil(plt.ylim()[1]+1)])

	plt.savefig("figures/emb/{instanceName}_multstretch.pdf".format(instanceName=instanceName), bbox_inches='tight')

def plotAdditiveStretch(instanceName, model, percent=1):
	g = loadNetwork("./instances/" + instanceName + "_topo.txt")
	xRange = range(1, g.order()+1)# if instanceName != "germany50" else range(1, 50, 2) + [50]
	yValue = [getAdditiveStrech(instanceName="{instanceName}_emb_{nbVNFs}".format(instanceName=instanceName, nbVNFs=nbVNFs),
		model=model, percent=percent) for nbVNFs in xRange]
	plt.clf()
	plt.boxplot(yValue, positions=xRange, widths=float(0.8), sym='', whis=[10, 90])
	
	plt.xlabel("# of NFV Nodes")
	if instanceName == "germany50":
		plt.xticks([1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50], [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50])
	plt.xlim([0, TOPO_ORDERS[instanceName]+1])
	plt.ylabel("Stretch")
	plt.ylim([0, math.ceil(plt.ylim()[1]+1)])

	plt.savefig("figures/emb/{instanceName}_addstretch.pdf".format(instanceName=instanceName), bbox_inches='tight')

def plotILPValueAlgotel(name, model, percent=1):
	listInt = []
	listFrac = []
	listX = []

	for instanceName in ["{}_emb_{}".format(name, n) for n in range(1, TOPO_ORDERS[name]+1)]:
		try: 
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = getCGMetrics("./results/{}_{}_{:.6f}.res".format(instanceName, model, percent))
			if intObj != -1:
				print name, " -> ", intObj, fracObj, nbNFV
				listInt.append(intObj)
				listFrac.append(fracObj)
				listX.append(nbNFV)
		except Exception as e:
			print e
			pass
	# plt.scatter(nbNFV, listFrac)
	plt.clf()
	plt.scatter(listX, listInt)
	plt.xlabel("# of NFV Nodes")
	plt.xlim([0, TOPO_ORDERS[name]])
	plt.ylabel("Bandwidth")
	plt.savefig("./figures/emb/"+name+"_"+model+"_ILPValueAlgotel.pdf", bbox_inches='tight')

def plotWallTime(name, model, percent=1):
	listInt = []
	listFrac = []
	listX = []

	for instName in ["{}_emb_{}".format(name, n) for n in range(1, TOPO_ORDERS[name]+1)]:
		try:
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, _, _ = getCGMetrics("./results/{}_{}_{:.6f}.res".format(instName, model, percent))
			if intObj != -1:
				print name, " -> ", intObj, WallTime, nbNFV
				listInt.append(WallTime / 1000.0)
				listX.append(nbNFV)
		except Exception as e:
			print e
			pass
	# plt.scatter(nbNFV, listFrac)
	plt.clf()
	plt.scatter(listX, listInt)
	plt.xlabel("# of NFV Nodes")
	plt.xlim([0, TOPO_ORDERS[name]])
	plt.ylabel("Time (s)")
	plt.savefig("./figures/emb/"+name+"_"+model+"_WallTime.pdf", bbox_inches='tight')

def main():
	parser = argparse.ArgumentParser(description='Plots for the ICC 2017 & Algotel 2018 papers')
	parser.add_argument('instance', help='name of the instance')
	parser.add_argument('-m', dest="method", choices=["pathThreadBF", "pathThread"], default="pathThreadBF", help='name of the algorithm to call')  
	args = parser.parse_args()
	latexify()
	
	plotNbHopsAlgotelMedFrstThrd(args.instance, args.method)
	plotILPValueAlgotel(args.instance, args.method)
	plotNbHopsAlgotel(args.instance, args.method)

	plotILPValue(args.instance, args.method)
	plotDelay(args.instance, args.method)
	plotMultiplicativeStrech(args.instance, args.method)
	plotAdditiveStretch(args.instance, args.method)
	plotNbCol(args.instance, args.method)
	plotCPUTime(args.instance, args.method)
	plotWallTime(args.instance, args.method)
	plotRatio(args.instance, args.method)

if __name__ == "__main__":
	main()
