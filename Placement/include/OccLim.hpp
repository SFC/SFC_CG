#ifndef OCCLIM_HPP
#define OCCLIM_HPP

#include <assert.h>
#include <iosfwd>
#include <utility.hpp>
#include <utility>

#include "SFC.hpp"

namespace SFC::OccLim {
using ::operator<<;

struct Instance : public SFC::Instance {

    Instance(DiGraph _network, std::vector<int> _nodeCapa,
        std::vector<Demand<>> _demands, std::vector<double> _funcCharge, int _nbLicenses);

    using SFC::Instance::demands;
    using SFC::Instance::funcCharge;
    using SFC::Instance::maxChainSize;
    using SFC::Instance::nbCores;
    using SFC::Instance::network;
    int nbLicenses;
};

boost::multi_array<bool, 2> getFunctionPlacement(const std::vector<ServicePath>& _sPaths, const Instance& _inst);

template <typename AdditionalInformation = unused>
class Solution {
  public:
    Solution(const Instance& _inst, double _objValue, double _lowerBound,
        std::vector<ServicePath> _paths,
        AdditionalInformation _additionalInformation)
        : inst(&_inst)
        , objValue(_objValue)
        , lowerBound(_lowerBound)
        , paths(std::move(_paths))
        , additionalInformation(std::move(_additionalInformation)) {}

    bool isValid() const {
        int i = 0;
        for (const auto& [nPath, locations, demandID] : paths) {
            if (nPath.empty()) {
                std::cerr << "nPath for demand" << inst->demands[i]
                          << "is empty\n";
                return false;
            }
            if (inst->demands[demandID].s != nPath.front()) {
                return false;
            }
            if (inst->demands[demandID].t != nPath.back()) {
                return false;
            }
            ++i;
        }
        const std::vector<double> linkUsage = getLinkUsage(paths, *inst);
        for (const auto ed : boost::make_iterator_range(edges(inst->network))) {
            if (linkUsage[inst->network[ed].id] > inst->network[ed].capacity) {
                std::cerr << "link " << incident(ed, inst->network) << " is overloaded\n";
                return false;
            }
        }
        const std::vector<int> nodeUsage = getNodeUsage(paths, *inst);
        for (const auto u : boost::make_iterator_range(vertices(inst->network))) {
            const auto& [id, capacity, isNFV] = inst->network[u];
            if (isNFV && nodeUsage[u] > capacity) {
                std::cerr << "node " << u << ") is overloaded\n";
                return false;
            }
        }
        return true;
    }

    void save(const std::string& _filename,
        const std::pair<double, double>& _time) const {
        std::ofstream ofs(_filename);
        ofs << objValue << '\t' << lowerBound << '\n';
        ofs << additionalInformation << std::endl;
        ofs << _time.first << '\t' << _time.second << std::endl;

        const std::vector<double> linkUsage = getLinkUsage(paths, *inst);
        const std::vector<int> nodeUsage = getNodeUsage(paths, *inst);
        const auto fPlacement = getFunctionPlacement(paths, *inst);
        for (function_descriptor f = 0; f < inst->funcCharge.size(); ++f) {
            int nbLicenses = 0;
            for (Node u = 0; u < num_vertices(inst->network); ++u) {
                nbLicenses += fPlacement[u][f];
            }
            assert([&]() {
                if (nbLicenses > inst->nbLicenses) {
                    std::cout << f << " is using too many licenses: [";
                    for (Node u = 0; u < num_vertices(inst->network); ++u) {
                        if (fPlacement[u][f]) {
                            std::cout << u << ", ";
                        }
                    }
                    std::cout << '\n';
                    return false;
                }
                return true;
            }());
        }

        ofs << num_vertices(inst->network) << '\n';
        for (const auto u : boost::make_iterator_range(vertices(inst->network))) {
            ofs << u << '\t' << nodeUsage[u] << '\t' << inst->network[u].capacity
                << '\n';
            assert(nodeUsage[u] <= inst->network[u].capacity);
        }

        ofs << num_edges(inst->network) << '\n';
        for (const auto ed : boost::make_iterator_range(edges(inst->network))) {
            const auto& [u, v] = incident(ed, inst->network);
            const auto& [id, capa] = inst->network[ed];

            ofs << std::fixed << u << '\t' << v << '\t'
                << linkUsage[id] << '\t'
                << capa << '\n';

            assert(linkUsage[id] <= capa);
        }

        ofs << paths.size() << '\n';
        for (const auto& [nPath, locations, demandID] : paths) {
            ofs << nPath.size() << '\t';
            for (const auto& u : nPath) {
                ofs << u << '\t';
            }

            for (int j = 0; j < locations.size(); ++j) {
                ofs << locations[j] << '\t'
                    << inst->demands[demandID].functions[j] << '\t';
            }

            ofs << '\n';
        }
        std::cout << "Results saved to :" << _filename << '\n';
    }

  private:
    const Instance* inst;
    double objValue;
    double lowerBound;
    std::vector<ServicePath> paths;
    AdditionalInformation additionalInformation;
};

} // namespace SFC::OccLim
#endif
