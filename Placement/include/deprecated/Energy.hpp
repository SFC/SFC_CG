// #ifndef ENERGY_HPP
// #define ENERGY_HPP

// #include "DiGraph.hpp"
// #include "ShortestPath.hpp"
// #include "utility.hpp"
// #include <cmath>
// #include <ilcplex/ilocplex.h>
// #include <map>
// #include <tuple>

// #define RC_EPS 1.0e-8

// typedef int function_descriptor;
// typedef std::tuple<int, int, double> Demand;
// typedef int Node;

// class Energy {
//     class PlacementModel {
//         friend Energy;

//       public:
//         PlacementModel(Energy& _placement)
//             : m_placement(_placement)
//             , m_model(m_placement.m_env)
//             , m_x(IloNumVarArray(m_placement.m_env, m_placement.m_inst.network.size(), 0.0, IloInfinity))
//             , m_y(IloNumVarArray(m_placement.m_env))
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_onePathCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size(), 1.0, IloInfinity)))
//             , m_linkCapasCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(), 0.0, IloInfinity)))
//             , m_solver(m_model)
//             , m_inst.edgeToId()
//             , m_paths() {
//             m_onePathCons.setNames("onePath");
//             int i = 0;
//             for (Graph::Edge e : m_placement.m_inst.network.getEdges()) {
//                 m_inst.edgeToId[e] = i;
//                 ++i;
//             }

//             for (Graph::Edge edge : m_placement.m_inst.network.getEdges()) {
//                 int Xe;
//                 int e = m_inst.edgeToId[edge];
//                 if (edge.first < edge.second) {
//                     Xe = m_inst.edgeToId[edge];
//                 } else {
//                     Xe = m_inst.edgeToId[Graph::Edge(edge.second, edge.first)];
//                 }
//                 m_linkCapasCons[e].setLinearCoef(m_x[Xe], m_placement.m_inst.network.getEdgeWeight(edge));
//                 m_linkCapasCons[e].setName(std::string("linkCapa" + toString(edge)).c_str());
//                 m_obj.setLinearCoef(m_x[Xe], 2.0);
//                 m_x[e].setName(std::string("x" + toString(edge)).c_str());
//             }

//             m_solver.setOut(m_placement.m_env.getNullStream());
//             // std::cout << m_model << std::endl;
//         }

//         PlacementModel& operator=(const PlacementModel&) = delete;
//         PlacementModel(const PlacementModel&) = delete;

//         void addPathCheck(const Graph::Path& _path, int _i) {
//             if (std::find(m_paths.begin(), m_paths.end(), _path) == m_paths.end()) {
//                 addPath(_path, _i);
//             }
//         }

//         void addPath(const Graph::Path& _path, int _i) {
//             std::cout << "addPath" << _path << std::endl;
//             assert([&]() {
//                 auto ite = std::find(m_paths.begin(), m_paths.end(), _path);
//                 if (ite == m_paths.end()) {
//                     return true;
//                 } else {
//                     std::cout << _path << " is already present!" << std::endl;
//                     return false;
//                 }
//             }());

//             // std::list<IloRange> changeRange;
//             const Demand& demand = m_placement.m_inst.demands[_i];
//             int s = std::get<0>(demand), t = std::get<1>(demand);
//             double d = std::get<2>(demand);

//             IloNumColumn inc(m_onePathCons[_i](1.0)); // + m_obj(d * (path.size() - 1) ));
//             for (auto iteU = _path.begin(), iteV = std::next(iteU); iteV != _path.end(); ++iteU, ++iteV) {
//                 int i = m_inst.edgeToId[Graph::Edge(*iteU, *iteV)];
//                 inc += m_linkCapasCons[i](-d);
//             }
//             auto yVar = IloNumVar(inc, 0.0, IloInfinity);
//             yVar.setName(std::string("y" + toString(std::make_tuple(s, t, _path))).c_str());
//             m_y.add(yVar);
//             m_paths.push_back(_path);

//             // for(auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//             // 	int i = m_inst.edgeToId[Graph::Edge(*iteU, *iteV)];
//             // 	std::cout << "m_activeCons1["<<Graph::Edge(*iteU, *iteV)<<"]: " << m_activeCons1[i] << std::endl;
//             // }
//         }

//         void solveInteger() {
//             m_model.add(IloConversion(m_placement.m_env, m_y, ILOBOOL));
//             m_model.add(IloConversion(m_placement.m_env, m_x, ILOBOOL));
//             if (m_solver.solve()) {
//                 std::cout << "Final obj value: " << m_solver.getObjValue() << "\t# Paths: " << m_paths.size() << std::endl;
//                 std::cout << 100 * (1 - m_solver.getObjValue() / double(m_placement.m_inst.network.size())) << std::endl;
//                 // std::cout << m_model << std::endl;
//                 m_solver.exportModel("lp.lp");
//             } else {
//                 std::cout << "No integer solution found!" << std::endl;
//             }
//         }

//       private:
//         Energy& m_placement;
//         IloModel m_model;

//         IloNumVarArray m_x;
//         IloNumVarArray m_y;

//         IloObjective m_obj;

//         IloRangeArray m_onePathCons;
//         IloRangeArray m_linkCapasCons;

//         IloCplex m_solver;

//         std::map<Graph::Edge, int> m_inst.edgeToId;
//         std::vector<Graph::Path> m_paths;
//     };

//     class PricingProblem {
//         friend Energy;

//       public:
//         PricingProblem(int _demandID, Energy& _chainPlacement)
//             : m_placement(_chainPlacement)
//             , m_demandID(_demandID)
//             , n(m_placement.m_inst.network.getOrder())
//             , m(m_placement.m_inst.network.size())
//             , m_model(m_placement.m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_f(IloAdd(m_model, IloIntVarArray(m_placement.m_env, m, 0, 1)))
//             , m_flowConsCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, n, 0.0, 0.0)))
//             , m_linkCapaCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m, 0.0, 0.0)))
//             , m_noLoopCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, 2 * n, 0.0, 1.0))) {
//             m_obj.setName(std::string("PP_" + toString(m_placement.m_inst.demands[m_demandID])).c_str());

//             const int &s = std::get<0>(m_placement.m_inst.demands[m_demandID]),
//                       t = std::get<1>(m_placement.m_inst.demands[m_demandID]);
//             const double& d = std::get<2>(m_placement.m_inst.demands[m_demandID]);
//             // const std::vector<function_descriptor>& functions = std::get<3>(m_placement.m_inst.demands[m_demandID]);

//             auto listEdges = m_placement.m_inst.network.getEdges();
//             std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

//             for (int e = 0; e < int(edges.size()); ++e) {
//                 m_linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(edges[e]));
//             }

//             for (int e = 0; e < m; ++e) {
//                 IloNumColumn inc(m_placement.m_env);
//                 inc += m_flowConsCons[edges[e].first](1.0);
//                 inc += m_flowConsCons[edges[e].second](-1.0);
//                 inc += m_linkCapaCons[e](d);
//                 inc += m_noLoopCons[edges[e].first](1.0);
//                 inc += m_noLoopCons[n + edges[e].second](1.0);

//                 m_f[e] = IloIntVar(inc);
//                 m_f[e].setName(std::string("f" + toString(edges[e])).c_str());
//             }

//             m_flowConsCons.setNames("flowConservation");
//             m_linkCapaCons.setNames("linkCapa");
//             // m_nodeCapaCons.setNames("nodeCapa");
//             m_noLoopCons.setNames("uniqueFlow");

//             m_flowConsCons[s].setUB(1.0);
//             m_flowConsCons[s].setLB(1.0);

//             m_flowConsCons[t].setLB(-1.0);
//             m_flowConsCons[t].setUB(-1.0);
//         }

//         void updateDual(const IloCplex& _sol) {
//             double d = std::get<2>(m_placement.m_inst.demands[m_demandID]);
//             m_obj.setConstant(-_sol.getDual(m_placement.m_placementModel.m_onePathCons[m_demandID]));

//             for (int e = 0; e < m; ++e) {
//                 m_obj.setLinearCoef(m_f[e],
//                     d * _sol.getDual(m_placement.m_placementModel.m_linkCapasCons[e]));
//             }
//         }

//         void updateDemand(int _demandID) {
//             int s = std::get<0>(m_placement.m_inst.demands[m_demandID]),
//                 t = std::get<1>(m_placement.m_inst.demands[m_demandID]);

//             m_flowConsCons[s].setBounds(0.0, 0.0);
//             m_flowConsCons[t].setBounds(0.0, 0.0);

//             m_demandID = _demandID;

//             s = std::get<0>(m_placement.m_inst.demands[m_demandID]);
//             t = std::get<1>(m_placement.m_inst.demands[m_demandID]);

//             m_flowConsCons[s].setBounds(1.0, 1.0);
//             m_flowConsCons[t].setBounds(-1.0, -1.0);
//         }

//         Graph::Path getPath(const IloCplex& _sol) const {
//             auto listEdges = m_placement.m_inst.network.getEdges();
//             std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

//             std::vector<int> parent(m_placement.m_inst.network.getOrder(), -1);
//             for (int e = 0; e < m; ++e) {
//                 if (_sol.getValue(m_f[e])) {
//                     assert(parent[edges[e].second] == -1 || [&]() {
//                         std::cout << "Already have a parent: " << parent[edges[e].second] << " vs. " << parent[edges[e].first] << std::endl;
//                         return false;
//                     }());
//                     parent[edges[e].second] = edges[e].first;
//                 }
//             }

//             int u = std::get<1>(m_placement.m_inst.demands[m_demandID]);
//             Graph::Path path{u};
//             while (u != std::get<0>(m_placement.m_inst.demands[m_demandID])) {
//                 u = parent[u];
//                 path.push_front(u);
//             }
//             return path;
//         }

//       private:
//         Energy& m_placement;
//         int m_demandID;
//         int n;
//         int m;
//         IloModel m_model;

//         IloObjective m_obj;

//         IloIntVarArray m_f;

//         IloRangeArray m_flowConsCons;
//         IloRangeArray m_linkCapaCons;
//         IloRangeArray m_noLoopCons;
//     };

//   public:
//     Energy(const DiGraph& _network, const std::vector<Demand>& _demands)
//         : m_env()
//         , m_inst.network(_network)
//         , m_inst.demands(_demands)
//         , m_placementModel(*this)
//         , m_pp(0, *this) {
//         DiGraph tempFlowGraph = m_inst.network; // Graph of flow installed
//         for (auto& edge : tempFlowGraph.getEdges()) {
//             tempFlowGraph.setEdgeWeight(edge, 0);
//         }

//         for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//             auto path = ShortestPath<DiGraph>(&m_inst.network).getShortestPath(std::get<0>(m_inst.demands[i]), std::get<1>(m_inst.demands[i]), [&](const int& __u, const int& __v) { return std::get<2>(m_inst.demands[i]) + tempFlowGraph.getEdgeWeight(__u, __v) < m_inst.network.getEdgeWeight(__u, __v); }, [&](const int& __u, const int& __v) { return 1 + std::get<2>(m_inst.demands[i]) + tempFlowGraph.getEdgeWeight(__u, __v) / m_inst.network.getEdgeWeight(__u, __v); });
//             for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                 tempFlowGraph.setEdgeWeight(*iteU, *iteV, tempFlowGraph.getEdgeWeight(*iteU, *iteV) + std::get<2>(m_inst.demands[i]));
//             }
//             m_placementModel.addPath(path, i);
//         }
//     }

//     Energy(const DiGraph& _network, const std::vector<Demand>& _demands, std::vector<Graph::Path> _paths)
//         : m_env()
//         , m_inst.network(_network)
//         , m_inst.demands(_demands)
//         , m_placementModel(*this)
//         , m_pp(0, *this) {
//         assert(_paths.size() == _demands.size());
//         for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//             m_placementModel.addPath(_paths[i], i);
//         }
//     }

//     void solve() {
//         m_placementModel.m_solver.setOut(m_env.getNullStream());
//         std::list<std::pair<Graph::Path, int>> pathsToAdd;
//         do {
//             m_placementModel.m_solver.solve();
//             std::cout << "########################################################################" << std::endl;
//             std::cout << "########################################################################" << std::endl;
//             std::cout << "########################################################################" << std::endl;
//             std::cout << "########################################################################" << std::endl;

//             std::cout << "Reduced Main Problem Objective Value = " << m_placementModel.m_solver.getObjValue() << std::endl;

//             pathsToAdd.clear();
//             searchPaths(pathsToAdd);
//             for (auto& p : pathsToAdd) {
//                 m_placementModel.addPath(p.first, p.second);
//             }
//         } while (!pathsToAdd.empty());
//         searchPaths(pathsToAdd, 0);
//         for (auto& p : pathsToAdd) {
//             m_placementModel.addPathCheck(p.first, p.second);
//         }
//         m_placementModel.solveInteger();
//     }

//     void searchPaths(std::list<std::pair<Graph::Path, int>>& _paths, double _threshold = -RC_EPS) {
//         for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//             /* For each demands, search for a shortest path in the augmented graph */
//             const Demand& demand = m_inst.demands[i];
//             m_pp.updateDemand(i);
//             m_pp.updateDual(m_placementModel.m_solver);

//             IloCplex ppSolver(m_pp.m_model);
//             // ppSolver.exportModel("pp.sav");
//             ppSolver.setOut(m_env.getNullStream());
//             if (ppSolver.solve()) {
//                 double reducedCost = ppSolver.getObjValue();
//                 std::cout << "\033[2K" << '\r' << "Solving PP for " << demand << "(" << i << ") -> " << reducedCost << std::flush;
//                 if (reducedCost <= _threshold) {
//                     Graph::Path sPath = m_pp.getPath(ppSolver);
//                     std::cout << " with " << sPath << std::endl;
//                     _paths.emplace_back(sPath, i);
//                     std::cout << "\033[2K" << '\r' << _paths.size() << " new paths" << std::flush;
//                 }
//             }
//             ppSolver.end();
//         }
//     }

//   private:
//     IloEnv m_env { };
//     DiGraph m_inst.network;
//     std::vector<Demand> m_inst.demands;

//     PlacementModel m_placementModel;
//     PricingProblem m_pp;
//     friend PlacementModel;
// };

// class ClassicalEnergy {
//   public:
//     ClassicalEnergy(const DiGraph& _network, const std::vector<Demand>& _demands)
//         : m_env()
//         , m_inst.network(_network)
//         , m_inst.demands(_demands)
//         , n(m_inst.network.getOrder())
//         , m(m_inst.network.size())
//         , m_model(m_env)
//         , m_obj(IloAdd(m_model, IloMinimize(m_env)))
//         , m_f(IloAdd(m_model, IloBoolVarArray(m_env, m * m_inst.demands.size())))
//         , m_x(IloAdd(m_model, IloBoolVarArray(m_env, m)))
//         , m_flowConsCons(IloAdd(m_model, IloRangeArray(m_env, n * m_inst.demands.size(), 0.0, 0.0)))
//         , m_linkCapaCons(IloAdd(m_model, IloRangeArray(m_env, m, 0.0, IloInfinity)))
//         , m_bothLinks(IloAdd(m_model, IloRangeArray(m_env, m / 2, 0.0, 0.0)))
//         , m_noLoopCons1(IloAdd(m_model, IloRangeArray(m_env, n * m_inst.demands.size(), 0.0, 1.0)))
//         , m_noLoopCons2(IloAdd(m_model, IloRangeArray(m_env, n * m_inst.demands.size(), 0.0, 1.0)))
//         , m_sumLinks(IloAdd(m_model, IloRange(m_env, int(m_inst.demands.size()) == n * (n - 1) ? 2 * (m_inst.network.getOrder() - 1) : 0, m_inst.network.size())))
//         ,
//         // m_sumLinks( IloAdd(m_model, IloRange(m_env, 10, 10)) ),
//         m_solver(m_model)
//         , m_inst.edgeToId() {

//         auto listEdges = m_inst.network.getEdges();
//         std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

//         int i = 0;
//         for (int e = 0; e < int(edges.size()); ++e) {
//             m_inst.edgeToId[edges[e]] = i++;
//             IloNumColumn inc = m_obj(1.0);
//             inc += m_linkCapaCons[e](m_inst.network.getEdgeWeight(edges[e]));
//             inc += m_sumLinks(1.0);
//             m_x[e] = IloBoolVar(inc);

//             m_x[e].setName(std::string("x" + toString(edges[e])).c_str());
//             m_linkCapaCons[e].setName(std::string("linkCapa" + toString(edges[e])).c_str());
//         }

//         i = 0;
//         for (Node u = 0; u < m_inst.network.getOrder(); ++u) {
//             for (auto& v : m_inst.network.getNeighbors(u)) {
//                 if (u < v) {
//                     m_bothLinks[i].setLinearCoef(m_x[m_inst.edgeToId[Graph::Edge(u, v)]], 1.0);
//                     m_bothLinks[i].setLinearCoef(m_x[m_inst.edgeToId[Graph::Edge(v, u)]], -1.0);
//                     // std::cout << m_bothLinks[i] << std::endl;
//                     ++i;
//                 }
//             }
//         }

//         for (auto i = 0; i < int(m_inst.demands.size()); ++i) {
//             int s = std::get<0>(m_inst.demands[i]),
//                 t = std::get<1>(m_inst.demands[i]);
//             double d = std::get<2>(m_inst.demands[i]);

//             for (int e = 0; e < m; ++e) {
//                 IloNumColumn inc(m_env);
//                 inc += m_flowConsCons[n * i + edges[e].first](1.0);
//                 inc += m_flowConsCons[n * i + edges[e].second](-1.0);
//                 inc += m_linkCapaCons[e](-d);
//                 inc += m_noLoopCons1[n * i + edges[e].first](1.0);
//                 inc += m_noLoopCons2[n * i + edges[e].second](1.0);

//                 m_f[m * i + e] = IloBoolVar(inc);
//                 m_f[m * i + e].setName(std::string("f" + toString(std::make_tuple(m_inst.demands[i], edges[e]))).c_str());
//             }
//             // m_nodeCapaCons.setNames("nodeCapa");

//             m_flowConsCons[n * i + s].setUB(1.0);
//             m_flowConsCons[n * i + s].setLB(1.0);

//             m_flowConsCons[n * i + t].setLB(-1.0);
//             m_flowConsCons[n * i + t].setUB(-1.0);
//         }

//         m_flowConsCons.setNames("flowConservation");
//         m_linkCapaCons.setNames("linkCapa");

//         for (auto i = 0; i < int(m_inst.demands.size()); ++i) {
//             for (int e = 0; e < m; ++e) {
//                 m_noLoopCons1[n * i + edges[e].first].setName(std::string("m_noLoopCons1" + toString(m_inst.demands[i]) + "_" + toString(edges[e])).c_str());
//                 m_noLoopCons2[n * i + edges[e].first].setName(std::string("m_noLoopCons2" + toString(m_inst.demands[i]) + "_" + toString(edges[e])).c_str());
//                 // std::cout << m_noLoopCons1[n * i + edges[e].first] << std::endl;
//                 // std::cout << m_noLoopCons2[n * i + edges[e].first] << std::endl;
//             }
//         }
//         m_solver.setParam(IloCplex::RootAlg, IloCplex::Network);
//         m_solver.setOut(m_env.getNullStream());
//         m_solver.exportModel("clp.lp");
//     }

//     void solve() {
//         m_solver.solve();
//         std::cout << "Optimal obj value: " << m_solver.getObjValue() << std::endl;
//         std::cout << 100 * (1 - m_solver.getObjValue() / double(m_inst.network.size())) << std::endl;
//     }

//     std::vector<Graph::Path> getPaths() const {
//         auto listEdges = m_inst.network.getEdges();
//         std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};
//         std::vector<Graph::Path> paths;
//         paths.reserve(m_inst.demands.size());

//         for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//             // std::cout << m_inst.demands[i] << std::endl;
//             std::vector<int> parent(m_inst.network.getOrder(), -1);
//             parent[std::get<0>(m_inst.demands[i])] = std::get<0>(m_inst.demands[i]);

//             for (int e = 0; e < m; ++e) {
//                 if (m_solver.getValue(m_f[m * i + e])) {
//                     // std::cout << "m_f" << edges[e] << std::endl;
//                     assert(parent[edges[e].second] == -1 || [&]() {
//                         std::cout << "Already have a parent: " << parent[edges[e].second] << " vs. " << edges[e].first << std::endl;
//                         return false;
//                     }());
//                     parent[edges[e].second] = edges[e].first;
//                 }
//             }
//             // std::cout << "\t parents ->" << parent << std::endl;
//             int u = std::get<1>(m_inst.demands[i]);
//             Graph::Path path{u};
//             while (u != std::get<0>(m_inst.demands[i])) {
//                 u = parent[u];
//                 path.push_front(u);
//                 // std::cout << "path.push_front("<<u<<");"<< std::endl;
//             }
//             paths.push_back(path);
//         }
//         return paths;
//     }

//   private:
//     IloEnv m_env { };
//     const DiGraph m_inst.network;
//     const std::vector<Demand> m_inst.demands;

//     int n;
//     int m;
//     IloModel m_model;

//     IloObjective m_obj;

//     IloBoolVarArray m_f;
//     IloBoolVarArray m_x;

//     IloRangeArray m_flowConsCons;
//     IloRangeArray m_linkCapaCons;
//     IloRangeArray m_bothLinks;
//     IloRangeArray m_noLoopCons1;
//     IloRangeArray m_noLoopCons2;
//     IloRange m_sumLinks;

//     IloCplex m_solver;

//     std::map<Graph::Edge, int> m_inst.edgeToId;
// };

// #endif