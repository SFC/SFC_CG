// #ifndef ENERGY_SUB_HPP
// #define ENERGY_SUB_HPP

// #include "DiGraph.hpp"
// #include "ShortestPath.hpp"
// #include "utility.hpp"
// #include <cmath>
// #include <ilcplex/ilocplex.h>
// #include <map>
// #include <tuple>

// #define RC_EPS 1.0e-8

// typedef int function_descriptor;
// typedef std::tuple<int, int, double> Demand;
// typedef int Node;

// class EnergySub {
//     class PlacementModel {
//         friend EnergySub;

//       public:
//         PlacementModel(EnergySub& _placement)
//             : m_placement(_placement)
//             , m_model(m_placement.m_env)
//             , m_y(IloNumVarArray(m_placement.m_env))
//             , m_z(IloNumVarArray(m_placement.m_env))
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_onePathCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size(), 1.0, 1.0)))
//             , m_linkCapasCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(), 0.0, IloInfinity)))
//             , m_oneSub(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(), 0.0, 1.0)))
//             , m_solver(m_model)
//             , m_inst.edgeToId()
//             , m_paths()
//             , m_graphs() {
//             m_onePathCons.setNames("onePath");
//             int i = 0;
//             for (Graph::Edge e : m_placement.m_inst.network.getEdges()) {
//                 m_inst.edgeToId[e] = i;
//                 m_linkCapasCons[i].setName(std::string("linkCapa" + toString(e)).c_str());
//                 ++i;
//             }
//             m_solver.setOut(m_placement.m_env.getNullStream());
//         }

//         PlacementModel& operator=(const PlacementModel&) = delete;
//         PlacementModel(const PlacementModel&) = delete;

//         void addPath(const Graph::Path& _path, int _i) {
//             std::cout << "addPath" << _path << std::endl;
//             assert([&]() {
//                 auto ite = std::find(m_paths.begin(), m_paths.end(), _path);
//                 if (ite == m_paths.end()) {
//                     return true;
//                 } else {
//                     std::cout << _path << " is already present!" << std::endl;
//                     return false;
//                 }
//             }());

//             const Demand& demand = m_placement.m_inst.demands[_i];
//             int s = std::get<0>(demand), t = std::get<1>(demand);
//             double d = std::get<2>(demand);

//             IloNumColumn inc(m_onePathCons[_i](1.0)); // + m_obj(d * (path.size() - 1) ));
//             for (auto iteU = _path.begin(), iteV = std::next(iteU); iteV != _path.end(); ++iteU, ++iteV) {
//                 int i = m_inst.edgeToId[Graph::Edge(*iteU, *iteV)];

//                 inc += m_linkCapasCons[i](-d);
//             }
//             IloNumVar yVar = IloNumVar(inc);
//             yVar.setName(std::string("y" + toString(std::make_tuple(s, t, _path))).c_str());
//             m_y.add(yVar);
//             m_paths.push_back(_path);
//         }

//         void addGraph(const DiGraph& _graph) {
//             std::cout << "addGraph" << _graph.getEdges() << std::endl;
//             assert([&]() {
//                 auto ite = std::find(m_graphs.begin(), m_graphs.end(), _graph);
//                 if (ite == m_graphs.end()) {
//                     return true;
//                 } else {
//                     std::cout << "Graph is already present!" << std::endl;
//                     return false;
//                 }
//             }());

//             IloNumColumn inc(m_obj(_graph.size()));
//             for (auto& edge : _graph.getEdges()) {
//                 inc += m_linkCapasCons[m_inst.edgeToId[edge]](m_placement.m_inst.network.getEdgeWeight(edge));
//                 inc += m_oneSub[m_inst.edgeToId[edge]](1.0);
//             }
//             IloNumVar zVar = IloNumVar(inc);
//             zVar.setName(std::string("z(" + toString(_graph.getEdges()) + ")").c_str());
//             m_z.add(zVar);
//             m_graphs.push_back(_graph);
//         }

//         void solveInteger() {
//             m_model.add(IloConversion(m_placement.m_env, m_y, ILOINT));
//             m_model.add(IloConversion(m_placement.m_env, m_z, ILOINT));
//             m_model.add(m_obj.getExpr() == 10);
//             m_solver.exportModel("lp.lp");
//             m_solver.solve();
//             std::cout << "Final obj value: " << m_solver.getObjValue() << "\t# Paths: " << m_paths.size() << std::endl;
//             std::cout << 100 * (1 - m_solver.getObjValue() / double(m_placement.m_inst.network.size())) << "% saved" << std::endl;
//             std::cout << m_model << std::endl;
//         }

//       private:
//         EnergySub& m_placement;
//         IloModel m_model;

//         IloNumVarArray m_y;
//         IloNumVarArray m_z;

//         IloObjective m_obj;

//         IloRangeArray m_onePathCons;
//         IloRangeArray m_linkCapasCons;
//         IloRangeArray m_oneSub;
//         // IloRangeArray m_nodeCapasCons;
//         // IloRangeArray m_activeCons1;
//         // IloRangeArray m_activeCons2;

//         IloCplex m_solver;

//         std::map<Graph::Edge, int> m_inst.edgeToId;
//         std::vector<Graph::Path> m_paths;
//         std::vector<DiGraph> m_graphs;
//     };

//     class PricingProblemPath {
//         friend EnergySub;

//       public:
//         PricingProblemPath(int _demandID, EnergySub& _chainPlacement)
//             : m_placement(_chainPlacement)
//             , m_demandID(_demandID)
//             , n(m_placement.m_inst.network.getOrder())
//             , m(m_placement.m_inst.network.size())
//             , m_model(m_placement.m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_f(IloAdd(m_model, IloBoolVarArray(m_placement.m_env, m)))
//             , m_flowConsCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, n, 0.0, 0.0)))
//             , m_linkCapaCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m, 0.0, 0.0)))
//             , m_noLoopCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, 2 * n, 0.0, 1.0))) {
//             m_obj.setName(std::string("PP_" + toString(m_placement.m_inst.demands[m_demandID])).c_str());

//             const int &s = std::get<0>(m_placement.m_inst.demands[m_demandID]),
//                       t = std::get<1>(m_placement.m_inst.demands[m_demandID]);
//             const double& d = std::get<2>(m_placement.m_inst.demands[m_demandID]);
//             // const std::vector<function_descriptor>& functions = std::get<3>(m_placement.m_inst.demands[m_demandID]);

//             auto listEdges = m_placement.m_inst.network.getEdges();
//             std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

//             for (int e = 0; e < int(edges.size()); ++e) {
//                 m_linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(edges[e]));
//             }

//             for (int e = 0; e < m; ++e) {
//                 IloNumColumn inc(m_placement.m_env);
//                 inc += m_flowConsCons[edges[e].first](1.0);
//                 inc += m_flowConsCons[edges[e].second](-1.0);
//                 inc += m_linkCapaCons[e](d);
//                 inc += m_noLoopCons[edges[e].first](1.0);
//                 inc += m_noLoopCons[n + edges[e].second](1.0);

//                 m_f[e] = IloBoolVar(inc);
//                 m_f[e].setName(std::string("f" + toString(edges[e])).c_str());
//             }

//             m_flowConsCons.setNames("flowConservation");
//             m_linkCapaCons.setNames("linkCapa");
//             // m_nodeCapaCons.setNames("nodeCapa");
//             m_noLoopCons.setNames("uniqueFlow");

//             m_flowConsCons[s].setUB(1.0);
//             m_flowConsCons[s].setLB(1.0);

//             m_flowConsCons[t].setLB(-1.0);
//             m_flowConsCons[t].setUB(-1.0);
//         }

//         void updateDual(const IloCplex& _sol) {
//             double d = std::get<2>(m_placement.m_inst.demands[m_demandID]);
//             m_obj.setConstant(-_sol.getDual(m_placement.m_placementModel.m_onePathCons[m_demandID]));
//             // std::cout << "One Path dual : " << -_sol.getDual(m_placement.m_placementModel.m_onePathCons[_i]) << std::endl;

//             for (int e = 0; e < m; ++e) {
//                 // auto& edge = *(std::next(m_placement.m_inst.network.getEdges().begin(), e));
//                 // std::cout << "link dual"<< edge << " : " << _sol.getDual(m_placement.m_placementModel.m_linkCapasCons[e]) << std::endl;
//                 m_obj.setLinearCoef(m_f[e], d * _sol.getDual(m_placement.m_placementModel.m_linkCapasCons[e])
//                     // + _sol.getDual(m_placement.m_placementModel.m_activeCons1[e])
//                     // - _sol.getDual(m_placement.m_placementModel.m_activeCons2[e])
//                     );
//             }
//         }

//         void updateDemand(int _demandID) {
//             int s = std::get<0>(m_placement.m_inst.demands[m_demandID]),
//                 t = std::get<1>(m_placement.m_inst.demands[m_demandID]);

//             m_flowConsCons[s].setUB(0.0);
//             m_flowConsCons[s].setLB(0.0);

//             m_flowConsCons[t].setLB(0.0);
//             m_flowConsCons[t].setUB(0.0);

//             m_demandID = _demandID;

//             s = std::get<0>(m_placement.m_inst.demands[m_demandID]);
//             t = std::get<1>(m_placement.m_inst.demands[m_demandID]);

//             m_flowConsCons[s].setUB(1.0);
//             m_flowConsCons[s].setLB(1.0);

//             m_flowConsCons[t].setLB(-1.0);
//             m_flowConsCons[t].setUB(-1.0);
//         }

//         Graph::Path getPath(const IloCplex& _sol) const {
//             auto listEdges = m_placement.m_inst.network.getEdges();
//             std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

//             std::vector<int> parent(m_placement.m_inst.network.getOrder(), -1);
//             for (int e = 0; e < m; ++e) {
//                 if (_sol.getValue(m_f[e])) {
//                     assert(parent[edges[e].second] == -1 || [&]() {
//                         std::cout << "Already have a parent: " << parent[edges[e].second] << " vs. " << parent[edges[e].first] << std::endl;
//                         return false;
//                     }());
//                     parent[edges[e].second] = edges[e].first;
//                 }
//             }

//             int u = std::get<1>(m_placement.m_inst.demands[m_demandID]);
//             Graph::Path path{u};
//             while (u != std::get<0>(m_placement.m_inst.demands[m_demandID])) {
//                 u = parent[u];
//                 path.push_front(u);
//             }
//             return path;
//         }

//       private:
//         EnergySub& m_placement;
//         int m_demandID;
//         int n;
//         int m;
//         IloModel m_model;

//         IloObjective m_obj;

//         IloBoolVarArray m_f;

//         IloRangeArray m_flowConsCons;
//         IloRangeArray m_linkCapaCons;
//         IloRangeArray m_noLoopCons;
//     };

//     class PricingProblemSubgraph {
//         friend EnergySub;

//       public:
//         PricingProblemSubgraph(EnergySub& _chainPlacement)
//             : m_placement(_chainPlacement)
//             , n(m_placement.m_inst.network.getOrder())
//             , m(m_placement.m_inst.network.size())
//             , m_model(m_placement.m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_x(IloAdd(m_model, IloBoolVarArray(m_placement.m_env, m)))
//             , m_bothLinks(IloAdd(m_model, IloRangeArray(m_placement.m_env, m / 2, 0.0, 0.0)))
//             , m_solver(m_model) {
//             m_obj.setName("PPBase");
//             auto listEdges = m_placement.m_inst.network.getEdges();
//             std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

//             for (int e = 0; e < int(edges.size()); ++e) {
//                 m_x[e].setName(std::string("x" + toString(edges[e])).c_str());
//             }

//             int i = 0;
//             for (Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                 for (auto& v : m_placement.m_inst.network.getNeighbors(u)) {
//                     if (u < v) {
//                         m_bothLinks[i].setLinearCoef(m_x[m_placement.m_placementModel.m_inst.edgeToId[Graph::Edge(u, v)]], 1.0);
//                         m_bothLinks[i].setLinearCoef(m_x[m_placement.m_placementModel.m_inst.edgeToId[Graph::Edge(v, u)]], -1.0);
//                         ++i;
//                     }
//                 }
//             }
//             m_solver.setOut(m_placement.m_env.getNullStream());
//         }

//         void updateDual(const IloCplex& _sol) {
//             auto listEdges = m_placement.m_inst.network.getEdges();
//             std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

//             // m_obj.setConstant(-_sol.getDual(m_placement.m_placementModel.m_onePathCons[m_demandID]));
//             // std::cout << "One Path dual : " << -_sol.getDual(m_placement.m_placementModel.m_onePathCons[_i]) << std::endl;

//             for (int e = 0; e < m; ++e) {
//                 // auto& edge = *(std::next(m_placement.m_inst.network.getEdges().begin(), e));
//                 // std::cout << "link dual"<< edge << " : " << _sol.getDual(m_placement.m_placementModel.m_linkCapasCons[e]) << std::endl;
//                 m_obj.setLinearCoef(m_x[e], 1
//                                                 - m_placement.m_inst.network.getEdgeWeight(edges[e]) * _sol.getDual(m_placement.m_placementModel.m_linkCapasCons[e])
//                                                 + _sol.getDual(m_placement.m_placementModel.m_oneSub[e]));
//             }
//         }

//         DiGraph getGraph() const {
//             auto listEdges = m_placement.m_inst.network.getEdges();
//             std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

//             DiGraph graph(n);
//             for (int e = 0; e < m; ++e) {
//                 if (m_solver.getValue(m_x[e])) {
//                     graph.addEdge(edges[e].first, edges[e].second);
//                     graph.addEdge(edges[e].second, edges[e].first);
//                 }
//             }
//             return graph;
//         }

//         double solve() {
//             std::cout << m_model << std::endl;
//             // m_solver.exportModel("subLp.lp");
//             m_solver.solve();
//             return m_solver.getObjValue();
//         }

//       private:
//         EnergySub& m_placement;
//         int n;
//         int m;
//         IloModel m_model;

//         IloObjective m_obj;

//         IloBoolVarArray m_x;

//         IloRangeArray m_bothLinks;

//         IloCplex m_solver;
//     };

//   public:
//     EnergySub(const DiGraph& _network, const std::vector<Demand>& _demands)
//         : m_env()
//         , m_inst.network(_network)
//         , m_inst.demands(_demands)
//         , m_placementModel(*this) {
//         DiGraph tempFlowGraph = m_inst.network; // Graph of flow installed
//         for (auto& edge : tempFlowGraph.getEdges()) {
//             tempFlowGraph.setEdgeWeight(edge, 0);
//         }

//         for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//             auto path = ShortestPath<DiGraph>(&m_inst.network).getShortestPath(std::get<0>(m_inst.demands[i]), std::get<1>(m_inst.demands[i]), [&](const int& __u, const int& __v) { return std::get<2>(m_inst.demands[i]) + tempFlowGraph.getEdgeWeight(__u, __v) < m_inst.network.getEdgeWeight(__u, __v); }, [&](const int& __u, const int& __v) { return 1 + std::get<2>(m_inst.demands[i]) + tempFlowGraph.getEdgeWeight(__u, __v) / m_inst.network.getEdgeWeight(__u, __v); });
//             for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                 tempFlowGraph.setEdgeWeight(*iteU, *iteV, tempFlowGraph.getEdgeWeight(*iteU, *iteV) + std::get<2>(m_inst.demands[i]));
//             }
//             m_placementModel.addPath(path, i);
//         }
//         m_placementModel.addGraph(m_inst.network);
//     }

//     void solve() {
//         IloCplex mainSolver(m_placementModel.m_model);
//         mainSolver.setOut(m_env.getNullStream());

//         PricingProblemPath pp(0, *this);
//         PricingProblemSubgraph ppSub(*this);
//         bool reducedCost;
//         do {
//             reducedCost = false;
//             mainSolver.solve();
//             std::cout << "########################################################################" << std::endl;
//             std::cout << "########################################################################" << std::endl;
//             std::cout << "########################################################################" << std::endl;
//             std::cout << "########################################################################" << std::endl;

//             std::cout << "Reduced Main Problem Objective Value = " << mainSolver.getObjValue() << std::endl;

//             std::list<std::pair<Graph::Path, int>> toAdd;
//             for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//                 /* For each demands, search for a shortest path in the augmented graph */
//                 const Demand& demand = m_inst.demands[i];
//                 pp.updateDemand(i);
//                 pp.updateDual(mainSolver);

//                 IloCplex ppSolver(pp.m_model);
//                 // ppSolver.exportModel("pp.sav");
//                 ppSolver.setOut(m_env.getNullStream());
//                 ppSolver.solve();

//                 double rc = ppSolver.getObjValue();
//                 std::cout << "\033[2K" << '\r' << "Solving PP for " << demand << "(" << i << ") -> " << rc << std::flush;
//                 if (rc < -RC_EPS) {
//                     std::cout << std::endl
//                               << "Reduced cost of " << ppSolver.getObjValue() << std::endl;
//                     // std::cout << -mainSolver.getDual(m_placementModel.m_onePathCons[i]) << std::endl;
//                     // for(int e = 0; e < m_inst.network.size(); ++e) {
//                     // 	auto& edge = *(std::next(m_inst.network.getEdges().begin(), e));
//                     // 	std::cout << '\t' << "link dual"<< edge << " : " << mainSolver.getDual(m_placementModel.m_linkCapasCons[e]) << std::endl;
//                     // }
//                     Graph::Path sPath = pp.getPath(ppSolver);
//                     std::cout << " with " << sPath << std::endl;
//                     reducedCost = true;
//                     toAdd.emplace_back(sPath, i);
//                     std::cout << "\033[2K" << '\r' << toAdd.size() << " new paths" << std::flush;
//                 }
//                 ppSolver.end();
//             }
//             std::cout << std::endl;
//             ppSub.updateDual(mainSolver);
//             double reducedCostSub = ppSub.solve();
//             std::cout << "Solving PP for subgraph ->" << reducedCostSub << std::endl;
//             if (reducedCostSub < -RC_EPS) {
//                 reducedCost = true;
//                 m_placementModel.addGraph(ppSub.getGraph());
//             }
//             for (auto& p : toAdd) {
//                 m_placementModel.addPath(p.first, p.second);
//             }

//         } while (reducedCost);
//         m_placementModel.solveInteger();
//     }

//   private:
//     IloEnv m_env { };
//     DiGraph m_inst.network;
//     std::vector<Demand> m_inst.demands;

//     PlacementModel m_placementModel;
//     friend PlacementModel;
// };

// #endif