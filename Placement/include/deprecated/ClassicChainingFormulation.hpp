// #ifndef ClassicChainingFormulation_HPP
// #define ClassicChainingFormulation_HPP

// class ClassicChainingFormulation {
//   public:
//     PricingProblem(const Demand& _demand, EnergyChaining& _chainPlacement)
//         : m_placement(_chainPlacement)
//         , m_demand(_demand)
//         , n(m_placement.m_inst.network.getOrder())
//         , m(m_placement.m_inst.network.size())
//         , nbLayers(std::get<3>(m_demand).size() + 1)
//         , m_model(m_placement.m_env)
//         , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//         , m_a(IloAdd(m_model, IloIntVarArray(m_placement.m_env, n * nbLayers, 0, 1)))
//         , m_f(IloAdd(m_model, IloIntVarArray(m_placement.m_env, m * nbLayers, 0, 1)))
//         , m_flowConsCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, n * nbLayers, 0.0, 0.0)))
//         , m_linkCapaCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m, 0.0, 0.0)))
//         , m_nodeCapaCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, n, 0.0, 100.0)))
//         , m_noLoopCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, 2 * n, 0.0, 1.0))) {
//         m_obj.setName(std::string("PP_" + toString(m_demand)).c_str());

//         int s = std::get<0>(m_demand),
//             t = std::get<1>(m_demand);
//         double d = std::get<2>(m_demand);
//         const std::vector<function_descriptor>& functions = std::get<3>(m_demand);

//         auto listEdges = m_placement.m_inst.network.getEdges();
//         std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

//         for (int e = 0; e < int(edges.size()); ++e) {
//             m_linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(edges[e]));
//         }

//         int source = n * 0 + s;
//         m_flowConsCons[source].setUB(1.0);
//         m_flowConsCons[source].setLB(1.0);

//         int sink = n * (nbLayers - 1) + t;
//         m_flowConsCons[sink].setLB(-1.0);
//         m_flowConsCons[sink].setUB(-1.0);

//         for (int e = 0; e < m; ++e) {
//             for (int i = 0; i < nbLayers; ++i) {
//                 IloNumColumn inc(m_placement.m_env);
//                 inc += m_flowConsCons[n * i + edges[e].first](1.0);
//                 inc += m_flowConsCons[n * i + edges[e].second](-1.0);
//                 inc += m_linkCapaCons[e](d);
//                 inc += m_noLoopCons[edges[e].first](1.0);
//                 inc += m_noLoopCons[n + edges[e].second](1.0);

//                 m_f[m * i + e] = IloIntVar(inc);
//                 m_f[m * i + e].setName(std::string("f" + toString(std::make_tuple(edges[e], i))).c_str());
//             }
//         }

//         for (Node u = 0; u < n; ++u) {
//             for (int i = 1; i < nbLayers; ++i) {
//                 function_descriptor f = functions[i - 1];
//                 IloNumColumn inc = m_flowConsCons[n * (i - 1) + u](1.0);
//                 inc += m_flowConsCons[n * i + u](-1.0);
//                 inc += m_nodeCapaCons[u](d * m_placement.m_funcCharge[f - 1]);

//                 m_a[n * i + u] = IloIntVar(inc);

//                 m_a[n * i + u].setName(std::string("a" + toString(std::make_tuple(i, u))).c_str());
//             }
//         }

//         m_flowConsCons.setNames("flowConservation");
//         m_linkCapaCons.setNames("linkCapa");
//         m_nodeCapaCons.setNames("nodeCapa");
//         m_noLoopCons.setNames("uniqueFlow");
//     }

//     void updateDual(const IloCplex& _sol, int _i) {
//         double d = std::get<2>(m_demand);
//         const std::vector<function_descriptor>& functions = std::get<3>(m_demand);
//         m_obj.setConstant(-_sol.getDual(m_placement.m_placementModel.m_onePathCons[_i]));
//         // std::cout << "One Path dual : " << -_sol.getDual(m_placement.m_placementModel.m_onePathCons[_i]) << std::endl;

//         for (int e = 0; e < m; ++e) {
//             // auto& edge = *(std::next(m_placement.m_inst.network.getEdges().begin(), e));
//             // std::cout << "link dual"<< edge <<" : " << _sol.getDual(m_placement.m_placementModel.m_linkCapasCons[e]) << std::endl;
//             for (int i = 0; i < nbLayers; ++i) {
//                 m_obj.setLinearCoef(m_f[m * i + e], d * _sol.getDual(m_placement.m_placementModel.m_linkCapasCons[e])
//                                                         + _sol.getDual(m_placement.m_placementModel.m_activeCons1[e])
//                                                         - _sol.getDual(m_placement.m_placementModel.m_activeCons2[e]));
//             }
//         }
//         for (Node u = 0; u < n; ++u) {
//             // std::cout << "Node capa dual : " << _sol.getDual(m_placement.m_placementModel.m_nodeCapasCons[u]) << std::endl;
//             for (int i = 1; i < nbLayers; ++i) {
//                 function_descriptor f = functions[i - 1];
//                 m_obj.setLinearCoef(m_a[n * i + u], _sol.getDual(m_placement.m_placementModel.m_nodeCapasCons[u]) * d * m_placement.m_funcCharge[f - 1]);
//             }
//         }
//         // sk
//     }

//     ServicePath getServicePath(const IloCplex& _sol) const {
//         auto listEdges = m_placement.m_inst.network.getEdges();
//         std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

//         std::vector<int> parent(m_placement.m_inst.network.getOrder(), -1);
//         for (int e = 0; e < m; ++e) {
//             for (int i = 0; i < nbLayers; ++i) {
//                 if (_sol.getValue(m_f[m * i + e])) {
//                     assert(parent[edges[e].second] == -1 || [&]() {
//                         std::cout << "Already have a parent: " << parent[edges[e].second] << " vs. " << parent[edges[e].first] << std::endl;
//                         return false;
//                     }());
//                     parent[edges[e].second] = edges[e].first;
//                 }
//             }
//         }

//         int u = std::get<1>(m_demand);
//         Graph::Path path{u};
//         while (u != std::get<0>(m_demand)) {
//             u = parent[u];
//             path.push_front(u);
//         }

//         std::list<std::pair<Node, function_descriptor>> installs;
//         for (Node u = 0; u < n; ++u) {
//             for (int i = 1; i < nbLayers; ++i) {
//                 if (_sol.getValue(m_a[n * i + u])) {
//                     installs.emplace_back(u, std::get<3>(m_demand)[i - 1]);
//                 }
//             }
//         }
//         return ServicePath(path, installs);
//     }

//   private:
//     EnergyChaining& m_placement;
//     const Demand& m_demand;
//     int n;
//     int m;
//     int nbLayers;
//     IloModel m_model;

//     IloObjective m_obj;

//     IloIntVarArray m_a;
//     IloIntVarArray m_f;

//     IloRangeArray m_flowConsCons;
//     IloRangeArray m_linkCapaCons;
//     IloRangeArray m_nodeCapaCons;
//     IloRangeArray m_noLoopCons;
// };