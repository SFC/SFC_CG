#ifndef CHAINING_PATH_THREAD_BF_HPP
#define CHAINING_PATH_THREAD_BF_HPP

#include <ilconcert/iloexpression.h>
#include <ilconcert/ilolinear.h>
#include <ilconcert/ilosys.h>
#include <ilcplex/ilocplex.h>
#include <ilcplex/ilocplexi.h>

#include "ColumnGeneration.hpp"
#include "SFC.hpp"

namespace SFC {

class PlacementDWD_Path;

struct DualValues {
    DualValues(const Instance& _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;
    IloNum getReducedCost(const CrossLayerLink& _link) const;
    IloNum getReducedCost(const IntraLayerLink& _link) const;
    double getDualSumRHS() const;
    double getAdditionalVariable() const;
    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;
};

class PlacementDWD_Path : public RMP<PlacementDWD_Path, DualValues, SFC::Solution<int>, ServicePath> {
    friend DualValues;

  public:
    using Base = RMP<PlacementDWD_Path, DualValues, SFC::Solution<int>, ServicePath>;
    friend Base;
    using Column = ServicePath;

    explicit PlacementDWD_Path(const Instance* _inst);
    PlacementDWD_Path(const PlacementDWD_Path&) = default;
    PlacementDWD_Path(PlacementDWD_Path&&) = default;
    PlacementDWD_Path& operator=(const PlacementDWD_Path&) = default;
    PlacementDWD_Path& operator=(PlacementDWD_Path&&) = default;
    ~PlacementDWD_Path() { m_env.end(); }

    const DualValues& getDualValues() const {
        return m_duals;
    }

    const DualValues& getDualValues_impl() const {
        return m_duals;
    }

    double getReducedCost_impl(const Column& _sPath, const DualValues& _dualValues) const;

    IloNumColumn getColumn_impl(const Column& _col);
    void getDuals_impl();
    void getNetworkUsage(const IloCplex& _sol) const;

    template <typename T>
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }
    using Base::getNbColumns;
    std::vector<Column> getServicePaths() const;
    Solution getSolution() const;
    void removeDummyIfPossible();

  private:
    const Instance* m_inst;

    using Base::m_columns;
    using Base::m_duals;
    using Base::m_dummyActive;
    using Base::m_env;
    using Base::m_fracObj;
    using Base::m_intObj;
    using Base::m_model;
    using Base::m_solver;

    IloObjective m_obj;
    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;
    std::vector<IloNumVar> m_dummyPaths;
    std::vector<double> m_linkCharge = std::vector<double>(num_edges(m_inst->network));
    std::vector<double> m_nodeCharge = std::vector<double>(num_vertices(m_inst->network));
};

} // namespace SFC
#endif
