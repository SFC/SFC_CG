#ifndef CHAINING_PATH_FUNCOCC_HPP
#define CHAINING_PATH_FUNCOCC_HPP

#include "ColumnGeneration.hpp"
#include "OccLim.hpp"
#include "cplex_utility.hpp"
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/multi_array.hpp>
#include <ilcplex/ilocplex.h>

namespace SFC::OccLim {

namespace Beta {

struct DualValues {
    DualValues(const Instance* _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;

    IloNum getReducedCost(const CrossLayerLink& _link) const;
    IloNum getReducedCost(const IntraLayerLink& _link) const;
    double getDualSumRHS() const;

    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;
    boost::multi_array<IloNum, 3> m_pathFuncDuals;
    std::vector<IloNum> m_nbLicensesDuals;
};

class Master : public RMP<Master, DualValues, SFC::OccLim::Solution<int>, ServicePath> {
    friend DualValues;

  public:
    using Base = RMP<Master, DualValues, SFC::OccLim::Solution<int>, ServicePath>;
    friend Base;
    using Column = ServicePath;

    explicit Master(const Instance* _inst);
    Master& operator=(const Master&) = delete;
    Master& operator=(Master&&) = delete;
    Master(const Master&) = delete;
    Master(Master&&) = delete;
    ~Master() { m_env.end(); }

    double getReducedCost_impl(const ServicePath& _col, const DualValues& _dualValues) const;
    IloNumColumn getColumn_impl(const ServicePath& _col);
    void getDuals_impl();
    void removeDummyIfPossible();
    const DualValues& getDualValues_impl() const;
    std::vector<ServicePath> getServicePaths() const;
    template <typename T>
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }
    Solution getSolution() const;
    using Base::getNbColumns;

  private:
    using Base::m_columns;
    using Base::m_duals;
    using Base::m_dummyActive;
    using Base::m_env;
    using Base::m_fracObj;
    using Base::m_intObj;
    using Base::m_model;
    using Base::m_solver;

    const Instance* m_inst;

    boost::multi_array<IloNumVar, 2> m_b;

    IloObjective m_obj;

    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;
    std::vector<IloRange> m_nbLicensesCons;
    boost::multi_array<IloRange, 2> m_pathFuncCons;

    std::vector<IloNumVar> m_dummyPaths;
    std::vector<double> m_linkCharge;
    std::vector<double> m_nodeCharge;
    boost::multi_array<double, 2> m_funcPath;
};

void barycenter(const DualValues& _nDuals, const DualValues& _bDual, DualValues& _sDuals, double _alpha);

} // namespace Beta

namespace Alpha {

struct DualValues {
    DualValues(const Instance* _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;

    IloNum getReducedCost(const CrossLayerLink& _link) const;
    IloNum getReducedCost(const IntraLayerLink& _link) const;
    double getDualSumRHS() const;

    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;
    boost::multi_array<IloNum, 3> m_pathFuncDuals;
    std::vector<IloNum> m_nbLicensesDuals;
};

class Master : public RMP<Master, DualValues, SFC::OccLim::Solution<int>, ServicePath> {
    friend DualValues;

  public:
    using Base = RMP<Master, DualValues, SFC::OccLim::Solution<int>, ServicePath>;
    friend Base;
    using Column = ServicePath;

    explicit Master(const Instance* _inst);
    Master& operator=(const Master&) = delete;
    Master& operator=(Master&&) = delete;
    Master(const Master&) = delete;
    Master(Master&&) = delete;
    ~Master() { m_env.end(); }

    double getReducedCost_impl(const ServicePath& _col, const DualValues& _dualValues) const;
    IloNumColumn getColumn_impl(const ServicePath& _col);
    void getDuals_impl();
    void removeDummyIfPossible();
    const DualValues& getDualValues_impl() const;
    std::vector<ServicePath> getServicePaths() const;
    template <typename T>
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }
    Solution getSolution() const;

    bool checkIntegrality_impl() const;
    std::vector<IloRange> getBranch_impl() const;

    using Base::getNbColumns;

  private:
    using Base::m_columns;
    using Base::m_duals;
    using Base::m_dummyActive;
    using Base::m_env;
    using Base::m_fracObj;
    using Base::m_integerModel;
    using Base::m_intObj;
    using Base::m_model;
    using Base::m_solver;

    const Instance* m_inst;

    boost::multi_array<IloNumVar, 2> m_b;
    IloObjective m_obj;
    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;
    std::vector<IloRange> m_nbLicensesCons;
    boost::multi_array<LazyConstraint, 3> m_pathFuncCons;

    std::vector<IloNumVar> m_dummyPaths;
    std::vector<IloNum> m_linkCharge;
    std::vector<IloNum> m_nodeCharge;
    boost::multi_array<IloNum, 2> m_funcPath;
};

bool checkIntegrality(const Master& _master);

void barycenter(const DualValues& _nDuals, const DualValues& _bDual, DualValues& _sDuals, double _alpha);

std::ostream& operator<<(std::ostream& _out, const DualValues& _dualValues);
} // namespace Alpha

} // namespace SFC::OccLim
#endif
