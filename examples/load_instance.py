import argparse
import json
import networkx as nx

import sfc


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()
    # Load an instance where all node can host VNFs
    instance_dict = json.load(
        open(args.filename))
    functions_charge = instance_dict['functions_charges']
    print("# functions: ", len(functions_charge))
    g = nx.Graph((edge['src'], edge['dst'], {'bandwidth': edge['bandwidth']})
                 for edge in instance_dict['edge_list'])
    g = nx.DiGraph(g)
    edge_list = list(g.edges())
    print(f"Edges: {edge_list}")
    link_capacities = [g[u][v]['bandwidth'] for u, v in edge_list]

    order = int(instance_dict['order'])
    print("# edges: ", len(edge_list))
    print("# vertices: ", order)
    network = sfc.Network(edge_list=edge_list, order=order)
    # Create one demand with id 0 between 1 and 2, 10 unit of bandwidth
    # and the service chain (f0, f1, f2, f3, f4)
    demands = [sfc.Demand(i, demand['src'], demand['dst'], demand['bandwidth'],
                          demand['functions'], demand['delay']) for i, demand in enumerate(instance_dict['demands'])]
    print("# demands: ", len(demands))
    # All nodes have 100 capacity
    node_capacities = instance_dict['nodeCapacity']
    # Create the instance and run the algorithm
    inst = sfc.Instance(network=network, link_capacities=link_capacities,
                        node_capacities=node_capacities, demands=demands, functions_charge=functions_charge)
    nb_threads = 1
    sol = sfc.cgAndBranchAndBound(inst, nb_threads)
    if not sol:
        print("No solution found")
    else:
        print(f"Found {len(sol.getServicePaths())} paths")
