import sfc
import networkx as nx

if __name__ == "__main__":
    # Create 6 NFVs with a one-to-one charge
    nb_functions = 6
    functions_charge = [1.0] * nb_functions
    # Create ring topology with 10 nodes and 1000 units of bw on each link
    g = nx.generators.cycle_graph(10)
    edge_list = [e for e in g.edges()]
    link_capacities = [1000] * g.size()
    link_delays = [1000] * g.size()
    order = g.order()
    network = sfc.Network(edge_list=edge_list,
                          order=order)
    # Create one demand with id 0 between 1 and 2, 10 unit of bandwidth
    # and the service chain (f0, f1, f2, f3, f4)
    demands = [sfc.Demand(id=0, source=0, dest=1, bandwidth=10.0, functions=[
                          0, 1, 2, 3, 4], cpu_charges=[1]*5, ram_charges=[1]*5, storage_charges=[1]*5, delay=10)]
    # All nodes have 100 capacity
    cpu_capacities = [100] * order
    ram_capacities = [100] * order
    storage_capacities = [100] * order
    # Create the instance and run the algorithm
    inst = sfc.Instance(network=network, link_capacities=link_capacities,
                        link_delays=link_delays, cpu_capacities=cpu_capacities, ram_capacities=ram_capacities,
                        storage_capacities=storage_capacities,
                        demands=demands)
    NB_LICENCES = 1
    NB_THREADS = 1
    TIME_LIMIT = 60
    SEED = 0
    VISITOR = sfc.CRGBnBVisitor()
    sol = sfc.crgAndBranchAndBound(
        inst, NB_LICENCES, NB_THREADS, TIME_LIMIT, SEED, VISITOR)
    if not sol:
        print("No solution found")
    else:
        for sPath in sol.getServicePaths():
            print(
                f"Path for demand {sPath.id}: {sPath.path}, NFVs at {sPath.locations}")
