"""
Print all commands for the replication experiments for a given network or all networks
Arguments:
	1st: Network name. If equal to "all", experiments will be run on all networks 
"""
import sys


orders = {
	"germany50": 50, 
	"germany50_HW": 50, 
	"atlanta": 15, 
	"atlanta_HW": 15, 
	"ta2": 65, 
	"zib54": 54,
	"pdh": 10,
	"pdh_HW": 10
}

percents = {
	"germany50": [1, 2.25, 3.5, 5.25 ,6], 
	"germany50_HW": [1, 2.25, 3.5, 5.25 ,6], 
	"atlanta": [1, 2.25, 3.5, 5.25 ,6], 
	"atlanta_HW": [1, 2.25, 3.5, 5.25 ,6], 
	"ta2": [1, 2.75, 4.5, 6.25, 8], 
	"zib54": [1, 3, 5, 7, 9],
	"pdh": [1, 2.25, 3.5, 5.25 ,6],#[1, 2.75, 4.5, 6.25, 8],
	"pdh_HW": [1, 2.25, 3.5, 5.25 ,6]#[1, 2.75, 4.5, 6.25, 8]
}
 
funcCoefs = {
	"germany50": 5, 
	"germany50_HW": 5, 
	"atlanta": 6,
	"atlanta_HW": 6,
	"ta2": 5, 
	"zib54": 5,
	"pdh": 5,
	"pdh_HW": 5
}

# names = [sys.argv[1]] if sys.argv[1] != "all" else 
# models = [sys.argv[2]] if sys.argv[2] != "all" else 

def getAllDay(names=orders.keys(), models=["path", "path2"]):
	for name in names:
	  for model in models:
	  	for percent in percents[name]:
			print "{} -m {} -d {} -c {}".format(name, model, percent, funcCoefs[name])