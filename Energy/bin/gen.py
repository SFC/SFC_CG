
percents = {
	"germany50": [1, 2.25, 3.5, 5.25, 6], 
	"germany50_HW": [1, 2.25, 3.5, 5.25, 6], 
	"atlanta": [1, 2.25, 3.5, 5.5, 6], 
	"atlanta_HW": [1, 2.25, 3.5, 5.25, 6], 
	"ta2": [1, 2.75, 4.5, 6.25, 8], 
	"zib54": [1, 3, 5, 7, 9],
	"abilene": [1],
	"pdh": [1, 2.25, 3.5, 5.25, 6],#[1, 2.75, 4.5, 6.25, 8],
	"pdh_HW": [1, 2.25, 3.5, 5.25, 6]#[1, 2.75, 4.5, 6.25, 8]
}

heurs = ["path_fromILPHeur", "path3_fromILPHeur", "path2_fromILPHeur"]

funcCoefs = {
	"germany50": 5, 
	"germany50_HW": 5, 
	"atlanta": 6,
	"atlanta_HW": 6,
	"ta2": 5, 
	"zib54": 5,
	"abilene": 5,
	"pdh": 5,
	"pdh_HW": 5
}

def getPeriods(name, heur): 
	for perc in percents[name]:
		print "{} -m {} -c {} -d {}".format(name, heur, funcCoefs[name], perc)

def getAllHeur(name):
	for heur in heurs:
		for perc in percents[name]:
			print "{} -m {} -c {} -d {}".format(name, heur, funcCoefs[name], perc)

def getAll(allNames=["pdh", "atlanta", "germany50"]):
	for name in allNames:
		for heur in heurs:
			for perc in percents[name]:
				print "{} -m {} -c {} -d {}".format(name, heur, funcCoefs[name], perc)
