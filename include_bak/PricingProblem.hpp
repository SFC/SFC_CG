#ifndef PRICING_PROBLEM_HPP
#define PRICING_PROBLEM_HPP

#include "SFC.hpp"
#include "cplex_utility.hpp"
#include <exception>
#include <ilcplex/ilocplex.h>

#include <boost/graph/bellman_ford_shortest_paths.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graph_utility.hpp>

namespace SFC {
class ShortestPathPerDemand {
  protected:
    const Instance* m_inst;
    std::size_t n;
    std::size_t m;
    std::size_t nbLayers;
    std::size_t m_demandID{0};
    DiGraph m_layeredGraph;
    std::vector<double> m_distance;
    std::vector<Node> m_predecessors;
    struct DjikstraVisitor : public boost::base_visitor<DjikstraVisitor> {
        Node dest;
        using event_filter = boost::on_finish_vertex;
        void operator()(Node u, const DiGraph& /*g*/) const {
            if (u == dest) {
                throw 0;
            }
        }
    } m_djikstra_visitor;
    bool isNegative{false};
    Path m_path{};
    // ServicePath m_sPath;
    double m_objValue{-std::numeric_limits<double>::max()};

  public:
    using ColumnType = ServicePath;

    explicit ShortestPathPerDemand(const Instance& _inst);
    ShortestPathPerDemand(const ShortestPathPerDemand&) = default;
    ShortestPathPerDemand(ShortestPathPerDemand&&) = default;
    ShortestPathPerDemand& operator=(const ShortestPathPerDemand&) = default;
    ShortestPathPerDemand& operator=(ShortestPathPerDemand&&) = default;
    ~ShortestPathPerDemand() noexcept = default;

    void setPricingID(std::size_t _demandID);

    template <typename DualValues>
    void updateDual(const DualValues& _dualValues) {
        const auto& weight_map = get(boost::edge_weight, m_layeredGraph);
        m_objValue = -_dualValues.m_onePathDuals[m_demandID];

        // Inter layer weight
        for (const auto u :
            boost::make_iterator_range(vertices(m_inst->network))) {
            if (m_inst->network[u].isNFV) {
                for (std::size_t j = 0; j < nbLayers - 1; ++j) {
                    put(weight_map,
                        edge(n * j + u, n * (j + 1) + u, m_layeredGraph).first,
                        _dualValues.getReducedCost(
                            CrossLayerLink{m_demandID, u, j}));
                }
            }
        }

        // Same layer weight
        for (std::size_t j = 0; j < nbLayers; ++j) {
            for (const auto ed :
                boost::make_iterator_range(edges(m_inst->network))) {
                const auto [u, v] = incident(ed, m_inst->network);
                put(weight_map,
                    edge(n * j + u, n * j + v, m_layeredGraph).first,
                    _dualValues.getReducedCost(
                        IntraLayerLink{m_demandID, {u, v}, j}));
            }
        }

        const auto [ite, end] = edges(m_layeredGraph);
        isNegative = std::any_of(ite, end, [&](auto&& _desc) {
            return epsilon_less<double>()(get(weight_map, _desc), 0.0);
        });
    }
    bool solve();
    [[nodiscard]] double getObjValue() const;
    [[nodiscard]] ServicePath getColumn() const;

    template <typename DualValues>
    ServicePath operator()(
        const Demand<>& _demand, const DualValues& _dualValues) {
        setPricingID(_demand.id);
        updateDual(_dualValues);
        return getColumn();
    }

  protected:
    const Instance* m_inst;
    std::size_t n;
    std::size_t m;
    std::size_t nbLayers;
    std::size_t m_demandID{0};
    DiGraph m_layeredGraph;
    std::vector<double> m_distance;
    std::vector<Node> m_predecessors;
    struct DjikstraVisitor : public boost::base_visitor<DjikstraVisitor> {
        Node dest;
        using event_filter = boost::on_finish_vertex;
        void operator()(Node u, const DiGraph& /*g*/) const {
            if (u == dest) {
                throw 0;
            }
        }
    } m_djikstra_visitor;
    bool isNegative{false};
    Path m_path{};
    // ServicePath m_sPath;
    double m_objValue{-std::numeric_limits<double>::max()};
};

class ShorestPathPerDemandLp {
  public:
    using Column = ServicePath;
    explicit ShorestPathPerDemandLp(const Instance& _inst);
    ShorestPathPerDemandLp(const ShorestPathPerDemandLp&);
    ShorestPathPerDemandLp(ShorestPathPerDemandLp&&) noexcept;
    ShorestPathPerDemandLp& operator=(const ShorestPathPerDemandLp&);
    ShorestPathPerDemandLp& operator=(ShorestPathPerDemandLp&&) noexcept;
    ~ShorestPathPerDemandLp() { m_env.end(); }

    void setPricingID(std::size_t _demandID);

    template <typename DualValues>
    void updateDual(const DualValues& _dualValues) {
        const auto& functions = m_inst->demands[m_demandID].functions;

        IloNumExpr expr(m_env, -_dualValues.m_onePathDuals[m_demandID]);
        for (const auto ed :
            boost::make_iterator_range(edges(m_inst->network))) {
            const auto [u, v] = incident(ed, m_inst->network);

            for (std::size_t j = 0; j <= functions.size(); ++j) {
                expr += m_f[m_inst->network[ed].id][j]
                        * _dualValues.getReducedCost(
                            IntraLayerLink{m_demandID, {u, v}, j});
            }
        }
        for (const auto u :
            boost::make_iterator_range(vertices(m_inst->network))) {
            if (m_inst->network[u].isNFV) {
                for (std::size_t j = 0; j < functions.size(); ++j) {
                    expr += m_a[u][j]
                            * _dualValues.getReducedCost(
                                CrossLayerLink{m_demandID, u, j});
                }
            }
        }
        m_obj.setExpr(expr);
        expr.end();
        m_solver.extract(m_model);
    }

    [[nodiscard]] ServicePath getColumn() const;
    bool solve();
    [[nodiscard]] double getObjValue() const;

  protected:
    const Instance* m_inst;

    std::size_t m_demandID = 0;
    std::size_t n;
    std::size_t m;
    std::size_t nbLayers;

    IloEnv m_env{};
    IloModel m_model{m_env};

    IloObjective m_obj;

    boost::multi_array<IloNumVar, 2> m_a;
    boost::multi_array<IloNumVar, 2> m_f;

    boost::multi_array<IloRange, 2> m_flowConsCons;
    std::vector<IloRange> m_linkCapaCons;
    std::vector<IloRange> m_nodeCapaCons;

    IloCplex m_solver;
};

class ShortestPathPerDemand_Fixed : public ShortestPathPerDemand {
  public:
    using Column = ServicePath;

    ShortestPathPerDemand_Fixed(
        const Instance& _inst, const boost::multi_array<bool, 2>& _locations)
        : ShortestPathPerDemand(_inst)
        , m_locations(_locations)

    {}
    ShortestPathPerDemand_Fixed(const ShortestPathPerDemand_Fixed&) = default;
    ShortestPathPerDemand_Fixed(ShortestPathPerDemand_Fixed&&) = default;
    ShortestPathPerDemand_Fixed& operator=(
        const ShortestPathPerDemand_Fixed&) = default;
    ShortestPathPerDemand_Fixed& operator=(
        ShortestPathPerDemand_Fixed&&) = default;
    ~ShortestPathPerDemand_Fixed() noexcept = default;

    template <typename DualValues>
    void updateDual(const DualValues& _dualValues) {
        const auto& weight_map = get(boost::edge_weight, m_layeredGraph);
        m_objValue = -_dualValues.m_onePathDuals[m_demandID];

        // Inter layer weight
        for (const auto u :
            boost::make_iterator_range(vertices(m_inst->network))) {
            if (m_inst->network[u].isNFV) {
                for (std::size_t j = 0; j < nbLayers - 1; ++j) {
                    if (m_locations[long(u)][long(
                            m_inst->demands[m_demandID].functions[j])]) {
                        put(weight_map,
                            edge(n * j + u, n * (j + 1) + u, m_layeredGraph)
                                .first,
                            _dualValues.getReducedCost(
                                CrossLayerLink{m_demandID, u, j}));
                    } else {
                        put(weight_map,
                            edge(n * j + u, n * (j + 1) + u, m_layeredGraph)
                                .first,
                            std::numeric_limits<double>::max());
                    }
                }
            }
        }

        // Same layer weight
        for (std::size_t j = 0; j < nbLayers; ++j) {
            for (const auto ed :
                boost::make_iterator_range(edges(m_inst->network))) {
                const auto [u, v] = incident(ed, m_inst->network);
                put(weight_map,
                    edge(n * j + u, n * j + v, m_layeredGraph).first,
                    _dualValues.getReducedCost(
                        IntraLayerLink{m_demandID, {u, v}, j}));
            }
        }

        const auto [ite, end] = edges(m_layeredGraph);
        isNegative = std::any_of(ite, end, [&](auto&& _desc) {
            return epsilon_less<double>()(get(weight_map, _desc), 0.0);
        });
    }

  private:
    boost::multi_array<bool, 2> m_locations;
};

class FilteredShortestPathPerDemand {
  public:
    struct Filter {
        const FilteredShortestPathPerDemand* pricing;
        const DiGraph* network;
        const std::set<IntraLayerLink>* filtered;
        bool operator()(DiGraph::edge_descriptor ed) const {
            const auto [u, v] = incident(ed, *network);
            const auto layeru = u / pricing->n;
            const auto layerv = v / pricing->n;
            if (layeru != layerv) {
                return true;
            }
            const auto baseu = u % pricing->n;
            const auto basev = v % pricing->n;
            return filtered->find(IntraLayerLink{pricing->m_demandID,
                       std::make_pair(baseu, basev), layeru})
                   == filtered->end();
        }
    };
    using Column = ServicePath;

    explicit FilteredShortestPathPerDemand(const Instance& _inst,
        const std::vector<std::set<IntraLayerLink>>& _filteredEdges);
    FilteredShortestPathPerDemand(
        const FilteredShortestPathPerDemand&) = default;
    FilteredShortestPathPerDemand(FilteredShortestPathPerDemand&&) = default;
    FilteredShortestPathPerDemand& operator=(
        const FilteredShortestPathPerDemand&) = default;
    FilteredShortestPathPerDemand& operator=(
        FilteredShortestPathPerDemand&&) = default;
    ~FilteredShortestPathPerDemand() noexcept = default;

    void setPricingID(std::size_t _demandID);

    template <typename DualValues>
    void updateDual(const DualValues& _dualValues) {
        const auto& weight_map = get(boost::edge_weight, m_layeredGraph);
        m_objValue = -_dualValues.m_onePathDuals[m_demandID];

        // Inter layer weight
        for (const auto u :
            boost::make_iterator_range(vertices(m_inst->network))) {
            if (m_inst->network[u].isNFV) {
                for (std::size_t j = 0; j < nbLayers - 1; ++j) {
                    put(weight_map,
                        edge(n * j + u, n * (j + 1) + u, m_layeredGraph).first,
                        _dualValues.getReducedCost(
                            CrossLayerLink{m_demandID, u, j}));
                }
            }
        }

        // Same layer weight
        for (std::size_t j = 0; j < nbLayers; ++j) {
            for (const auto ed :
                boost::make_iterator_range(edges(m_inst->network))) {
                const auto [u, v] = incident(ed, m_inst->network);
                put(weight_map,
                    edge(n * j + u, n * j + v, m_layeredGraph).first,
                    _dualValues.getReducedCost(
                        IntraLayerLink{m_demandID, {u, v}, j}));
            }
        }

        const auto [ite, end] = edges(m_layeredGraph);
        isNegative = std::any_of(ite, end, [&](auto&& _desc) {
            return epsilon_less<double>()(get(weight_map, _desc), 0.0);
        });
    }
    bool solve();
    [[nodiscard]] double getObjValue() const;
    [[nodiscard]] ServicePath getColumn() const;

  protected:
    const Instance* m_inst;
    const std::vector<std::set<IntraLayerLink>>* m_filteredEdges;
    std::size_t n;
    std::size_t m;
    std::size_t nbLayers;
    std::size_t m_demandID{0};
    DiGraph m_layeredGraph;
    std::vector<double> m_distance;
    std::vector<Node> m_predecessors;
    struct DjikstraVisitor : public boost::base_visitor<DjikstraVisitor> {
        Node dest;
        using event_filter = boost::on_finish_vertex;
        template <typename G>
        void operator()(Node u, const G& /*unused*/) const {
            if (u == dest) {
                throw std::exception{};
            }
        }
    } m_djikstra_visitor;
    bool isNegative{false};
    Path m_path{};
    // ServicePath m_sPath;
    double m_objValue{-std::numeric_limits<double>::max()};
};

class LayeredFilteredShortestPathPerDemand {
  public:
    struct Filter {
        const LayeredFilteredShortestPathPerDemand* pricing;
        const DiGraph* network;
        const std::set<IntraLayerLink>* filteredIntra;
        const std::set<CrossLayerLink>* filteredCross;
        bool operator()(DiGraph::edge_descriptor ed) const {
            const auto [u, v] = incident(ed, *network);
            const auto layeru = u / pricing->n;
            const auto layerv = v / pricing->n;
            const auto baseu = u % pricing->n;
            const auto basev = v % pricing->n;
            if (layeru != layerv) {
                return filteredCross->find(
                           CrossLayerLink{pricing->m_demandID, baseu, layeru})
                       == filteredCross->end();
            }
            return filteredIntra->find(IntraLayerLink{pricing->m_demandID,
                       std::make_pair(baseu, basev), layeru})
                   == filteredIntra->end();
        }
    };
    using Column = ServicePath;

    explicit LayeredFilteredShortestPathPerDemand(const Instance& _inst,
        const std::vector<std::set<IntraLayerLink>>& _filteredIntra,
        const std::vector<std::set<CrossLayerLink>>& _filteredCross);
    LayeredFilteredShortestPathPerDemand(
        const LayeredFilteredShortestPathPerDemand&) = default;
    LayeredFilteredShortestPathPerDemand(
        LayeredFilteredShortestPathPerDemand&&) = default;
    LayeredFilteredShortestPathPerDemand& operator=(
        const LayeredFilteredShortestPathPerDemand&) = default;
    LayeredFilteredShortestPathPerDemand& operator=(
        LayeredFilteredShortestPathPerDemand&&) = default;
    ~LayeredFilteredShortestPathPerDemand() noexcept = default;

    void setPricingID(std::size_t _demandID);

    template <typename DualValues>
    void updateDual(const DualValues& _dualValues) {
        const auto& weight_map = get(boost::edge_weight, m_layeredGraph);
        m_objValue = -_dualValues.m_onePathDuals[m_demandID];

        // Inter layer weight
        for (const auto u :
            boost::make_iterator_range(vertices(m_inst->network))) {
            if (m_inst->network[u].isNFV) {
                for (std::size_t j = 0; j < nbLayers - 1; ++j) {
                    put(weight_map,
                        edge(n * j + u, n * (j + 1) + u, m_layeredGraph).first,
                        _dualValues.getReducedCost(
                            CrossLayerLink{m_demandID, u, j}));
                }
            }
        }

        // Same layer weight
        for (std::size_t j = 0; j < nbLayers; ++j) {
            for (const auto ed :
                boost::make_iterator_range(edges(m_inst->network))) {
                const auto [u, v] = incident(ed, m_inst->network);
                put(weight_map,
                    edge(n * j + u, n * j + v, m_layeredGraph).first,
                    _dualValues.getReducedCost(
                        IntraLayerLink{m_demandID, {u, v}, j}));
            }
        }

        const auto [ite, end] = edges(m_layeredGraph);
        isNegative = std::any_of(ite, end, [&](auto&& _desc) {
            return epsilon_less<double>()(get(weight_map, _desc), 0.0);
        });
    }
    bool solve();
    [[nodiscard]] double getObjValue() const;
    [[nodiscard]] ServicePath getColumn() const;

  protected:
    const Instance* m_inst;
    const std::vector<std::set<IntraLayerLink>>* m_filteredIntra;
    const std::vector<std::set<CrossLayerLink>>* m_filteredCross;
    std::size_t n;
    std::size_t m;
    std::size_t nbLayers;
    std::size_t m_demandID{0};
    DiGraph m_layeredGraph;
    std::vector<double> m_distance;
    std::vector<Node> m_predecessors;
    struct DjikstraVisitor : public boost::base_visitor<DjikstraVisitor> {
        Node dest;
        using event_filter = boost::on_finish_vertex;
        template <typename G>
        void operator()(Node u, const G& /*unused*/) const {
            if (u == dest) {
                throw std::exception{};
            }
        }
    } m_djikstra_visitor;
    bool isNegative{false};
    Path m_path{};
    // ServicePath m_sPath;
    double m_objValue{-std::numeric_limits<double>::max()};
};

} // namespace SFC

#endif
