#ifndef SFC_OCCLIM_COMPACTFORMULATION_HPP
#define SFC_OCCLIM_COMPACTFORMULATION_HPP

#include "SFC/OccLim.hpp"

namespace SFC::OccLim {
class CompactFormulation {
  public:
    CompactFormulation(const Instance& _inst)
        : m_inst(&_inst)
        , m_f([&]() {
            std::cout << "Adding m_f\n";
            IloNumVarArray f(m_model.getEnv(),
                num_edges(m_inst->network) * (m_inst->maxChainSize + 1)
                    * m_inst->demands.size(),
                0.0, IloInfinity);
            return IloAdd(m_model, f);
        }())
        , m_a([&]() {
            std::cout << "Adding m_a\n";
            IloNumVarArray a(m_model.getEnv(),
                num_vertices(m_inst->network) * m_inst->maxChainSize
                    * m_inst->demands.size(),
                0.0, IloInfinity);
            return IloAdd(m_model, a);
        }())
        , m_b([&]() {
            std::cout << "Adding m_b\n";
            IloNumVarArray b(m_model.getEnv(),
                num_vertices(m_inst->network) * m_inst->funcCharge.size(), 0.0,
                IloInfinity);
            return IloAdd(m_model, b);
        }())
        , m_obj([&]() {
            std::cout << "Adding m_obj\n";
            IloNumVarArray vars(m_model.getEnv());
            IloNumArray vals(m_model.getEnv());
            for (int e = 0; e < num_edges(m_inst->network); ++e) {
                for (int i = 0; i < m_inst->demands.size(); ++i) {
                    for (int j = 0; j <= m_inst->demands[i].functions.size();
                         ++j) {
                        vars.add(m_f[getIndexF(e, i, j)]);
                        vals.add(m_inst->demands[i].d);
                    }
                }
            }
            auto obj = IloAdd(m_model,
                IloMinimize(m_model.getEnv(), IloScalProd(vars, vals)));
            vars.end();
            vals.end();
            return obj;
        }())
        , m_flowConservationConstraints([&]() {
            std::cout << "Adding m_flowConservationConstraints\n";
            IloRangeArray flowCons = IloRangeArray(m_model.getEnv(),
                num_vertices(m_inst->network) * (m_inst->maxChainSize + 1)
                    * m_inst->demands.size());
            for (const auto u :
                boost::make_iterator_range(vertices(m_inst->network))) {
                for (int i = 0; i < m_inst->demands.size(); ++i) {
                    for (int j = 0; j <= m_inst->demands[i].functions.size();
                         ++j) {
                        IloNumVarArray vars(m_model.getEnv());
                        IloNumArray vals(m_model.getEnv());
                        for (const auto ed : boost::make_iterator_range(
                                 out_edges(u, m_inst->network))) {
                            // Outgoing flows
                            vars.add(
                                m_f[getIndexF(_inst.network[ed].id, i, j)]);
                            vals.add(1.0);
                        }
                        for (const auto ed : boost::make_iterator_range(
                                 in_edges(u, m_inst->network))) {
                            // Outgoing flows
                            vars.add(
                                m_f[getIndexF(_inst.network[ed].id, i, j)]);
                            vals.add(-1.0);
                        }
                        // Incoming flows
                        if (m_inst->network[u].isNFV) {
                            if (j < m_inst->demands[i].functions.size()) {
                                // Going next layer
                                vars.add(m_a[getIndexA(u, i, j)]);
                                vals.add(1.0);
                            }
                            if (j > 0) {
                                // Coming from last layer
                                vars.add(m_a[getIndexA(u, i, j - 1)]);
                                vals.add(-1.0);
                            }
                        }
                        flowCons[getIndexFC(u, i, j)] =
                            IloAdd(m_model, IloScalProd(vars, vals) == 0);
                        vars.end();
                        vals.end();
                    }
                }
            }

            for (int i = 0; i < m_inst->demands.size(); ++i) {
                flowCons[getIndexFC(m_inst->demands[i].s, i, 0)].setBounds(
                    1.0, 1.0);
                flowCons[getIndexFC(m_inst->demands[i].t, i,
                             m_inst->demands[i].functions.size())]
                    .setBounds(-1.0, -1.0);
            }
            return flowCons;
        }())
        , m_linkCapacityConstraitns([&]() {
            std::cout << "Adding m_linkCapacityConstraitns\n";
            IloRangeArray linkCapaConstraints(
                m_model.getEnv(), num_edges(m_inst->network), 0.0, 0.0);
            for (const auto ed :
                boost::make_iterator_range(edges(m_inst->network))) {
                const auto e = m_inst->network[ed].id;
                IloNumVarArray vars(m_model.getEnv());
                IloNumArray vals(m_model.getEnv());
                for (int i = 0; i < m_inst->demands.size(); ++i) {
                    for (int j = 0; j <= m_inst->demands[i].functions.size();
                         ++j) {
                        vars.add(m_f[getIndexF(e, i, j)]);
                        vals.add(m_inst->demands[i].d);
                    }
                }
                linkCapaConstraints[e] =
                    (IloScalProd(vars, vals) <= m_inst->network[ed].capacity);
                vars.end();
                vals.end();
            }
            return IloAdd(m_model, linkCapaConstraints);
        }())
        , m_nodeCapacityConstraints([&]() {
            std::cout << "Adding m_nodeCapacityConstraints\n";
            IloRangeArray nodeCapaConstraints(
                m_model.getEnv(), num_vertices(m_inst->network), 0.0, 0.0);
            for (const auto u :
                boost::make_iterator_range(vertices(m_inst->network))) {
                if (!m_inst->network[u].isNFV) {
                    continue;
                }
                IloNumVarArray vars(m_model.getEnv());
                IloNumArray vals(m_model.getEnv());
                for (int i = 0; i < m_inst->demands.size(); ++i) {
                    for (int j = 0; j < m_inst->demands[i].functions.size();
                         ++j) {
                        vars.add(m_a[getIndexA(u, i, j)]);
                        vals.add(m_inst->nbCores[i][j]);
                    }
                }
                nodeCapaConstraints[u] =
                    IloAdd(m_model, IloScalProd(vars, vals)
                                        <= IloInt(m_inst->network[u].capacity));
                vars.end();
                vals.end();
            }
            return nodeCapaConstraints;
        }())
        , m_funcNodeUsageCons([&]() {
            std::cout << "Adding m_funcNodeUsageCons\n";
            IloRangeArray funcNodeUsageCons(m_model.getEnv(),
                num_vertices(m_inst->network) * m_inst->maxChainSize
                    * m_inst->demands.size());
            for (const auto u :
                boost::make_iterator_range(vertices(m_inst->network))) {
                if (!m_inst->network[u].isNFV) {
                    continue;
                }
                for (int i = 0; i < m_inst->demands.size(); ++i) {
                    for (int j = 0; j < m_inst->demands[i].functions.size();
                         ++j) {
                        funcNodeUsageCons[getIndexA(u, i, j)] =
                            (m_b[getIndexB(u, m_inst->demands[i].functions[j])]
                                - m_a[getIndexA(u, i, j)])
                            >= 0;
                    }
                }
            }
            return IloAdd(m_model, funcNodeUsageCons);
        }())
        , m_nbLicensesCons([&]() {
            std::cout << "Adding m_nbLicensesCons\n";
            IloRangeArray nbLicensesCons(
                m_model.getEnv(), m_inst->funcCharge.size());
            IloNumVarArray vars(
                m_model.getEnv(), num_vertices(m_inst->network));
            for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
                IloExpr expr(m_model.getEnv());
                for (const auto u :
                    boost::make_iterator_range(vertices(m_inst->network))) {
                    if (m_inst->network[u].isNFV) {
                        expr += m_b[getIndexB(u, f)];
                    }
                }
                nbLicensesCons[f] = (expr <= m_inst->nbLicenses);
            }
            vars.end();
            return IloAdd(m_model, nbLicensesCons);
        }())

    {
        IloAdd(m_model, IloConversion(m_model.getEnv(), m_a, ILOBOOL));
        IloAdd(m_model, IloConversion(m_model.getEnv(), m_b, ILOBOOL));
        IloAdd(m_model, IloConversion(m_model.getEnv(), m_f, ILOBOOL));
    }

    void addMIPStart(
        const std::vector<ServicePath>& _sPaths, IloCplex& _solver) {
        IloNumArray fVal(m_model.getEnv(), m_f.getSize());
        for (IloInt i = 0; i < fVal.getSize(); ++i) {
            fVal[i] = 0.0;
        }
        IloNumArray aVal(m_model.getEnv(), m_a.getSize());
        for (IloInt i = 0; i < aVal.getSize(); ++i) {
            aVal[i] = 0.0;
        }
        IloNumArray bVal(m_model.getEnv(), m_b.getSize());
        for (IloInt i = 0; i < bVal.getSize(); ++i) {
            bVal[i] = 0.0;
        }

        for (const auto& [nPath, locations, demandID] : _sPaths) {
            for (int i = 0; i < _sPaths.size(); ++i) {
                assert(m_inst->demands[demandID].s == nPath.front());
                assert(m_inst->demands[demandID].t == nPath.back());
                std::cout << i << ": " << _sPaths[i] << '\n';

                int j = 0;
                for (auto iteU = nPath.begin(), iteV = std::next(iteU);
                     iteV != nPath.end(); ++iteU, ++iteV) {
                    // If the node contains functions
                    if (j < locations.size() && locations[j] == *iteU) {
                        aVal[getIndexA(*iteU, demandID, j)] = 1.0;
                        bVal[getIndexB(*iteU,
                            m_inst->demands[demandID].functions[j])] = 1.0;
                        ++j;
                    }
                    fVal[getIndexF(edge(*iteU, *iteV, m_inst->network).second,
                        demandID, j)] = 1.0;
                }
                // If the rest of the function are on t
                for (; j < locations.size(); ++j) {
                    aVal[getIndexA(m_inst->demands[demandID].t, demandID, j)] =
                        1.0;
                    bVal[getIndexB(m_inst->demands[demandID].t,
                        m_inst->demands[demandID].functions[j])] = 1.0;
                }
            }
        }

        IloNumVarArray vars(m_model.getEnv());
        IloNumArray vals(m_model.getEnv());
        vars.add(m_f);
        vals.add(fVal);

        vars.add(m_a);
        vals.add(aVal);

        vars.add(m_b);
        vals.add(bVal);

        _solver.addMIPStart(vars, vals);
        _solver.setWarning(m_model.getEnv().getNullStream());
        vars.end();
        vals.end();

        IloConstraintArray consArr(m_model.getEnv());
        IloNumArray numArr(m_model.getEnv());
        for (IloModel::Iterator iter(m_model); iter.ok(); ++iter) {
            if ((*iter).asConstraint().getImpl()) {
                consArr.add((*iter).asConstraint());
                numArr.add(1);
            }
        }
        for (IloModel::Iterator iter(m_model); iter.ok(); ++iter) {
            if ((*iter).asVariable().getImpl()) {
                consArr.add(IloBound((*iter).asVariable(), IloBound::Lower));
                numArr.add(1);
                consArr.add(IloBound((*iter).asVariable(), IloBound::Upper));
                numArr.add(1);
            }
        }
        if (_solver.refineMIPStartConflict(0, consArr, numArr)) {
            const auto conflict = _solver.getConflict(consArr);
            m_model.getEnv().getImpl()->useDetailedDisplay(IloTrue);
            for (IloInt i = 0; i < consArr.getSize(); ++i) {
                if (conflict[i] == IloCplex::ConflictMember) {
                    std::cout << "Proved  : " << consArr[i] << '\n';
                } else if (conflict[i] == IloCplex::ConflictPossibleMember) {
                    std::cout << "Possible: " << consArr[i] << '\n';
                }
            }
            exit(-8);
        }
    }

    ServicePath getServicePath(const Demand<>& _demand,
        const IloNumArray& _aVal, const IloNumArray& _fVal) const {
        ServicePath retval;
        retval.demand = _demand.id;
        retval.nPath.push_back(_demand.s);
        int j = 0;
        while (
            retval.nPath.back() != _demand.t || j < _demand.functions.size()) {
            if (j < _demand.functions.size()
                && m_inst->network[retval.nPath.back()].isNFV
                && epsilon_equal<double>()(
                       _aVal[getIndexA(retval.nPath.back(), _demand.id, j)],
                       IloTrue)) {
                retval.locations.push_back(retval.nPath.back());
                ++j;
            } else {
                for (const auto ed : boost::make_iterator_range(
                         out_edges(retval.nPath.back(), m_inst->network))) {
                    const auto e = m_inst->network[ed].id;
                    if (epsilon_equal<double>()(
                            _fVal[getIndexF(e, _demand.id, j)], 1.0)) {
                        retval.nPath.push_back(target(ed, m_inst->network));
                        break;
                    }
                }
            }
        }
        assert(isValid(retval, _demand));
        return retval;
    }

    std::vector<ServicePath> getServicePaths(const IloCplex& _solver) const {
        IloNumArray aVal(m_model.getEnv());
        _solver.getValues(m_a, aVal);
        IloNumArray fVal(m_model.getEnv());
        _solver.getValues(m_f, fVal);
        std::vector<ServicePath> pathsUsed(m_inst->demands.size());
        std::transform(m_inst->demands.begin(), m_inst->demands.end(),
            pathsUsed.begin(), [&](auto&& _demand) {
                return getServicePath(_demand, aVal, fVal);
            });

        return pathsUsed;
    }

    const LinearModel& getModel() const { return m_model; }

    Solution getSolution(const IloCplex& _solver) const {
        return {*m_inst, _solver.getObjValue(), _solver.getBestObjValue(),
            getServicePaths(_solver)};
    }

  private:
    const Instance* m_inst;
    // ILP
    LinearModel m_model;

    IloNumVarArray m_f;
    IloNumVarArray m_a;
    IloNumVarArray m_b;

    IloObjective m_obj;

    IloRangeArray m_flowConservationConstraints;
    IloRangeArray m_linkCapacityConstraitns;
    IloRangeArray m_nodeCapacityConstraints;
    IloRangeArray m_funcNodeUsageCons;
    IloRangeArray m_nbLicensesCons;

    int getIndexA(const Node _u, const int _i, const int _j) const {
        assert(m_inst->network[_u].isNFV);
        assert(_i < m_inst->demands.size());
        assert(_j < m_inst->demands[_i].functions.size());
        return _j + _i * m_inst->maxChainSize
               + _u * m_inst->demands.size() * m_inst->maxChainSize;
    }

    int getIndexB(const Node _u, const int _f) const {
        assert(m_inst->network[_u].isNFV);
        return num_vertices(m_inst->network) * _f + _u;
    }

    int getIndexFC(const Node _u, const int _i, const int _j) const {
        assert(_u < num_vertices(m_inst->network));
        assert(_i < m_inst->demands.size());
        assert(_j <= m_inst->demands[_i].functions.size());
        return _j + _i * (m_inst->maxChainSize + 1)
               + _u * m_inst->demands.size() * (m_inst->maxChainSize + 1);
    }

    int getIndexF(const int _e, const int _i, const int _j) const {
        assert(_e < num_edges(m_inst->network));
        assert(_i < m_inst->demands.size());
        assert(_j <= m_inst->demands[_i].functions.size());
        return _j + _i * (m_inst->maxChainSize + 1)
               + _e * m_inst->demands.size() * (m_inst->maxChainSize + 1);
    }
}; // namespace SFC::OccLim
} // namespace SFC::OccLim

#endif
