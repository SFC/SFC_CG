#ifndef OCCLIM_PATH_DECOMPOSITION_LAGRANGIAN_HPP
#define OCCLIM_PATH_DECOMPOSITION_LAGRANGIAN_HPP

#include "ColumnGeneration.hpp"
#include "Occlim.hpp"

namespace SFC::Partial::OccLim::PathDecompositionLagrangian {

struct DualValues {
    DualValues(const Instance& _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;

    IloNum getReducedCost(const CrossLayerLink& _link) const;
    IloNum getReducedCost(const IntraLayerLink& _link) const;
    IloNum getReducedCost(const Demand& _demand) const;
    double getDualSumRHS() const;

    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;
    boost::multi_array<IloNum, 3> m_pathFuncDuals;
};

class Master : public RMP<Master, DualValues, Solution, ServicePath> {
    friend DualValues;

  public:
    using Base = RMP<Master, DualValues, Solution, ServicePath>;
    friend Base;
    using Column = ServicePath;
    using Solution = Solution;
    using DualValues = DualValues;

    explicit Master(const Instance& _inst);
    Master& operator=(const Master&) = delete;
    Master& operator=(Master&&) = delete;
    Master(const Master&) = delete;
    Master(Master&&) = delete;
    ~Master() { m_env.end(); }

    double getReducedCost_impl(
        const ServicePath& _col, const DualValues& _dualValues) const;
    IloNumColumn getColumn_impl(const ServicePath& _col);
    void getDuals_impl();
    void removeDummyIfPossible();
    const DualValues& getDualValues_impl() const;
    std::vector<ServicePath> getServicePaths() const;
    template <typename T>
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }
    Solution getSolution() const;

    template <typename Column>
    void applyBasis_impl(IloCplex::BasisStatusArray& _cstat,
        IloNumVarArray& _vars, IloCplex::BasisStatusArray& _rstat,
        IloRangeArray& _ranges) {
        if constexpr (std::is_same_v<Column, ServicePath>) {
            if (getColumnStorage<ServicePath>().empty()) {
                return;
            }
            IloCplex::BasisStatusArray tmp_cstat(m_env, m_inst->demands.size());
            IloNumVarArray tmp_vars(m_env, m_inst->demands.size());
            IloCplex::BasisStatusArray tmp_rstat(m_env);
            IloRangeArray tmp_ranges(m_env);
            for_each(getColumnStorage<ServicePath>().rbegin(),
                getColumnStorage<ServicePath>().rend(),
                [&](auto&& _varObjPair) {
                    if (tmp_vars[_varObjPair.obj.demand].getImpl() == nullptr) {
                        tmp_cstat[_varObjPair.obj.demand] =
                            IloCplex::BasisStatus::Basic;
                        tmp_vars[_varObjPair.obj.demand] = _varObjPair.var;
                    }
                });
            for (const auto& rng : m_onePathCons) {
                tmp_ranges.add(rng);
                tmp_rstat.add(IloCplex::BasisStatus::AtLower);
            }
            _cstat.add(tmp_cstat);
            _vars.add(tmp_vars);
            _rstat.add(tmp_rstat);
            _ranges.add(tmp_ranges);
        }
    }

    boost::multi_array<double, 2> getLocations() const;

    void updateObjective() noexcept;
    void updateLagrangianMultipliers() noexcept;
    void updateStepSize(double _alpha, double _bestObjValue) noexcept;

    using Base::getNbColumns;

  private:
    using Base::m_columns;
    using Base::m_duals;
    using Base::m_dummyActive;
    using Base::m_env;
    using Base::m_fracObj;
    using Base::m_intObj;
    using Base::m_model;
    using Base::m_solver;

    const Instance* m_inst;

    std::vector<double> m_nbLicensesMultipliers;
    boost::multi_array<IloNumVar, 2> m_b;

    IloObjective m_obj;

    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;
    boost::multi_array<LazyConstraint, 3> m_pathFuncCons;

    std::vector<IloNumVar> m_dummyPaths;
    std::vector<double> m_linkCharge;
    std::vector<double> m_nodeCharge;

    boost::multi_array<double, 2> m_funcPath;
};

std::ostream& operator<<(std::ostream& _out, const DualValues& _dualValues);
void barycenter(const DualValues& _nDuals, const DualValues& _bDuals,
    DualValues& _sDuals, double _alpha);

} // namespace SFC::Partial::OccLim::PathDecompositionLagrangian

#endif
