#ifndef SFC_OCCLIM_ALGORITHM_HPP
#define SFC_OCCLIM_ALGORITHM_HPP

#include <optional>
#include <random>

#include <boost/dynamic_bitset.hpp>
#include <boost/graph/r_c_shortest_paths.hpp>

#include "BranchAndBound.hpp"
#include "ColumnGeneration.hpp"
#include "SFC/OccLim.hpp"
#include "cplex_utility.hpp"
//#include "SFC/OccLim/PathDecomposition.hpp"
//#include "SFC/RCShortestPathPerDemand.hpp"

namespace SFC::OccLim {

std::optional<SFC::OccLim::Solution> solveWithBandP(
    const SFC::OccLim::Instance& _inst,
    const ColumnGenerationParameters& _cgParams,
    const StabilizationParameters& _stabParams,
    const BaseBranchAndPriceParameters& _bapParams);

inline std::optional<SFC::OccLim::Solution> heur(const SFC::OccLim::Instance&,
    const ColumnGenerationParameters&, const StabilizationParameters&,
    const BaseBranchAndPriceParameters&) {
    return std::nullopt;
}
/*
inline std::optional<SFC::OccLim::Solution<int>> heur(
    const SFC::OccLim::Instance& _inst,
    const ColumnGenerationParameters& _cgParams,
    const StabilizationParameters& _stabParams,
    const BaseBranchAndPriceParameters& _bapParams) {
    SFC::OccLim::Localisation loc(_inst);
    if (loc.solve()) {
        const auto funcLocations = loc.getLocalisation();
        using Master = SFC::PlacementDWD_Path;
        using MasterOcc = SFC::PlacementDWD_Path;
        using PricingFixed = SFC::ShortestPathPerDemand_Fixed;

        Master rmp(_inst);
        // rmp.setBBounds(funcLocations);
        bool solved = false;
        if (_stabParams.alpha >= 0.0) {
            solved = solveStabilized<Master, PricingFixed>(
                rmp, PricingFixed(_inst, funcLocations), _stabParams);
        } else {
            solved = solve<Master, PricingFixed>(
                rmp, PricingFixed(_inst, funcLocations), _cgParams);
        }
        if (solved) {
            MasterOcc rmpOcc(_inst);
            const auto initCol = rmp.getServicePaths();
            rmpOcc.solved = false;
            if (_stabParams.alpha >= 0.0) {
                solved = solveStabilized<Master,
                    SFC::OccLim::LocationPricingProblem>(
                    rmp, Pricing(_inst), _stabParams);
            } else {
                solved = solve<SFC::ShortestPathPerDemand,
                    SFC::OccLim::LocationPricingProblem>(
                    rmp, Pricing(_inst), _cgParams);
            }
            if (solved) {
                return rmp.getSolution();
            }
        }
    } else {
        return std::nullopt;
    }
}
*/
} // namespace SFC::OccLim

namespace SFC::Partial::OccLim::Rounding {

// Randomized rounding

struct Parameters {

    int nbIterations = 500;
    bool verbose = true;
};

struct Node {
    int id;
    boost::dynamic_bitset<> installedFunctions;
    double residualCapacity;
};

struct Link {
    int id;
    double residualCapacity;
};

struct GraphProperties {
    std::vector<int> remainingLicenses;
};

using GreedyNetwork = boost::adjacency_list<boost::vecS, boost::vecS,
    boost::bidirectionalS, Node, Link, GraphProperties>;

GreedyNetwork createNetwork(const SFC::Partial::OccLim::Instance& _inst);
bool isAvailable(const ServicePath& _path, const Demand& _demand,
    const GreedyNetwork& _network, const Instance& _inst);
bool route(const ServicePath& _path, const Demand& _demand,
    GreedyNetwork& _network, const Instance& _inst);
std::optional<ServicePath> findRoute(const Demand& _demand,
    const GreedyNetwork& _network, const Instance& _inst);

std::optional<Solution> rounding(const SFC::Partial::OccLim::Instance& _inst,
    const RelaxedSolution& _relaxedSol,
    const Parameters& _params = Parameters());

std::optional<Solution> path_rounding(
    const Instance& _inst, const StabilizationParameters& _params);

class ResourceContainer {
    friend bool dominance(
        const ResourceContainer& _rc1, const ResourceContainer& _rc2);
    friend bool operator<(
        const ResourceContainer& _rc1, const ResourceContainer& _rc2);

  public:
    /**
     * Create a container for a demand with _chainSize functions
     */
    ResourceContainer(const GreedyNetwork& _network, const Demand& _demand,
        const Instance& _inst)
        : m_inst(&_inst)
        , m_demand(&_demand)
        , usedFunction(m_demand->functions.size())
        , availableLinkCapacity(num_edges(_inst.network))
        , availableNodeCapacity(num_vertices(_inst.network))
        , installedFunctions(
              boost::extents[num_vertices(_inst.network)][usedFunction.size()])
        , availableLicenses(m_demand->functions.size()) {
        for (const auto ed : boost::make_iterator_range(edges(_network))) {
            availableLinkCapacity[_network[ed].id] =
                _network[ed].residualCapacity;
        }

        for (const auto vd : boost::make_iterator_range(vertices(_network))) {
            availableNodeCapacity[_network[vd].id] =
                _network[vd].residualCapacity;
        }

        for (int j = 0; j < m_demand->functions.size(); ++j) {
            const auto f = m_demand->functions.getFunction(j);
            for (int u = 0; u < num_vertices(_network); ++u) {
                installedFunctions[u][j] = _network[u].installedFunctions[f];
            }
            availableLicenses[j] =
                _network[boost::graph_bundle].remainingLicenses[f];
        }
    }

    ResourceContainer extend(const SFC::Partial::Link& _link) const {
        ResourceContainer retval = *this;
        retval.totalWeight += _link.weight;
        const auto origEd =
            edge(_link.original.first, _link.original.second, m_inst->network)
                .first;
        const auto& origLink = m_inst->network[origEd];
        if (_link.isIntraLayer()) {
            retval.nbHops++;
            retval.availableLinkCapacity[origLink.id] -= m_demand->d;
        } else {
            const auto& origNode =
                m_inst->network[source(origEd, m_inst->network)];
            const auto u = origNode.id;
            const auto f =
                m_demand->functions.getFunction(_link.function_index);
            retval.usedFunction[_link.function_index] = true;
            retval.availableNodeCapacity[u] -=
                m_inst->getNbCores(f, m_demand->d);
            retval.function_indexes.push_back(_link.function_index);
            if (!installedFunctions[u][_link.function_index]) {
                --retval.availableLicenses[_link.function_index];
            }
        }
        return retval;
    }

    bool canExtend(const SFC::Partial::Link& _link) const {
        const auto origEd =
            edge(_link.original.first, _link.original.second, m_inst->network)
                .first;
        const auto& origLink = m_inst->network[origEd];
        if (_link.isIntraLayer()) {
            /*std::cout << "canExtend on " << incident(origEd,
               m_inst->network)
               << " at layer " << _link.layer
                      << '\r';*/
            return availableLinkCapacity[origLink.id] >= m_demand->d;
        } else {
            if (usedFunction[_link.function_index]) {
                return false;
            } else {
                const auto& origNode =
                    m_inst->network[source(origEd, m_inst->network)];
                const auto u = origNode.id;
                const auto f =
                    m_demand->functions.getFunction(_link.function_index);
                /*std::cout << "canExtend on " << u << " at layer " <<
                 * _link.layer << '\r';*/
                return availableNodeCapacity[u]
                           >= m_inst->getNbCores(f, m_demand->d)
                       && (installedFunctions[u][_link.function_index]
                              || availableLicenses[_link.function_index] > 0);
            }
        }
    }

    ~ResourceContainer() = default;
    ResourceContainer(const ResourceContainer&) = default;
    ResourceContainer& operator=(const ResourceContainer&) = default;
    ResourceContainer(ResourceContainer&&) = default;
    ResourceContainer& operator=(ResourceContainer&&) = default;

    double getWeight() const { return totalWeight; }

    std::vector<function_descriptor> getFunctionIndexes() const {
        return function_indexes;
    }

  private:
    const Instance* m_inst;
    const Demand* m_demand;
    boost::dynamic_bitset<> usedFunction;
    std::vector<function_descriptor> function_indexes{};
    std::vector<double> availableLinkCapacity;
    std::vector<double> availableNodeCapacity;
    boost::multi_array<int, 2> installedFunctions;
    std::vector<int> availableLicenses;
    double totalWeight = 0.0;
    int nbHops = 0;
};

bool dominance(const ResourceContainer& _rc1, const ResourceContainer& _rc2);
bool operator<(const ResourceContainer& _rc1, const ResourceContainer& _rc2);

/**
 * Create a filtered view of the layered graph for a particular demand
 */
struct DemandFilter {
    const Demand* m_demand;
    const LayeredGraph* m_layeredGraph;
    using edge_descriptor = boost::graph_traits<LayeredGraph>::edge_descriptor;
    using vertex_descriptor =
        boost::graph_traits<LayeredGraph>::vertex_descriptor;

    bool operator()(vertex_descriptor _u) const {
        if ((*m_layeredGraph)[_u].layer > m_demand->functions.size()) {
            return false;
        }
        return true;
    }

    bool operator()(edge_descriptor _ed) const {
        /// we cannot extend a non existing function
        if ((*m_layeredGraph)[_ed].function_index
            >= m_demand->functions.size()) {
            return false;
        }
        return true;
    }
};

class GreedyRCShortestPathPerDemand {
  public:
    explicit GreedyRCShortestPathPerDemand(
        const Instance& _inst, const GreedyNetwork& _network);
    GreedyRCShortestPathPerDemand(
        const GreedyRCShortestPathPerDemand&) = default;
    GreedyRCShortestPathPerDemand(GreedyRCShortestPathPerDemand&&) = default;
    GreedyRCShortestPathPerDemand& operator=(
        const GreedyRCShortestPathPerDemand&) = default;
    GreedyRCShortestPathPerDemand& operator=(
        GreedyRCShortestPathPerDemand&&) = default;
    ~GreedyRCShortestPathPerDemand() noexcept = default;

    void setDemand(const Demand& _demand);
    void setDemand(Demand&& _demand) = delete;

    template <typename GetWeightFunction>
    void updateWeight(GetWeightFunction _func) {
        for (const auto ed :
            boost::make_iterator_range(edges(m_layeredGraph))) {
            m_layeredGraph[ed].weight = _func(ed, m_layeredGraph);

            // Check that weight are positive
            assert([&]() {
                if (epsilon_less<double>()(m_layeredGraph[ed].weight, 0)) {
                    std::cout
                        << m_layeredGraph[source(ed, m_layeredGraph)] << ",  "
                        << m_layeredGraph[target(ed, m_layeredGraph)] << " => "
                        << m_layeredGraph[ed] << '\n';
                    return false;
                }
                return true;
            }());
        }
    }

    bool solve();
    double getObjValue() const;
    ServicePath getPath() const;
    std::vector<ServicePath> getAllColumn() const;

  protected:
    const Instance* m_inst;
    const GreedyNetwork* m_network;
    int n;
    int m;
    int nbLayers;
    const Demand* m_demand{nullptr};
    LayeredGraph m_layeredGraph;
    DemandFilter m_demandFilter{nullptr, &m_layeredGraph};
    std::vector<boost::graph_traits<LayeredGraph>::edge_descriptor> m_paths{};
    ResourceContainer m_containers;
    double m_objValue{-std::numeric_limits<double>::max()};
    std::allocator<
        boost::r_c_shortest_paths_label<LayeredGraph, ResourceContainer>>
        m_allocator;
};

std::optional<Solution> greedy(const SFC::Partial::OccLim::Instance& _inst);
} // namespace SFC::Partial::OccLim::Rounding

#endif
