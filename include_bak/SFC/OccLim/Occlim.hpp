#ifndef SFC_PARTIAL_OCCLIM_HPP
#define SFC_PARTIAL_OCCLIM_HPP

#include "Partial.hpp"
#include "cplex_utility.hpp"
#include <optional>
#include <random>

namespace SFC::Partial::OccLim {

using Solution = SFC::Partial::Solution;

struct Instance : public SFC::Partial::Instance {
    Instance(Network _network, std::vector<Demand> _demands,
        std::vector<double> _funcCharge, int _nbLicenses);
    Instance(SFC::Partial::Instance _inst, int _nbLicenses);

    using SFC::Partial::Instance::demands;
    using SFC::Partial::Instance::funcCharge;
    using SFC::Partial::Instance::maxChainSize;
    using SFC::Partial::Instance::network;
    int nbLicenses;
};

Instance loadInstance(std::ifstream& _ifs, int _nbLicenses);

template <typename RandGen>
boost::multi_array<bool, 2> getLocations(
    const boost::multi_array<double, 2>& _bProba, RandGen& _gen) {
    boost::multi_array<bool, 2> retval(
        boost::extents[_bProba.shape()[0]][_bProba.shape()[1]]);
    std::transform(_bProba.data(), _bProba.data() + _bProba.num_elements(),
        retval.data(), [&](auto&& _p) {
            return std::generate_canonical<double, 10>(_gen)
                   <= std::clamp(_p, 0.01, 0.99);
        });
    return retval;
}

boost::multi_array<bool, 2> getLocations(const Solution& _sol);

bool isValid(
    const boost::multi_array<bool, 2>& _locations, const Instance& _inst);

template <typename T>
bool print(const boost::multi_array<T, 2>& _locations, const Instance& _inst) {
    for (int f = 0; f < _inst.funcCharge.size(); ++f) {
        std::cout << "f: ";
        for (int u = 0; u < num_vertices(_inst.network); ++u) {
            if (_locations[u][f]) {
                std::cout << u << ", ";
            }
        }
        std::cout << '\n';
    }
    std::cout << '\n';
    return true;
}

template <typename Solution>
bool isValid(const Solution& _sol, const Instance& _inst) {
    const auto locations = getLocations(_sol);
    for (int f = 0; f < _inst.funcCharge.size(); ++f) {
        int nbLicenses = 0;
        for (int u = 0; u < num_vertices(_inst.network); ++u) {
            if (locations[u][f]) {
                nbLicenses++;
            }
        }
        if (nbLicenses > _inst.nbLicenses) {
            std::cerr << "Exceding # licenses for " << f << " -> " << nbLicenses
                      << '\n';
            return false;
        }
    }
    /*assert((const auto& sPaths = _sol.getServicePaths(),
        std::all_of(sPaths.begin(), sPaths.end(), [&](const auto& sPath) {
            _inst.demands[sPath.demand].s == sPath.nPath.front()
                && _inst.demands[sPath.demand].t == sPath.nPath.back();
        })));*/
    return true;
}

struct RelaxedSolution {
    std::vector<std::vector<Weighted<ServicePath>>> paths;
    boost::multi_array<double, 2> locations;
};
} // namespace SFC::Partial::OccLim
#endif
