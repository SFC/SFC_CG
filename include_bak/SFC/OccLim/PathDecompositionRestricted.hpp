#ifndef PATH_DECOMPOSITION_RESTRICTED_HPP
#define PATH_DECOMPOSITION_RESTRICTED_HPP

#include "ColumnGeneration.hpp"
#include "Occlim.hpp"

namespace SFC::Partial::OccLim::PathDecompositionRestricted {

struct DualValues {
    DualValues(const Instance& _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;

    IloNum getReducedCost(const CrossLayerLink& _link) const;
    IloNum getReducedCost(const IntraLayerLink& _link) const;
    IloNum getReducedCost(const Demand& _demand) const;
    double getDualSumRHS() const;
    void clear();

    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;
    boost::multi_array<IloNum, 3> m_pathFuncDuals;
    std::vector<IloNum> m_nbLicensesDuals;
    IloNum m_branchesDual;
};

class Master : public RMP<Master, DualValues, Solution, ServicePath> {
    friend DualValues;

  public:
    using Base = RMP<Master, DualValues, Solution, ServicePath>;
    friend Base;
    using Column = ServicePath;
    using Solution = Solution;
    using DualValues = DualValues;

    explicit Master(const Instance& _inst);
    Master& operator=(const Master&) = delete;
    Master& operator=(Master&&) = delete;
    Master(const Master&) = delete;
    Master(Master&&) = delete;
    ~Master() { m_env.end(); }

    double getReducedCost_impl(
        const ServicePath& _col, const DualValues& _dualValues) const;
    IloNumColumn getColumn_impl(const ServicePath& _col);
    void getDuals_impl();
    void removeDummyIfPossible();
    const DualValues& getDualValues_impl() const;
    std::vector<ServicePath> getServicePaths() const;
    template <typename T>
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }
    RelaxedSolution getRelaxedSolution() const {
        RelaxedSolution retval{
            {}, boost::multi_array<double, 2>(boost::extents[num_vertices(
                    m_inst->network)][m_inst->funcCharge.size()])};
        retval.paths.resize(m_inst->demands.size());
        for (const auto& [var, col] : getColumnStorage<ServicePath>()) {
            retval.paths[col.demand].emplace_back(
                Weighted<ServicePath>{m_solver.getValue(var), col});
        }

        std::transform(m_b.data(), m_b.data() + m_b.num_elements(),
            retval.locations.data(),
            [&](const auto& _bVar) { return m_solver.getValue(_bVar); });
        return retval;
    }
    Solution getSolution() const;
    std::vector<IloRange> getBranch_impl() const;
    bool checkIntegrality_impl() const;

    void applyBranch(IloRange _rng);
    void applyBranch(const FixedIntraLayerLink& _link);
    void applyBranch(const FixedCrossLayerLink& _link);
    void applyBranch(const std::variant<IloRange, FixedIntraLayerLink>& _rng);

    void removeBranch(IloRange _rng);
    void removeBranch(const FixedIntraLayerLink& _link);
    void removeBranch(const FixedCrossLayerLink& _link);
    void removeBranch(const std::variant<IloRange, FixedIntraLayerLink>& _rng);

    void clearBranches();
    bool generatePathFuncCuts();

    template <typename FixedVariable>
    std::vector<FixedVariable> getBranchDecision() const {
        // CrossLayerLink
        if constexpr (std::is_same_v<FixedVariable, FixedCrossLayerLink>) {
            if (const auto nodeBranch = getNodeBranches();
                nodeBranch.has_value()) {
                return nodeBranch.value();
            }
            return {};
        }
        // IntralayerLink
        if constexpr (std::is_same_v<FixedVariable, FixedIntraLayerLink>) {
            if (const auto edgeBranch = getLinkBranches();
                edgeBranch.has_value()) {
                return edgeBranch.value();
            }
            return {};
        }
        // IntralayerLink
        if constexpr (std::is_same_v<FixedVariable,
                          std::variant<IloRange, FixedIntraLayerLink>>) {
            if (const auto sumBranch = getLocationsBranches();
                sumBranch.has_value()) {
                return {sumBranch->begin(), sumBranch->end()};
            }
            if (const auto edgeBranch = getLinkBranches();
                edgeBranch.has_value()) {
                return {edgeBranch->begin(), edgeBranch->end()};
            }
            return {};
        }
    }

    std::optional<std::vector<FixedIntraLayerLink>> getLinkBranches() const;
    std::optional<std::vector<FixedCrossLayerLink>> getNodeBranches() const;
    std::optional<std::vector<IloRange>> getLocationsBranches() const;

    using Base::getNbColumns;

  private:
    using Base::m_columns;
    using Base::m_duals;
    using Base::m_dummyActive;
    using Base::m_env;
    using Base::m_fracObj;
    using Base::m_intObj;
    using Base::m_model;
    using Base::m_solver;

    const Instance* m_inst;

    boost::multi_array<IloNumVar, 2> m_b;

    IloObjective m_obj;

    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;
    std::vector<IloRange> m_nbLicensesCons;
    boost::multi_array<LazyConstraint, 3> m_pathFuncCons;
    std::vector<IloRange> m_branches;

    std::vector<IloNumVar> m_dummyPaths;
    std::vector<double> m_linkCharge;
    std::vector<double> m_nodeCharge;

    boost::multi_array<double, 2> m_funcPath;
    mutable boost::multi_array<double, 3> m_edgeUsage;
    mutable boost::multi_array<double, 3> m_nodeUsage;
};

std::ostream& operator<<(std::ostream& _out, const DualValues& _dualValues);
void barycenter(const DualValues& _nDuals, const DualValues& _bDuals,
    DualValues& _sDuals, double _alpha);

} // namespace SFC::Partial::OccLim::PathDecompositionRestricted

#endif
