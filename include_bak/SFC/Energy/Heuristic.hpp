#ifndef AUGMENTED_GRAPH_HPP
#define AUGMENTED_GRAPH_HPP

#include <map>

#include <utility.hpp>

#include "Energy.hpp"
#include "SFC.hpp"
#include <boost/graph/dijkstra_shortest_paths.hpp>

namespace SFC::Energy {

struct Arc {
    int index{-1};
    double capacity{0.0};
    double weight{0.0};
    double residualCapacity{0.0};
    bool active{true};
    std::vector<demand_descriptor> demands{};
    bool removable{true};
};

struct Router {
    int index;
    double usage;
    double capacity;
};

using MutableDiGraph = boost::adjacency_list<
    boost::vecS,
    boost::vecS,
    boost::bidirectionalS,
    Router,
    Arc,
    boost::no_property,
    boost::vecS>;

struct ActiveFilter {
    const MutableDiGraph* _graph;
    bool operator()(const boost::graph_traits<MutableDiGraph>::edge_descriptor& _edge) const {
        return (*_graph)[_edge].active;
    }
};

using Link = std::pair<boost::graph_traits<MutableDiGraph>::edge_descriptor,
    boost::graph_traits<MutableDiGraph>::edge_descriptor>;

class Heuristic {
  public:
    enum State {
        Solved,
        NotSolved,
        Infeasible
    };

    Heuristic(const Instance& _inst);
    Heuristic(const Heuristic& _other) = default;
    Heuristic& operator=(Heuristic& _other) = default;
    Heuristic(Heuristic&& _other) = default;
    Heuristic& operator=(Heuristic&& _other) = default;
    ~Heuristic() = default;

    State solve();
    Solution<unused> getSolution() const;
    bool getAllShortestPaths();
    bool placeFunctions();
    double getEnergyUsedForLinks() const;
    double getEnergyUsedForNodes() const;
    double getEnergyUsed() const;
    std::vector<Link> getRemovableLinks() const;
    void restoreLink(const Link& _link);
    void removeLink(const Link& _link);
    void clear();
    std::vector<ServicePath> getServicePaths() const;
    State getState() const;
    auto getEdgeIndex() const {
        return get(&Arc::index, m_flowGraph);
    }

  private:
    // Problem Input
    const Instance* m_inst;

    // Problem data structures
    MutableDiGraph m_flowGraph;
    std::vector<double> m_distance;
    std::vector<Node> m_predecessors;
    struct DjikstraVisitor : public boost::base_visitor<DjikstraVisitor> {
        int dest;
        using event_filter = boost::on_finish_vertex;
        template <typename Graph>
        void operator()(const Node u, const Graph& g) const {
            if (g[u].index == dest) {
                throw 0;
            }
        }
    } m_djikstra_visitor;
    std::vector<Path> m_paths;
    std::vector<Node> m_chainLocations;
    std::map<std::vector<function_descriptor>, std::vector<int>> m_chainsToDemands;
    State m_state;
};

std::vector<ServicePath> loadFromDisk(const std::string& _filename,
    const std::vector<Demand<>>& _demands);
} // namespace SFC::Energy

#endif
