#ifndef ENERGY_CHAINING_PATH2_HP
#define ENERGY_CHAINING_PATH2_HP

#include "ColumnGeneration.hpp"
#include "Energy.hpp"
#include <ilcplex/ilocplex.h>

/*
* CG-cuts+ formulation
*/
namespace SFC::Energy::CG_CutsPlus {

struct DualValues {
    explicit DualValues(const Instance& _inst) noexcept;
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() noexcept = default;
    double getReducedCost(const CrossLayerLink& _link) const noexcept;
    double getReducedCost(const IntraLayerLink& _link) const noexcept;
    double getReducedCost(const UsedLink& _link) const noexcept;
    double getDualSumRHS() const noexcept;
    double getAdditionalVariable() const noexcept;
    const Instance* inst;
    std::vector<double> m_onePathDuals;
    std::vector<double> m_linkCapasDuals;
    std::vector<double> m_nodeCapaDuals;
    std::vector<double> m_kBoundsDuals;
    boost::multi_array<double, 2> m_linkUsableDuals;
    double m_networkCutsDual = 0.0;
    std::vector<double> m_neighborhoodCutsDuals;
};

class Master
    : public RMP<Master, DualValues, SFC::Energy::Solution<int>, ServicePath> {
  public:
    using Base = RMP<Master, DualValues, SFC::Energy::Solution<int>, ServicePath>;
    using Column = ServicePath;

    explicit Master(const Instance& _inst);
    Master& operator=(const Master&) = delete;
    Master(const Master&) = delete;
    Master& operator=(Master&&) = default;
    Master(Master&&) = default;
    ~Master() = default;

    double getReducedCost_impl(const Column& _sPath,
        const DualValues& _dualValues) const noexcept;

    IloNumColumn getColumn_impl(const Column& _col) noexcept;

    void getDuals_impl() noexcept;

    inline int getNbPricing() const noexcept {
        return m_inst->demands.size();
    }

    const DualValues& getDualValues() const noexcept {
        return m_duals;
    }

    const DualValues& getDualValues_impl() const noexcept {
        return m_duals;
    }

    template <typename Column>
    int getNbPricing() const noexcept {
        return m_inst->demands.size();
    }
    std::vector<ServicePath> getServicePaths() const noexcept;
    Solution getSolution() const noexcept;
    void removeDummyIfPossible() noexcept;

  private:
    const Instance* m_inst;

    using Base::m_columns;
    using Base::m_duals;
    using Base::m_dummyActive;
    using Base::m_env;
    using Base::m_fracObj;
    using Base::m_integerModel;
    using Base::m_intObj;
    using Base::m_model;
    using Base::m_solver;

    std::vector<IloNumVar> m_x;
    std::vector<IloNumVar> m_k;

    IloObjective m_obj;

    std::vector<IloRange> m_onePathCons;                    /// Exactly one path is used by constraints
    std::vector<IloRange> m_linkCapasCons;                  /// Link capacity constraints
    boost::multi_array<LazyConstraint, 2> m_linkUsableCons; /// m_linkUsableCons[e][i] links path of i with x[e]
    std::vector<IloRange> m_nodeCapasCons;                  /// Node capacity constraints sum < m_k
    std::vector<IloRange> m_kBounds;                        /// Bounds on m_k
    // Cuts
    IloRange m_networkCutCons;                  /// sum x > 2(n - 1)
    std::vector<IloRange> m_neighborhoodCutsIn; /// At least one edge per node

    std::vector<double> m_valsLinkCapa;
    std::vector<double> m_valsLinkUsed;
    std::vector<double> m_valsNode;
};

class PricingProblem {
  public:
    using Column = ServicePath;

    explicit PricingProblem(const Instance& _inst);
    PricingProblem& operator=(const PricingProblem&);
    PricingProblem(const PricingProblem&);
    PricingProblem& operator=(PricingProblem&&);
    PricingProblem(PricingProblem&&);
    ~PricingProblem() {
        m_env.end();
    }

    bool solve();
    double getObjValue() const;

    void setPricingID(const int _demandID);
    void updateDual(const DualValues& _dualValues);
    ServicePath getColumn() const;

  private:
    const Instance* m_inst;
    IloEnv m_env{};

    int m_demandID;
    const std::size_t n;
    const std::size_t m;
    const std::size_t nbLayers;
    IloModel m_model;

    IloObjective m_obj;

    boost::multi_array<IloNumVar, 2> m_a;
    boost::multi_array<IloNumVar, 2> m_f;
    std::vector<IloNumVar> m_x;

    boost::multi_array<IloRange, 2> m_flowConsCons;
    boost::multi_array<IloRange, 2> m_usableLink;
    std::vector<IloRange> m_linkCapaCons;
    std::vector<IloRange> m_nodeCapaCons;

    boost::multi_array<IloRange, 2> m_noInLoopCons;
    boost::multi_array<IloRange, 2> m_noOutLoopCons;

    IloCplex m_solver;

    ServicePath m_servicePath{};
};
} // namespace SFC::Energy::CG_CutsPlus

#endif
