#ifndef ENERGY_CHAINING_PATH_HP
#define ENERGY_CHAINING_PATH_HP

#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <map>
#include <thread>
#include <tuple>

#include "ColumnGeneration.hpp"
#include <cplex_utility.hpp>
#include <utility.hpp>

#include "Energy.hpp"
#include "SFC.hpp"

namespace SFC::Energy {

struct DualValues {
    DualValues(const Instance& _inst) noexcept;
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) noexcept = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) noexcept = default;
    ~DualValues() noexcept = default;
    IloNum getReducedCost(const CrossLayerLink& _link) const noexcept;
    IloNum getReducedCost(const IntraLayerLink& _link) const noexcept;
    double getDualSumRHS() const noexcept;
    double getAdditionalVariable() const noexcept;
    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals1;
    std::vector<IloNum> m_nodeCapasDuals2;
};

class Placement_Path
    : public RMP<Placement_Path, DualValues, SFC::Energy::Solution<int>, ServicePath> {
  public:
    friend DualValues;
    using Base = RMP<Placement_Path, DualValues, SFC::Energy::Solution<int>, ServicePath>;
    using Column = ServicePath;

    Placement_Path(const Instance& _inst) noexcept;
    Placement_Path& operator=(const Placement_Path&) = default;
    Placement_Path(const Placement_Path&) = default;
    Placement_Path& operator=(Placement_Path&&) = default;
    Placement_Path(Placement_Path&&) = default;
    ~Placement_Path() { m_env.end(); }

    double getReducedCost_impl(const Column& _sPath,
        const DualValues& _dualValues) const noexcept;
    IloNumColumn getColumn_impl(const Column& _col) noexcept;
    void getDuals_impl() noexcept;
    inline int getNbPricing() const noexcept {
        return m_inst->demands.size();
    }
    const DualValues& getDualValues() const noexcept {
        return m_duals;
    }
    const DualValues& getDualValues_impl() const noexcept {
        return m_duals;
    }

    template <typename Column>
    int getNbPricing() const noexcept {
        return m_inst->demands.size();
    }
    std::vector<ServicePath> getServicePaths() const noexcept;
    Solution getSolution() const noexcept;
    void removeDummyIfPossible() noexcept;
    DiGraph getActiveNetwork() const noexcept;

  private:
    const Instance* m_inst;

    using Base::m_columns;
    using Base::m_duals;
    using Base::m_dummyActive;
    using Base::m_env;
    using Base::m_fracObj;
    using Base::m_integerModel;
    using Base::m_intObj;
    using Base::m_model;
    using Base::m_solver;

    std::vector<IloNumVar> m_x;
    std::vector<IloNumVar> m_k;
    IloObjective m_obj;
    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons1;
    std::vector<IloRange> m_nodeCapasCons2;
    std::vector<IloRange> m_linkActiveEqConstraints;

    std::vector<IloNumVar> m_dummyPaths;
    // Cuts
    IloRange m_networkCutCons;
    std::vector<IloRange> m_neighborhoodCutsIn;

    std::vector<double> m_valsLink;
    std::vector<double> m_valsNode;
};
} // namespace SFC::Energy

#endif
