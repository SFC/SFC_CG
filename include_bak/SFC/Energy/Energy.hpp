#ifndef ENERGY_HPP
#define ENERGY_HPP

#include <fstream>
#include <iostream>

#include "SFC.hpp"
#include "utility.hpp"
#include <boost/format.hpp>
#include <boost/graph/graph_utility.hpp>
#include <ilcplex/ilocplex.h>

namespace SFC::Energy {

using ::operator<<;
struct Consumption {
    Consumption() = default;
    Consumption(const Consumption&) = default;
    Consumption(Consumption&&) = default;
    Consumption& operator=(const Consumption&) = default;
    Consumption& operator=(Consumption&&) = default;
    static Consumption fromFile(const std::string& _filename);

    double activeLink = 0.0;
    double propLink = 0.0;
    double activeCore = 0.0;
};

struct UsedLink {
    int demandID;
    Edge edge;
};

std::ostream& operator<<(std::ostream& _out, const SFC::Energy::Consumption& _cons);

std::vector<SFC::ServicePath> getServicePathsFromILPHeur(const std::vector<Demand<>>&
                                                             _demands,
    const std::string& _name, const double& _factor, const int _funcCoef);

struct Instance : public SFC::Instance {
    Instance(DiGraph _network, std::vector<int> _nodeCapa,
        std::vector<Demand<>> _demands, std::vector<double> _funcCharge,
        Consumption _energyCons);

    using SFC::Instance::demands;
    using SFC::Instance::funcCharge;
    using SFC::Instance::maxChainSize;
    using SFC::Instance::nbCores;
    using SFC::Instance::network;
    Consumption energyCons;
};

template <typename AdditionalInformation = unused>
class Solution {
  public:
    Solution(const Instance& _inst, double _objValue, double _lowerBound,
        DiGraph _activeNetwork,
        std::vector<ServicePath> _paths,
        AdditionalInformation _additionalInformation)
        : inst(&_inst)
        , activeNetwork(_activeNetwork)
        , objValue(_objValue)
        , lowerBound(_lowerBound)
        , paths(std::move(_paths))
        , additionalInformation(std::move(_additionalInformation)) {}

    bool isValid() const {
        int i = 0;
        for (const auto& [nPath, locations, demandID] : paths) {
            if (nPath.empty()) {
                std::cerr << "nPath for demand" << inst->demands[i] << "is empty\n";
                return false;
            }
            if (inst->demands[demandID].s != nPath.front()) {
                return false;
            }
            if (inst->demands[demandID].t != nPath.back()) {
                return false;
            }
            ++i;
        }

        const auto linkUsage = getLinkUsage(paths, *inst);
        for (const auto ed : boost::make_iterator_range(edges(inst->network))) {
            const auto [u, v] = incident(ed, inst->network);
            if (linkUsage[inst->network[ed].id] > inst->network[ed].capacity) {
                std::cerr << "link " << incident(ed, inst->network) << " is overloaded\n";
                return false;
            }
            if (linkUsage[inst->network[ed].id] > 0
                && !edge(u, v, activeNetwork).second) {
                std::cerr << boost::format("Using an inactive link: (%1%, %2%) with %3%\n")
                                 % u % v % linkUsage[inst->network[ed].id];
                return false;
            }
        }

        const auto nodeUsage = getNodeUsage(paths, *inst);
        for (Node u = 0; u < num_vertices(inst->network); ++u) {
            if (inst->network[u].isNFV
                && nodeUsage[u] > inst->network[u].capacity) {
                std::cerr << "node " << u << ") is overloaded\n";
                return false;
            }
        }
        return true;
    }

    void save(const std::string& _filename,
        const std::pair<double, double>& _time) const {
        std::ofstream ofs(_filename);
        ofs << objValue << '\t' << lowerBound << '\n';
        ofs << additionalInformation << '\n';
        ofs << _time.first << '\t' << _time.second << '\n';

        const auto nodeUsage = getNodeUsage(paths, *inst);
        ofs << num_vertices(inst->network) << '\n';
        for (Node u = 0; u < num_vertices(inst->network); ++u) {
            ofs << u << '\t' << nodeUsage[u] << '\t' << inst->network[u].capacity << '\n';
            assert(nodeUsage[u] <= inst->network[u].capacity);
        }

        const auto linkUsage = getLinkUsage(paths, *inst);
        ofs << num_edges(inst->network) << '\n';
        for (auto [ite, end] = edges(inst->network); ite != end; ++ite) {
            ofs << std::fixed << source(*ite, inst->network) << '\t' << target(*ite, inst->network) << '\t'
                << linkUsage[inst->network[*ite].id] << '\t' << inst->network[*ite].capacity << '\n';
            assert(linkUsage[inst->network[*ite].id] <= inst->network[*ite].capacity);
        }

        ofs << paths.size() << '\n';
        for (const auto& [nPath, locations, demandID] : paths) {
            ofs << nPath.size() << '\t';
            for (const auto& u : nPath) {
                ofs << u << '\t';
            }

            for (int j = 0; j < locations.size(); ++j) {
                ofs << locations[j] << '\t' << inst->demands[demandID].functions[j]
                    << '\t';
            }

            ofs << '\n';
        }
        std::cout << "Results saved to :" << _filename << '\n';
    }

  private:
    const Instance* inst;
    DiGraph activeNetwork;
    double objValue;
    double lowerBound;
    std::vector<ServicePath> paths;
    AdditionalInformation additionalInformation;
};

std::vector<IloNumVar> getLinkActivationVariables(const Instance& _inst,
    IloModel& _model);
std::vector<IloNumVar> getNumberActiveCoreVariables(const Instance& _inst,
    IloModel& _model);
IloRange getConnectivityConstraint(const Instance& _inst, IloModel& _model,
    const std::vector<IloNumVar>& _x);
std::vector<IloRange> getNeighborhoodConstraints(const Instance& _inst,
    IloModel& _model, const std::vector<IloNumVar>& _x);
std::vector<IloRange> getLinkCapacityConstraints(const Instance& _inst,
    IloModel& _model, const std::vector<IloNumVar>& _x);
std::vector<IloRange> getNodeCapacityConstraints(const Instance& _inst, IloModel& _model,
    const std::vector<IloNumVar>& _k);
IloExpr getNetworkEnergyConsumption(const Instance& _inst, const std::vector<IloNumVar>& _x,
    const std::vector<IloNumVar>& _k);
} // namespace SFC::Energy

std::ostream& operator<<(std::ostream& _out, const SFC::Energy::Consumption& _cons);

#endif
