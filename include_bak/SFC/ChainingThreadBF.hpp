#ifndef CHAINING_PATH_THREAD_BF_HPP
#define CHAINING_PATH_THREAD_BF_HPP

#include <ilconcert/iloexpression.h>
#include <ilconcert/ilolinear.h>
#include <ilconcert/ilosys.h>
#include <ilcplex/ilocplex.h>
#include <ilcplex/ilocplexi.h>

#include "ColumnGeneration.hpp"
#include "LinearProgramming.hpp"
#include "SFC.hpp"

namespace SFC {

class PlacementDWD_Path;

struct DualValues {
    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;

    explicit DualValues(const Instance& _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;

    [[nodiscard]] IloNum getReducedCost(const CrossLayerLink& _link) const;
    [[nodiscard]] IloNum getReducedCost(const IntraLayerLink& _link) const;
    [[nodiscard]] double getDualSumRHS() const;
    [[nodiscard]] double getAdditionalVariable() const;
};

class PlacementDWD_Path {
    const Instance* m_inst;

    IloEnv m_env{};
    IloModel m_model{IloModel(m_env)};
    IloObjective m_obj;

    std::vector<std::vector<ColumnPair<ServicePath>>> m_columns;

    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;
    std::vector<IloNumVar> m_dummyPaths;
    std::vector<double> m_linkCharge =
        std::vector<double>(num_edges(m_inst->network));
    std::vector<double> m_nodeCharge =
        std::vector<double>(num_vertices(m_inst->network));

  public:
    using ColumnType = ServicePath;

    explicit PlacementDWD_Path(const Instance& _inst);
    PlacementDWD_Path(const PlacementDWD_Path&) = delete;
    PlacementDWD_Path(PlacementDWD_Path&&) = default;
    PlacementDWD_Path& operator=(const PlacementDWD_Path&) = delete;
    PlacementDWD_Path& operator=(PlacementDWD_Path&&) = default;
    ~PlacementDWD_Path() = default;

    void addColumn(const ColumnType& _col);
    [[nodiscard]] DualValues getDualValues(const IloCplex& _solver) const;
    void getNetworkUsage(const IloCplex& _sol) const;
    [[nodiscard]] double getReducedCost(
        const ColumnType& _col, const DualValues& _dualValues) const;
    [[nodiscard]] bool isImprovingColumn(
        const ColumnType& _col, const DualValues& _dualValues) const;
    [[nodiscard]] IloEnv getEnv() const { return m_env; }

    [[nodiscard]] std::size_t getNbColumns() const { return m_columns.size(); }
    [[nodiscard]] std::vector<ColumnType> getServicePaths() const;
    [[nodiscard]] const IloModel& getModel() const { return m_model; }
};

} // namespace SFC
#endif
