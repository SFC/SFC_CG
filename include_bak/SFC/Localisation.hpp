#ifndef LOCALISATION_HPP
#define LOCALISATION_HPP

#include <boost/multi_array.hpp>
#include <boost/property_map/property_map.hpp>
#include <ilconcert/iloenv.h>
#include <ilconcert/ilolinear.h>
#include <ilconcert/ilomodel.h>
#include <ilcplex/ilocplex.h>
#include <ilcplex/ilocplexi.h>
#include <vector>

#include "OccLim.hpp"

namespace SFC {
namespace OccLim {
struct Instance;
} // namespace OccLim
} // namespace SFC

namespace SFC::OccLim {
class Localisation {
  public:
    Localisation(const Instance& _instance);
    ~Localisation();
    bool solve();
    boost::multi_array<bool, 2> getLocalisation() const;

  private:
    const Instance* m_instance;

    IloEnv m_env{};
    IloModel m_model;

    boost::multi_array<IloNumVar, 3> m_x;
    boost::multi_array<IloNumVar, 2> m_b;

    IloObjective m_obj;

    boost::multi_array<IloRange, 2> m_oneNodePerDemands;
    std::vector<IloRange> m_nodeCapacities;
    std::vector<IloRange> m_nbLicenses;
    boost::multi_array<IloRange, 3> m_demandNodeLinkage;

    IloCplex m_solver;
};

} // namespace SFC::OccLim

struct ConstantWeight {
    using value_type = int;
    using reference = int;
    using key_type = boost::graph_traits<DiGraph>::edge_descriptor;
    using category = boost::readable_property_map_tag;
    int value;
};

constexpr ConstantWeight::value_type get(const ConstantWeight& _w,
    const ConstantWeight::key_type& /*unused*/);

#endif
