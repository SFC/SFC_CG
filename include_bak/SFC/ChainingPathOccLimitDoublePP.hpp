#ifndef CHAINING_PATH_FUNCOCC_DOUBLEPP_HPP
#define CHAINING_PATH_FUNCOCC_DOUBLEPP_HPP

#include <boost/multi_array.hpp>
#include <ilconcert/iloexpression.h>
#include <ilconcert/ilolinear.h>
#include <iosfwd>
#include <type_traits>

#include "ColumnGeneration.hpp"
#include "Decomposition.hpp"
#include "OccLim.hpp"
#include "SFC.hpp"

namespace SFC::OccLim {

struct FunctionLocation {
    FunctionLocation(function_descriptor _f, std::vector<bool> _locations)
        : f(_f)
        , locations(std::move(_locations)) {}

    FunctionLocation() = default;
    ~FunctionLocation() = default;

    FunctionLocation(const FunctionLocation&) = default;
    FunctionLocation& operator=(const FunctionLocation&) = default;
    FunctionLocation(FunctionLocation&&) noexcept = default;
    FunctionLocation& operator=(FunctionLocation&&) noexcept = default;

    function_descriptor f = 0;
    std::vector<bool> locations;
};

std::ostream& operator<<(
    std::ostream& _out, const FunctionLocation& _fLocations);

bool operator==(const FunctionLocation& _lhs, const FunctionLocation& _rhs);

struct LicenseUsage {
    Node u;
    function_descriptor f;
};

std::vector<IloRange> getOneLocationPerFunctionConstraints(
    const Instance& _inst, IloModel& _model);
boost::multi_array<LazyConstraint, 3> getPathLocationsLinkConstraints(
    const Instance& _inst, IloModel& _model);

struct PathLocDualValues {
    PathLocDualValues(const Instance& _inst);
    double getReducedCost(const CrossLayerLink& _link) const;
    double getReducedCost(const IntraLayerLink& _link) const;
    double getReducedCost(const LicenseUsage& _link) const;
    double getDualSumRHS() const;

    const Instance* inst;
    std::vector<double> m_onePathDuals;
    std::vector<double> m_oneLocationDuals;
    std::vector<double> m_linkCapasDuals;
    std::vector<double> m_nodeCapasDuals;
    boost::multi_array<double, 3> m_pathFuncDuals;
};

class DoublePlacement {
  public:
    DoublePlacement(const Instance& _inst);
    DoublePlacement& operator=(const DoublePlacement&) = delete;
    DoublePlacement(const DoublePlacement&) = delete;
    DoublePlacement& operator=(DoublePlacement&&) = default;
    DoublePlacement(DoublePlacement&&) = default;
    ~DoublePlacement() = default;

    bool checkReducedCosts() const;
    double getReducedCost_impl(
        const ServicePath& _sPath, const PathLocDualValues& _dualValues) const;
    IloNumColumn getColumn_impl(const ServicePath& _col);

    double getReducedCost_impl(const FunctionLocation& _sPath,
        const PathLocDualValues& _dualValues) const;
    IloNumColumn getColumn_impl(const FunctionLocation& _col);

    void getDuals_impl();

    template <typename T>
    inline int getNbPricing() const {
        if constexpr (std::is_same<T, ServicePath>::value) {
            return m_inst->demands.size();
        } else if constexpr (std::is_same<T, FunctionLocation>::value) {
            return m_inst->funcCharge.size();
        }
    }

    const PathLocDualValues& getDualValues() const { return m_duals; }

    const PathLocDualValues& getDualValues_impl() const { return m_duals; }

    int getNbColumns() const;

    std::vector<ServicePath> getServicePaths() const;

    Solution getSolution() const;
    void removeDummyIfPossible();

  private:
    std::vector<std::vector<ColumnPair<ServicePath>>> m_paths;
    std::vector<std::vector<ColumnPair<FunctionLocation>>> m_locs;
    PathLocDualValues m_duals;
    LinearModel m_model;

    const Instance* m_inst;

    IloObjective m_obj; /// Minimizing the bandwidth

    std::vector<IloRange> m_onePathCons;   /// Exactly one path per request
    std::vector<IloRange> m_linkCapasCons; /// Link capacity constraints
    std::vector<IloRange> m_nodeCapasCons; /// Node capacity constraints
    std::vector<IloRange>
        m_oneLocationCons; /// Exactly one location per function
    boost::multi_array<LazyConstraint, 3>
        m_pathFuncCons; /// Link path and function locations

    std::vector<IloNumVar> m_dummyPaths;
    std::vector<IloNumVar> m_dummyLocations;

    std::vector<double> m_linkCharge;
    std::vector<double> m_nodeCharge;
};

class LocationPricingProblem {
  public:
    using Column = FunctionLocation;
    LocationPricingProblem(const Instance& _inst);
    void setPricingID(function_descriptor _function);

    template <typename DualValues>
    void updateDual(const DualValues& _dualValues) {
        std::transform(vertices(m_inst->network).first,
            vertices(m_inst->network).second, m_weight.begin(), [&](auto&& u) {
                return _dualValues.getReducedCost(LicenseUsage{u, m_f});
            });
        // Sort nodes by ascending sum of dual values
        std::sort(m_sortedNodes.begin(), m_sortedNodes.end(),
            [&](auto&& __u1, auto&& __u2) {
                return m_weight[__u1] < m_weight[__u2];
            });
        m_objValue = -_dualValues.m_oneLocationDuals[m_f];
    }
    bool solve();
    double getObjValue() const;
    FunctionLocation getColumn() const;

  private:
    const Instance* m_inst;
    function_descriptor m_f;
    FunctionLocation m_funcLocation;
    std::vector<double> m_weight;
    std::vector<Node> m_sortedNodes;
    double m_objValue;
};

void barycenter(const PathLocDualValues& _nDuals,
    const PathLocDualValues& _bDuals, PathLocDualValues& _sDuals,
    double _alpha);

} // namespace SFC::OccLim
#endif
