#ifndef INSTANCE_GENERATOR_HPP
#define INSTANCE_GENERATOR_HPP

#include "Generator.hpp"
#include "Partial.hpp"
#include <random>
#include <string_view>

namespace SFC::Partial {

class ChainGenerator {
  public:
    ChainGenerator(int _nbFunctions, int _chainSize, double _precedence_proba)
        : m_function_distribution(0.0, _nbFunctions - 1)
        , m_chainSize(_chainSize)
        , m_precedence_proba(_precedence_proba) {}

    ~ChainGenerator() = default;
    ChainGenerator(const ChainGenerator&) = default;
    ChainGenerator& operator=(const ChainGenerator&) = default;
    ChainGenerator(ChainGenerator&&) noexcept = default;
    ChainGenerator& operator=(ChainGenerator&&) noexcept = default;

    template <typename RandGenerator>
    Generator::Service<SFC::Partial::Chain> operator()(
        RandGenerator& _gen) noexcept {
        std::vector<function_descriptor> functions(m_chainSize);
        std::generate_n(functions.begin(), m_chainSize,
            [&]() { return m_function_distribution(_gen); });
        std::vector<std::vector<function_descriptor>> precedingFunctions(
            m_chainSize);
        for (int i = 0; i < functions.size(); ++i) {
            for (int j = i + 1; j < functions.size(); ++j) {
                if (std::generate_canonical<double, 10>(_gen)
                    <= m_precedence_proba) {
                    precedingFunctions[j].push_back(i);
                }
            }
        }
        return {{functions, precedingFunctions}, m_charge_distribution(_gen)};
    }

  private:
    std::uniform_int_distribution<> m_function_distribution;
    int m_chainSize; /// Number of function in the service
    double m_precedence_proba;
    std::uniform_real_distribution<> m_charge_distribution{
        78 / 1000.0, 36456 / 1000.0};
};

void save(std::ostream& _out, std::string_view _instanceName,
    const Instance& _inst) noexcept;
Network loadTopology(std::ifstream& _ifs);
} // namespace SFC::Partial
#endif
