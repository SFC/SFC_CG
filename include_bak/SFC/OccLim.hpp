#ifndef SFC_OCCLIM_HPP
#define SFC_OCCLIM_HPP

#include <cassert>
#include <iosfwd>
#include <optional>
#include <random>
#include <utility>

#include "SFC.hpp"
#include "SFC/Partial.hpp"

namespace SFC::OccLim {
using ::operator<<;

struct Instance : public SFC::Instance {

    Instance(DiGraph _network, std::vector<int> _nodeCapa,
        std::vector<Demand<>> _demands, std::vector<double> _funcCharge,
        int _nbLicenses);

    using SFC::Instance::demands;
    using SFC::Instance::funcCharge;
    using SFC::Instance::maxChainSize;
    using SFC::Instance::nbCores;
    using SFC::Instance::network;
    int nbLicenses;
};

boost::multi_array<bool, 2> getFunctionPlacement(
    const std::vector<ServicePath>& _sPaths, const Instance& _inst);

class Solution {
  public:
    Solution(const Instance& _inst, SFC::Solution _sol)
        : inst(&_inst)
        , objValue(_sol.getObjValue())
        , lowerBound(_sol.getLowerBound())
        , paths(std::move(_sol).getServicePaths()) {}
    Solution(const Instance& _inst, double _objValue, double _lowerBound,
        std::vector<ServicePath> _paths)
        : inst(&_inst)
        , objValue(_objValue)
        , lowerBound(_lowerBound)
        , paths(std::move(_paths)) {}

    bool isValid() const {
        int i = 0;
        for (const auto& [nPath, locations, demandID] : paths) {
            if (nPath.empty()) {
                std::cerr << "nPath for demand" << inst->demands[i]
                          << "is empty\n";
                return false;
            }
            if (inst->demands[demandID].s != nPath.front()) {
                std::cerr << "nPath.front() of demand " << demandID
                          << " should be " << inst->demands[demandID].s
                          << " but is " << nPath.front() << '\n';
                return false;
            }
            if (inst->demands[demandID].t != nPath.back()) {
                std::cerr << "nPath.back() of demand " << demandID
                          << " should be " << inst->demands[demandID].s
                          << " but is " << nPath.back() << '\n';
                return false;
            }
            ++i;
        }
        const std::vector<double> linkUsage = getLinkUsage(paths, *inst);
        for (const auto ed : boost::make_iterator_range(edges(inst->network))) {
            if (linkUsage[inst->network[ed].id] > inst->network[ed].capacity) {
                std::cerr << "link " << incident(ed, inst->network)
                          << " is overloaded\n";
                return false;
            }
        }
        const auto nodeUsage = getNodeUsage(paths, *inst);
        for (const auto u :
            boost::make_iterator_range(vertices(inst->network))) {
            const auto& [id, capacity, isNFV] = inst->network[u];
            if (isNFV && nodeUsage[u] > capacity) {
                std::cerr << "node " << u << ") is overloaded\n";
                return false;
            }
        }
        return true;
    }

    void save(const std::string& _filename,
        const std::pair<double, double>& _time) const {
        std::ofstream ofs(_filename);
        ofs << objValue << '\t' << lowerBound << '\n';
        ofs << _time.first << '\t' << _time.second << std::endl;

        const auto linkUsage = getLinkUsage(paths, *inst);
        const auto nodeUsage = getNodeUsage(paths, *inst);
        const auto fPlacement = getFunctionPlacement(paths, *inst);
        for (function_descriptor f = 0; f < inst->funcCharge.size(); ++f) {
            int nbLicenses = 0;
            for (Node u = 0; u < num_vertices(inst->network); ++u) {
                nbLicenses += fPlacement[u][f];
            }
            assert([&]() {
                if (nbLicenses > inst->nbLicenses) {
                    std::cout << f << " is using too many licenses: [";
                    for (Node u = 0; u < num_vertices(inst->network); ++u) {
                        if (fPlacement[u][f]) {
                            std::cout << u << ", ";
                        }
                    }
                    std::cout << '\n';
                    return false;
                }
                return true;
            }());
        }

        ofs << num_vertices(inst->network) << '\n';
        for (const auto u :
            boost::make_iterator_range(vertices(inst->network))) {
            ofs << u << '\t' << nodeUsage[u] << '\t'
                << inst->network[u].capacity << '\n';
            assert(nodeUsage[u] <= inst->network[u].capacity);
        }

        ofs << num_edges(inst->network) << '\n';
        for (const auto ed : boost::make_iterator_range(edges(inst->network))) {
            const auto& [u, v] = incident(ed, inst->network);
            const auto& [id, capa] = inst->network[ed];

            ofs << std::fixed << u << '\t' << v << '\t' << linkUsage[id] << '\t'
                << capa << '\n';

            assert(linkUsage[id] <= capa);
        }

        ofs << paths.size() << '\n';
        for (const auto& [nPath, locations, demandID] : paths) {
            ofs << nPath.size() << '\t';
            for (const auto& u : nPath) {
                ofs << u << '\t';
            }

            for (int j = 0; j < locations.size(); ++j) {
                ofs << locations[j] << '\t'
                    << inst->demands[demandID].functions[j] << '\t';
            }

            ofs << '\n';
        }
        std::cout << "Results saved to :" << _filename << '\n';
    }

    void setObjValue(double _objValue) { objValue = _objValue; }

    void setLowerBound(double _lb) { lowerBound = _lb; }
    double getObjValue() const noexcept { return objValue; }

    double getLowerBound() const noexcept { return lowerBound; }

    double getEpsilon() const noexcept {
        return (objValue - lowerBound) / lowerBound;
    }

  private:
    const Instance* inst;
    double objValue;
    double lowerBound;
    std::vector<ServicePath> paths;
};

struct LocationSum {
    IloRange rng;
};
} // namespace SFC::OccLim

namespace SFC::Partial::OccLim {

using Solution = SFC::Partial::Solution;

struct Instance : public SFC::Partial::Instance {
    Instance(Network _network, std::vector<Demand> _demands,
        std::vector<double> _funcCharge, int _nbLicenses);
    Instance(SFC::Partial::Instance _inst, int _nbLicenses);

    using SFC::Partial::Instance::demands;
    using SFC::Partial::Instance::funcCharge;
    using SFC::Partial::Instance::maxChainSize;
    using SFC::Partial::Instance::network;
    int nbLicenses;
};

Instance loadInstance(std::ifstream& _ifs, int _nbLicenses);

template <typename RandGen>
boost::multi_array<bool, 2> getLocations(
    const boost::multi_array<double, 2>& _bProba, RandGen& _gen) {
    boost::multi_array<bool, 2> retval(
        boost::extents[_bProba.shape()[0]][_bProba.shape()[1]]);
    std::transform(_bProba.data(), _bProba.data() + _bProba.num_elements(),
        retval.data(), [&](auto&& _p) {
            return std::generate_canonical<double, 10>(_gen)
                   <= std::clamp(_p, 0.01, 0.99);
        });
    return retval;
}

boost::multi_array<bool, 2> getLocations(const Solution& _sol);

bool isValid(
    const boost::multi_array<bool, 2>& _locations, const Instance& _inst);

template <typename T>
bool print(const boost::multi_array<T, 2>& _locations, const Instance& _inst) {
    for (int f = 0; f < _inst.funcCharge.size(); ++f) {
        std::cout << "f: ";
        for (int u = 0; u < num_vertices(_inst.network); ++u) {
            if (_locations[u][f]) {
                std::cout << u << ", ";
            }
        }
        std::cout << '\n';
    }
    std::cout << '\n';
    return true;
}

template <typename Solution>
bool isValid(const Solution& _sol, const Instance& _inst) {
    const auto locations = getLocations(_sol);
    for (int f = 0; f < _inst.funcCharge.size(); ++f) {
        int nbLicenses = 0;
        for (int u = 0; u < num_vertices(_inst.network); ++u) {
            if (locations[u][f]) {
                nbLicenses++;
            }
        }
        if (nbLicenses > _inst.nbLicenses) {
            std::cerr << "Exceding # licenses for " << f << " -> " << nbLicenses
                      << '\n';
            return false;
        }
    }
    /*assert((const auto& sPaths = _sol.getServicePaths(),
        std::all_of(sPaths.begin(), sPaths.end(), [&](const auto& sPath) {
            _inst.demands[sPath.demand].s == sPath.nPath.front()
                && _inst.demands[sPath.demand].t == sPath.nPath.back();
        })));*/
    return true;
}

struct RelaxedSolution {
    std::vector<std::vector<Weighted<ServicePath>>> paths;
    boost::multi_array<double, 2> locations;
};

} // namespace SFC::Partial::OccLim
#endif
