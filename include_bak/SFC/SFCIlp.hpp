#ifndef SFC_ILP_HPP
#define SFC_ILP_HPP

#include <ilcplex/ilocplex.h>

#include <boost/multi_array.hpp>
#include <cplex_utility.hpp>
#include <utility.hpp>

#include "SFC.hpp"

namespace SFC {
class ILP {
  public:
    ILP(const Instance& _inst)
        : m_inst(&_inst)
        , m_env([&]() {
            IloEnv retval;
            retval.setNormalizer(false);
            return retval;
        }())
        , m_f([&]() {
            boost::multi_array<IloNumVar, 3> f(
                boost::extents[long(num_edges(m_inst->network))][long(
                    m_inst->demands.size())][long(m_inst->maxChainSize + 1)]);
            for (const auto ed :
                boost::make_iterator_range(edges(m_inst->network))) {
                for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
                    for (std::size_t j = 0;
                         j <= m_inst->demands[i].functions.size(); ++j) {
                        f[long(m_inst->network[ed].id)][long(i)][long(j)] =
                            IloAdd(
                                m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                        // setIloName(f[m_inst->network[ed].id][i][j], "f" +
                        // toString(std::make_tuple(m_inst->network[ed].id, i,
                        // j)));
                    }
                }
            }
            return f;
        }())
        , m_a([&]() {
            boost::multi_array<IloNumVar, 3> a(
                boost::extents[long(num_vertices(m_inst->network))][long(
                    m_inst->demands.size())][long(m_inst->maxChainSize)]);
            for (const auto u :
                boost::make_iterator_range(vertices(m_inst->network))) {
                if (m_inst->network[u].isNFV) {
                    for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
                        for (std::size_t j = 0;
                             j < m_inst->demands[i].functions.size(); ++j) {
                            a[long(u)][long(i)][long(j)] = IloAdd(
                                m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                            // setIloName(a[u][i][j], "a" +
                            // toString(std::make_tuple(u, i, j)));
                        }
                    }
                }
            }
            return a;
        }())
        , m_obj([&]() {
            IloExpr expr(m_env);
            for (const auto ed :
                boost::make_iterator_range(edges(m_inst->network))) {
                for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
                    for (std::size_t j = 0;
                         j <= m_inst->demands[i].functions.size(); ++j) {
                        expr += m_inst->demands[i].d
                                * m_f[long(m_inst->network[ed].id)][long(i)]
                                     [long(j)];
                    }
                }
            }
            IloObjective retval = IloObjective(m_env, expr);
            expr.end();
            return IloAdd(m_model, retval);
        }())
        , m_flowConservationConstraints([&]() {
            boost::multi_array<IloRange, 3> flowCons(
                boost::extents[long(num_vertices(m_inst->network))][long(
                    m_inst->demands.size())][long(m_inst->maxChainSize + 1)]);

            IloExpr expr(m_env);
            for (Node u = 0; u < num_vertices(m_inst->network); ++u) {
                for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
                    for (std::size_t j = 0;
                         j <= m_inst->demands[i].functions.size(); ++j) {
                        expr.clear();
                        // Outgoing flows
                        for (const auto ed : boost::make_iterator_range(
                                 out_edges(u, m_inst->network))) {
                            expr += m_f[long(m_inst->network[ed].id)][long(i)]
                                       [long(j)];
                        }
                        // Incoming flows
                        for (const auto ed : boost::make_iterator_range(
                                 in_edges(u, m_inst->network))) {
                            expr += -m_f[long(m_inst->network[ed].id)][long(i)]
                                        [long(j)];
                        }
                        if (m_inst->network[u].isNFV) {
                            // Next layer
                            if (j < m_inst->demands[i].functions.size()) {
                                expr += m_a[long(u)][long(i)][long(j)];
                            }
                            // Previous layer
                            if (j >= 1) {
                                expr += -m_a[long(u)][long(i)][long(j - 1)];
                            }
                        }
                        flowCons[long(u)][long(i)][long(j)] =
                            IloAdd(m_model, IloRange(m_env, 0.0, expr, 0.0));
                        // setIloName(flowCons[u][i][j], "flowCons" +
                        // toString(std::make_tuple(u, i, j)));
                    }
                }
            }
            for (const auto& [id, s, t, d, functions] : m_inst->demands) {
                flowCons[long(s)][long(id)][0].setBounds(1.0, 1.0);
                flowCons[long(t)][long(id)][long(functions.size())].setBounds(
                    -1.0, -1.0);
            }
            expr.end();
            return flowCons;
        }())
        , m_linkCapacityConstraitns([&]() {
            std::vector<IloRange> linkCapaConstraints(
                num_edges(m_inst->network));
            IloExpr expr(m_env);
            for (const auto ed :
                boost::make_iterator_range(edges(m_inst->network))) {
                expr.clear();
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
                    for (std::size_t j = 0;
                         j <= m_inst->demands[i].functions.size(); ++j) {
                        expr += m_inst->demands[i].d
                                * m_f[long(m_inst->network[ed].id)][long(i)]
                                     [long(j)];
                    }
                }
                linkCapaConstraints[m_inst->network[ed].id] =
                    IloAdd(m_model, expr <= m_inst->network[ed].capacity);
                // setIloName(linkCapaConstraints[m_inst->network[ed].id],
                // "linkCapa" + toString(incident(ed, m_inst->network)));
            }
            expr.end();
            return linkCapaConstraints;
        }())
        , m_nodeCapacityConstraints([&]() {
            std::vector<IloRange> nodeCapaConstraints(
                num_vertices(m_inst->network));
            IloExpr expr(m_env);
            for (const auto u :
                boost::make_iterator_range(vertices(m_inst->network))) {
                if (m_inst->network[u].isNFV) {
                    expr.clear();
                    for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
                        for (std::size_t j = 0;
                             j < m_inst->demands[i].functions.size(); ++j) {
                            expr += m_inst->nbCores[long(i)][long(j)]
                                    * m_a[long(u)][long(i)][long(j)];
                        }
                    }
                    nodeCapaConstraints[u] =
                        IloAdd(m_model, expr <= m_inst->network[u].capacity);
                    // setIloName(nodeCapaConstraints[u], "nodeCapa" +
                    // toString(u));
                }
            }
            expr.end();
            return nodeCapaConstraints;
        }())
        , m_solver([&]() {
            IloCplex solver(m_model);
            solver.setParam(IloCplex::Threads, 1);
            solver.setOut(m_env.getNullStream());
            solver.setWarning(m_env.getNullStream());
            return solver;
        }()) {}

    bool solve() {
        if (!m_solver.solve()) {
            std::cout << "No solution found!\n";
            m_solver.exportModel("ILP.lp");
            return false;
        }
        m_intObj = m_solver.getObjValue();
        std::cout << "Solution found :" << m_intObj << '\n';
        return true;
    }

    void save(
        const std::string& _filename, const std::pair<double, double>& _time) {
        std::ofstream ofs(_filename);
        ofs << m_intObj << '\t' << m_fractObj << '\n';
        ofs << _time.first << '\t' << _time.second << std::endl;
    }

  private:
    // Data
    const Instance* m_inst;

    // Results
    double m_intObj = -1;
    double m_fractObj = -1;
    // ILP
    IloEnv m_env{};
    IloModel m_model{m_env};

    boost::multi_array<IloNumVar, 3> m_f;
    boost::multi_array<IloNumVar, 3> m_a;

    IloObjective m_obj;

    boost::multi_array<IloRange, 3> m_flowConservationConstraints;
    std::vector<IloRange> m_linkCapacityConstraitns;
    std::vector<IloRange> m_nodeCapacityConstraints;

    IloCplex m_solver;
};
} // namespace SFC

#endif
