#ifndef SFC_CHAININGPATHOCCLIMIT_HPP
#define SFC_CHAININGPATHOCCLIMIT_HPP

#include <variant>

#include "cplex_utility.hpp"
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/multi_array.hpp>

#include "ColumnGeneration.hpp"
#include "LinearProgramming.hpp"
#include "OccLim.hpp"

namespace SFC::OccLim {

namespace Beta {

struct DualValues {
    DualValues(const Instance& _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;

    IloNum getReducedCost(const CrossLayerLink& _link) const;
    IloNum getReducedCost(const IntraLayerLink& _link) const;
    double getDualSumRHS() const;

    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;
    boost::multi_array<IloNum, 3> m_pathFuncDuals;
    std::vector<IloNum> m_nbLicensesDuals;
    double m_branchesDual;
};

class Master {
    friend DualValues;

  public:
    using Column = ServicePath;

    explicit Master(const Instance& _inst);
    Master& operator=(const Master&) = delete;
    Master& operator=(Master&&) = delete;
    Master(const Master&) = delete;
    Master(Master&&) = delete;
    ~Master() = default;

    double getReducedCost_impl(
        const ServicePath& _col, const DualValues& _dualValues) const;
    IloNumColumn getColumn_impl(const ServicePath& _col);
    void getDuals_impl();
    void removeDummyIfPossible();
    const DualValues& getDualValues_impl() const;
    std::vector<ServicePath> getServicePaths() const;
    template <typename T>
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }
    Solution getSolution() const;
    void setNbLicenses(const int _nbLicenses) {
        for (auto& cons : m_nbLicensesCons) {
            cons.setUB(_nbLicenses);
        }
    }
    int getNbColumns() const;

  private:
    std::vector<std::vector<ColumnPair<ServicePath>>> m_columns;
    DualValues m_duals;
    LinearModel m_model;

    const Instance* m_inst;

    boost::multi_array<IloNumVar, 2> m_b;

    IloObjective m_obj;

    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;
    std::vector<IloRange> m_nbLicensesCons;
    boost::multi_array<IloRange, 2> m_pathFuncCons;

    std::vector<IloNumVar> m_dummyPaths;
    std::vector<double> m_linkCharge;
    std::vector<double> m_nodeCharge;
    boost::multi_array<double, 2> m_funcPath;
};

void barycenter(const DualValues& _nDuals, const DualValues& _bDual,
    DualValues& _sDuals, double _alpha);

} // namespace Beta

namespace Alpha {

struct DualValues {
    DualValues(const Instance& _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;

    IloNum getReducedCost(const CrossLayerLink& _link) const;
    IloNum getReducedCost(const IntraLayerLink& _link) const;
    double getDualSumRHS() const;

    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;
    boost::multi_array<IloNum, 3> m_pathFuncDuals;
    std::vector<IloNum> m_nbLicensesDuals;
    double m_branchesDual;
};

class Master {
    friend DualValues;

  public:
    using Column = ServicePath;

    explicit Master(const Instance& _inst);
    Master& operator=(const Master&) = delete;
    Master& operator=(Master&&) = delete;
    Master(const Master&) = delete;
    Master(Master&&) = delete;
    ~Master() = default;

    double getReducedCost_impl(
        const ServicePath& _col, const DualValues& _dualValues) const;
    IloNumColumn getColumn_impl(const ServicePath& _col);
    void getDuals_impl();
    void removeDummyIfPossible();
    const DualValues& getDualValues_impl() const;
    std::vector<ServicePath> getServicePaths() const;
    template <typename T>
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }
    Solution getSolution() const;

    std::vector<Weighted<ServicePath>> getWeightedServicePath(
        int _demandId) const;
    bool isIntegral() const;
    std::vector<IloRange> getBranch_impl() const;

    std::optional<Weighted<std::vector<Edge>>> getMostFractionalCut(
        const int _demandID) const;
    std::optional<Weighted<EdgeCut>> getMostFractionalCut(
        const int _demandID, const int _layer) const;

    std::optional<std::vector<FixedVariable<SFC::IntraLayerLink>>>
    getLinkBranches() const;
    std::optional<std::vector<ForbiddenLayeredCut>>
    getLayeredCutBranches() const;
    std::optional<std::vector<std::vector<NetworkLink>>>
    getEdgeCutBranches() const;
    std::optional<std::vector<FixedVariable<SFC::CrossLayerLink>>>
    getNodeBranches() const;
    std::optional<std::vector<LocationSum>> getLocationsBranches() const;

    boost::multi_array<double, 3> getEdgeUsage() const;

    void add(LocationSum _rng);
    void add(const FixedVariable<IntraLayerLink>& _link);
    void add(const FixedVariable<CrossLayerLink>& _link);
    void add(const std::vector<NetworkLink>& _edgeCut);
    void add(const ForbiddenLayeredCut& _layeredCut);

    template <typename... VariantType>
    void add(const std::variant<VariantType...>& _rng) {
        std::visit([&](auto&& _br) { this->add(_br); }, _rng);
    }

    void remove(LocationSum _rng);
    void remove(const FixedVariable<IntraLayerLink>& _link);
    void remove(const FixedVariable<CrossLayerLink>& _link);
    void remove(const std::vector<NetworkLink>& _edgeCut);
    void remove(const ForbiddenLayeredCut& _layeredCut);

    template <typename... VariantType>
    void remove(const std::variant<VariantType...>& _rng);

    void clearBranches();
    int getNbColumns() const;

    const std::vector<std::set<CrossLayerLink>>&
    getForbiddenCrossLinks() const {
        return m_forbiddenCrossLinks;
    }
    const std::vector<std::set<IntraLayerLink>>&
    getForbiddenIntraLinks() const {
        return m_forbiddenIntraLinks;
    }

  private:
    std::vector<std::vector<ColumnPair<ServicePath>>> m_columns;
    DualValues m_duals;
    LinearModel m_model;

    const Instance* m_inst;

    boost::multi_array<IloNumVar, 2> m_b;
    IloObjective m_obj;
    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;
    std::vector<IloRange> m_nbLicensesCons;
    boost::multi_array<LazyConstraint, 3> m_pathFuncCons;
    std::vector<IloRange> m_branches{};

    std::vector<IloNumVar> m_dummyPaths;
    std::vector<IloNum> m_linkCharge;
    std::vector<IloNum> m_nodeCharge;
    mutable boost::multi_array<double, 3> m_edgeUsage;
    mutable boost::multi_array<double, 3> m_nodeUsage;
    std::vector<std::set<CrossLayerLink>> m_forbiddenCrossLinks{
        std::vector<std::set<CrossLayerLink>>(m_inst->demands.size())};
    std::vector<std::set<IntraLayerLink>> m_forbiddenIntraLinks{
        std::vector<std::set<IntraLayerLink>>(m_inst->demands.size())};
};

void barycenter(const DualValues& _nDuals, const DualValues& _bDual,
    DualValues& _sDuals, double _alpha);

std::ostream& operator<<(std::ostream& _out, const DualValues& _dualValues);
} // namespace Alpha

} // namespace SFC::OccLim
#endif
