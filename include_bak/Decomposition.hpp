#ifndef DECOMPOSITION_HPP
#define DECOMPOSITION_HPP

#include <vector>

#include "SFC.hpp"
#include <ilcplex/ilocplex.h>

namespace SFC {

template <typename Instance>
std::vector<IloRange> getOnePathPerDemandConstraints(
    const Instance& _inst, IloModel& _model) {
    std::vector<IloRange> onePathCons(_inst.demands.size());
    std::generate_n(onePathCons.begin(), _inst.demands.size(), [&]() {
        return IloAdd(_model, IloRange(_model.getEnv(), 1.0, IloInfinity));
    });
    return onePathCons;
}

template <typename Instance>
std::vector<IloRange> getLinkCapacityConstraints(
    const Instance& _inst, IloModel& _model) {
    std::vector<IloRange> linkCapaCons(num_edges(_inst.network));

    for (const auto ed : boost::make_iterator_range(edges(_inst.network))) {
        linkCapaCons[_inst.network[ed].id] = IloAdd(_model,
            IloRange(_model.getEnv(), -IloInfinity, _inst.network[ed].capacity,
                ("linkCapaCons(" + std::to_string(_inst.network[ed].id) + ')')
                    .c_str()));
    }
    return linkCapaCons;
}

template <typename Instance>
std::vector<IloRange> getNodeCapacityConstraints(
    const Instance& _inst, IloModel& _model) {
    std::vector<IloRange> nodeCapasCons(num_vertices(_inst.network));
    std::transform(vertices(_inst.network).first,
        vertices(_inst.network).second, nodeCapasCons.begin(), [&](auto&& u) {
            return IloAdd(_model,
                IloRange(_model.getEnv(), -IloInfinity,
                    _inst.network[u].capacity,
                    ("nodeCapaCons(" + std::to_string(u) + ')').c_str()));
        });
    return nodeCapasCons;
}
} // namespace SFC
#endif
