# SFC column generation algorithms

This repository contains algorithm related to the Service Function Placement problem published in `Huin, Nicolas, Brigitte Jaumard, and Frederic Giroire. 2018. “Optimal Network Service Chain Provisioning.” IEEE/ACM Transactions on Networking 26 (3): 1320–33. https://doi.org/10.1109/TNET.2018.2833815`.

## Dependencies

* CPLEX
* Conan

## Python library

Even though the algorithms are implemented in C++, we also provide a python library to ease their use. The library is compatible with pip and can be simply installed with the following commands
```
conan remote add gitlab https://gitlab.inria.fr/api/v4/projects/39945/packages/conan
python3 -m pip install git+https://gitlab.inria.fr/SFC/SFC_CG
```

Make sure that the CPLEX library is installed.

```
python3 -c 'import sfc
inst = sfc.loadInstance("../SFC_CG_data/data/instances/occlim/germany50.json")
visitor = sfc.CRGBnBVisitor()
sol = sfc.crgAndBranchAndBound(inst, 50, 48, 600.0, 0, visitor)'
```

