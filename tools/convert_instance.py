import csv
import argparse
import json
import os


def read_demand_file(filename):
    with open(filename, newline='') as csvfile:
        retval = {}
        reader = csv.reader(csvfile, delimiter='\t')
        retval['functions_charges'] = [
            float(v) for i, v in enumerate(next(reader))]
        retval['demands'] = [{'src': int(row[0]), 'dst': int(row[1]), 'bandwidth': float(
            row[2]), 'functions': [int(f) for f in row[3:]]} for row in reader]
        return retval


def read_node_file(filename):
    with open(filename, newline='') as csvfile:
        retval = {}
        reader = csv.reader(csvfile, delimiter='\t')
        retval['nodeCapacity'] = [
            float(row[1]) if row[1] != '-1' else 0.0 for row in reader]
        return retval


def read_topo_file(filename):
    with open(filename, newline='') as csvfile:
        retval = {}
        reader = csv.reader(csvfile, delimiter='\t')
        # Skip the other two columns, since they should the same value
        retval['order'] = next(reader)[0]
        retval['edge_list'] = [{'src': int(row[0]), 'dst': int(
            row[1]), 'bandwidth': float(row[2])} for row in reader]
        return retval


def convert_instance(instance_name, base_folder, output_filename=None):
    if output_filename == None:
        output_filename = instance_name + ".json"
    instance = read_demand_file(os.path.join(
        base_folder, instance_name + "_demand.txt"))
    instance.update(read_node_file(os.path.join(
        base_folder, instance_name + "_nodeCapa.txt")))
    instance.update(read_topo_file(os.path.join(
        base_folder, instance_name + "_topo.txt")))
    with open(os.path.join(base_folder, output_filename), "w") as jsonfile:
        json.dump(instance, jsonfile, indent=1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "instance_name", help="Name of the instance to convert")
    parser.add_argument(
        "base_folder", help="Base folder where the instance exists")
    parser.add_argument("--output_filename",
                        help="Name of the output file", default=None)
    args = parser.parse_args()
    convert_instance(args.instance_name,
                     args.base_folder, args.output_filename)
