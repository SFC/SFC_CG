#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this
                          // in one cpp file
#include "catch.hpp"
#include <iostream>
#include <utility.hpp>
#include <vector>

#include "Partial.hpp"
#include "RCShortestPathPerDemand.hpp"

struct MoocDuals {
    template <typename T>
    double getReducedCost(const T& /*_v*/) const {
        if constexpr (std::is_same<T, SFC::Partial::Demand>::value) {
            return 0.0;
        } else {
            return 0.0;
        }
    }
};

SCENARIO("", "[]") {
    // const SFC::Partial::Chain c {};
    // std::vector<SFC::Partial::Demand> demands;
    // demands.emplace_back(
    // 	0, 0, 1, 1,
    // 	SFC::Partial::Chain({0, 1, 2, 3, 4}, {{}, {0}, {}, {2}, {0, 1, 2, 3}})
    // );
    // demands.emplace_back(
    // 	1, 0, 1, 1,
    // 	SFC::Partial::Chain({2, 4, 3}, {{}, {}, {}})
    // );

    // // SFC::Partial::Demand demand3 {2, 1, 0, 1,
    // // 	{{0, 2}, {{0, 2}, {0, 2}}}
    // // };

    // const std::vector<double> funcCharge {1, 1, 1, 1, 1, 1};
    // const auto network = SFC::loadNetwork("./test/test1_topo.txt");
    // std::cout << "Network has " << num_vertices(network) << " nodes and "
    // 	<< num_edges(network) << " edges.\n";
    // const auto nodeCapas = SFC::loadNodeCapa("./test/test1_nodeCapa.txt");

    // const SFC::Partial::Instance inst(network, nodeCapas, demands,
    // funcCharge);
    std::ifstream in("./test/test1.inst");
    const SFC::Partial::Instance inst = SFC::Partial::loadInstance(in);

    SFC::Partial::RCShortestPathPerDemand pricing(&inst);
    for (int i = 0; i < inst.demands.size(); ++i) {
        pricing.setPricingID(inst.demands[i]);
        pricing.solve();
        const auto sPath = pricing.getColumn();
        REQUIRE(inst.demands[i].s == sPath.nPath.front());
        REQUIRE(inst.demands[i].t == sPath.nPath.back());
        std::cout << "Service Path " << i << ": " << sPath << '\n';
    }
}
