#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this
                          // in one cpp file
#include "catch.hpp"
#include <iostream>
#include <utility.hpp>
#include <vector>

#include "Partial.hpp"

SCENARIO("", "[]") {
    std::vector<std::pair<std::vector<SFC::function_descriptor>,
        std::vector<
            std::pair<SFC::function_descriptor, SFC::function_descriptor>>>>
        sfcs{{{0, 1, 2, 3, 4}, {{0, 1}, {1, 2}, {3, 4}, {4, 2}}},
            {{0, 1, 2, 3, 4}, {}},
            {{0, 1, 2, 3, 4, 5, 6},
                {{0, 1}, {1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}}}};
    for (const auto& [functions, sfc] : sfcs) {
        const auto chain =
            SFC::Partial::Chain(functions, sfc.begin(), sfc.end());
        const auto reqOrder = chain.getExplicitOrder();
        REQUIRE(reqOrder.size() == sfc.size());
        for (const auto& order : sfc) {
            REQUIRE(std::find(reqOrder.begin(), reqOrder.end(), order)
                    != reqOrder.end());
        }
    }
}
