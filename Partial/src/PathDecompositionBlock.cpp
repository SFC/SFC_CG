#include <boost/multi_array/base.hpp>
#include <ilconcert/iloexpression.h>
#include <ilconcert/ilolinear.h>
#include <ilconcert/ilosys.h>
#include <iostream>
#include <numeric>

#include "Decomposition.hpp"
#include "Partial.hpp"
#include "PathDecompositionBlock.hpp"

using ranges = boost::multi_array_types::index_range;

namespace SFC::Partial::PathDecompositionBlock {

Master::Master(const Instance& _inst)
    : Base(DualValues(_inst))
    , m_inst(&_inst)
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_oneBlockCons(IloAdd(m_model, IloRange(m_env, 1.0, 1.0)))
    , m_linkCapasCons(getLinkCapacityConstraints(*m_inst, m_model))
    , m_nodeCapasCons(getNodeCapacityConstraints(*m_inst, m_model))
    , m_dummyPath([&]() {
        IloNumVar retval;
        const double totalDemand =
            std::accumulate(m_inst->demands.begin(), m_inst->demands.end(), 0.0,
                [&](auto&& _acc, auto&& _demand) { return _acc + _demand.d; });
        retval = m_oneBlockCons(1.0)
                 + m_obj(10 * totalDemand * num_edges(m_inst->network));
        m_model.add(retval);
        m_integerModel.add(retval == 0);
        setIloName(retval, "dummyPath");
        return retval;
    }())
    , m_linkCharge(num_edges(m_inst->network))
    , m_nodeCharge(num_vertices(m_inst->network)) {
    m_solver.setOut(m_solver.getEnv().getNullStream());
    m_solver.setWarning(m_solver.getEnv().getNullStream());
}

double Master::getReducedCost_impl(
    const std::vector<ServicePath>& _col, const DualValues& _dualValues) const {
    double retval = _dualValues.getReducedCost(DualValues::OneBlockTag{});
    for (const auto& [nPath, locations, function_indexes, demandID] : _col) {
        for (auto iteU = nPath.begin(), iteV = std::next(iteU);
             iteV != nPath.end(); ++iteU, ++iteV) {
            retval += _dualValues.getReducedCost(
                IntraLayerLink{demandID, {*iteU, *iteV},
                    -1}); /// layer # is not use for the reduced cost
        }

        for (int j = 0; j < locations.size(); ++j) {
            retval += _dualValues.getReducedCost(
                CrossLayerLink{demandID, locations[j], j, function_indexes[j]});
        }
    }
    return retval;
}

IloNumColumn Master::getColumn_impl(const std::vector<ServicePath>& _col) {
    IloNumColumn col = m_oneBlockCons(1.0);
    double objCoeff = 0.0;
    std::fill(m_linkCharge.begin(), m_linkCharge.end(), 0.0);
    std::fill(m_nodeCharge.begin(), m_nodeCharge.end(), 0.0);
    for (const auto& [nPath, locations, function_indexes, demandID] : _col) {
        const auto& [id, s, t, d, chain] = m_inst->demands[demandID];
        objCoeff += d * (nPath.size() - 1);

        for (auto iteU = nPath.begin(), iteV = std::next(iteU);
             iteV != nPath.end(); ++iteU, ++iteV) {
            m_linkCharge
                [m_inst->network[edge(*iteU, *iteV, m_inst->network).first]
                        .id] += d;
        }

        for (int j = 0; j < locations.size(); ++j) {
            const auto f = chain.getFunction(function_indexes[j]);
            m_nodeCharge[locations[j]] += m_inst->getNbCores(f, d);
        }
    }

    col += std::inner_product(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_linkCharge.begin(), IloNumColumn(m_env), reduceColumn<IloNumColumn>,
        ::getColumn<IloRange>);
    col += std::inner_product(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_nodeCharge.begin(), IloNumColumn(m_env), reduceColumn<IloNumColumn>,
        ::getColumn<IloRange>);
    col += m_obj(objCoeff);
    return col;
}

void Master::getDuals_impl() {
    const auto get = [&](auto&& _cons) { return getDual(m_solver, _cons); };
    std::transform(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_duals.m_linkCapasDuals.begin(), get);
    std::transform(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_duals.m_nodeCapasDuals.begin(), get);
    m_duals.m_oneBlockDual = get(m_oneBlockCons);
}

Solution Master::getSolution() const {
    return {*m_inst, getIntegerObjValue(), getRelaxationObjValue(),
        getServicePaths()};
}

void Master::removeDummyIfPossible() {
    if (m_dummyActive) {
        const double dummySum = m_solver.getValue(m_dummyPath);
        if (epsilon_equal<double>()(dummySum, 0.0)) {
            m_dummyActive = false;
        }
    }
}

const DualValues& Master::getDualValues_impl() const { return m_duals; }

std::vector<ServicePath> Master::getServicePaths() const {
    std::vector<ServicePath> sPaths(m_inst->demands.size());
    for (const auto& [var, block] :
        getColumnStorage<std::vector<ServicePath>>()) {
        if (epsilon_equal<double>()(m_solver.getValue(var), IloTrue)) {
            return block;
        }
    }
    return {};
}

DualValues::DualValues(const Instance& _inst)
    : inst(&_inst)
    , m_oneBlockDual(0.0)
    , m_linkCapasDuals(num_edges(inst->network), 0.0)
    , m_nodeCapasDuals(num_vertices(inst->network), 0.0) {}

IloNum DualValues::getReducedCost(const CrossLayerLink& _link) const {
    const auto demand = inst->demands[_link.demandID];
    const auto d = demand.d;
    const auto f = demand.functions.getFunction(_link.function_index);
    const auto rc = inst->getNbCores(f, d) * -m_nodeCapasDuals[_link.u];
    assert(epsilon_less<double>()(0.0, rc) || epsilon_equal<double>()(0.0, rc));
    return rc;
}

IloNum DualValues::getReducedCost(const IntraLayerLink& _link) const {
    const auto ed =
        edge(_link.edge.first, _link.edge.second, inst->network).first;
    const auto rc = inst->demands[_link.demandID].d
                    * (1 - m_linkCapasDuals[inst->network[ed].id]);
    assert(epsilon_less<double>()(0.0, rc) || epsilon_equal<double>()(0.0, rc));
    return rc;
}

IloNum DualValues::getReducedCost(DualValues::OneBlockTag /*_tag*/) const {
    const auto rc = -m_oneBlockDual;
    assert(epsilon_less<double>()(rc, 0.0) || epsilon_equal<double>()(0.0, rc));
    return rc;
}

double DualValues::getDualSumRHS() const {
    // Node capa duals
    double retval = std::inner_product(m_nodeCapasDuals.begin(),
        m_nodeCapasDuals.end(), vertices(inst->network).first, 0.0,
        std::plus<>(), [&](auto&& dual, auto&& u) {
            return dual * inst->network[u].capacity;
        });

    // Link capa duals
    for (const auto ed : boost::make_iterator_range(edges(inst->network))) {
        retval +=
            m_linkCapasDuals[inst->network[ed].id] * inst->network[ed].capacity;
    }
    // one path duals
    retval += m_oneBlockDual;

    return retval;
}

void barycenter(const DualValues& _nDuals, const DualValues& _bDuals,
    DualValues& _sDuals, double _alpha) {
    const auto update = [_alpha](const double _a, const double _b) -> double {
        return _alpha * _a + ((1 - _alpha) * _b);
    };
    std::transform(_nDuals.m_linkCapasDuals.begin(),
        _nDuals.m_linkCapasDuals.end(), _bDuals.m_linkCapasDuals.begin(),
        _sDuals.m_linkCapasDuals.begin(), update);

    std::transform(_nDuals.m_nodeCapasDuals.begin(),
        _nDuals.m_nodeCapasDuals.end(), _bDuals.m_nodeCapasDuals.begin(),
        _sDuals.m_nodeCapasDuals.begin(), update);

    _sDuals.m_oneBlockDual =
        update(_nDuals.m_oneBlockDual, _bDuals.m_oneBlockDual);
}

std::ostream& operator<<(std::ostream& _out, const DualValues& _dualValues) {
    return _out << "{lc: "
                << std::accumulate(_dualValues.m_linkCapasDuals.begin(),
                       _dualValues.m_linkCapasDuals.end(), 0.0)
                << ", nc: "
                << std::accumulate(_dualValues.m_nodeCapasDuals.begin(),
                       _dualValues.m_nodeCapasDuals.end(), 0.0)
                << ", ob: " << _dualValues.m_oneBlockDual << '}';
}

} // namespace SFC::Partial::PathDecompositionBlock
