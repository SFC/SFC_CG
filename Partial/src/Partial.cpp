#include "Partial.hpp"

#include <limits>
#include <vector>

#include <cplex_utility.hpp>
#include <utility.hpp>

#include "boost/graph/graphviz.hpp"
#include "boost/graph/r_c_shortest_paths.hpp"
#include "boost/graph/topological_sort.hpp"

#include <boost/log/trivial.hpp>

#include <boost/tokenizer.hpp>

namespace SFC::Partial {

Chain::Chain(std::vector<function_descriptor> _functions,
    std::vector<std::vector<function_descriptor>> _order)
    : m_functions(std::move(_functions))
    , m_precedingFunctions(std::move(_order)) {}

function_descriptor Chain::getFunction(
    const int /*_layer*/, const int _index) const {
    return m_functions[_index];
}

function_descriptor Chain::getFunction(const int _index) const {
    return m_functions[_index];
}

ServicePath::ServicePath(Path _nPath, std::vector<::Node> _locations,
    std::vector<function_descriptor> _function_indexes,
    demand_descriptor _demand)
    : nPath(_nPath)
    , locations(_locations)
    , function_indexes(_function_indexes)
    , demand(_demand) {}

std::ostream& operator<<(std::ostream& _out, const ServicePath& _sPath) {
    return _out << '{' << "nPath: " << _sPath.nPath << ", "
                << "locations: " << _sPath.locations << ", "
                << "function_indexes: " << _sPath.function_indexes << ", "
                << "demand: " << _sPath.demand << '}';
}

bool isValid(const ServicePath& _sPath, const Demand& _demand) {
    const auto [id, s, t, d, functions] = _demand;
    const auto [nPath, locations, function_indexes, demand] = _sPath;
    if (std::make_tuple(demand, nPath.front(), nPath.back())
        != std::make_tuple(id, s, t)) {
        return false;
    }
    for (int k = 0; k < function_indexes.size(); ++k) {
        const auto& precedingIndexes =
            functions.getPrecedingFunctions(function_indexes[k]);
        if (std::any_of(precedingIndexes.begin(), precedingIndexes.end(),
                [&](auto&& _index) {
                    return std::find(_sPath.function_indexes.begin(),
                               _sPath.function_indexes.end(), _index)
                           == _sPath.function_indexes.end();
                })) {
            return false;
        }
    }
    return true;
}

bool operator==(const Link& _lhs, const Link& _rhs) {
    return std::make_tuple(
               _lhs.index, _lhs.weight, _lhs.layer, _lhs.function_index)
           == std::make_tuple(
                  _rhs.index, _rhs.weight, _rhs.layer, _rhs.function_index);
}

LayeredGraph getLayeredGraph(const Instance& _inst) {
    const auto n = num_vertices(_inst.network);
    const auto nbLayers = _inst.maxChainSize + 1;

    LayeredGraph layeredGraph(n * nbLayers);
    // Nodes
    for (int u = 0; u < n; ++u) {
        for (int j = 0; j < nbLayers; ++j) {
            layeredGraph[n * j + u] = {n * j + u, u, j};
        }
    }

    // Cross-layer links
    int edgeIdx = 0;
    for (int u = 0; u < num_vertices(_inst.network); ++u) {
        if (_inst.network[u].isNFV) {
            for (int j = 0; j < nbLayers - 1; ++j) {
                for (int k = 0; k < _inst.maxChainSize; ++k) {
                    add_edge(n * j + u, n * (j + 1) + u,
                        {edgeIdx++, 0.0, j, k, {u, u}}, layeredGraph);
                }
            }
        }
    }

    // Intra-layer links
    for (int j = 0; j < nbLayers; ++j) {
        for (auto [ite, end] = edges(_inst.network); ite != end; ++ite) {
            const auto [u, v] = incident(*ite, _inst.network);
            add_edge(n * j + u, n * j + v, {edgeIdx++, 0.0, j, -1, {u, v}},
                layeredGraph);
        }
    }
    return layeredGraph;
}

Instance::Instance(DiGraph _network, std::vector<int> _nodeCapa,
    std::vector<Demand> _demands, std::vector<double> _funcCharge)
    : network([&]() {
        Network retval(num_vertices(_network));
        for (const auto ed : boost::make_iterator_range(edges(_network))) {
            const auto& [u, v] = incident(ed, _network);
            add_edge(u, v,
                {get(boost::edge_index, _network, ed),
                    get(boost::edge_weight, _network, ed)},
                retval);
        }
        for (const auto u : boost::make_iterator_range(vertices(_network))) {
            retval[u] = {u, _nodeCapa[u]};
        }
        return retval;
    }())
    , funcCharge(std::move(_funcCharge))
    , demands(std::move(_demands))
    , maxChainSize(getMaximumChainSize(demands)) {}

double Instance::getNbCores(function_descriptor _f, double _bandwidth) const {
    return ceil(_bandwidth / funcCharge[_f]);
}

double Instance::getTotalNbCores() const {
    double totalNbCores = 0.0;
    for (const auto& [id, s, t, d, functions] : demands) {
        for (int j = 0; j < functions.size(); ++j) {
            totalNbCores += getNbCores(functions.getFunction(j), d);
        }
    }
    return totalNbCores;
}

Instance::Instance(Network _network, std::vector<Demand> _demands,
    std::vector<double> _funcCharge)
    : network(std::move(_network))
    , funcCharge(std::move(_funcCharge))
    , demands(std::move(_demands))
    , maxChainSize(getMaximumChainSize(demands)) {}

SFC::Partial::Instance loadInstance(std::istream& _in) {
    // Read node capacity (capacity)+
    if (!_in) {
        throw std::runtime_error("Invalid istream");
    }
    std::string name;
    _in >> name;
    int nbVertices;
    if (!(_in >> nbVertices)) {
        throw std::runtime_error("Error reading nbVertices");
    }
    Network network(name);
    for (int i = 0; i < nbVertices; ++i) {
        int nodeCapa;
        if (!(_in >> nodeCapa)) {
            throw std::runtime_error("Error reading node capacity");
        }
        network[add_vertex(network)] = {i, nodeCapa};
    }

    // Read edges (u, v, capacity)+
    int nbEdges;
    if (!(_in >> nbEdges)) {
        throw std::runtime_error("Error reading nbVertices");
    }
    for (int e = 0; e < nbEdges; ++e) {
        int u, v;
        double capacity;
        if (!(_in >> u >> v >> capacity)) {
            throw std::runtime_error("Error reading edges");
        }
        add_edge(u, v, SFC::Link{e, capacity}, network);
    }

    // Read functions (charge)+
    int nbFunctions;
    if (!(_in >> nbFunctions)) {
        throw std::runtime_error("Error reading nbFunctions");
    }
    std::vector<double> funcCharge(nbFunctions);
    for (int f = 0; f < nbFunctions; ++f) {
        double charge;
        if (!(_in >> charge)) {
            throw std::runtime_error("Error reading function charge");
        }
        funcCharge[f] = charge;
    }

    // Read requests (s, t, d, (f_i, f_j)+)+
    int nbRequests;
    if (!(_in >> nbRequests)) {
        throw std::runtime_error("Error reading nbRequests");
    }
    std::vector<SFC::Partial::Demand> demands;
    for (int i = 0; i < nbRequests; ++i) {
        int s, t;
        double d;
        int req_nbFunctions;
        if (!(_in >> s >> t >> d >> req_nbFunctions)) {
            throw std::runtime_error("Error reading requests");
        }
        std::vector<function_descriptor> functions(req_nbFunctions);
        for (int j = 0; j < req_nbFunctions; ++j) {
            int f;
            if (!(_in >> f)) {
                throw std::runtime_error("Error reading requests");
            }
            assert(f < funcCharge.size());
            functions[j] = f;
        }

        int nbOrderEdge;
        if (!(_in >> nbOrderEdge)) {
            throw std::runtime_error("Error reading requests");
        }
        std::vector<std::pair<function_descriptor, function_descriptor>> edges;
        for (int e = 0; e < nbOrderEdge; ++e) {
            function_descriptor f1, f2;
            if (!(_in >> f1 >> f2)) {
                throw std::runtime_error("Error reading chain");
            }
            assert(f1 < req_nbFunctions);
            assert(f2 < req_nbFunctions);
            edges.emplace_back(f1, f2);
        }
        demands.emplace_back(i, s, t, d,
            SFC::Partial::Chain(functions, edges.begin(), edges.end()));
    }
    return {network, demands, funcCharge};
}

bool contains(const ServicePath& _sPath, const IntraLayerLink& _link) {
    assert(_sPath.demand == _link.demandID);
    // std::cout << _sPath << " contains " << _link << "? ";
    int j = 0;
    auto iteU = _sPath.nPath.begin(), iteV = std::next(iteU);
    while (j < _sPath.locations.size() || iteV != _sPath.nPath.end()) {
        // Check if right edge
        if (j < _sPath.locations.size() && _sPath.locations[j] == *iteU) {
            ++j;
        } else {
            if (_link.edge.first == *iteU && _link.edge.second == *iteV
                && _link.j == j) {
                // std::cout << "yes\n";
                return true;
            } else {
                ++iteU;
                ++iteV;
            }
        }
    }
    // std::cout << "no\n";
    return false;
}

bool contains(const ServicePath& _sPath, const CrossLayerLink& _link) {
    assert(_sPath.demand == _link.demandID);
    int j = 0;
    auto iteU = _sPath.nPath.begin(), iteV = std::next(iteU);
    while (j < _sPath.locations.size() || iteV != _sPath.nPath.end()) {
        // Check if right edge
        if (_link.function_index == _sPath.function_indexes[j]
            && *iteU == _link.u) {
            return true;
        } else { // Otherwise, move in the s-path
            if (_sPath.locations[j] == *iteU) {
                ++j;
            } else {
                ++iteU;
                ++iteV;
            }
        }
    }
    return false;
}

std::ostream& operator<<(std::ostream& _out, const Node& _n) {
    return _out << _n.original << "(" << _n.layer << ")";
}

std::ostream& operator<<(std::ostream& _out, const Link& _l) {
    _out << "{weight: " << _l.weight;
    if (_l.isIntraLayer()) {
        _out << ", layer: " << _l.layer;
    } else {
        _out << ", function_index: " << _l.function_index;
    }
    return _out << '}';
}

bool operator<(const Chain& _lhs, const Chain& _rhs) noexcept {
    return _lhs.m_functions < _rhs.m_functions
           && _lhs.m_precedingFunctions < _rhs.m_precedingFunctions;
}

std::ostream& operator<<(std::ostream& _out, const Chain& _rhs) noexcept {
    return _out << "{ functions: " << _rhs.m_functions
                << ", precedence: " << _rhs.m_precedingFunctions << '}';
}

std::vector<std::pair<int, int>> Chain::getExplicitOrder() const noexcept {
    // build all order
    std::vector<std::pair<int, int>> orders;
    for (int j = 0; j < m_functions.size(); ++j) {
        for (const auto i : m_precedingFunctions[j]) {
            orders.emplace_back(i, j);
        }
    }

    // Return an implicit order if it exists
    // const auto getImplicitOrder =
    //     [&](int _i, int _j, int _k) -> std::optional<std::pair<int, int>> {
    //     const std::pair<int, int> order1 = {_i, _j}, order2 = {_i, _k},
    //                               order3 = {_j, _k};
    //     const auto ite1 = std::find(orders.begin(), orders.end(), order1);
    //     const auto ite2 = std::find(orders.begin(), orders.end(), order2);
    //     const auto ite3 = std::find(orders.begin(), orders.end(), order3);
    //     if (ite1 != orders.end() && ite2 != orders.end()
    //         && ite3 != orders.end()) {
    //         return order3;
    //     } else {
    //         return std::nullopt;
    //     }
    // };

    // Test all triples to see if an implicit order exists
    /*for (int i = 0; i < m_functions.size(); ++i) {
        for (int j = 0; j < m_functions.size(); ++j) {
            for (int k = 0; k < m_functions.size(); ++k) {
                if (i != j && j != k && i != k) {
                    if (const auto optPair = getImplicitOrder(i, j, k);
                        optPair.has_value()) {
                        orders.erase(std::remove(orders.begin(), orders.end(),
                                         optPair.value()),
                            orders.end());
                    }
                }
            }
        }
    }*/
    return orders;
}

bool operator==(const CrossLayerLink& _lhs, const CrossLayerLink& _rhs) {
    return std::make_tuple(_lhs.demandID, _lhs.u, _lhs.j, _lhs.function_index)
           == std::make_tuple(
                  _rhs.demandID, _rhs.u, _rhs.j, _rhs.function_index);
}

bool operator==(const IntraLayerLink& _lhs, const IntraLayerLink& _rhs) {
    return std::make_tuple(_lhs.demandID, _lhs.edge, _lhs.j)
           == std::make_tuple(_rhs.demandID, _rhs.edge, _rhs.j);
}

bool operator<(const CrossLayerLink& _lhs, const CrossLayerLink& _rhs) {
    return std::make_tuple(_lhs.demandID, _lhs.u, _lhs.j, _lhs.function_index)
           < std::make_tuple(
                 _rhs.demandID, _rhs.u, _rhs.j, _rhs.function_index);
}

bool operator<(const IntraLayerLink& _lhs, const IntraLayerLink& _rhs) {
    return std::make_tuple(_lhs.demandID, _lhs.edge, _lhs.j)
           < std::make_tuple(_rhs.demandID, _rhs.edge, _rhs.j);
}

bool operator==(const Link& _link, const IntraLayerLink& _illink) {
    return _link.isIntraLayer()
           && std::make_tuple(_link.layer, _link.original)
                  == std::make_tuple(_illink.j, _illink.edge);
}

bool operator==(const IntraLayerLink& _illink, const Link& _link) {
    return _link == _illink;
}

bool operator==(const Link& _link, const CrossLayerLink& _clink) {
    return !_link.isIntraLayer()
           && std::make_tuple(_link.original.first, _link.function_index)
                  == std::make_tuple(_clink.u, _clink.function_index);
}

bool operator==(const CrossLayerLink& _clink, const Link& _link) {
    return _link == _clink;
}

bool operator==(const ServicePath& _lhs, const ServicePath& _rhs) {
    return std::make_tuple(
               _lhs.demand, _lhs.nPath, _lhs.locations, _lhs.function_indexes)
           == std::make_tuple(_rhs.demand, _rhs.nPath, _rhs.locations,
                  _rhs.function_indexes);
}

std::ostream& operator<<(std::ostream& _out, const Demand& _d) {
    return _out << "{id=" << _d.id << ", s=" << _d.s << ", t=" << _d.t
                << ", d=" << _d.d << ", functions=" << _d.functions << "}";
}

std::ostream& operator<<(std::ostream& _out, const IntraLayerLink& _link) {
    return _out << "{edge: (" << _link.edge.first << ", " << _link.edge.second
                << "), layer " << _link.j << ", demand: " << _link.demandID
                << "}";
}

bool operator==(const FunctionLocation& _lhs, const FunctionLocation& _rhs) {
    return std::make_tuple(_lhs.function, _lhs.node)
           == std::make_tuple(_rhs.function, _rhs.node);
}

bool operator<(const FunctionLocation& _lhs, const FunctionLocation& _rhs) {
    return std::make_tuple(_lhs.function, _lhs.node)
           < std::make_tuple(_rhs.function, _rhs.node);
}

bool contains(const ServicePath& _sPath, const Demand& _demand,
    const FixedLocationFunctions& _funcLoc) {
    assert(_demand.id == _sPath.demand);
    int j = 0;
    auto iteU = _sPath.nPath.begin(), iteV = std::next(iteU);
    while (j < _sPath.locations.size() || iteV != _sPath.nPath.end()) {
        // Check if right edge
        if (j < _sPath.locations.size() && _sPath.locations[j] == *iteU) {
            if (_funcLoc.vars.find(
                    {_demand.functions.getFunction(_sPath.function_indexes[j]),
                        _sPath.locations[j]})
                != _funcLoc.vars.end()) {
                return true;
            }
            ++j;
        } else {
            ++iteU;
            ++iteV;
        }
    }
    return false;
}

} // namespace SFC::Partial
