#include "RCShortestPathPerDemand.hpp"

#include <cplex_utility.hpp>

namespace SFC::Partial {
RCShortestPathPerDemand::RCShortestPathPerDemand(const Instance& _inst)
    : m_inst(&_inst)
    , n(num_vertices(m_inst->network))
    , m(num_edges(m_inst->network))
    , nbLayers(m_inst->maxChainSize + 1)
    , m_layeredGraph(getLayeredGraph(*m_inst))
    , m_containers(0) {}

void RCShortestPathPerDemand::setPricingID(const Demand& _demand) {
    m_demand = &_demand;
    m_demandFilter.m_demand = m_demand;
}

void RCShortestPathPerDemand::setPricingID(demand_descriptor _demandID) {
    m_demand = &m_inst->demands[_demandID];
    m_demandFilter.m_demand = m_demand;
}

bool RCShortestPathPerDemand::solve() {
    m_paths.clear();
    const int dest = n * (m_demand->functions.size()) + m_demand->t;
    boost::filtered_graph<LayeredGraph, DemandFilter, DemandFilter>  g(m_layeredGraph, m_demandFilter, m_demandFilter);
    assert(std::find(vertices(g).first, vertices(g).second, dest)
           != vertices(g).second);

    r_c_shortest_paths(g, get(&Node::index, g), get(&Link::index, g),
        m_demand->s, dest, m_paths, m_containers,
        SFC::Partial::ResourceContainer{m_demand->functions.size()},
        [&](auto&&... args) { // extension predicate
            return extension(*m_demand, args...);
        },
        [&](auto&&... args) { // dominance predicate
            return dominance(args...);
        });
    m_objValue += m_containers.getWeight();
    assert(
        m_containers.getFunctionIndexes().size() == m_demand->functions.size());
    if (m_paths.empty()) {
        return false;
    }
    return true;
}

double RCShortestPathPerDemand::getObjValue() const { return m_objValue; }

ServicePath RCShortestPathPerDemand::getColumn() const {
    boost::filtered_graph g(m_layeredGraph, m_demandFilter, m_demandFilter);
    return SFC::Partial::getServicePath(
        *m_demand, m_paths, m_containers.getFunctionIndexes(), g);
}

bool dominance(const ResourceContainer& _rc1, const ResourceContainer& _rc2) {
    return _rc1.usedFunction == _rc2.usedFunction && _rc1 < _rc2;
    /*&& (epsilon_less<double>()(_rc1.totalWeight, _rc2.totalWeight)
       || (epsilon_equal<double>()(
                                _rc1.totalWeight, _rc2.totalWeight)
                  && _rc1.nbHops < _rc2.nbHops));*/
}

bool operator<(const ResourceContainer& _lhs, const ResourceContainer& _rhs) {
    return epsilon_less<double>()(_lhs.totalWeight, _rhs.totalWeight)
           || (epsilon_equal<double>()(_lhs.totalWeight, _rhs.totalWeight)
                  && _lhs.nbHops < _rhs.nbHops);
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           PartialShortestPath_LP                          //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

PartialShortestPath_LP::PartialShortestPath_LP(const Instance& _inst)
    : m_inst(&_inst)
    , m_phi([&]() {
        boost::multi_array<IloNumVar, 2> phi(boost::extents[num_edges(
            m_inst->network)][m_inst->maxChainSize + 1]);
        std::generate(phi.data(), phi.data() + phi.num_elements(),
            [&]() { return IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0)); });
        return phi;
    }())
    , m_alpha([&]() {
        boost::multi_array<IloNumVar, 3> alpha(boost::extents[num_vertices(
            m_inst->network)][m_inst->maxChainSize][m_inst->maxChainSize+1]);
        std::generate(alpha.data(), alpha.data() + alpha.num_elements(),
            [&]() { return IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0)); });
        return alpha;
    }())
    , m_flowConservationConstraints([&]() {
        boost::multi_array<IloRange, 2> flowConservationConstraints(
            boost::extents[num_vertices(m_inst->network)]
                          [m_inst->maxChainSize+1]);
        IloExpr expr(m_env);
        for (const auto u :
            boost::make_iterator_range(vertices(m_inst->network))) {
            for (int j = 0; j < m_inst->maxChainSize + 1; ++j) {
                expr.clear();
                for (const auto ed :
                    boost::make_iterator_range(out_edges(u, m_inst->network))) {
                    expr += m_phi[m_inst->network[ed].id][j];
                }
                for (const auto ed :
                    boost::make_iterator_range(in_edges(u, m_inst->network))) {
                    expr -= m_phi[m_inst->network[ed].id][j];
                }

                if (j > 0) {
                    for (int i = 0; i < m_inst->maxChainSize; ++i) {
                        expr -= m_alpha[u][i][j];
                    }
                }

                if (j < m_inst->maxChainSize) {
                    for (int i = 0; i < m_inst->maxChainSize; ++i) {
                        expr += m_alpha[u][i][j];
                    }
                }
                flowConservationConstraints[u][j] =
                    IloAdd(m_model, IloRange(m_env, 0.0, expr, 0.0));
            }
        }
        expr.end();
        return flowConservationConstraints;
    }())
    , m_precedingConstraints([&]() {
        boost::multi_array<IloRange, 2> precedingConstraints(
            boost::extents[m_inst->maxChainSize][m_inst->maxChainSize]);
        IloExpr expr(m_env);
        for (int i1 = 0; i1 < m_inst->maxChainSize; ++i1) {
            for (int i2 = 0; i2 < m_inst->maxChainSize; ++i2) {
                expr.clear();
                for (const auto u :
                    boost::make_iterator_range(vertices(m_inst->network))) {
                    for (int j = 0; j < m_inst->maxChainSize; ++j) {
                        expr += i1 * m_alpha[u][i1][j] - i2 * m_alpha[u][i2][j];
                    }
                }
                /*precedingConstraints[i1][i2] =
                    IloRange(m_env, -IloInfinity, expr, 0.0);*/
            }
        }
        expr.end();
        return precedingConstraints;
    }())
    , m_uniqueFunctionsConstraints([&]() {
        std::vector<IloRange> uniqueFunctionsConstraints(m_inst->maxChainSize);
        IloExpr expr(m_env);
        for (int i = 0; i < m_inst->maxChainSize; ++i) {
            expr.clear();
            for (const auto u :
                boost::make_iterator_range(vertices(m_inst->network))) {
                for (int j = 0; j < m_inst->maxChainSize; ++j) {
                    expr += m_alpha[u][i][j];
                }
            }
            /*uniqueFunctionsConstraints[i] =
                IloAdd(m_model, IloRange(m_env, -IloInfinity, expr, 1.0));*/
        }
        expr.end();
        return uniqueFunctionsConstraints;
    }()) {}

PartialShortestPath_LP::PartialShortestPath_LP(
    const PartialShortestPath_LP& _other)
    : PartialShortestPath_LP(*_other.m_inst) {}

PartialShortestPath_LP& PartialShortestPath_LP::operator=(
    const PartialShortestPath_LP& _other) {
    if (this != &_other) {
        *this = PartialShortestPath_LP(*_other.m_inst);
    }
    return *this;
}

bool PartialShortestPath_LP::solve() { return m_solver.solve() == IloTrue; }

double PartialShortestPath_LP::getObjValue() const {
    return m_solver.getObjValue();
}

ServicePath PartialShortestPath_LP::getColumn() const {
    ServicePath retval;
    retval.nPath.emplace_back(m_demand->s);
    int u = m_demand->s;
    int j = 0;
    while (j != m_inst->maxChainSize + 1 && u != m_demand->t) {
        // First check flow between layers
        bool movedLayers = false;
        for (int i = 0; i < m_inst->maxChainSize; ++i) {
            if (epsilon_equal<double>()(
                    m_solver.getValue(m_alpha[u][i][j]), IloTrue)) {
                retval.locations.emplace_back(u);
                retval.function_indexes.emplace_back(i);
                ++j;
                movedLayers = true;
            }
        }
        if (movedLayers) {
            continue;
        }

        // Otherwise, we check for intra layer links
        for (const auto ed :
            boost::make_iterator_range(out_edges(u, m_inst->network))) {
            if (epsilon_equal<double>()(
                    m_solver.getValue(m_phi[m_inst->network[ed].id][j]),
                    IloTrue)) {
                u = target(ed, m_inst->network);
                retval.nPath.emplace_back(u);
                break;
            }
        }
    }
    return retval;
}

PartialShortestPath_LP::PartialShortestPath_LP(PartialShortestPath_LP&& _other)
    : m_inst(_other.m_inst)
    , m_demand(_other.m_demand)
    , m_env(_other.m_env)
    , m_model(_other.m_model)
    , m_obj(_other.m_obj)
    , m_phi(_other.m_phi)
    , m_alpha(_other.m_alpha)
    , m_flowConservationConstraints(_other.m_flowConservationConstraints)
    , m_precedingConstraints(_other.m_precedingConstraints)
    , m_uniqueFunctionsConstraints(_other.m_uniqueFunctionsConstraints)
    , m_solver(_other.m_solver) {
    _other.m_env = IloEnv();
}

PartialShortestPath_LP& PartialShortestPath_LP::operator=(
    PartialShortestPath_LP&& _other) {
    if (this != &_other) {
        m_inst = _other.m_inst;
        m_demand = _other.m_demand;
        m_env = _other.m_env;
        m_model = _other.m_model;
        m_obj = _other.m_obj;
        m_phi = _other.m_phi;
        m_alpha = _other.m_alpha;
        m_flowConservationConstraints = _other.m_flowConservationConstraints;
        m_precedingConstraints = _other.m_precedingConstraints;
        m_uniqueFunctionsConstraints = _other.m_uniqueFunctionsConstraints;
        m_solver = _other.m_solver;
        _other.m_env = IloEnv();
    }
    return *this;
}
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                            RCShortestPathBlock                            //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

RCShortestPathBlock::RCShortestPathBlock(const Instance& _inst)
    : m_inst(&_inst)
    , n(num_vertices(m_inst->network))
    , m(num_edges(m_inst->network))
    , nbLayers(m_inst->maxChainSize + 1)
    , m_layeredGraph(getLayeredGraph(*m_inst))
    , m_paths(m_inst->demands.size())
    , m_containers(m_inst->demands.size()) {}

std::vector<ServicePath> RCShortestPathBlock::getColumn() const {
    std::vector<ServicePath> retval(m_inst->demands.size());
    std::transform(m_inst->demands.begin(), m_inst->demands.end(),
        retval.begin(), [&](auto&& _demand) {
            return SFC::Partial::getServicePath(_demand, m_paths[_demand.id],
                m_containers[_demand.id].function_indexes, m_layeredGraph);
        });
    return retval;
}

bool RCShortestPathBlock::extension(const Demand& _demand,
    const LayeredGraph& g, ResourceContainer& new_cont,
    const ResourceContainer& old_cont,
    boost::graph_traits<LayeredGraph>::edge_descriptor ed) {
    // Intra layer links are straight forward: propagate and add the weight of
    // the link
    if (g[ed].isIntraLayer()) {
        new_cont = old_cont;
        new_cont.totalWeight += g[ed].weight;
        ++new_cont.nbHops;
        /*std::cout << "Extending with weight: " << new_cont.totalWeight
                  << ", function_indexes: " << new_cont.function_indexes << " on
           ("
                  << g[source(ed, g)] << ", " << g[target(ed, g)] << ") => "
                  << g[ed] << '\n';*/
        return true;
    } else {
        // Cross layer link are more complicated
        // First, we check that we are not above the ending layer
        const auto v = target(ed, g);
        if (g[v].layer > _demand.functions.size()) {
            return false;
        }

        // We also check that we're not above the maximum number of functions
        // Because we have a general layered graph for every request
        if (g[ed].function_index >= _demand.functions.size()) {
            return false;
        }

        // We then check that all the preceding functions have been done
        const auto& predFunctions =
            _demand.functions.getPrecedingFunctions(g[ed].function_index);
        if (std::any_of(predFunctions.begin(), predFunctions.end(),
                [&](auto&& _i) { return !old_cont.usedFunction[_i]; })) {
            /*std::cout << "Current function_indexes: " <<
               old_cont.function_indexes
                      << " is missing some of "
                      << m_demand->functions.getPrecedingFunctions(
                             g[ed].function_index)
                      << '\n';*/
            return false;
        };

        // Now we're free to propagate if the edge's function has not been done
        if (old_cont.usedFunction[g[ed].function_index]) {
            return false;
        }
        new_cont = old_cont;
        new_cont.usedFunction[g[ed].function_index] = true;
        new_cont.function_indexes.push_back(g[ed].function_index);
        new_cont.totalWeight += g[ed].weight;
        /*std::cout << "Extending with weight: " << new_cont.totalWeight
                  << ", function_indexes: " << new_cont.function_indexes << " on
           ("
                  << g[source(ed, g)] << ", " << g[target(ed, g)] << ") => "
                  << g[ed] << '\n';*/
        return true;
    }
}

bool RCShortestPathBlock::dominance(
    const ResourceContainer& _rc1, const ResourceContainer& _rc2) {
    return _rc1.usedFunction == _rc2.usedFunction
           && (epsilon_less<double>()(_rc1.totalWeight, _rc2.totalWeight)
                  || epsilon_equal<double>()(
                         _rc1.totalWeight, _rc2.totalWeight))
           && _rc1.nbHops <= _rc2.nbHops;
}

bool operator<(const RCShortestPathBlock::ResourceContainer& _lhs,
    const RCShortestPathBlock::ResourceContainer& _rhs) {
    return epsilon_less<double>()(_lhs.totalWeight, _rhs.totalWeight)
           || (epsilon_equal<double>()(_lhs.totalWeight, _rhs.totalWeight)
                  && _lhs.nbHops < _rhs.nbHops);
}

bool operator==(const RCShortestPathBlock::ResourceContainer& _lhs,
    const RCShortestPathBlock::ResourceContainer& _rhs) {
    return epsilon_equal<double>()(_lhs.totalWeight, _rhs.totalWeight);
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                      Fixed::RCShortestPathPerDemand                       //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

namespace Fixed {

RCShortestPathPerDemand::RCShortestPathPerDemand(
    const Instance& _inst, boost::multi_array<bool, 2> _locations)
    : SFC::Partial::RCShortestPathPerDemand(_inst)
    , m_locations(std::move(_locations)) {}

bool RCShortestPathPerDemand::solve() {
    m_paths.clear();
    const int dest = n * (m_demand->functions.size()) + m_demand->t;
    r_c_shortest_paths(m_layeredGraph, get(&Node::index, m_layeredGraph),
        get(&Link::index, m_layeredGraph), m_demand->s, dest, m_paths,
        m_containers, ResourceContainer{m_demand->functions.size()},
        [&](auto&&... args) { // extension predicate
            return extension(args...);
        },
        [&](auto&&... args) { // dominance predicate
            return dominance(args...);
        });
    if (m_paths.empty()) {
        return false;
    }
    m_objValue += m_containers.getWeight();
    return true;
}

bool RCShortestPathPerDemand::extension(const LayeredGraph& g,
    ResourceContainer& new_cont, const ResourceContainer& old_cont,
    boost::graph_traits<LayeredGraph>::edge_descriptor ed) {
    // Intra layer links are straight forward: propagate and add the weight of
    // the link
    if (g[ed].isIntraLayer()) {
        new_cont = ResourceContainer(old_cont, g[ed].weight);
        return true;
    } else {
        // Cross layer link are more complicated
        // First, we check that we are not above the ending layer
        const auto v = target(ed, g);
        if (g[v].layer > m_demand->functions.size()) {
            return false;
        }

        if (!m_locations[g[v].original][m_demand->functions.getFunction(
                g[ed].function_index)]) {
            return false;
        }

        // We check we can execute the function on v
        if (!old_cont.canExtendFunction(*m_demand, g[ed].function_index)) {
            return false;
        }
        new_cont =
            ResourceContainer(old_cont, g[ed].weight, g[ed].function_index);
        return true;
    }
}

RCShortestPathBlock::RCShortestPathBlock(
    const Instance& _inst, boost::multi_array<bool, 2> _locations)
    : SFC::Partial::RCShortestPathBlock(_inst)
    , m_locations(std::move(_locations)) {}

bool RCShortestPathBlock::extension(const Demand& _demand,
    const LayeredGraph& g, ResourceContainer& new_cont,
    const ResourceContainer& old_cont,
    boost::graph_traits<LayeredGraph>::edge_descriptor ed) {
    // Intra layer links are straight forward: propagate and add the weight of
    // the link
    if (g[ed].isIntraLayer()) {
        new_cont = old_cont;
        new_cont.totalWeight += g[ed].weight;
        ++new_cont.nbHops;
        /*std::cout << "Extending with weight: " << new_cont.totalWeight
                  << ", function_indexes: " << new_cont.function_indexes << " on
           ("
                  << g[source(ed, g)] << ", " << g[target(ed, g)] << ") => "
                  << g[ed] << '\n';*/
        return true;
    } else {
        // Cross layer link are more complicated
        // First, we check that we are not above the ending layer
        const auto v = target(ed, g);
        if (g[v].layer > _demand.functions.size()) {
            return false;
        }

        if (!m_locations[g[v].original]
                        [_demand.functions.getFunction(g[ed].function_index)]) {
            return false;
        }

        // We also check that we're not above the maximum number of functions
        // Because we have a general layered graph for every request
        if (g[ed].function_index >= _demand.functions.size()) {
            return false;
        }

        // We then check that all the preceding functions have been done
        const auto& predFunctions =
            _demand.functions.getPrecedingFunctions(g[ed].function_index);
        if (std::any_of(predFunctions.begin(), predFunctions.end(),
                [&](auto&& _i) { return !old_cont.usedFunction[_i]; })) {
            /*std::cout << "Current function_indexes: " <<
               old_cont.function_indexes
                      << " is missing some of "
                      << _demand.functions.getPrecedingFunctions(
                             g[ed].function_index)
                      << '\n';*/
            return false;
        };

        // Now we're free to propagate if the edge's function has not been done
        if (old_cont.usedFunction[g[ed].function_index]) {
            return false;
        }
        new_cont = old_cont;
        new_cont.usedFunction[g[ed].function_index] = true;
        new_cont.function_indexes.push_back(g[ed].function_index);
        new_cont.totalWeight += g[ed].weight;
        /*std::cout << "Extending with weight: " << new_cont.totalWeight
                  << ", function_indexes: " << new_cont.function_indexes << " on
           ("
                  << g[source(ed, g)] << ", " << g[target(ed, g)] << ") => "
                  << g[ed] << '\n';*/
        return true;
    }
}

} // namespace Fixed
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                     RCShortestPathPerDemandRestricted                     //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

RCShortestPathPerDemandRestricted::RCShortestPathPerDemandRestricted(
    const Instance& _inst, const FixedLinks& _fixedLinks)
    : m_inst(&_inst)
    , m_fixedLinks(&_fixedLinks)
    , n(num_vertices(m_inst->network))
    , m(num_edges(m_inst->network))
    , nbLayers(m_inst->maxChainSize + 1)
    , m_layeredGraph(getLayeredGraph(*m_inst)) {}

void RCShortestPathPerDemandRestricted::setPricingID(const Demand& _demand) {
    m_demand = &_demand;
}

void RCShortestPathPerDemandRestricted::setPricingID(
    demand_descriptor _demandID) {
    m_demand = &m_inst->demands[_demandID];
}

bool RCShortestPathPerDemandRestricted::solve() {
    const int dest = n * (m_demand->functions.size()) + m_demand->t;
    r_c_shortest_paths(m_layeredGraph, get(&Node::index, m_layeredGraph),
        get(&Link::index, m_layeredGraph), m_demand->s, dest, m_paths,
        m_containers,
        {boost::dynamic_bitset<>(m_demand->functions.size()), {}, 0.0, 0, {}},
        [&](auto&&... args) { // extension predicate
            return extension(args...);
        },
        [&](auto&&... args) { // dominance predicate
            return dominance(args...);
        });
    m_objValue += m_containers.totalWeight;
    return true;
}

double RCShortestPathPerDemandRestricted::getObjValue() const {
    return m_objValue;
}

ServicePath RCShortestPathPerDemandRestricted::getColumn() const {
    return SFC::Partial::getServicePath(
        *m_demand, m_paths, m_containers.function_indexes, m_layeredGraph);
}

bool RCShortestPathPerDemandRestricted::extension(const LayeredGraph& g,
    ResourceContainer& new_cont, const ResourceContainer& old_cont,
    boost::graph_traits<LayeredGraph>::edge_descriptor ed) {
    // Check if forbidden edge
    if (m_fixedLinks->isForbidden(m_layeredGraph[ed])) {
        return false;
    }

    const bool isMandotary = m_fixedLinks->isMandatory(m_layeredGraph[ed]);

    if (isMandotary
        && m_layeredGraph[target(ed, m_layeredGraph)].index
               != n * (m_demand->functions.size()) + m_demand->t
        && m_fixedLinks->containAllMandatory(
               old_cont.mandatory_links, m_layeredGraph)) {
        return false;
    }

    // Intra layer links are straight forward: propagate and add the weight of
    // the link
    if (g[ed].isIntraLayer()) {
        new_cont = old_cont;
        new_cont.totalWeight += g[ed].weight;
        ++new_cont.nbHops;
        if (isMandotary) {
            new_cont.mandatory_links.push_back(ed);
        }

        // Check all mandatory edges are done if target is destination
        if (m_layeredGraph[target(ed, m_layeredGraph)].index
                == n * (m_demand->functions.size()) + m_demand->t
            && !m_fixedLinks->containAllMandatory(
                   new_cont.mandatory_links, m_layeredGraph)) {
            return false;
        }

        /*std::cout << "Extending with weight: " << new_cont.totalWeight
                  << ", function_indexes: " << new_cont.function_indexes << " on
           ("
                  << g[source(ed, g)] << ", " << g[target(ed, g)] << ") => "
                  << g[ed] << '\n';*/
        return true;
    } else {
        // Cross layer link are more complicated
        // First, we check that we are not above the ending layer
        const auto v = target(ed, g);
        if (g[v].layer > m_demand->functions.size()) {
            return false;
        }

        // We also check that we're not above the maximum number of functions
        // Because we have a general layered graph for every request
        if (g[ed].function_index >= m_demand->functions.size()) {
            return false;
        }

        // We then check that all the preceding functions have been done
        const auto& predFunctions =
            m_demand->functions.getPrecedingFunctions(g[ed].function_index);
        if (std::any_of(predFunctions.begin(), predFunctions.end(),
                [&](auto&& _i) { return !old_cont.usedFunction[_i]; })) {
            /*std::cout << "Current function_indexes: " <<
               old_cont.function_indexes
                      << " is missing some of "
                      << m_demand->functions.getPrecedingFunctions(
                             g[ed].function_index)
                      << '\n';*/
            return false;
        };

        // Now we're free to propagate if the edge's function has not been done
        if (old_cont.usedFunction[g[ed].function_index]) {
            return false;
        }
        new_cont = old_cont;
        new_cont.usedFunction[g[ed].function_index] = true;
        new_cont.function_indexes.push_back(g[ed].function_index);
        new_cont.totalWeight += g[ed].weight;
        if (isMandotary) {
            new_cont.mandatory_links.push_back(ed);
        }

        // Check all mandatory edges are done if target is destination
        if (m_layeredGraph[target(ed, m_layeredGraph)].index
                == n * (m_demand->functions.size()) + m_demand->t
            && !m_fixedLinks->containAllMandatory(
                   new_cont.mandatory_links, m_layeredGraph)) {
            return false;
        }

        /*std::cout << "Extending with weight: " << new_cont.totalWeight
                  << ", function_indexes: " << new_cont.function_indexes << " on
           ("
                  << g[source(ed, g)] << ", " << g[target(ed, g)] << ") => "
                  << g[ed] << '\n';*/
        return true;
    }
}

bool RCShortestPathPerDemandRestricted::dominance(
    const ResourceContainer& _rc1, const ResourceContainer& _rc2) {
    // Same functions executed and same weight
    return _rc1.usedFunction == _rc2.usedFunction
           && (epsilon_less<double>()(_rc1.totalWeight, _rc2.totalWeight)
                  || epsilon_equal<double>()(
                         _rc1.totalWeight, _rc2.totalWeight))
           && _rc1.nbHops <= _rc2.nbHops;
}

bool operator<(const RCShortestPathPerDemandRestricted::ResourceContainer& _lhs,
    const RCShortestPathPerDemandRestricted::ResourceContainer& _rhs) {
    return epsilon_less<double>()(_lhs.totalWeight, _rhs.totalWeight)
           || (epsilon_equal<double>()(_lhs.totalWeight, _rhs.totalWeight)
                  && _lhs.nbHops < _rhs.nbHops);
}

bool operator==(
    const RCShortestPathPerDemandRestricted::ResourceContainer& _lhs,
    const RCShortestPathPerDemandRestricted::ResourceContainer& _rhs) {
    return epsilon_equal<double>()(_lhs.totalWeight, _rhs.totalWeight);
}

} // namespace SFC::Partial
