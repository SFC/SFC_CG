#include "Partial.hpp"
#include <tclap/CmdLine.hpp>

#include "ColumnGeneration.hpp"
#include "MyTimer.hpp"
#include "PathDecomposition.hpp"
#include "PathDecompositionBlock.hpp"
#include "RCShortestPathPerDemand.hpp"
#include <boost/format.hpp>

template <typename Master, typename... Pricings>
std::optional<SFC::Partial::Solution> solveWithCG(
    const SFC::Partial::Instance& _inst, int _nbThreads, double _alpha,
    bool _verbose) {
    Master rmp(_inst);
    if (solveStabilized<Master, Pricings...>(
            rmp, Pricings(_inst)..., {_nbThreads, _verbose, true, _alpha, 1e-6})) {
        return rmp.getSolution();
    }
    return std::nullopt;
}

template <>
struct PricingProblemTraits<SFC::Partial::RCShortestPathBlock> {
    using Column = typename std::vector<SFC::Partial::ServicePath>;
    using IsUnique = std::true_type;
};

int main(int argc, char** argv) {
    using Function = std::optional<SFC::Partial::Solution> (*)(
        const SFC::Partial::Instance&, int, double, bool);
    const std::map<std::string, Function> dispatchMap{
        {{"path",
             +[](const SFC::Partial::Instance& _inst, int _nbThreads,
                  double _alpha, bool _verbose) {
                 return solveWithCG<SFC::Partial::PathDecomposition::Master,
                     SFC::Partial::RCShortestPathPerDemand>(
                     _inst, _nbThreads, _alpha, _verbose);
             }},
            /*
            {"block",
                +[](const SFC::Partial::Instance& _inst, int _nbThreads,
                     double _alpha,
                     bool _verbose) -> std::optional<SFC::Partial::Solution> {
                    SFC::Partial::PathDecompositionBlock::Master rmp(_inst);
                    if (solveStabilized<
                            SFC::Partial::PathDecompositionBlock::Master,
                            SFC::Partial::RCShortestPathBlock>(rmp,
                            SFC::Partial::RCShortestPathBlock(_inst),
<<<<<<< Updated upstream
                            _nbThreads, _alpha, _verbose)) {
=======
			    {_nbThreads, _verbose, true, _alpha, 1e-6})) {
>>>>>>> Stashed changes
                        return rmp.getSolution();
                    }
                    return std::nullopt;
                }},
                */
        }};
    const auto [topologyFilename, model, nbThreads, alpha, demandFactor,
        percentDemand, verbose] = [&]() {
        TCLAP::CmdLine cmd(
            "Solve the partial SFC provisioning problem", ' ', "0.1");

        TCLAP::UnlabeledValueArg<std::string> topoArg(
            "network", "Specify the network name", true, "", "network");
        cmd.add(&topoArg);

        TCLAP::ValuesConstraint<std::string> vCons({"path", "block"});
        TCLAP::ValueArg<std::string> modelName(
            "m", "model", "Specify the model used", false, "path", &vCons);
        cmd.add(&modelName);

        TCLAP::ValueArg<double> demandFactorArg("d", "demandFactor",
            "Specify the multiplicative factor for the demands", false, 1,
            "double");
        cmd.add(&demandFactorArg);

        TCLAP::ValueArg<double> alphaArg("a", "alpha",
            "Specify the alpha for the stabilization", false, 1, "double");
        cmd.add(&alphaArg);

        TCLAP::SwitchArg verboseArg(
            "v", "verbose", "Define the verbosity of the program", false);
        cmd.add(&verboseArg);

        TCLAP::ValueArg<int> nbThreadsArg(
            "j", "nbThreads", "Specify the number of threads", false, 1, "int");
        cmd.add(&nbThreadsArg);

        TCLAP::ValueArg<double> percentDemandArg("p", "percentDemand",
            "Specify the percentage of demand used", false, 100.0, "double");
        cmd.add(&percentDemandArg);

        cmd.parse(argc, argv);

        return std::make_tuple(topoArg.getValue(), modelName.getValue(),
            nbThreadsArg.getValue(), alphaArg.getValue(),
            demandFactorArg.getValue(), percentDemandArg.getValue(),
            verboseArg.getValue());
    }();

    std::ifstream ifs(topologyFilename);
    const auto instance = SFC::Partial::loadInstance(ifs);
    std::cout << "Loaded " << topologyFilename << '\n';

    const int totalAvailableCores =
        std::accumulate(vertices(instance.network).first,
            vertices(instance.network).second, 0, [&](auto&& _acc, auto&& _u) {
                return _acc + instance.network[_u].capacity;
            });
    std::cout << "totalAvailableCores: " << totalAvailableCores << " for "
              << instance.getTotalNbCores() << " needed cores\n";
    Time timer;
    timer.start();
    const auto sol = dispatchMap.at(model)(instance, nbThreads, alpha, verbose);
    if (sol.has_value()) {
        std::cout << "Found a solution!\n";
        const auto instanceName = instance.network[boost::graph_bundle];
        const auto resFilename =
            percentDemand != 100.0
                ? str(boost::format("./results/partial/%1%_%2%_%3%.res")
                      % instanceName % model % instance.demands.size())
                : str(boost::format("./results/partial/%1%_%2%.res")
                      % instanceName % model);
        std::ofstream ofs(resFilename);
        const auto times = timer.get();
        nlohmann::json time{
            {"CPUTime", times.first}, {"WallTime", times.second}};
        sol->save(ofs);
        std::cout << "Saved to " << resFilename << '\n';
    } else {
        timer.get();
        std::cout << "No solution found!\n";
    }
    return 0;
}
