#include "instance-generator.hpp"
#include <boost/format.hpp>

namespace SFC::Partial {

void save(std::ostream& _out, std::string_view _instanceName,
    const Instance& _inst) noexcept {
    // Save node capacities
    _out << _instanceName << '\n';
    _out << num_vertices(_inst.network) << '\n';
    for (int u = 0; u < num_vertices(_inst.network); ++u) {
        _out << _inst.network[u].capacity << '\n';
    }
    // Save edges
    _out << num_edges(_inst.network) << '\n';
    for (const auto ed : boost::make_iterator_range(edges(_inst.network))) {
        _out << source(ed, _inst.network) << ' ' << target(ed, _inst.network)
             << ' ' << _inst.network[ed].capacity << '\n';
    }
    // Save function requirements
    _out << _inst.funcCharge.size() << '\n';
    for (function_descriptor f = 0; f < _inst.funcCharge.size(); ++f) {
        _out << _inst.funcCharge[f] << ' ';
    }
    _out << '\n';
    // Save requests
    _out << _inst.demands.size() << '\n';
    boost::format fmter("%1% %2% %3% %4% ");
    for (const auto& [id, s, t, d, functions] : _inst.demands) {
        _out << fmter % s % t % d % functions.size();
        for (int i = 0; i < functions.size(); ++i) {
            _out << functions.getFunction(i) << ' ';
        }
        const auto expl = functions.getExplicitOrder();
        _out << expl.size() << ' ';
        for (const auto& [f1, f2] : expl) {
            _out << f1 << ' ' << f2 << ' ';
        }
        _out << '\n';
    }
}

Network loadTopology(std::ifstream& _ifs) {
    if (!_ifs) {
        throw std::runtime_error("Invalid ifstream");
    }
    std::string name;
    _ifs >> name;
    int nbVertices;
    _ifs >> nbVertices;
    std::vector<int> nodeCapa(nbVertices);
    for (int u = 0; u < nbVertices; ++u) {
        if (!(_ifs >> nodeCapa[u])) {
            throw std::runtime_error("Error reading network capacity");
        }
    }
    int nbEdges;
    _ifs >> nbEdges;
    std::vector<SFC::Link> linkProperties;
    linkProperties.reserve(nbEdges);

    std::vector<Edge> edges;
    edges.reserve(nbEdges);
    int e = 0;
    for (std::istream_iterator<WeightedEdge> ite(_ifs), end; ite != end;
         ++ite) {
        assert(ite->u < nbEdges);
        assert(ite->v < nbEdges);
        assert(ite->capa > 0);
        edges.emplace_back(ite->u, ite->v);
        edges.emplace_back(ite->v, ite->u);
        linkProperties.emplace_back(e++, ite->capa);
        linkProperties.emplace_back(e++, ite->capa);
    }
    Network retval(edges.begin(), edges.end(), linkProperties.begin(),
        nbVertices, 0, name);
    for (int u = 0; u < nbVertices; ++u) {
        retval[u] = Router{u, nodeCapa[u]};
    }
    return retval;
}

} // namespace SFC::Partial
