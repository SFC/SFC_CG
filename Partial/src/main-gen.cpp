#include <boost/format.hpp>
#include <tclap/CmdLine.hpp>

#include "Generator.hpp"
#include "instance-generator.hpp"
#include "pair-generator.hpp"
#include "utility.hpp"

int main(int argc, char** argv) {
    const auto [topologyFilename, load, chainSize, nbFunctions, proba,
        seed] = [&]() {
        TCLAP::CmdLine cmd("Generate instance for the partially ordered "
                           "service function chaining problem",
            ' ', "0.0");

        TCLAP::UnlabeledValueArg<std::string> topologyFilenameArg(
            "network", "Specify the network name", true, "", "network");
        cmd.add(&topologyFilenameArg);

        TCLAP::ValueArg<int> loadArg("l", "load",
            "Specify the load to generated in MB", true, 1e9, "int");
        cmd.add(&loadArg);

        TCLAP::ValueArg<int> chainSizeArg(
            "c", "chainSize", "Specify the size of the chains", true, 5, "int");
        cmd.add(&chainSizeArg);

        TCLAP::ValueArg<int> nbFunctionsArg("f", "nbFunctions",
            "Specify the size of the number of functions", true, 15, "int");
        cmd.add(&nbFunctionsArg);

        TCLAP::ValueArg<double> probaArg("p", "proba",
            "Specify the probability of having a precedence constraints", true,
            .5, "double");
        cmd.add(&probaArg);

        TCLAP::ValueArg<int> seedArg("s", "seed",
            "Specify the size of the number of nodes", true, 0, "int");
        cmd.add(&seedArg);
        cmd.parse(argc, argv);
        return std::make_tuple(topologyFilenameArg.getValue(), loadArg.getValue(),
            chainSizeArg.getValue(), nbFunctionsArg.getValue(), probaArg.getValue(),
            seedArg.getValue());
    }();

    std::ifstream ifs(topologyFilename, std::ifstream::in);
    const auto network = SFC::Partial::loadTopology(ifs);
    std::cout << boost::format(
                     "Loaded from %1%: %2% with %3% nodes and %4% edges.\n")
                     % topologyFilename % network[boost::graph_bundle]
                     % num_vertices(network) % num_edges(network);

    std::seed_seq seed_s = {seed};
    std::mt19937 randGen(seed_s);
    SFC::Partial::ChainGenerator chainGen(nbFunctions, chainSize, proba);
    Generator::UniformPairGenerator pairGen(num_vertices(network));

    auto demands = genDemands(load, chainGen, pairGen, randGen);
    std::transform(
        demands.begin(), demands.end(), demands.begin(), [](auto&& demand) {
            demand.d /= 1000.0;
            return demand;
        });
    const auto funcCharge = Generator::FunctionRequirementGenerator(
        nbFunctions, {1, 10, 100})(randGen);

    const auto instanceName = (boost::format("%1%_%2%_%3%_%4%_%5$.6f_%6%")
                               % network[boost::graph_bundle] % load % chainSize
                               % nbFunctions % proba % seed)
                                  .str();
    const auto instanceFilename =
        (boost::format("./instances/partial/%1%.inst") % instanceName).str();
    std::cout << boost::format("Saving to %1%.\n") % instanceFilename;
    std::ofstream ofs(instanceFilename);
    SFC::Partial::save(ofs, instanceName, {network, demands, funcCharge});
}
