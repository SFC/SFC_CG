Create instance for internet2
```bash
parallel ../build/Partial/PartialGenerator instances/partial/internet2.topo ::: -l ::: 1000000 ::: -c ::: 5 ::: -f ::: 15 ::: -p ::: $(seq 0 .1 1) ::: -s ::: 0
```

Launch all instances of internet2
```bash
parallel --bar --joblog exec_internet2.log -j 1 ../build/Partial/LP_Partial ::: $(find instances/ -name "internet*.inst") ::: -v ::: -j ::: 3
```

Launch all instances of internet2 with licenses limit
```bash
parallel --bar --joblog exec_internet2.log -j 1 ../build/Partial/OccLim/LP_Partial_OCC ::: $(find instances/ -name "internet*.inst") ::: -v ::: -j ::: 3 ::: -r ::: {1..10..-1}
```
