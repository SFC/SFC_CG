#ifndef SFC_PARTIAL_OCCLIM_ALGORITHM_HPP
#define SFC_PARTIAL_OCCLIM_ALGORITHM_HPP

#include "../../include/PathDecomposition.hpp"
#include "ColumnGeneration.hpp"
#include "Occlim.hpp"
#include "PathDecomposition.hpp"
#include "RCShortestPathPerDemand.hpp"

namespace SFC::Partial::OccLim {

// Randomized rounding

struct RoundingParameter {
    int nbIterations = 500;
    double scaling = .99;
};

template <typename MasterRelaxed, typename PricingRelaxed,
    typename MasterRestricted, typename PricingRestricted>
std::optional<Solution> rounding(const SFC::Partial::OccLim::Instance& _inst,
    double _alpha, int _nbThreads, bool _verbose,
    const RoundingParameter& _params = RoundingParameter()) {
    std::optional<Solution> bestSol;
    MasterRelaxed rmp(_inst);
    if (solveStabilized<MasterRelaxed, PricingRelaxed>(
            rmp, PricingRelaxed(_inst), _nbThreads, _alpha, _verbose, false)) {
        if (_verbose) {
            std::cout << "Found fractional solution, LB = " << rmp.getObjValue()
                      << "\n\n";
        }
        const auto relaxedLocations = rmp.getLocations();
        std::mt19937 randGen;
        for (int i = 0; i < _params.nbIterations; ++i) {
            std::seed_seq seedSeq{i};
            randGen.seed(seedSeq);
            const auto randomizedLocations = [&]() {
                auto retval =
                    getLocations(relaxedLocations, _params.scaling, randGen);
                while (!isValid(retval, _inst)) {
                    retval = getLocations(
                        relaxedLocations, _params.scaling, randGen);
                }
                return retval;
            }();
            MasterRestricted rmpRestricted(_inst);
            try {
                if (solveStabilized<MasterRestricted, PricingRestricted>(
                        rmpRestricted,
                        PricingRestricted(_inst, randomizedLocations),
                        _nbThreads, _alpha, false, true)) {
                    const auto currentSolution = rmpRestricted.getSolution();
                    if (!bestSol.has_value()
                        || bestSol->getObjValue()
                               > currentSolution.getObjValue()) {
                        bestSol = currentSolution;
                        bestSol->setLowerBound(rmp.getObjValue());
                        if (epsilon_equal<double>()(
                                bestSol->getObjValue(), rmp.getObjValue())) {
                            return bestSol;
                        }
                    }
                }
            } catch (std::runtime_error& e) {
                std::cerr << e.what() << '\n';
            }
            if (_verbose) {
                std::cout << '\r' << "# rounding " << (i + 1) << " / "
                          << _params.nbIterations << ", best UB: "
                          << (bestSol
                                     ? bestSol->getObjValue()
                                     : std::numeric_limits<double>::quiet_NaN())
                          << std::flush;
            }
        }
        std::cout << '\n';
    }
    if (bestSol.has_value()) {
        const auto sPaths = bestSol->getServicePaths();
        if constexpr (std::is_same_v<std::decay_t<decltype(sPaths)>,
                          typename MasterRelaxed::Column>) {
            rmp.addColumn(std::move(sPaths));
        } else if constexpr (std::is_same_v<decltype(sPaths)::value_type,
                                 typename MasterRelaxed::Column>) {
            rmp.addColumns(sPaths.begin(), sPaths.end());
        } else {
            return bestSol;
        }
        if (solveStabilized<MasterRelaxed, PricingRelaxed>(rmp,
                PricingRelaxed(_inst), _nbThreads, _alpha, _verbose, true)) {
            return rmp.getSolution();
        }
    }
    return bestSol;
}

std::optional<Solution> path_rounding(
    const SFC::Partial::OccLim::Instance& _inst, double _alpha, int _nbThreads,
    bool _verbose);

} // namespace SFC::Partial::OccLim
#endif
