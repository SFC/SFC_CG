#ifndef OCCLIM_PATH_DECOMPOSITION_HPP
#define OCCLIM_PATH_DECOMPOSITION_HPP

#include "ColumnGeneration.hpp"
#include "Occlim.hpp"

namespace SFC::Partial::OccLim::PathDecomposition {

struct DualValues {
    DualValues(const Instance& _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;

    IloNum getReducedCost(const CrossLayerLink& _link) const;
    IloNum getReducedCost(const IntraLayerLink& _link) const;
    IloNum getReducedCost(const Demand& _demand) const;
    double getDualSumRHS() const;

    const Instance* inst;
    std::vector<IloNum> m_onePathDuals;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;
    boost::multi_array<IloNum, 3> m_pathFuncDuals;
    std::vector<IloNum> m_nbLicensesDuals;
};

class Master : public RMP<Master, DualValues, Solution, ServicePath> {
    friend DualValues;

  public:
    using Base = RMP<Master, DualValues, Solution, ServicePath>;
    friend Base;
    using Column = ServicePath;
    using Solution = Solution;
    using DualValues = DualValues;

    explicit Master(const Instance& _inst);
    Master& operator=(const Master&) = delete;
    Master& operator=(Master&&) = delete;
    Master(const Master&) = delete;
    Master(Master&&) = delete;
    ~Master() { m_env.end(); }

    double getReducedCost_impl(
        const ServicePath& _col, const DualValues& _dualValues) const;
    IloNumColumn getColumn_impl(const ServicePath& _col);
    void getDuals_impl();
    void removeDummyIfPossible();
    const DualValues& getDualValues_impl() const;
    std::vector<ServicePath> getServicePaths() const;
    
    template <typename T>
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }

    Solution getSolution() const;
	RelaxedSolution getRelaxedSolution() const {
		RelaxedSolution retval;
		retval.paths.resize(m_inst->demands.size());
		for(const auto& [var, col] : getColumnStorage<ServicePath>()) {
			retval.paths[col.demand].emplace_back(Weighted<ServicePath>{m_solver.getValue(var), col});
		}
		return retval;
	}
    boost::multi_array<double, 2> getLocations() const;

    using Base::getNbColumns;

  private:
    using Base::m_columns;
    using Base::m_duals;
    using Base::m_dummyActive;
    using Base::m_env;
    using Base::m_fracObj;
    using Base::m_intObj;
    using Base::m_model;
    using Base::m_solver;

    const Instance* m_inst;

    boost::multi_array<IloNumVar, 2> m_b;

    IloObjective m_obj;

    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;
    std::vector<IloRange> m_nbLicensesCons;
    boost::multi_array<LazyConstraint, 3> m_pathFuncCons;

    std::vector<IloNumVar> m_dummyPaths;
    std::vector<double> m_linkCharge;
    std::vector<double> m_nodeCharge;

    boost::multi_array<double, 2> m_funcPath;
    mutable boost::multi_array<double, 2> m_edgeUsage;
    mutable boost::multi_array<double, 2> m_nodeUsage;
    mutable boost::multi_array<double, 2> m_licenseUsage;
};

std::ostream& operator<<(std::ostream& _out, const DualValues& _dualValues);
void barycenter(const DualValues& _nDuals, const DualValues& _bDuals,
    DualValues& _sDuals, double _alpha);

} // namespace SFC::Partial::OccLim::PathDecomposition

#endif
