#ifndef SFC_PARTIAL_OCCLIM_HPP
#define SFC_PARTIAL_OCCLIM_HPP

#include "Partial.hpp"
#include "cplex_utility.hpp"
#include <optional>
#include <random>

namespace SFC::Partial::OccLim {

using Solution = SFC::Partial::Solution;

struct Instance : public SFC::Partial::Instance {
    Instance(Network _network, std::vector<Demand> _demands,
        std::vector<double> _funcCharge, int _nbLicenses);
    Instance(SFC::Partial::Instance _inst, int _nbLicenses);

    using SFC::Partial::Instance::demands;
    using SFC::Partial::Instance::funcCharge;
    using SFC::Partial::Instance::maxChainSize;
    using SFC::Partial::Instance::network;
    int nbLicenses;
};

Instance loadInstance(std::ifstream& _ifs, int _nbLicenses);

template <typename RandGen>
boost::multi_array<bool, 2> getLocations(
    const boost::multi_array<double, 2>& _bProba, double _scaling,
    RandGen& _gen) {
    boost::multi_array<bool, 2> retval(
        boost::extents[_bProba.shape()[0]][_bProba.shape()[1]]);
    std::transform(_bProba.data(), _bProba.data() + _bProba.num_elements(),
        retval.data(), [&](auto&& _p) {
            if (epsilon_equal<double>()(_p, 1.0)) {
                return std::generate_canonical<double, 10>(_gen) <= _scaling;
            } else if (epsilon_equal<double>()(_p, 0.0)) {
                return std::generate_canonical<double, 10>(_gen)
                       <= (1 - _scaling);
            } else {
                return std::generate_canonical<double, 10>(_gen) <= _p;
            }
        });
    return retval;
}

boost::multi_array<bool, 2> getLocations(const Solution& _sol);

bool isValid(
    const boost::multi_array<bool, 2>& _locations, const Instance& _inst);

template <typename T>
bool print(const boost::multi_array<T, 2>& _locations, const Instance& _inst) {
    for (int f = 0; f < _inst.funcCharge.size(); ++f) {
        std::cout << "f: ";
        for (int u = 0; u < num_vertices(_inst.network); ++u) {
            if (_locations[u][f]) {
                std::cout << u << ", ";
            }
        }
        std::cout << '\n';
    }
    std::cout << '\n';
    return true;
}

template <typename Solution>
bool isValid(const Solution& _sol, const Instance& _inst) {
    const auto locations = getLocations(_sol);
    for (int f = 0; f < _inst.funcCharge.size(); ++f) {
        int nbLicenses = 0;
        for (int u = 0; u < num_vertices(_inst.network); ++u) {
            if (locations[u][f]) {
                nbLicenses++;
            }
        }
        if (nbLicenses > _inst.nbLicenses) {
            std::cerr << "Exceding # licenses for " << f << " -> " << nbLicenses
                      << '\n';
            return false;
        }
    }
    for (const auto& sPath : _sol.getServicePaths()) {
        assert(_inst.demands[sPath.demand].s == sPath.nPath.front());
        assert(_inst.demands[sPath.demand].t == sPath.nPath.back());
    }
    return true;
}

} // namespace SFC::Partial::OccLim
#endif
