#include <boost/multi_array/base.hpp>
#include <ilconcert/iloexpression.h>
#include <ilconcert/ilolinear.h>
#include <ilconcert/ilosys.h>
#include <iostream>

#include "Decomposition.hpp"
#include "PathDecompositionRestricted.hpp"
#include <boost/format.hpp>
#include <boost/log/trivial.hpp>
#include <range/v3/all.hpp>

namespace SFC::Partial::OccLim::PathDecompositionRestricted {

Master::Master(const Instance& _inst)
    : Base(DualValues(_inst))
    , m_inst(&_inst)
    , m_b([&]() {
        boost::multi_array<IloNumVar, 2> b(boost::extents[num_vertices(
            m_inst->network)][m_inst->funcCharge.size()]);
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            for (int u = 0; u < num_vertices(m_inst->network); ++u) {
                b[u][f] = IloNumVar(m_env);
                IloAdd(m_integerModel, IloConversion(m_env, b[u][f], ILOBOOL));
                IloAdd(m_model, b[u][f]);
                setIloName(b[u][f], "b[u=" + std::to_string(u)
                                        + ", f=" + std::to_string(f) + "]");
            }
        }
        return b;
    }())
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_onePathCons(getOnePathPerDemandConstraints(*m_inst, m_model))
    , m_linkCapasCons(getLinkCapacityConstraints(*m_inst, m_model))
    , m_nodeCapasCons(getNodeCapacityConstraints(*m_inst, m_model))
    , m_nbLicensesCons([&]() {
        std::vector<IloRange> funcLicensesCons(m_inst->funcCharge.size());
        const auto allDemandsRange = boost::multi_array_types::index_range(
            0, num_vertices(m_inst->network));

        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            const auto allNode_f = m_b[boost::indices[allDemandsRange][f]];
            funcLicensesCons[f] =
                IloAdd(m_model, IloRange(m_env, -IloInfinity,
                                    std::accumulate(allNode_f.begin(),
                                        allNode_f.end(), IloExpr(m_env)),
                                    m_inst->nbLicenses));
            setIloName(
                funcLicensesCons[f], "funcLicensesCons(" + toString(f) + ")");
            // vars.end();
        }
        return funcLicensesCons;
    }())
    , m_pathFuncCons([&]() {
        boost::multi_array<LazyConstraint, 3> pathFuncCons(
            boost::extents[m_inst->demands.size()][m_inst->maxChainSize]
                          [num_vertices(m_inst->network)]);

        for (int i = 0; i < m_inst->demands.size(); ++i) {
            for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                for (int u = 0; u < num_vertices(m_inst->network); ++u) {
                    pathFuncCons[i][j][u] = LazyConstraint(m_model,
                        -IloInfinity,
                        -m_b[u][m_inst->demands[i].functions.getFunction(j)],
                        0.0);
                    setIloName(pathFuncCons[i][j][u],
                        "pathFuncCons" + toString(std::make_tuple(i, j, u)));
                }
            }
        }
        return pathFuncCons;
    }())
    , m_dummyPaths([&]() {
        std::vector<IloNumVar> retval(m_inst->demands.size());
        for (int i = 0; i < retval.size(); ++i) {
            retval[i] =
                m_onePathCons[i](1.0)
                + m_obj(10 * m_inst->demands[i].d * num_edges(m_inst->network));
            // m_model.add(retval[i]);
            m_integerModel.add(retval[i] == 0);
            // setIloName(retval[i], "dummyPaths" +
            // toString(m_inst->demands[i]));
        }
        return retval;
    }())
    , m_linkCharge(num_edges(m_inst->network))
    , m_nodeCharge(num_vertices(m_inst->network))
    , m_edgeUsage(
          boost::extents[m_inst->demands.size()][m_inst->maxChainSize + 1]
                        [num_edges(m_inst->network)])
    , m_nodeUsage(
          boost::extents[m_inst->demands.size()][m_inst->maxChainSize + 1]
                        [num_vertices(m_inst->network)]) {
    m_solver.setOut(m_solver.getEnv().getNullStream());
    m_solver.setWarning(m_solver.getEnv().getNullStream());
}

double Master::getReducedCost_impl(
    const ServicePath& _col, const DualValues& _dualValues) const {
    const auto& [nPath, locations, function_indexes, demandID] = _col;
    double retval = _dualValues.getReducedCost(m_inst->demands[demandID]);

    /*std::stringstream message;
    message << "getReducedCost_impl: " << _col;
    message << boost::format(" -> onePath(%1%): %2%, ")
        % demandID % _dualValues.getReducedCost(m_inst->demands[demandID]);*/

    int j = 0;
    auto iteU = nPath.begin(), iteV = std::next(iteU);
    while (j < locations.size() || iteV != nPath.end()) {
        if (j < locations.size() && *iteU == locations[j]) {
            retval += _dualValues.getReducedCost(
                CrossLayerLink{demandID, locations[j], j, function_indexes[j]});
            /*message <<  boost::format("locations[%1%]: %2%, ") % j %
               _dualValues.getReducedCost( CrossLayerLink{demandID,
               locations[j], j, function_indexes[j]});*/
            ++j;
        } else {
            const auto rc = _dualValues.getReducedCost(
                IntraLayerLink{demandID, {*iteU, *iteV}, j});
            retval += rc;
            /*message <<  boost::format("link(%1%, %2%): %3%, ") % *iteU % *iteV
             * % rc;*/
            ++iteU;
            ++iteV;
        }
    }
    return retval;
}

IloNumColumn Master::getColumn_impl(const ServicePath& _col) {
    const auto& [nPath, locations, function_indexes, demandID] = _col;
    const auto& [id, s, t, d, chain] = m_inst->demands[demandID];

    assert(isValid(_col, m_inst->demands[demandID]));

    // Link constraints
    std::fill(m_linkCharge.begin(), m_linkCharge.end(), 0.0);
<<<<<<< Updated upstream
    std::fill(m_nodeCharge.begin(), m_nodeCharge.end(), 0.0);

    IloNumColumn col =
        m_onePathCons[demandID](1.0) + m_obj(d * (nPath.size() - 1));
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
         ++iteU, ++iteV) {
        m_linkCharge[m_inst->network[edge(*iteU, *iteV, m_inst->network).first]
                         .id] += d;
=======
    IloNumColumn col = m_onePathCons[demandID](1.0) + m_obj(d * (nPath.size() - 1));
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end(); ++iteU, ++iteV) {
        m_linkCharge[m_inst->network[edge(*iteU, *iteV, m_inst->network).first].id] += d;
>>>>>>> Stashed changes
    }
    const auto mult = [](const IloRange& _const, double _val) {
        return _const(_val);
    };
    const auto add = [](auto&& _acc, auto&& _newVal) {
        return _acc += _newVal;
    };
    col += std::inner_product(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_linkCharge.begin(), IloNumColumn(m_env), add, mult);

    // Node constraints
    std::fill(m_nodeCharge.begin(), m_nodeCharge.end(), 0.0);
    for (int j = 0; j < locations.size(); ++j) {
        const auto f = chain.getFunction(function_indexes[j]);
<<<<<<< Updated upstream
        m_nodeCharge[locations[j]] += m_inst->getNbCores(f, d);
        col += m_pathFuncCons[demandID][function_indexes[j]][locations[j]](1.0);
=======
        const auto u = locations[j];
        m_nodeCharge[u] += m_inst->getNbCores(f, d);
        if (m_pathFuncCons[demandID][function_indexes[j]][u].isActive()) {
            col += m_pathFuncCons[demandID][function_indexes[j]][u](1.0);
        }
>>>>>>> Stashed changes
    }
    col += std::inner_product(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_nodeCharge.begin(), IloNumColumn(m_env), add, mult);

    return col;
}

void Master::removeBranch(IloRange _rng) {
    m_model.remove(_rng);
    m_branches.erase(
        std::remove_if(m_branches.begin(), m_branches.end(),
            [&](auto&& _cons) { return _cons.getImpl() == _rng.getImpl(); }),
        m_branches.end());
}

void Master::removeBranch(const std::variant<IloRange, FixedIntraLayerLink>& _rng) {
    if(std::holds_alternative<IloRange>(_rng)) {
        removeBranch(std::get<IloRange>(_rng));
    } else {
        removeBranch(std::get<FixedIntraLayerLink>(_rng));
    }
}


void Master::applyBranch(const std::variant<IloRange, FixedIntraLayerLink>& _rng) {
    if(std::holds_alternative<IloRange>(_rng)) {
        applyBranch(std::get<IloRange>(_rng));
    } else {
        applyBranch(std::get<FixedIntraLayerLink>(_rng));
    }
}

void Master::applyBranch(IloRange _rng) {
    m_model.add(_rng);
    m_branches.emplace_back(_rng);
}

void Master::clearBranches() {
    for (auto& rng : m_branches) {
        m_model.remove(rng);
    }
    m_branches.clear();
}

void Master::getDuals_impl() {
    const auto getDualValue = [&](auto&& _cons) {
        return getDual(m_solver, _cons);
    };
    std::transform(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_duals.m_linkCapasDuals.begin(), getDualValue);

    std::transform(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_duals.m_nodeCapasDuals.begin(), getDualValue);

    std::transform(m_pathFuncCons.data(),
        m_pathFuncCons.data() + m_pathFuncCons.num_elements(),
        m_duals.m_pathFuncDuals.data(), getDualValue);

    std::transform(m_onePathCons.begin(), m_onePathCons.end(),
        m_duals.m_onePathDuals.begin(), getDualValue);

    std::transform(m_nbLicensesCons.begin(), m_nbLicensesCons.end(),
        m_duals.m_nbLicensesDuals.begin(), getDualValue);

<<<<<<< Updated upstream
    // double sum = 0.0;
    // for(auto ite = IloModel::Iterator(m_model); ite.ok(); ++ite) {
    // if((*ite).isConstraint()) {
    // const auto cons =
    // IloRange(static_cast<IloRangeI*>((*ite).asConstraint().getImpl()));
    // if(epsilon_less<double>()(cons.getUB(), IloInfinity)) {
    // sum += (getDual(m_solver, cons) * cons.getUB());
    //}
    // if(epsilon_less<double>()(-IloInfinity, cons.getLB())) {
    // sum -= (getDual(m_solver, cons) * cons.getUB());
    //}
    //}
    //}
    // std::cout << "Sum: " <<  sum << '\n';

    // std::cout << "Branches: " << m_branches << '\n';
    m_duals.m_branchesDual = std::accumulate(m_branches.begin(),
        m_branches.end(), 0.0, [&](auto&& _acc, auto&& _cons) {
=======
    m_duals.m_branchesDual =
        std::accumulate(m_branches.begin(), m_branches.end(), 0.0, [&](auto&& _acc, auto&& _cons) {
>>>>>>> Stashed changes
            const auto dual = getDual(m_solver, _cons);
            if (epsilon_less<double>()(_cons.getUB(), IloInfinity)) {
                return _acc + dual * _cons.getUB();
            }
            if (epsilon_less<double>()(-IloInfinity, _cons.getLB())) {
                return _acc + dual * _cons.getLB();
            }
            assert(false);
            return 0.0;
        });
}

Solution Master::getSolution() const {
    return {*m_inst, getIntegerObjValue(), getRelaxationObjValue(),
        getServicePaths()};
}

void Master::removeDummyIfPossible() {
    if (epsilon_equal<double>()(
            std::accumulate(m_dummyPaths.begin(), m_dummyPaths.end(), 0.0,
                [&](auto&& _acc, auto&& _var) {
                    return _acc + m_solver.getValue(_var);
                }),
            0.0)) {
        m_dummyActive = false;
    }
}

const DualValues& Master::getDualValues_impl() const { return m_duals; }

std::vector<ServicePath> Master::getServicePaths() const {
    std::vector<ServicePath> sPaths(m_inst->demands.size());
    for (const auto& [var, sPath] : getColumnStorage<ServicePath>()) {
        if (epsilon_equal<double>()(m_solver.getValue(var), IloTrue)) {
            sPaths[sPath.demand] = sPath;
        }
    }
    return sPaths;
}

bool Master::checkIntegrality_impl() const {
    // Get edge usage for each requests
    std::fill(m_edgeUsage.data(),
        m_edgeUsage.data() + m_edgeUsage.num_elements(), 0.0);
    for (const auto& [var, sPath] : getColumnStorage<ServicePath>()) {
        int j = 0;
        auto iteU = sPath.nPath.begin(), iteV = std::next(iteU);
        while (j < sPath.locations.size() && iteV != sPath.nPath.end()) {
            if (sPath.locations[j] != *iteU) {
                m_edgeUsage
                    [sPath.demand][j]
                    [m_inst->network[edge(*iteU, *iteV, m_inst->network).first]
                            .id] += m_solver.getValue(var);
                ++iteU;
                ++iteV;
            } else {
                ++j;
            }
        }
    }

    // Get node usage for each requests
    std::fill(m_nodeUsage.data(),
        m_nodeUsage.data() + m_nodeUsage.num_elements(), 0.0);
    for (const auto& [var, path] : getColumnStorage<ServicePath>()) {
        for (int j = 0; j < path.locations.size(); ++j) {
            m_nodeUsage[path.demand][j][path.locations[j]] += m_solver.getValue(var);
        }
    }
    std::clog <<
        std::all_of(m_edgeUsage.data(), m_edgeUsage.data() + m_edgeUsage.num_elements(), isInteger<double>)
        << ", " << std::all_of(
                m_nodeUsage.data(), m_nodeUsage.data() + m_nodeUsage.num_elements(), isInteger<double>)
        << ", " << std::all_of(m_b.data(), m_b.data() + m_b.num_elements(),
                  [&](auto&& _bVar) { return isInteger<double>(m_solver.getValue(_bVar)); })
        << '\n';

    return std::all_of(m_edgeUsage.data(),
               m_edgeUsage.data() + m_edgeUsage.num_elements(),
               isInteger<double>)
           && std::all_of(m_nodeUsage.data(),
                  m_nodeUsage.data() + m_nodeUsage.num_elements(),
                  isInteger<double>)
           && std::all_of(m_b.data(), m_b.data() + m_b.num_elements(),
                  [&](auto&& _bVar) {
                      return isInteger<double>(m_solver.getValue(_bVar));
                  });
}

std::optional<std::vector<FixedIntraLayerLink>>
Master::getLinkBranches() const {

    // Get most fractional value
    int minI = 0;
    auto minEd = *edges(m_inst->network).first;
    int minLayer = 0;
    double minValue = m_edgeUsage[minI][minLayer][m_inst->network[minEd].id];

    for (int i = 0; i < m_inst->demands.size(); ++i) {
        for (const auto ed :
            boost::make_iterator_range(edges(m_inst->network))) {
            for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                if (isMostFractional(
                        m_edgeUsage[i][j][m_inst->network[ed].id], minValue)) {
                    minValue = m_edgeUsage[i][j][m_inst->network[ed].id];
                    minI = i;
                    minEd = ed;
                    minLayer = j;
                }
            }
        }
    }
    std::cout
        << boost::format(
               "most fractional: {i=%1%, j=%2%, l=(%3%, %4%)} with value = %5%")
               % minI % minLayer % source(minEd, m_inst->network)
               % target(minEd, m_inst->network) % minValue
        << '\n';

    if (!isInteger(m_edgeUsage[minI][minLayer][m_inst->network[minEd].id])) {
        const IntraLayerLink ilink{
            minI, incident(minEd, m_inst->network), minLayer};
        return {{{ilink, true}, {ilink, false}}};
    }
    return std::nullopt;
}
std::optional<std::vector<IloRange>> Master::getLocationsBranches() const {
    // Find most fractional m_b
    struct FractionalFuncLocation {
        int u;
        int f;
        double value;
    };
    const FractionalFuncLocation mostFrac = [&]{
        FractionalFuncLocation retval{0, 0, m_solver.getValue(m_b[0][0])};
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            for (int u = 0; u < num_vertices(m_inst->network); ++u) {
                const auto currentVal = m_solver.getValue(m_b[u][f]);
                if (isMostFractional(currentVal, retval.value)) {
                    retval = {u, f, currentVal};                
                }
            }
        }
        return retval;
    }();
    if (isInteger(mostFrac.value)) {
        return std::nullopt;
    }

    // Get expression
    IloExpr expr(m_env);
    int total = 1;
    for (int u = 0; u < num_vertices(m_inst->network); ++u) {
        const auto var = m_b[u][mostFrac.f];
        if (epsilon_equal<double>()(m_solver.getValue(var), 1.0)) {
            expr += var;
            total++;
        }
    }
<<<<<<< Updated upstream
    expr += m_b[bestLocation][bestFunction];
    return std::vector<IloRange>{IloRange(m_env, total, expr, IloInfinity),
        IloRange(m_env, -IloInfinity, expr, total - 1)};
=======
    expr += m_b[mostFrac.u][mostFrac.f];
    return std::vector<IloRange>{
        IloRange(m_env, total, expr, IloInfinity), IloRange(m_env, -IloInfinity, expr, total - 1)};
>>>>>>> Stashed changes
}

std::optional<std::vector<FixedCrossLayerLink>>
Master::getNodeBranches() const {
    return std::nullopt;
}

void Master::applyBranch(const FixedIntraLayerLink& _link) {
    // const int before = getColumnStorage<ServicePath>().size();
    getColumnStorage<ServicePath>().erase(
        std::remove_if(getColumnStorage<ServicePath>().begin(),
            getColumnStorage<ServicePath>().end(),
            [&](auto&& _varPath) {
                if (_varPath.obj.demand != _link.var.demandID) {
                    return false;
                }
                const auto isIn = contains(_varPath.obj, _link.var);
                if ((_link.use && !isIn) || (!_link.use && isIn)) {
                    // std::cout << "Removing " << _varPath.obj << " (" <<
                    // _varPath.var << ")\n";
                    _varPath.var.end();
                    return true;
                }
                return false;
            }),
        getColumnStorage<ServicePath>().end());
}

void Master::applyBranch(const FixedCrossLayerLink& /*_link*/) {}

void Master::removeBranch(const FixedIntraLayerLink& /*_link*/) {}

void Master::removeBranch(const FixedCrossLayerLink& /*_link*/) {}

//////////////////////////////////////////////////////////////////////////////
///                                                                        ///
///                               DualValues                               ///
///                                                                        ///
//////////////////////////////////////////////////////////////////////////////

DualValues::DualValues(const Instance& _inst)
    : inst(&_inst)
    , m_onePathDuals(inst->demands.size(), 0.0)
    , m_linkCapasDuals(num_edges(inst->network), 0.0)
    , m_nodeCapasDuals(num_vertices(inst->network), 0.0)
    , m_pathFuncDuals(boost::extents[inst->demands.size()][inst->maxChainSize]
                                    [num_vertices(inst->network)])
    , m_nbLicensesDuals(inst->funcCharge.size(), 0.0)
    , m_branchesDual(0.0) {}

void DualValues::clear() {
    std::fill(m_onePathDuals.begin(), m_onePathDuals.end(), 0.0);
    std::fill(m_linkCapasDuals.begin(), m_linkCapasDuals.end(), 0.0);
    std::fill(m_nodeCapasDuals.begin(), m_nodeCapasDuals.end(), 0.0);
    std::fill(m_pathFuncDuals.data(),
        m_pathFuncDuals.data()+m_pathFuncDuals.num_elements(), 0.0);
    m_branchesDual = 0.0;
}

<<<<<<< Updated upstream

=======
bool Master::generatePathFuncCuts() {
    struct LinkCut {
        int demand;
        int u;
        int j;
    };
    std::vector < LinkCut > cuts;
    boost::multi_array<double, 2> usage(
        boost::extents[num_vertices(m_inst->network)][m_inst->maxChainSize]);
    for (int i = 0; i < m_inst->demands.size(); ++i) {
        const auto& demand = m_inst->demands[i];
        std::fill_n(usage.data(), usage.num_elements(), 0.0);
        // Get usage on each node for each function
        for (const auto& [var, path] : getColumnStorage<ServicePath>()) {
            if(path.demand == i) {
                for (int j = 0; j < path.locations.size(); ++j) {
                    usage[path.locations[j]][path.function_indexes[j]] +=
                        m_solver.getValue(var);
                }
            }
        }
        const auto placement = [&] () -> std::optional<LinkCut> {
            for (const auto u : boost::make_iterator_range(vertices(m_inst->network))) {
                for (int j = 0; j < demand.functions.size(); ++j) {
                    const auto f = demand.functions.getFunction(j);
                    if (epsilon_less<double>()(m_solver.getValue(m_b[u][f]), usage[u][j])) {
                        return LinkCut{i, u, j};
                    }
                }
            }
            return std::nullopt;
        }();
        if (placement) {
            cuts.emplace_back(*placement);
        }
    }
    IloExpr expr(m_env);
    for(const auto& [demandId, u, function_index] : cuts) {
        assert(!m_pathFuncCons[demandId][function_index][u].isActive());
        const auto& demand = m_inst->demands[demandId];
        const auto f = demand.functions.getFunction(function_index);
        expr.clear();
        expr += -m_b[u][f];
        for (const auto& [var, path] : getColumnStorage<ServicePath>()) {
            if(path.demand == demandId) {
                for (int j = 0; j < path.locations.size(); ++j) {
                    if(path.locations[j] == u 
                            && path.function_indexes[j] == function_index) {
                        expr += 1.0 * var;
                    }
                }
            }
        }
        m_pathFuncCons[demandId][function_index][u].setConstraint(IloRange(m_env, -IloInfinity, expr, 0.0));
        assert(m_pathFuncCons[demandId][function_index][u].isActive());
        //std::clog << m_pathFuncCons[demandId][function_index][u].getConstraint() << '\n';
    }
    expr.end();
    return !cuts.empty();
}
>>>>>>> Stashed changes
IloNum DualValues::getReducedCost(const CrossLayerLink& _link) const {
    const auto demand = inst->demands[_link.demandID];
    const auto d = demand.d;
    const auto f = demand.functions.getFunction(_link.function_index);
    auto rc = inst->getNbCores(f, d) * -m_nodeCapasDuals[_link.u]
              - m_pathFuncDuals[_link.demandID][_link.function_index][_link.u];
    assert(epsilon_less<double>()(0.0, rc) || epsilon_equal<double>()(0.0, rc));
    return rc;
}

IloNum DualValues::getReducedCost(const IntraLayerLink& _link) const {
    const auto ed =
        edge(_link.edge.first, _link.edge.second, inst->network).first;
    auto rc = inst->demands[_link.demandID].d
              * (1 - m_linkCapasDuals[inst->network[ed].id]);

    // Check rc is positive
    assert([&]() {
        if (epsilon_less<double>()(0.0, rc)
            || epsilon_equal<double>()(0.0, rc)) {
            return true;
        }
        std::cerr << "Reduced cost of " << _link << " (=" << rc
                  << ") should be positive (" << inst->demands[_link.demandID]
                  << ")\n";
        return false;
    }());
    return rc;
}

IloNum DualValues::getReducedCost(const Demand& _demand) const {
    const auto rc = -m_onePathDuals[_demand.id];
    assert(epsilon_less<double>()(rc, 0.0) || epsilon_equal<double>()(rc, 0.0));
    return rc;
}

double DualValues::getDualSumRHS() const {
    double retval = std::inner_product(m_nodeCapasDuals.begin(),
        m_nodeCapasDuals.end(), vertices(inst->network).first, 0.0,
        std::plus<>(), [&](auto&& _dual, auto&& _u) {
            return _dual * inst->network[_u].capacity;
        });

    for (const auto ed : boost::make_iterator_range(edges(inst->network))) {
        retval +=
            m_linkCapasDuals[inst->network[ed].id] * inst->network[ed].capacity;
    }
    retval +=
        std::accumulate(m_onePathDuals.begin(), m_onePathDuals.end(), 0.0);
    retval +=
        std::accumulate(m_nbLicensesDuals.begin(), m_nbLicensesDuals.end(), 0.0)
        * inst->nbLicenses;
    retval += m_branchesDual;
    return retval;
}

void barycenter(const DualValues& _nDuals, const DualValues& _bDuals,
    DualValues& _sDuals, double _alpha) {
    const auto update = [_alpha](const double _a, const double _b) -> double {
        return _alpha * _a + ((1 - _alpha) * _b);
    };

    std::transform(_nDuals.m_linkCapasDuals.begin(),
        _nDuals.m_linkCapasDuals.end(), _bDuals.m_linkCapasDuals.begin(),
        _sDuals.m_linkCapasDuals.begin(), update);

    std::transform(_nDuals.m_nodeCapasDuals.begin(),
        _nDuals.m_nodeCapasDuals.end(), _bDuals.m_nodeCapasDuals.begin(),
        _sDuals.m_nodeCapasDuals.begin(), update);

    std::transform(_nDuals.m_onePathDuals.begin(), _nDuals.m_onePathDuals.end(),
        _bDuals.m_onePathDuals.begin(), _sDuals.m_onePathDuals.begin(), update);

    std::transform(_nDuals.m_pathFuncDuals.data(),
        _nDuals.m_pathFuncDuals.data() + _nDuals.m_pathFuncDuals.num_elements(),
        _bDuals.m_pathFuncDuals.data(), _sDuals.m_pathFuncDuals.data(), update);

    std::transform(_nDuals.m_nbLicensesDuals.begin(),
        _nDuals.m_nbLicensesDuals.end(), _bDuals.m_nbLicensesDuals.begin(),
        _sDuals.m_nbLicensesDuals.begin(), update);

    _sDuals.m_branchesDual =
        update(_nDuals.m_branchesDual, _bDuals.m_branchesDual);
}

std::ostream& operator<<(std::ostream& _out, const DualValues& _dualValues) {
    return _out << "{lc: "
                << std::accumulate(_dualValues.m_linkCapasDuals.begin(),
                       _dualValues.m_linkCapasDuals.end(), 0.0)
                << ", nc: "
                << std::accumulate(_dualValues.m_nodeCapasDuals.begin(),
                       _dualValues.m_nodeCapasDuals.end(), 0.0)
                << ", pf: "
                << std::accumulate(_dualValues.m_pathFuncDuals.data(),
                       _dualValues.m_pathFuncDuals.data()
                           + _dualValues.m_pathFuncDuals.num_elements(),
                       0.0)
                << ", op: "
                << std::accumulate(_dualValues.m_onePathDuals.begin(),
                       _dualValues.m_onePathDuals.end(), 0.0)
                << ", nl: "
                << std::accumulate(_dualValues.m_nbLicensesDuals.begin(),
                       _dualValues.m_nbLicensesDuals.end(), 0.0)
                << ", br: " << _dualValues.m_branchesDual << '}';
}

} // namespace SFC::Partial::OccLim::PathDecompositionRestricted
