#include "Occlim.hpp"

namespace SFC::Partial::OccLim {
Instance::Instance(Network _network, std::vector<Demand> _demands,
    std::vector<double> _funcCharge, int _nbLicenses)
    : SFC::Partial::Instance(_network, _demands, _funcCharge)
    , nbLicenses(_nbLicenses) {}

Instance::Instance(SFC::Partial::Instance _instance, int _nbLicenses)
    : SFC::Partial::Instance(std::move(_instance))
    , nbLicenses(_nbLicenses) {}

Instance loadInstance(std::ifstream& _ifs, int _nbLicenses) {
    return {SFC::Partial::loadInstance(_ifs), _nbLicenses};
}

bool isValid(
    const boost::multi_array<bool, 2>& _locations, const Instance& _inst) {
    for (int f = 0; f < _inst.funcCharge.size(); ++f) {
        int nbLicenses = 0;
        for (int u = 0; u < num_vertices(_inst.network); ++u) {
            if (_locations[u][f]) {
                nbLicenses++;
            }
        }
        if (nbLicenses > _inst.nbLicenses) {
            return false;
        }
    }
    return true;
}

boost::multi_array<bool, 2> getLocations(const Solution& _sol) {
    boost::multi_array<bool, 2> retval(boost::extents[num_vertices(
        _sol.getInstance().network)][_sol.getInstance().funcCharge.size()]);
    std::fill_n(retval.data(), retval.num_elements(), false);
    for (const auto& [nPath, locations, function_indexes, demandID] :
        _sol.getServicePaths()) {
        assert(locations.size() == function_indexes.size());
        assert(demandID < _sol.getInstance().demands.size());
        const auto& demand = _sol.getInstance().demands[demandID];
        for (int j = 0; j < locations.size(); ++j) {
            const auto u = locations[j];
            const auto f = demand.functions.getFunction(function_indexes[j]);
            retval[u][f] = true;
        }
    }
    return retval;
}

} // namespace SFC::Partial::OccLim
