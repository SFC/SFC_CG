#include "Algorithm.hpp"

#include <random>

namespace SFC::Partial::OccLim {
std::optional<Solution> path_rounding(
    const Instance& _inst, double _alpha, int _nbThreads, bool _verbose) {
    using MasterRelaxed = PathDecomposition::Master;
    using PricingRelaxed = SFC::Partial::RCShortestPathPerDemand;

<<<<<<< Updated upstream
    using MasterRestricted = SFC::Partial::PathDecomposition::Master;
    using PricingRestricted = SFC::Partial::Fixed::RCShortestPathPerDemand;

    return rounding<MasterRelaxed, PricingRelaxed, MasterRestricted,
        PricingRestricted>(_inst, _alpha, _nbThreads, _verbose);
}

} // namespace SFC::Partial::OccLim
=======
std::optional<Solution> greedy(const SFC::Partial::OccLim::Instance& _inst) {
    std::cout << "Solving with greedy\n";
    auto network = createNetwork(_inst);
    std::vector<ServicePath> paths(_inst.demands.size());
    double objValue = 0.0;
    int i = 0;
    for (const auto& demand : _inst.demands) {
        std::cout << i << '/' << _inst.demands.size() << std::flush;
        auto greedyPath = findRoute(demand, network, _inst);
        if (greedyPath && isAvailable(*greedyPath, demand, network, _inst)) {
            route(*greedyPath, demand, network, _inst);
            paths[demand.id] = std::move(*greedyPath);
            objValue += (paths[demand.id].nPath.size() - 1) * demand.d;
        } else {
            std::cout << "Could not find path for " << demand.id << '\n';
            return {};
        }
        ++i;
        std::cout << '\r' << std::flush;
    }
    return Solution{
        _inst, objValue, -std::numeric_limits<double>::infinity(), paths};
}

std::optional<Solution> rounding(const SFC::Partial::OccLim::Instance& _inst,
    const RelaxedSolution& _relaxedSol, const Parameters& _params) {
    std::cout << "Rounding relaxed solution\n";
    std::optional<Solution> bestSol;
    std::seed_seq seed_s = {0};
    std::mt19937 randGen(seed_s);
    for (int k = 0; k < _params.nbIterations; ++k) {
        // Could be init outside of loop
        auto network = createNetwork(_inst);
        // Get locations of function
        const auto locations = [&] {
            auto loc = getLocations(_relaxedSol.locations, randGen);
            while (!isValid(loc, _inst)) {
                loc = getLocations(_relaxedSol.locations, randGen);
            }
            return loc;
        }();
        for (int u = 0; u < num_vertices(_inst.network); ++u) {
            for (int f = 0; f < _inst.funcCharge.size(); ++f) {
                network[u].installedFunctions[f] = locations[u][f];
            }
        }
        std::fill(network[boost::graph_bundle].remainingLicenses.begin(),
            network[boost::graph_bundle].remainingLicenses.end(), 0);
        std::vector<ServicePath> paths(_inst.demands.size());
        double objValue = 0.0;
        std::vector<double> weights;
        auto sortedDemands = _inst.demands;
        std::shuffle(sortedDemands.begin(), sortedDemands.end(), randGen);
        bool isValid = true;
        for (const auto& demand : sortedDemands) {
            const auto& wPath = _relaxedSol.paths[demand.id];
            weights.resize(wPath.size());
            std::transform(wPath.begin(), wPath.end(), weights.begin(),
                [](const auto& _valued) {
                    return std::clamp(_valued.value, 0.01, 0.99);
                });
            for (int l = 0; l < weights.size(); ++l) {
                std::discrete_distribution<> distr{
                    weights.begin(), weights.end()};
                const auto index = distr(randGen);
                const auto& path = wPath[index].obj;
                if (isAvailable(path, demand, network, _inst)) {
                    route(path, demand, network, _inst);
                    paths[demand.id] = std::move(path);
                    objValue += (paths[demand.id].nPath.size() - 1) * demand.d;
                    break;
                } else {
                    weights[index] = 0.0;
                }
            }
        }

        for (const auto& demand : sortedDemands) {
            if (paths[demand.id].nPath.empty()) {
                auto greedyPath = findRoute(demand, network, _inst);
                if (greedyPath
                    && isAvailable(*greedyPath, demand, network, _inst)) {
                    route(*greedyPath, demand, network, _inst);
                    paths[demand.id] = std::move(*greedyPath);
                    objValue += (paths[demand.id].nPath.size() - 1) * demand.d;
                } else {
                    isValid = false;
                    break;
                }
                /*isValid = false;
                break;*/
            }
        }
        if (isValid) {
            if (!bestSol) {
                bestSol = {_inst, objValue,
                    -std::numeric_limits<double>::infinity(), paths};
            } else if (bestSol->getObjValue() > objValue) {
                bestSol->setObjValue(objValue);
                bestSol->setPaths(paths);
            }
        }
    }
    return bestSol;
}

GreedyNetwork createNetwork(const SFC::Partial::OccLim::Instance& _inst) {
    std::vector<Link> edgesProp;
    std::vector<std::pair<int, int>> edgeList;
    edgeList.reserve(num_edges(_inst.network));
    for (const auto ed : boost::make_iterator_range(edges(_inst.network))) {
        edgeList.push_back(incident(ed, _inst.network));
        edgesProp.push_back(
            Link{_inst.network[ed].id, _inst.network[ed].capacity});
    }
    GraphProperties gProp{
        std::vector<int>(_inst.funcCharge.size(), _inst.nbLicenses)};
    GreedyNetwork retval(edgeList.begin(), edgeList.end(), edgesProp.begin(),
        num_vertices(_inst.network), 0, std::move(gProp));
    for (const auto vd : boost::make_iterator_range(vertices(_inst.network))) {
        retval[vd] = {_inst.network[vd].id,
            boost::dynamic_bitset<>(_inst.funcCharge.size()),
            _inst.network[vd].capacity};
    }
    return retval;
}

bool isAvailable(const ServicePath& _path, const Demand& _demand,
    const GreedyNetwork& _network, const Instance& _inst) {
    std::vector<double> linkUsage(num_edges(_network), 0.0);
    std::vector<double> nodeUsage(num_vertices(_network), 0.0);

    for (auto iteU = _path.nPath.begin(), iteV = std::next(iteU);
         iteV != _path.nPath.end(); ++iteU, ++iteV) {
        const auto ed = edge(*iteU, *iteV, _network).first;
        linkUsage[_network[ed].id] += _demand.d;
    }
    for (int j = 0; j < _path.locations.size(); ++j) {
        const auto u = _path.locations[j];
        const auto f = _demand.functions.getFunction(j);
        nodeUsage[_network[u].id] += _inst.getNbCores(f, _demand.d);
        if (!_network[u].installedFunctions[f]
            && _network[boost::graph_bundle].remainingLicenses[f] == 0) {
            //			std::cout << "Cannot install "<<f<<" on "<<u<<'\n';
            return false;
        }
    }
    for (const auto ed : boost::make_iterator_range(edges(_network))) {
        if (_network[ed].residualCapacity < linkUsage[_network[ed].id]) {
            return false;
        }
    }
    for (const auto vd : boost::make_iterator_range(vertices(_network))) {
        if (_network[vd].residualCapacity < nodeUsage[_network[vd].id]) {
            return false;
        }
    }
    return true;
}

bool route(const ServicePath& _path, const Demand& _demand,
    GreedyNetwork& _network, const Instance& _inst) {
    for (auto iteU = _path.nPath.begin(), iteV = std::next(iteU);
         iteV != _path.nPath.end(); ++iteU, ++iteV) {
        const auto ed = edge(*iteU, *iteV, _network).first;
        _network[ed].residualCapacity -= _demand.d;
    }
    for (int j = 0; j < _path.locations.size(); ++j) {
        const auto u = _path.locations[j];
        const auto f = _demand.functions.getFunction(j);
        _network[u].residualCapacity -= _inst.getNbCores(f, _demand.d);
        if (!_network[u].installedFunctions[f]) {
            --_network[boost::graph_bundle].remainingLicenses[f];
            _network[u].installedFunctions[f] = true;
        }
    }
    return true;
}

std::optional<ServicePath> findRoute(const Demand& _demand,
    const GreedyNetwork& _network, const Instance& _inst) {
    GreedyRCShortestPathPerDemand rc(_inst, _network);
    rc.setDemand(_demand);
    const auto& updateWeight = [&](const auto& ed, const auto& _graph) {
        if (_graph[ed].isIntraLayer()) {
            const auto& origGreedyLink = _network[edge(
                _graph[ed].original.first, _graph[ed].original.second, _network)
                                                      .first];
            const auto& originalLink =
                _inst.network[edge(_graph[ed].original.first,
                    _graph[ed].original.second, _inst.network)
                                  .first];
            return 1
                   + (originalLink.capacity - origGreedyLink.residualCapacity
                         + _demand.d)
                         / originalLink.capacity;
        } else {
            const auto [u, v] = incident(ed, _graph);
            const auto& originalNode = _inst.network[_graph[u].original];
            const auto f =
                _demand.functions.getFunction(_graph[ed].function_index);
            return 1
                   + (originalNode.capacity
                         - _network[originalNode.id].residualCapacity
                         + _inst.getNbCores(f, _demand.d))
                         / originalNode.capacity;
        }
    };
    rc.updateWeight(updateWeight);
    if (rc.solve()) {
        return rc.getPath();
    }
    return {};
}

GreedyRCShortestPathPerDemand::GreedyRCShortestPathPerDemand(
    const Instance& _inst, const GreedyNetwork& _network)
    : m_inst(&_inst)
    , m_network(&_network)
    , n(num_vertices(m_inst->network))
    , m(num_edges(m_inst->network))
    , nbLayers(m_inst->maxChainSize + 1)
    , m_layeredGraph(getLayeredGraph(*m_inst))
    , m_containers(*m_network, m_inst->demands.front(), *m_inst) {}

void GreedyRCShortestPathPerDemand::setDemand(const Demand& _demand) {
    m_demand = &_demand;
    m_demandFilter.m_demand = m_demand;
}

bool GreedyRCShortestPathPerDemand::solve() {
    m_paths.clear();
    const int dest = n * (m_demand->functions.size()) + m_demand->t;
    boost::filtered_graph<LayeredGraph, DemandFilter, DemandFilter> g(
        m_layeredGraph, m_demandFilter, m_demandFilter);
    assert(std::find(vertices(g).first, vertices(g).second, dest)
           != vertices(g).second);

    r_c_shortest_paths(g, get(&SFC::Partial::Node::index, g),
        get(&SFC::Partial::Link::index, g), m_demand->s, dest, m_paths,
        m_containers,
        ResourceContainer{
            *m_network,
            *m_demand,
            *m_inst,
        },
        [&](const auto& _g, ResourceContainer& _newContainer,
            const ResourceContainer& _oldContainer,
            auto _ed) { // extension predicate
            if (_oldContainer.canExtend(_g[_ed])) {
                _newContainer = std::move(_oldContainer.extend(_g[_ed]));
                return true;
            }
            return false;
        },
        [&](const ResourceContainer& _rc1,
            const ResourceContainer& _rc2) { // dominance predicate
            return dominance(_rc1, _rc2);
        });
    m_objValue += m_containers.getWeight();
    if (m_paths.empty()) {
        return false;
    }
    return true;
}

double GreedyRCShortestPathPerDemand::getObjValue() const { return m_objValue; }

ServicePath GreedyRCShortestPathPerDemand::getPath() const {
    boost::filtered_graph<LayeredGraph, DemandFilter, DemandFilter> g(
        m_layeredGraph, m_demandFilter, m_demandFilter);
    return SFC::Partial::getServicePath(
        *m_demand, m_paths, m_containers.getFunctionIndexes(), g);
}

bool dominance(const ResourceContainer& _rc1, const ResourceContainer& _rc2) {
    return _rc1.usedFunction == _rc2.usedFunction && _rc1 < _rc2;
    /*&& (epsilon_less<double>()(_rc1.totalWeight, _rc2.totalWeight)
       || (epsilon_equal<double>()(
                                _rc1.totalWeight, _rc2.totalWeight)
                  && _rc1.nbHops < _rc2.nbHops));*/
}

bool operator<(const ResourceContainer& _lhs, const ResourceContainer& _rhs) {
    return std::tie(_lhs.totalWeight, _lhs.nbHops)
           < std::tie(_rhs.totalWeight, _rhs.nbHops);
}
} // namespace SFC::Partial::OccLim::Rounding
>>>>>>> Stashed changes
