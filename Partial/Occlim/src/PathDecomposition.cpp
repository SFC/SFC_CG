#include <boost/multi_array/base.hpp>
#include <ilconcert/iloexpression.h>
#include <ilconcert/ilolinear.h>
#include <ilconcert/ilosys.h>
#include <iostream>

#include "Decomposition.hpp"
#include "PathDecomposition.hpp"
#include <boost/format.hpp>
#include <range/v3/all.hpp>

namespace SFC::Partial::OccLim::PathDecomposition {

Master::Master(const Instance& _inst)
    : Base(DualValues(_inst))
    , m_inst(&_inst)
    , m_b([&]() {
        boost::multi_array<IloNumVar, 2> b(boost::extents[num_vertices(
            m_inst->network)][m_inst->funcCharge.size()]);
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            for (int u = 0; u < num_vertices(m_inst->network); ++u) {
                b[u][f] = IloNumVar(m_env);
                IloAdd(m_integerModel, IloConversion(m_env, b[u][f], ILOBOOL));
                IloAdd(m_model, b[u][f]);
                setIloName(b[u][f], "b[u=" + std::to_string(u)
                                        + ", f=" + std::to_string(f) + "]");
            }
        }
        return b;
    }())
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_onePathCons(getOnePathPerDemandConstraints(*m_inst, m_model))
    , m_linkCapasCons(getLinkCapacityConstraints(*m_inst, m_model))
    , m_nodeCapasCons(getNodeCapacityConstraints(*m_inst, m_model))
    , m_nbLicensesCons([&]() {
        std::vector<IloRange> funcLicensesCons(m_inst->funcCharge.size());
        const auto allDemandsRange = boost::multi_array_types::index_range(
            0, num_vertices(m_inst->network));

        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            const auto allNode_f = m_b[boost::indices[allDemandsRange][f]];
            funcLicensesCons[f] =
                IloAdd(m_model, IloRange(m_env, -IloInfinity,
                                    std::accumulate(allNode_f.begin(),
                                        allNode_f.end(), IloExpr(m_env)),
                                    m_inst->nbLicenses));
            setIloName(
                funcLicensesCons[f], "funcLicensesCons(" + toString(f) + ")");
            // vars.end();
        }
        return funcLicensesCons;
    }())
    , m_pathFuncCons([&]() {
        boost::multi_array<LazyConstraint, 3> pathFuncCons(
            boost::extents[m_inst->demands.size()][m_inst->maxChainSize]
                          [num_vertices(m_inst->network)]);

        for (int i = 0; i < m_inst->demands.size(); ++i) {
            for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                for (int u = 0; u < num_vertices(m_inst->network); ++u) {
                    pathFuncCons[i][j][u] = LazyConstraint(m_model,
                        -IloInfinity,
                        -m_b[u][m_inst->demands[i].functions.getFunction(j)],
                        0.0);
                    setIloName(pathFuncCons[i][j][u],
                        "pathFuncCons" + toString(std::make_tuple(i, j, u)));
                }
            }
        }
        return pathFuncCons;
    }())
    , m_dummyPaths([&]() {
        std::vector<IloNumVar> retval(m_inst->demands.size());
        for (int i = 0; i < retval.size(); ++i) {
            retval[i] = m_onePathCons[i](1.0)
                        + m_obj(10 * m_inst->demands[i].d
                                * num_vertices(m_inst->network));
            m_model.add(retval[i]);
            m_integerModel.add(retval[i] == 0);
        }
        return retval;
    }())
    , m_linkCharge(num_edges(m_inst->network))
    , m_nodeCharge(num_vertices(m_inst->network))
    , m_edgeUsage(
          boost::extents[m_inst->demands.size()][num_edges(m_inst->network)])
    , m_nodeUsage(
          boost::extents[m_inst->demands.size()][num_vertices(m_inst->network)])
    , m_licenseUsage(boost::extents[num_vertices(m_inst->network)]
                                   [m_inst->funcCharge.size()]) {
    m_solver.setOut(m_solver.getEnv().getNullStream());
    m_solver.setWarning(m_solver.getEnv().getNullStream());
}

double Master::getReducedCost_impl(
    const ServicePath& _col, const DualValues& _dualValues) const {
    const auto& [nPath, locations, function_indexes, demandID] = _col;
    double retval = _dualValues.getReducedCost(m_inst->demands[demandID]);

    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
         ++iteU, ++iteV) {
        retval += _dualValues.getReducedCost(IntraLayerLink{demandID,
            {*iteU, *iteV}, -1}); /// layer # is not use for the reduced cost
    }

    for (int j = 0; j < locations.size(); ++j) {
        retval += _dualValues.getReducedCost(
            CrossLayerLink{demandID, locations[j], j, function_indexes[j]});
    }
    return retval;
}

IloNumColumn Master::getColumn_impl(const ServicePath& _col) {
    const auto& [nPath, locations, function_indexes, demandID] = _col;
    const auto& [id, s, t, d, chain] = m_inst->demands[demandID];
    assert(isValid(_col, m_inst->demands[demandID]));

    IloNumColumn col =
        m_onePathCons[demandID](1.0) + m_obj(d * (nPath.size() - 1));

    std::fill(m_linkCharge.begin(), m_linkCharge.end(), 0.0);
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
         ++iteU, ++iteV) {
        m_linkCharge[m_inst->network[edge(*iteU, *iteV, m_inst->network).first]
                         .id] += d;
    }
    const auto mult = [](const IloRange& _const, double _val) {
        return _const(_val);
    };
    const auto add = [](auto&& _acc, auto&& _newVal) {
        return _acc += _newVal;
    };
    col += std::inner_product(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_linkCharge.begin(), IloNumColumn(m_env), add, mult);

    std::fill(m_nodeCharge.begin(), m_nodeCharge.end(), 0.0);
    for (int j = 0; j < locations.size(); ++j) {
        const auto f = chain.getFunction(function_indexes[j]);
        m_nodeCharge[locations[j]] += m_inst->getNbCores(f, d);
        col += m_pathFuncCons[demandID][function_indexes[j]][locations[j]](1.0);
    }
    col += std::inner_product(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_nodeCharge.begin(), IloNumColumn(m_env), add, mult);
    return col;
}

void Master::getDuals_impl() {
    const auto get = [&](auto&& _cons) { return getDual(m_solver, _cons); };

    std::transform(m_linkCapasCons.begin(), m_linkCapasCons.end(),
        m_duals.m_linkCapasDuals.begin(), get);

    std::transform(m_nodeCapasCons.begin(), m_nodeCapasCons.end(),
        m_duals.m_nodeCapasDuals.begin(), get);

    std::transform(m_onePathCons.begin(), m_onePathCons.end(),
        m_duals.m_onePathDuals.begin(), get);

    std::transform(m_pathFuncCons.data(),
        m_pathFuncCons.data() + m_pathFuncCons.num_elements(),
        m_duals.m_pathFuncDuals.data(), get);

    std::transform(m_nbLicensesCons.begin(), m_nbLicensesCons.end(),
        m_duals.m_nbLicensesDuals.begin(), get);
}

Solution Master::getSolution() const {
    return {*m_inst, getIntegerObjValue(), getRelaxationObjValue(),
        getServicePaths()};
}

void Master::removeDummyIfPossible() {
    if (m_dummyActive) {
        const double dummySum = std::accumulate(m_dummyPaths.begin(),
            m_dummyPaths.end(), 0.0, [&](auto&& _acc, auto&& _var) {
                return _acc + m_solver.getValue(_var);
            });
        if (epsilon_equal<double>()(dummySum, 0.0)) {
            m_dummyActive = false;
            // solve();
        }
    }
}

const DualValues& Master::getDualValues_impl() const { return m_duals; }

std::vector<ServicePath> Master::getServicePaths() const {
    std::vector<ServicePath> sPaths(m_inst->demands.size());
    for (const auto& [var, sPath] : getColumnStorage<ServicePath>()) {
        if (epsilon_equal<double>()(m_solver.getValue(var), IloTrue)) {
            sPaths[sPath.demand] = sPath;
        }
    }
    return sPaths;
}

boost::multi_array<double, 2> Master::getLocations() const {
    boost::multi_array<double, 2> retval(boost::extents[num_vertices(
        m_inst->network)][m_inst->funcCharge.size()]);
    std::transform(m_b.data(), m_b.data() + m_b.num_elements(), retval.data(),
        [&](auto&& _bVar) { return m_solver.getValue(_bVar); });
    return retval;
}

////////////////////////////////////////////////////
///                                              ///
///                  DualValues                  ///
///                                              ///
////////////////////////////////////////////////////

DualValues::DualValues(const Instance& _inst)
    : inst(&_inst)
    , m_onePathDuals(inst->demands.size(), 0.0)
    , m_linkCapasDuals(num_edges(inst->network), 0.0)
    , m_nodeCapasDuals(num_vertices(inst->network), 0.0)
    , m_pathFuncDuals(boost::extents[inst->demands.size()][inst->maxChainSize]
                                    [num_vertices(inst->network)])
    , m_nbLicensesDuals(inst->funcCharge.size(), 0.0) {}

IloNum DualValues::getReducedCost(const CrossLayerLink& _link) const {
    const auto demand = inst->demands[_link.demandID];
    const auto d = demand.d;
    const auto f = demand.functions.getFunction(_link.function_index);
    const auto rc =
        inst->getNbCores(f, d) * -m_nodeCapasDuals[_link.u]
        - m_pathFuncDuals[_link.demandID][_link.function_index][_link.u];
    assert([&]() {
        if (epsilon_less<double>()(0.0, rc)
            || epsilon_equal<double>()(0.0, rc)) {
            return true;
        }
        std::cout << rc << '\n';
        return false;
    }());
    return rc;
}

IloNum DualValues::getReducedCost(const IntraLayerLink& _link) const {
    const auto ed =
        edge(_link.edge.first, _link.edge.second, inst->network).first;
    const auto rc = inst->demands[_link.demandID].d
                    * (1 - m_linkCapasDuals[inst->network[ed].id]);
    assert([&]() {
        if (epsilon_less<double>()(0.0, rc)
            || epsilon_equal<double>()(0.0, rc)) {
            return true;
        }
        std::cout << "RC of " << _link << " -> "
                  << boost::format("%1% = %2% * (1 - %3%)\n") % rc
                         % inst->demands[_link.demandID].d
                         % m_linkCapasDuals[inst->network[ed].id];
        return false;
    }());
    return rc;
}

IloNum DualValues::getReducedCost(const Demand& _demand) const {
    const auto rc = -m_onePathDuals[_demand.id];
    assert([&]() {
        if (epsilon_less<double>()(rc, 0.0)
            || epsilon_equal<double>()(rc, 0.0)) {
            return true;
        }
        std::cout << rc << '\n';
        return false;
    }());
    return rc;
}

double DualValues::getDualSumRHS() const {
    // Node capa duals
    double retval = std::inner_product(m_nodeCapasDuals.begin(),
        m_nodeCapasDuals.end(), vertices(inst->network).first, 0.0,
        std::plus<>(), [&](auto&& dual, auto&& u) {
            return dual * inst->network[u].capacity;
        });

    // Link capa duals
    for (const auto ed : boost::make_iterator_range(edges(inst->network))) {
        retval +=
            m_linkCapasDuals[inst->network[ed].id] * inst->network[ed].capacity;
    }
    // one path duals
    retval +=
        std::accumulate(m_onePathDuals.begin(), m_onePathDuals.end(), 0.0);

    // licenses duals
    retval +=
        std::accumulate(m_nbLicensesDuals.begin(), m_nbLicensesDuals.end(), 0.0)
        * inst->nbLicenses;
    return retval;
}

/**
 * Produce the barycenter between _nDuals and _bDuals.
 * if _alpha == 0 -> _bDuals
 * if _alpha == 1 -> _nDuals
 */
void barycenter(const DualValues& _nDuals, const DualValues& _bDuals,
    DualValues& _sDuals, double _alpha) {
    const auto update = [_alpha](
                            const double _norma, const double _best) -> double {
        return _alpha * _norma + ((1 - _alpha) * _best);
    };
    std::transform(_nDuals.m_linkCapasDuals.begin(),
        _nDuals.m_linkCapasDuals.end(), _bDuals.m_linkCapasDuals.begin(),
        _sDuals.m_linkCapasDuals.begin(), update);

    std::transform(_nDuals.m_nodeCapasDuals.begin(),
        _nDuals.m_nodeCapasDuals.end(), _bDuals.m_nodeCapasDuals.begin(),
        _sDuals.m_nodeCapasDuals.begin(), update);

    std::transform(_nDuals.m_onePathDuals.begin(), _nDuals.m_onePathDuals.end(),
        _bDuals.m_onePathDuals.begin(), _sDuals.m_onePathDuals.begin(), update);

    std::transform(_nDuals.m_pathFuncDuals.data(),
        _nDuals.m_pathFuncDuals.data() + _nDuals.m_pathFuncDuals.num_elements(),
        _bDuals.m_pathFuncDuals.data(), _sDuals.m_pathFuncDuals.data(), update);

    std::transform(_nDuals.m_nbLicensesDuals.begin(),
        _nDuals.m_nbLicensesDuals.end(), _bDuals.m_nbLicensesDuals.begin(),
        _sDuals.m_nbLicensesDuals.begin(), update);
}

std::ostream& operator<<(std::ostream& _out, const DualValues& _dualValues) {
    return _out << "{lc: "
                << std::accumulate(_dualValues.m_linkCapasDuals.begin(),
                       _dualValues.m_linkCapasDuals.end(), 0.0)
                << ", op: "
                << std::accumulate(_dualValues.m_onePathDuals.begin(),
                       _dualValues.m_onePathDuals.end(), 0.0)
                << ", nc: "
                << std::accumulate(_dualValues.m_nodeCapasDuals.begin(),
                       _dualValues.m_nodeCapasDuals.end(), 0.0)
                << ", pf: "
                << std::accumulate(_dualValues.m_pathFuncDuals.data(),
                       _dualValues.m_pathFuncDuals.data()
                           + _dualValues.m_pathFuncDuals.num_elements(),
                       0.0)
                << ", nl: "
                << std::accumulate(_dualValues.m_nbLicensesDuals.begin(),
                       _dualValues.m_nbLicensesDuals.end(), 0.0)
                << '}';
}

} // namespace SFC::Partial::OccLim::PathDecomposition
