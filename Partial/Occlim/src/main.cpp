#include "Occlim.hpp"
#include <tclap/CmdLine.hpp>

#include "../../include/PathDecomposition.hpp"
#include "../../include/PathDecompositionBlock.hpp"
#include "Algorithm.hpp"
#include "BranchAndBound.hpp"
#include "ColumnGeneration.hpp"
#include "Occlim.hpp"
#include "PathDecomposition.hpp"
#include "PathDecompositionBlock.hpp"
#include "PathDecompositionRestricted.hpp"
#include "RCShortestPathPerDemand.hpp"

#include "MyTimer.hpp"
#include <boost/format.hpp>
#include <boost/log/utility/setup/file.hpp>

using Branch = std::variant<IloRange, SFC::Partial::FixedIntraLayerLink>;

template <>
struct BAB_traits<SFC::Partial::OccLim::PathDecompositionRestricted::Master> {
    using Solution = SFC::Partial::Solution;
    using Branch = std::variant<IloRange, SFC::Partial::FixedIntraLayerLink>;
};

template <>
<<<<<<< Updated upstream
std::vector<SFC::Partial::FixedIntraLayerLink> getBranchDecisionInTree<
    SFC::Partial::OccLim::PathDecompositionRestricted::Master,
    SFC::Partial::FixedIntraLayerLink>(
    SFC::Partial::OccLim::PathDecompositionRestricted::Master& _model) {
=======
std::vector<SFC::Partial::FixedIntraLayerLink>
getBranchDecisionInTree<SPO::PathDecompositionRestricted::Master,
    SFC::Partial::FixedIntraLayerLink>(
    SPO::PathDecompositionRestricted::Master& _model) {
>>>>>>> Stashed changes
    return _model.getBranchDecision<SFC::Partial::FixedIntraLayerLink>();
}

template <>
<<<<<<< Updated upstream
std::vector<SFC::Partial::FixedIntraLayerLink> getBranchDecisionAtRoot<
    SFC::Partial::OccLim::PathDecompositionRestricted::Master,
    SFC::Partial::FixedIntraLayerLink>(
    SFC::Partial::OccLim::PathDecompositionRestricted::Master& _model) {
=======
std::vector<SFC::Partial::FixedIntraLayerLink>
getBranchDecisionAtRoot<SPO::PathDecompositionRestricted::Master,
    SFC::Partial::FixedIntraLayerLink>(
    SPO::PathDecompositionRestricted::Master& _model) {
>>>>>>> Stashed changes
    return _model.getBranchDecision<SFC::Partial::FixedIntraLayerLink>();
}

template <>
struct PricingProblemTraits<SFC::Partial::Fixed::RCShortestPathBlock> {
    using Column = typename std::vector<SFC::Partial::ServicePath>;
    using IsUnique = std::true_type;
};

template <>
struct PricingProblemTraits<SFC::Partial::RCShortestPathBlock> {
    using Column = typename std::vector<SFC::Partial::ServicePath>;
    using IsUnique = std::true_type;
};

template <>
std::string getName<std::vector<SFC::Partial::ServicePath>>(
    const std::vector<SFC::Partial::ServicePath>& _sPath) {
    std::stringstream stream;
    stream << "set of sPaths(" << &_sPath << ')';
    return stream.str();
}

template <>
<<<<<<< Updated upstream
std::vector<IloRange> getBranchDecisionInTree<
    SFC::Partial::OccLim::PathDecompositionRestricted::Master, IloRange>(
    SFC::Partial::OccLim::PathDecompositionRestricted::Master& _model) {
    const auto br = _model.getLocationsBranches();
    if (br) {
        return *br;
=======
std::vector<std::variant<IloRange, SFC::Partial::FixedIntraLayerLink>>
getBranchDecisionInTree<SPO::PathDecompositionRestricted::Master,
    std::variant<IloRange, SFC::Partial::FixedIntraLayerLink>>(
    SPO::PathDecompositionRestricted::Master& _model) {
    const auto lbr = _model.getLocationsBranches();
    if (lbr) {
        return {lbr->begin(), lbr->end()};
    }
    const auto linkBr = _model.getLinkBranches();
    if (linkBr) {
        return {linkBr->begin(), linkBr->end()};
>>>>>>> Stashed changes
    }
    return {};
}

template <>
<<<<<<< Updated upstream
std::vector<IloRange> getBranchDecisionAtRoot<
    SFC::Partial::OccLim::PathDecompositionRestricted::Master, IloRange>(
    SFC::Partial::OccLim::PathDecompositionRestricted::Master& _model) {
    const auto br = _model.getLocationsBranches();
    if (br) {
        return *br;
=======
std::vector<std::variant<IloRange, SFC::Partial::FixedIntraLayerLink>>
getBranchDecisionAtRoot<SPO::PathDecompositionRestricted::Master,
    std::variant<IloRange, SFC::Partial::FixedIntraLayerLink>>(
    SPO::PathDecompositionRestricted::Master& _model) {
    const auto lbr = _model.getLocationsBranches();
    if (lbr) {
        return {lbr->begin(), lbr->end()};
    }
    const auto linkBr = _model.getLinkBranches();
    if (linkBr) {
        return {linkBr->begin(), linkBr->end()};
>>>>>>> Stashed changes
    }
    return {};
}

using Function = std::optional<SPO::Solution> (*)(const SPO::Instance&,
    const ColumnGenerationParameters&, const StabilizationParameters&,
    const BaseBranchAndPriceParameters&);

int main(int argc, char** argv) {
    std::cout << std::setprecision(10);
    std::cerr << std::setprecision(10);
<<<<<<< Updated upstream
    using Function = std::optional<SFC::Partial::OccLim::Solution> (*)(
        const SFC::Partial::OccLim::Instance&, double, int, bool);

    const std::map<std::string, Function> dispatchMap{{
        {"path",
            +[](const SFC::Partial::OccLim::Instance& _inst, double /*_alpha*/,
                 int _nbThreads, bool _verbose)
                -> std::optional<SFC::Partial::OccLim::Solution> {
                using Master = SFC::Partial::OccLim::PathDecomposition::Master;
                using Pricing = SFC::Partial::RCShortestPathPerDemand;

                Master rmp(_inst);
                if (solveStabilized<Master, Pricing>(
                        rmp, Pricing(_inst), _nbThreads, _verbose, true)) {
                    return rmp.getSolution();
                }
                return std::nullopt;
            }},
        {"path_rounding", &SFC::Partial::OccLim::path_rounding},
        {"block_rounding",
            +[](const SFC::Partial::OccLim::Instance& _inst, double _alpha,
                 int _nbThreads, bool _verbose) {
                using MasterRelaxed =
                    SFC::Partial::OccLim::PathDecompositionBlock::Master;
                using PricingRelaxed = SFC::Partial::RCShortestPathBlock;
                using MasterRestricted =
                    SFC::Partial::PathDecompositionBlock::Master;
                using PricingRestricted =
                    SFC::Partial::Fixed::RCShortestPathBlock;

                return SFC::Partial::OccLim::rounding<MasterRelaxed,
                    PricingRelaxed, MasterRestricted, PricingRestricted>(
                    _inst, _alpha, _nbThreads, _verbose);
            }},
        {"path_restr",
            +[](const SFC::Partial::OccLim::Instance& _inst, double /*_alpha*/,
                 int _nbThreads, bool _verbose)
                -> std::optional<SFC::Partial::OccLim::Solution> {
                using Master =
                    SFC::Partial::OccLim::PathDecompositionRestricted::Master;
                using Pricing = SFC::Partial::RCShortestPathPerDemandRestricted;

                SFC::Partial::FixedLinks fixedLinks;
                Master rmp(_inst);
                if (solve<Master, Pricing>(rmp, Pricing(_inst, fixedLinks),
                        _nbThreads, _verbose, true)) {
                    return rmp.getSolution();
                }
                return std::nullopt;
            }},
        {"bab_pricing",
            +[](const SFC::Partial::OccLim::Instance& _inst, double /*_alpha*/,
                 int _nbThreads, bool _verbose)
                -> std::optional<SFC::Partial::OccLim::Solution> {
                using Master =
                    SFC::Partial::OccLim::PathDecompositionRestricted::Master;
                using Pricing = SFC::Partial::RCShortestPathPerDemandRestricted;

                SFC::Partial::FixedLinks fixedLinks;
                Master rmp(_inst);

                if (branchAndPrice<Master, SFC::Partial::FixedLinks, Pricing>(
                        rmp, fixedLinks, Pricing(_inst, fixedLinks), _nbThreads,
                        _verbose)) {
                    return rmp.getSolution();
                }
                return std::nullopt;
            }},
        {"bab_master",
            +[](const SFC::Partial::OccLim::Instance& _inst, double /*_alpha*/,
                 int _nbThreads, bool _verbose)
                -> std::optional<SFC::Partial::OccLim::Solution> {
                using Master =
                    SFC::Partial::OccLim::PathDecompositionRestricted::Master;
                using Pricing = SFC::Partial::RCShortestPathPerDemand;

                Master rmp(_inst);
                const auto solveFunc = [](auto&& _rmp, auto&& _base,
                                           auto&& __verbose) {
                    return solve_impl(_rmp, _base, __verbose);
                };

                const auto sol = branchAndPriceOnMaster<Master,
                    decltype(solveFunc), Pricing>(
                    rmp, Pricing(_inst), solveFunc, _nbThreads, _verbose);
                if (sol && !isValid(*sol, _inst)) {
                    exit(66);
                }
                return sol;
            }},
        {"bab_master_stab",
            +[](const SFC::Partial::OccLim::Instance& _inst, double _alpha,
                 int _nbThreads, bool _verbose)
                -> std::optional<SFC::Partial::OccLim::Solution> {
                using Master =
                    SFC::Partial::OccLim::PathDecompositionRestricted::Master;
                using Pricing = SFC::Partial::RCShortestPathPerDemand;

                Master rmp(_inst);
                auto bestDual = rmp.getDualValues();
                const auto solveFunc = [&](auto&& _rmp, auto&& _base,
                                           auto&& __verbose) {
                    bool res = solveStabilized_impl(
                        _rmp, _base, bestDual, _alpha, __verbose);
                    bestDual.clear();
                    return res;
                };

                const auto sol = branchAndPriceOnMaster<Master,
                    decltype(solveFunc), Pricing>(
                    rmp, Pricing(_inst), solveFunc, _nbThreads, _verbose);
                if (sol && !isValid(*sol, _inst)) {
                    exit(66);
                }
                return sol;
            }},
        {"path_stab",
            +[](const SFC::Partial::OccLim::Instance& _inst, double _alpha,
                 int _nbThreads, bool _verbose)
                -> std::optional<SFC::Partial::OccLim::Solution> {
                using Master = SFC::Partial::OccLim::PathDecomposition::Master;
                using Pricing = SFC::Partial::RCShortestPathPerDemand;

                SFC::Partial::OccLim::PathDecomposition::Master rmp(_inst);
                if (solveStabilized<Master, Pricing>(rmp, Pricing(_inst),
                        _nbThreads, _alpha, _verbose, true)) {
                    return rmp.getSolution();
                }
                return std::nullopt;
            }}
        /*,
        {"bab", +[](const SFC::Partial::OccLim::Instance& _inst, int
        _nbThreads, bool _verbose) {
            ColumnGenerationModel<SFC::Partial::OccLim::Instance,
        SFC::Partial::OccLim::PathDecomposition::Master> cgModel(_inst);
            return bab_dfs(cgModel, _nbThreads, _verbose, false);
        }}*/
    }};

    const auto [topologyFilename, model, nbLicenses, alpha, nbThreads,
        demandFactor, percentDemand, verbose] = [&]() {
=======

    const std::map<std::string, Function> dispatchMap{
        {{"path",
             +[](const SPO::Instance& _inst,
                  const ColumnGenerationParameters& /*_cgParams*/,
                  const StabilizationParameters& _stabParams,
                  const BaseBranchAndPriceParameters
                      & /*_bapParams*/) -> std::optional<SPO::Solution> {
                 using Master = SPO::PathDecomposition::Master;
                 using Pricing = SFC::Partial::RCShortestPathPerDemand;

                 Master rmp(_inst);
                 if (solveStabilized<Master, Pricing>(
                         rmp, Pricing(_inst), _stabParams)) {
                     return rmp.getSolution();
                 }
                 return std::nullopt;
             }},
            {"path_restr",
                +[](const SPO::Instance& _inst,
                     const ColumnGenerationParameters& _cgParams,
                     const StabilizationParameters& /*_stabParams*/,
                     const BaseBranchAndPriceParameters
                         & /*_bapParams*/) -> std::optional<SPO::Solution> {
                    using Master = SPO::PathDecompositionRestricted::Master;
                    using Pricing =
                        SFC::Partial::RCShortestPathPerDemandRestricted;

                    SFC::Partial::FixedLinks fixedLinks;
                    Master rmp(_inst);
                    if (solve<Master, Pricing>(
                            rmp, Pricing(_inst, fixedLinks), _cgParams)) {
                        return rmp.getSolution();
                    }
                    return std::nullopt;
                }},
            {"bab_deep_master_lp",
                +[](const SPO::Instance& _inst,
                     const ColumnGenerationParameters& _cgParams,
                     const StabilizationParameters& /*_stabParams*/,
                     const BaseBranchAndPriceParameters& _bapParams)
                    -> std::optional<SPO::Solution> {
                    using Master = SPO::PathDecompositionRestricted::Master;
                    using Pricing = SFC::Partial::PartialShortestPath_LP;

                    Master rmp(_inst);
                    const auto solveFunc = [&](auto&& _rmp, auto&& _base,
                                               bool isRoot) {
                        if (isRoot) {
                            return solve_impl(_rmp, _base, _cgParams);
                        } else {
                            auto params = _cgParams;
                            params.verbose = false;
                            return solve_impl(_rmp, _base, params);
                        }
                    };
                    const auto getUpperBoudFromRounding =
                        [&](const Master& _rmp) {
                            const auto relaxedSolution =
                                _rmp.getRelaxedSolution();
                            return SPO::Rounding::rounding(
                                _inst, rmp.getRelaxedSolution(), {100, true});
                        };
                    const auto sol = branchAndPriceOnMaster<Master,
                        decltype(solveFunc), decltype(getUpperBoudFromRounding),
                        DeepestBestBoundExplorationPolicy, Branch, Pricing>(rmp,
                        Pricing(_inst), solveFunc, getUpperBoudFromRounding,
                        _bapParams);
                    if (sol && !isValid(*sol, _inst)) {
                        exit(66);
                    }
                    return sol;
                }},
            {"bab_deep_master",
                +[](const SPO::Instance& _inst,
                     const ColumnGenerationParameters& _cgParams,
                     const StabilizationParameters& /*_stabParams*/,
                     const BaseBranchAndPriceParameters& _bapParams)
                    -> std::optional<SPO::Solution> {
                    using Master = SPO::PathDecompositionRestricted::Master;
                    using Pricing = SFC::Partial::RCShortestPathPerDemand;

                    Master rmp(_inst);
                    const auto solveFunc = [&](auto&& _rmp, auto&& _base,
                                               bool isRoot) {
                        if (isRoot) {
                            bool foundRows;
                            do {
                                foundRows = false;
                                solve_impl(_rmp, _base, _cgParams);
                                while (rmp.generatePathFuncCuts()) {
                                    foundRows = true;
                                    _rmp.solve();
                                }
                            } while (foundRows);
                            return true;
                        } else {
                            auto params = _cgParams;
                            params.verbose = false;
                            do {
                                solve_impl(_rmp, _base, params);
                            } while (rmp.generatePathFuncCuts());
                            return true;
                        }
                    };
                    const auto getUpperBoudFromRounding =
                        [&](const Master& _rmp) {
                            const auto relaxedSolution =
                                _rmp.getRelaxedSolution();
                            return SPO::Rounding::rounding(
                                _inst, rmp.getRelaxedSolution(), {1, true});
                        };
                    const auto sol = branchAndPriceOnMaster<Master,
                        decltype(solveFunc), decltype(getUpperBoudFromRounding),
                        DeepestBestBoundExplorationPolicy, Branch, Pricing>(rmp,
                        Pricing(_inst), solveFunc, getUpperBoudFromRounding,
                        _bapParams);
                    if (sol && !isValid(*sol, _inst)) {
                        exit(66);
                    }
                    return sol;
                }},
            {"bab_best_master",
                +[](const SPO::Instance& _inst,
                     const ColumnGenerationParameters& _cgParams,
                     const StabilizationParameters& /*_stabParams*/,
                     const BaseBranchAndPriceParameters& _bapParams)
                    -> std::optional<SPO::Solution> {
                    using Master = SPO::PathDecompositionRestricted::Master;
                    using Pricing = SFC::Partial::RCShortestPathPerDemand;

                    Master rmp(_inst);
                    const auto solveFunc = [&](auto&& _rmp, auto&& _base,
                                               bool isRoot) {
                        if (isRoot) {
                            bool foundRows;
                            do {
                                foundRows = false;
                                solve_impl(_rmp, _base, _cgParams);
                                while (rmp.generatePathFuncCuts()) {
                                    foundRows = true;
                                    _rmp.solve();
                                }
                            } while (foundRows);
                            return true;
                        } else {
                            auto params = _cgParams;
                            params.verbose = false;
                            do {
                                solve_impl(_rmp, _base, params);
                            } while (rmp.generatePathFuncCuts());
                            return true;
                        }
                    };

                    const auto getUpperBoudFromRounding =
                        [&](const Master& _rmp) {
                            const auto relaxedSolution =
                                _rmp.getRelaxedSolution();
                            return SPO::Rounding::rounding(
                                _inst, rmp.getRelaxedSolution(), {100, true});
                        };
                    const auto sol = branchAndPriceOnMaster<Master,
                        decltype(solveFunc), decltype(getUpperBoudFromRounding),
                        BestBoundExplorationPolicy, Branch, Pricing>(rmp,
                        Pricing(_inst), solveFunc, getUpperBoudFromRounding,
                        _bapParams);
                    if (sol && !isValid(*sol, _inst)) {
                        exit(66);
                    }
                    return sol;
                }},
            {"bab_best_master_stab",
                +[](const SPO::Instance& _inst,
                     const ColumnGenerationParameters& /*_cgParams*/,
                     const StabilizationParameters& _stabParams,
                     const BaseBranchAndPriceParameters& _bapParams)
                    -> std::optional<SPO::Solution> {
                    using Master = SPO::PathDecompositionRestricted::Master;
                    using Pricing = SFC::Partial::RCShortestPathPerDemand;

                    Master rmp(_inst);
                    auto bestDual = rmp.getDualValues();
                    const auto solveFunc = [&](auto&& _rmp, auto&& _base,
                                               auto&& _isRoot) {
                        bool res;
                        if (_isRoot) {
                            res = solveStabilized_impl(
                                _rmp, _base, bestDual, _stabParams);
                        } else {
                            auto params = _stabParams;
                            params.verbose = false;
                            res = solveStabilized_impl(
                                _rmp, _base, bestDual, params);
                        }
                        bestDual.clear();
                        return res;
                    };

                    const auto getUpperBoudFromRounding =
                        [&](const Master& _rmp) {
                            const auto relaxedSolution =
                                _rmp.getRelaxedSolution();
                            return SPO::Rounding::rounding(
                                _inst, rmp.getRelaxedSolution(), {100, true});
                        };
                    const auto sol = branchAndPriceOnMaster<Master,
                        decltype(solveFunc), decltype(getUpperBoudFromRounding),
                        BestBoundExplorationPolicy, Branch, Pricing>(rmp,
                        Pricing(_inst), solveFunc, getUpperBoudFromRounding,
                        _bapParams);
                    if (sol && !isValid(*sol, _inst)) {
                        exit(66);
                    }
                    return sol;
                }},
            {"bab_deep_master_stab",
                +[](const SPO::Instance& _inst,
                     const ColumnGenerationParameters& /*_cgParams*/,
                     const StabilizationParameters& _stabParams,
                     const BaseBranchAndPriceParameters& _bapParams)
                    -> std::optional<SPO::Solution> {
                    using Master = SPO::PathDecompositionRestricted::Master;
                    using Pricing = SFC::Partial::RCShortestPathPerDemand;

                    Master rmp(_inst);
                    auto bestDual = rmp.getDualValues();
                    const auto solveFunc = [&](auto&& _rmp, auto&& _base,
                                               auto&& _isRoot) {
                        bool res;
                        if (_isRoot) {
                            res = solveStabilized_impl(
                                _rmp, _base, bestDual, _stabParams);
                        } else {
                            auto params = _stabParams;
                            params.verbose = false;
                            res = solveStabilized_impl(
                                _rmp, _base, bestDual, params);
                        }
                        bestDual.clear();
                        return res;
                    };

                    const auto getUpperBoudFromRounding =
                        [&](const Master& _rmp) {
                            const auto relaxedSolution =
                                _rmp.getRelaxedSolution();
                            return SPO::Rounding::rounding(
                                _inst, rmp.getRelaxedSolution(), {100, true});
                        };
                    const auto sol = branchAndPriceOnMaster<Master,
                        decltype(solveFunc), decltype(getUpperBoudFromRounding),
                        DeepestBestBoundExplorationPolicy, Branch, Pricing>(rmp,
                        Pricing(_inst), solveFunc, getUpperBoudFromRounding,
                        _bapParams);
                    if (sol && !isValid(*sol, _inst)) {
                        exit(66);
                    }
                    return sol;
                }},
            {"path_stab",
                +[](const SPO::Instance& _inst,
                     const ColumnGenerationParameters& /*_cgParams*/,
                     const StabilizationParameters& _stabParams,
                     const BaseBranchAndPriceParameters
                         & /*_bapParams*/) -> std::optional<SPO::Solution> {
                    using Master = SPO::PathDecomposition::Master;
                    using Pricing = SFC::Partial::RCShortestPathPerDemand;

                    SPO::PathDecomposition::Master rmp(_inst);
                    if (solveStabilized<Master, Pricing>(
                            rmp, Pricing(_inst), _stabParams)) {
                        return rmp.getSolution();
                    }
                    return std::nullopt;
                }}}};

    const auto [topologyFilename, model, nbLicenses, alpha, nbThreads,
        demandFactor, percentDemand, verbose, timeout, gapEps] = [&]() {
>>>>>>> Stashed changes
        TCLAP::CmdLine cmd("Solve the partial SFC provisioning problem with "
                           "limited number of licenses",
            ' ', "0.1");

        TCLAP::UnlabeledValueArg<std::string> topologyFilenameArg(
            "network", "Specify the network name", true, "", "network");
        cmd.add(&topologyFilenameArg);

        std::vector<std::string> modelNames;
        std::transform(dispatchMap.begin(), dispatchMap.end(),
            std::back_inserter(modelNames),
            [&](auto&& _pair) { return _pair.first; });

        TCLAP::ValuesConstraint<std::string> vCons(modelNames);
        TCLAP::ValueArg<std::string> modelName(
            "m", "model", "Specify the model used", false, "path", &vCons);
        cmd.add(&modelName);

        TCLAP::ValueArg<int> nbLicensesArg("r", "nbLicenses",
            "Specify the number of licenses", true, 1, "int");
        cmd.add(&nbLicensesArg);

        TCLAP::ValueArg<double> demandFactorArg("d", "demandFactor",
            "Specify the multiplicative factor for the demands", false, 1,
            "double");
        cmd.add(&demandFactorArg);

        TCLAP::ValueArg<double> epsilonArg(
            "e", "epsilon", "Specify the epsilon", false, 1e-6, "double");
        cmd.add(&epsilonArg);

<<<<<<< Updated upstream
=======
        TCLAP::ValueArg<double> gapEpsArg(
            "g", "gapEps", "Specify the gapEps", false, 1e-6, "double");
        cmd.add(&gapEpsArg);

        TCLAP::ValueArg<int> timeoutArg(
            "t", "timeout", "Specify the timeout in seconds", false, 6, "int");
        cmd.add(&timeoutArg);

>>>>>>> Stashed changes
        TCLAP::ValueArg<double> alphaArg(
            "a", "alpha", "Specify the alpha", false, .9, "double");
        cmd.add(&alphaArg);

        TCLAP::SwitchArg verboseArg(
            "v", "verbose", "Define the verbosity of the program", false);
        cmd.add(&verboseArg);

        TCLAP::ValueArg<int> nbThreadsArg(
            "j", "nbThreads", "Specify the number of threads", false, 1, "int");
        cmd.add(&nbThreadsArg);

        TCLAP::ValueArg<double> percentDemandArg("p", "percentDemand",
            "Specify the percentage of demand used", false, 100.0, "double");
        cmd.add(&percentDemandArg);

        cmd.parse(argc, argv);

<<<<<<< Updated upstream
        return std::make_tuple(topologyFilenameArg.getValue(),
            modelName.getValue(), nbLicensesArg.getValue(), alphaArg.getValue(),
            nbThreadsArg.getValue(), demandFactorArg.getValue(),
            percentDemandArg.getValue(), verboseArg.getValue());
    }();
    // epsilon_value = 1e-9;
    std::ifstream ifs(topologyFilename);
    const auto instance = SFC::Partial::OccLim::loadInstance(ifs, nbLicenses);
    std::cout << "Dumb lower bound: " << SFC::computeLowerBound(instance)
              << '\n';

=======
        // epsilon_value = epsilonArg.getValue();

        return std::make_tuple(topologyFilenameArg.getValue(),
            modelName.getValue(), nbLicensesArg.getValue(), alphaArg.getValue(),
            nbThreadsArg.getValue(), demandFactorArg.getValue(),
            percentDemandArg.getValue(), verboseArg.getValue(),
            timeoutArg.getValue(), gapEpsArg.getValue());
    }();

    ColumnGenerationParameters cgParams{nbThreads, verbose, true};
    StabilizationParameters stabParams{nbThreads, verbose, true, alpha, 1e-6};
    BaseBranchAndPriceParameters bapParams{
        nbThreads, verbose, std::chrono::seconds(timeout), gapEps};

    // epsilon_value = 1e-9;
    std::ifstream ifs(topologyFilename);
    const auto instance = SPO::loadInstance(ifs, nbLicenses);
    std::cout << "Dumb lower bound: " << SFC::computeLowerBound(instance)
              << '\n';
    const auto [minIte, maxIte] =
        std::minmax_element(instance.demands.begin(), instance.demands.end(),
            [](auto&& _d1, auto&& _d2) { return _d1.d < _d2.d; });
    std::cout << "Min. demand: " << *minIte << ", max. demand: " << *maxIte
              << '\n';
>>>>>>> Stashed changes
    const auto instanceName = instance.network[boost::graph_bundle];
    const auto resFilename =
        percentDemand != 100.0
            ? str(boost::format("./results/partial/occlim/%1%_%2%_%3%_%4%.res")
                  % instanceName % nbLicenses % model % instance.demands.size())
            : str(boost::format("./results/partial/occlim/%1%_%2%_%3%.res")
                  % instanceName % nbLicenses % model);

<<<<<<< Updated upstream
    boost::log::add_file_log(boost::log::keywords::file_name =
                                 str(boost::format("%1%_%2%_%3%") % instanceName
                                     % nbLicenses % model)
                                 + "_%N%.log",
        boost::log::keywords::rotation_size = 10 * 1024 * 1024,
        boost::log::keywords::time_based_rotation =
            boost::log::sinks::file::rotation_at_time_point(0, 0, 0),
        boost::log::keywords::format = "[%TimeStamp%]: %Message%",
        boost::log::keywords::auto_flush = true);

=======
    /*boost::log::add_file_log(
        boost::log::keywords::file_name =
            str(boost::format("%1%_%2%_%3%") % instanceName % nbLicenses %
       model) + "_%N%.log", boost::log::keywords::rotation_size = 10 * 1024 *
       1024, boost::log::keywords::time_based_rotation =
       boost::log::sinks::file::rotation_at_time_point(0, 0, 0),
        boost::log::keywords::format = "[%TimeStamp%]: %Message%",
       boost::log::keywords::auto_flush = true);
*/
>>>>>>> Stashed changes
    std::cout << "Loaded " << topologyFilename << '\n';

    const int totalAvailableCores =
        std::accumulate(vertices(instance.network).first,
            vertices(instance.network).second, 0, [&](auto&& _acc, auto&& _u) {
                return _acc + instance.network[_u].capacity;
            });
    std::cout << "totalAvailableCores: " << totalAvailableCores << " for "
              << instance.getTotalNbCores() << " needed cores\n";
    Time timer;
    timer.start();
<<<<<<< Updated upstream
    const auto sol = dispatchMap.at(model)(instance, alpha, nbThreads, verbose);
=======
    const auto sol =
        dispatchMap.at(model)(instance, cgParams, stabParams, bapParams);
>>>>>>> Stashed changes
    if (sol.has_value()) {
        std::cout << "Found a solution\n"
                  << "Obj. value = " << sol->getObjValue()
                  << ", LB = " << sol->getLowerBound()
                  << ", epsilon = " << sol->getEpsilon() << '\n';
        std::ofstream ofs(resFilename);
        const auto times = timer.get();
        nlohmann::json time{
            {"CPUTime", times.first}, {"WallTime", times.second}};
        sol->save(ofs, time);
        std::cout << "Saved to " << resFilename << '\n';
    } else {
        timer.get();
        std::cout << "No solution found for " << topologyFilename << '\n';
    }
    return 0;
}
