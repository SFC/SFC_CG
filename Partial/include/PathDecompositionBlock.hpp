#ifndef PATH_DECOMPOSITION_BLOCK_HPP
#define PATH_DECOMPOSITION_BLOCK_HPP

#include "ColumnGeneration.hpp"
#include "Partial.hpp"

namespace SFC::Partial::PathDecompositionBlock {

struct DualValues {
    struct OneBlockTag {};
    DualValues(const Instance& _inst);
    DualValues(const DualValues& _other) = default;
    DualValues(DualValues&& _other) = default;
    DualValues& operator=(const DualValues& _other) = default;
    DualValues& operator=(DualValues&& _other) = default;
    ~DualValues() = default;

    IloNum getReducedCost(const CrossLayerLink& _link) const;
    IloNum getReducedCost(const IntraLayerLink& _link) const;
    IloNum getReducedCost(DualValues::OneBlockTag /*_tag*/) const;
    double getDualSumRHS() const;

    const Instance* inst;
    IloNum m_oneBlockDual;
    std::vector<IloNum> m_linkCapasDuals;
    std::vector<IloNum> m_nodeCapasDuals;
};

class Master
    : public RMP<Master, DualValues, Solution, std::vector<ServicePath>> {
    friend DualValues;

  public:
    using Base = RMP<Master, DualValues, Solution, std::vector<ServicePath>>;
    friend Base;
    using Column = std::vector<ServicePath>;
    using Solution = Solution;
    using DualValues = DualValues;

    explicit Master(const Instance& _inst);
    Master& operator=(const Master&) = delete;
    Master& operator=(Master&&) = delete;
    Master(const Master&) = delete;
    Master(Master&&) = delete;
    ~Master() { m_env.end(); }

    double getReducedCost_impl(const std::vector<ServicePath>& _col,
        const DualValues& _dualValues) const;
    IloNumColumn getColumn_impl(const std::vector<ServicePath>& _col);
    void getDuals_impl();
    void removeDummyIfPossible();
    const DualValues& getDualValues_impl() const;
    std::vector<ServicePath> getServicePaths() const;
    template <typename T>
    constexpr inline int getNbPricing() const {
        return 1;
    }
    Solution getSolution() const;
    using Base::getNbColumns;

  private:
    using Base::m_columns;
    using Base::m_duals;
    using Base::m_dummyActive;
    using Base::m_env;
    using Base::m_fracObj;
    using Base::m_intObj;
    using Base::m_model;
    using Base::m_solver;

    const Instance* m_inst;
    IloObjective m_obj;

    IloRange m_oneBlockCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;

    IloNumVar m_dummyPath;
    std::vector<double> m_linkCharge;
    std::vector<double> m_nodeCharge;
};

void barycenter(const DualValues& _nDuals, const DualValues& _bDuals,
    DualValues& _sDuals, double _alpha);
std::ostream& operator<<(std::ostream& _out, const DualValues& _dualValues);
} // namespace SFC::Partial::PathDecompositionBlock

#endif
