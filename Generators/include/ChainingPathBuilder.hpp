#ifndef CHAININGPATHBUILDER_HPP
#define CHAININGPATHBUILDER_HPP

#include <ilcplex/ilocplex.h>
#include <random>
#include <vector>

#include "ColumnGeneration.hpp"
#include "SFC.hpp"

// namespace SFC::Uncapacitated {

// struct DualValues {
//     DualValues(const Instance& _inst);
//     DualValues(const DualValues& _other) = default;
//     DualValues(DualValues&& _other) noexcept = default;
//     DualValues& operator=(const DualValues& _other) = default;
//     DualValues& operator=(DualValues&& _other) noexcept = default;
//     ~DualValues() = default;

//     IloNum getReducedCost(const CrossLayerLink& _link) const;
//     IloNum getReducedCost(const IntraLayerLink& _link) const;
//     double getDualSumRHS() const;

//     const Instance* inst;
//     std::vector<IloNum> m_onePathDuals;
// };

// class Master : public RMP<Master, DualValues, Solution<int>, ServicePath> {
//     friend DualValues;

//   public:
//     using Base = RMP<Master, DualValues, Solution<int>, ServicePath>;
//     friend Base;
//     using Column = ServicePath;
//     using Solution = Solution<int>;
//     using DualValues = DualValues;

//     explicit Master(const Instance& _inst);
//     Master& operator=(const Master&) = delete;
//     Master& operator=(Master&&) = delete;
//     Master(const Master&) = delete;
//     Master(Master&&) = delete;
//     ~Master() { m_env.end(); }

//     double getReducedCost_impl(
//         const ServicePath& _col, const DualValues& _dualValues) const;
//     IloNumColumn getColumn_impl(const ServicePath& _col);
//     void getDuals_impl();
//     void removeDummyIfPossible();
//     const DualValues& getDualValues_impl() const;
//     std::vector<ServicePath> getServicePaths() const;
//     template <typename T>
//     inline int getNbPricing() const {
//         return m_inst->demands.size();
//     }
//     Solution getSolution() const;

//     bool checkIntegrality_impl() const;
//     std::vector<IloRange> getBranch_impl() const;

//     template <typename RandomGenerator>
//     Network getNetwork(RandomGenerator& _gen) const noexcept {
//         const auto sPaths = getServicePaths();
//         assert(sPaths.size() == m_inst->demands.size());

//         // Build node capacities
//         const auto nodeUsage = getNodeUsage(sPaths, *m_inst);
//         const std::size_t totalNbCores =
//             std::accumulate(nodeUsage.begin(), nodeUsage.end(), 0ul);
//         std::cout << "Total # cores: " << totalNbCores << '\n';

//         // Build link capacities
//         std::uniform_real_distribution<double> dis(0.9, 1.2);
//         const auto linkUsage = getLinkUsage(sPaths, *m_inst);
//         std::vector<double> realLinkCapas(linkUsage.size());
//         std::transform(linkUsage.begin(), linkUsage.end(),
//             realLinkCapas.begin(),
//             [&](auto&& _val) -> double { return dis(_gen) * _val; });

//         SFC::Network graph(num_vertices(m_inst->network));
//         for (const auto ed :
//             boost::make_iterator_range(edges(m_inst->network))) {
//             const auto [u, v] = incident(ed, m_inst->network);
//             add_edge(u, v,
//                 {m_inst->network[ed].id, realLinkCapas[m_inst->network[ed].id]},
//                 graph);
//         }

//         std::vector<std::size_t> realNodeCapas(nodeUsage.size());
//         std::transform(nodeUsage.begin(), nodeUsage.end(),
//             realNodeCapas.begin(), [&](auto&& _val) -> std::size_t {
//                 return std::ceil(dis(_gen) * _val);
//             });
//         for (int u = 0; u < num_vertices(graph); ++u) {
//             graph[u] = {u, realNodeCapas[u]};
//         }
//         return graph;
//     }

//     using Base::getNbColumns;

//   private:
//     using Base::m_columns;
//     using Base::m_duals;
//     using Base::m_dummyActive;
//     using Base::m_env;
//     using Base::m_fracObj;
//     using Base::m_integerModel;
//     using Base::m_intObj;
//     using Base::m_model;
//     using Base::m_solver;

//     const Instance* m_inst;

//     IloObjective m_obj;
//     std::vector<IloRange> m_onePathCons;
//     std::vector<IloNumVar> m_dummyPaths;
// };

// } // namespace SFC::Uncapacitated
#endif
