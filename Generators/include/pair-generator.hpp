#ifndef PAIR_GENERATOR_HPP
#define PAIR_GENERATOR_HPP

#include <random>
#include <string>
#include <utility>
#include <vector>

#include <boost/multi_array.hpp>

namespace Generator {

std::vector<std::pair<int, int>> genAllNodePair(std::size_t _nbNodes);
std::vector<double> genPopulation(std::size_t _nbNodes, double _alpha);

class PopulationPairGenerator {
  public:
    PopulationPairGenerator(
        const std::size_t _nbNodes, const double _alpha, const boost::multi_array<double, 2>& _distance);

    /**
     * Return a random pair of nodes
     */
    template <typename RandomGenerator>
    std::pair<int, int> operator()(RandomGenerator& _gen) {
        const auto nodePair = pairs[nodesDistr(_gen)];
        return {nodePair.first, nodePair.second};
    }

    std::string getName() const noexcept;

  private:
    std::string name;
    std::vector<std::pair<int, int>> pairs;
    std::vector<double> population;
    boost::multi_array<double, 2> distance;
    std::discrete_distribution<std::size_t> nodesDistr;
};

class UniformPairGenerator {
  public:
    explicit UniformPairGenerator(const std::size_t _nbNodes);

    /**
     * Return a uniformly random pair of nodes
     */
    template <typename RandomGenerator>
    std::pair<int, int> operator()(RandomGenerator& _gen) {
        const auto nodePair = pairs[nodesDistr(_gen)];
        return {nodePair.first, nodePair.second};
    }

    std::string getName() const noexcept;

  private:
    std::string name;
    std::vector<std::pair<int, int>> pairs;
    std::uniform_int_distribution<std::size_t> nodesDistr;
};

} // namespace Generator
#endif
