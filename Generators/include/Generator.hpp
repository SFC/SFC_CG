#ifndef GENERATOR_HPP
#define GENERATOR_HPP

#include <map>
#include <random>
#include <type_traits>
#include <vector>

#include "SFC.hpp"
#include "boost/multi_array.hpp"

namespace Generator {
template <typename ChainGenerator, typename PairGenerator, typename RandomGenerator>
auto genDemands(
    std::size_t _load, ChainGenerator& _chainGen, PairGenerator& _pairGen, RandomGenerator& _gen) {
    using NetworkService = decltype(_chainGen(_gen).chain);
    using BandwidthType = double;
    static_assert(std::is_same<decltype(_pairGen(_gen)), std::pair<int, int>>::value);

    std::clog << "genDemands(" + std::to_string(_load) + ")\n";

    std::map<std::tuple<Node, Node, NetworkService>, BandwidthType> requests;
    BandwidthType genLoad = 0.0;
    while (genLoad < _load) {
        const auto [s, t] = _pairGen(_gen);
        const auto [chain, d] = _chainGen(_gen);
        genLoad += d;
        assert(genLoad > 0.0);
        requests[{s, t, chain}] += d;
    }

    std::vector<SFC::Demand<NetworkService>> demands;
    demands.reserve(requests.size());
    std::transform(
        requests.begin(), requests.end(), std::back_inserter(demands), [&, id = 0](auto&& _req) mutable {
            return SFC::Demand<NetworkService>{
                id++, std::get<0>(_req.first), std::get<1>(_req.first), _req.second, std::get<2>(_req.first)};
        });
    return demands;
}

template <typename T>
struct Range {
    T start;
    T end;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
//                                              ChainGenerator //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename NetworkService>
struct Service {
    NetworkService chain;
    double charge;
};

/**
 * The SetChainGenerator offers a way to get a Service Chain at random among a
 * set of chain according to a discrete_distribution
 */
template <typename NetworkService>
class SetChainGenerator {
  public:
    SetChainGenerator(std::vector<Service<NetworkService>> _services,
        std::vector<double> _nodeResourceRequirement, std::discrete_distribution<std::size_t> _distr)
        : nodeResourceRequirement(std::move(_nodeResourceRequirement))
        , services(std::move(_services))
        , distr(std::move(_distr)) {}

    SetChainGenerator(
        std::vector<Service<NetworkService>> _services, std::vector<double> _nodeResourceRequirement)
        : nodeResourceRequirement(std::move(_nodeResourceRequirement))
        , services(std::move(_services))
        , distr(_services.size(), 0, 1, [](auto&& /*_v*/) { return 1; }) {}

    template <typename RandomGenerator>
    Service<NetworkService> operator()(RandomGenerator& _gen) {
        return services[distr(_gen)];
    }

    const std::vector<double>& getNodeResourceRequirement() const { return nodeResourceRequirement; }

  private:
    std::vector<double> nodeResourceRequirement;
    std::vector<Service<NetworkService>> services;
    std::discrete_distribution<std::size_t> distr;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
//                                                                                                          //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

class FunctionRequirementGenerator {
  public:
    FunctionRequirementGenerator(std::size_t _nbFunctions, std::vector<double> _functionsCharges)
        : nbFunctions(_nbFunctions)
        , chargesSet(std::move(_functionsCharges)) {}

    template <typename RandomGenerator>
    std::vector<double> operator()(RandomGenerator& _gen) {
        std::vector<double> functionCharge(nbFunctions);
        std::generate(
            functionCharge.begin(), functionCharge.end(), [&]() { return chargesSet[distr(_gen)]; });
        return functionCharge;
    }

  private:
    std::size_t nbFunctions;
    std::vector<double> chargesSet;
    std::uniform_int_distribution<int> distr = std::uniform_int_distribution<int>(0, chargesSet.size() - 1);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
//                                                                                                          //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename RandomGenerator, typename Graph = boost::adjacency_matrix<boost::undirectedS>>
Graph generateRandomTree(const std::size_t _n, RandomGenerator& _gen) {
    assert(_n != 0);
    const std::vector<std::size_t> code = [&]() {
        std::vector<std::size_t> tmpval(_n - 2);
        std::uniform_int_distribution<std::size_t> distr(0, _n - 1);
        std::generate(tmpval.begin(), tmpval.end(), [&]() { return distr(_gen); });
        return tmpval;
    }();

    std::vector<int> count(_n, 0);
    for (const auto s : code) {
        count[s]++;
    }

    Graph retval(_n);
    for (const auto s : code) {
        const auto ite = std::find(count.begin(), count.end(), 0);
        assert(ite != count.end());
        auto x = static_cast<std::size_t>(std::distance(count.begin(), ite));
        count[x] = -1;
        add_edge(x, s, retval);
        assert(x != s);
        count[s]--;
    }
    const auto iteU = std::find(count.begin(), count.end(), 0);
    const auto iteV = std::find(std::next(iteU), count.end(), 0);
    assert(std::find(std::next(iteV), count.end(), 0) == count.end());
    const auto u = std::distance(count.begin(), iteU), v = std::distance(count.begin(), iteV);
    assert(u != v);
    add_edge(u, v, retval);
    assert(num_edges(retval) == _n - 1);
    return retval;
}
} // namespace Generator
#endif
