#include <algorithm>
#include <chrono>
#include <numeric>
#include <optional>

#include "ChainingPathBuilder.hpp"
#include "Decomposition.hpp"

// namespace SFC::Uncapacitated {

// DualValues::DualValues(const Instance& _inst)
//     : inst(&_inst)
//     , m_onePathDuals(inst->demands.size(), 0.0) {}

// IloNum DualValues::getReducedCost(const CrossLayerLink& /*_link*/) const {
//     return 0.0;
// }

// IloNum DualValues::getReducedCost(const IntraLayerLink& /*_link*/) const {
//     return 0.0;
// }

// double DualValues::getDualSumRHS() const {
//     return std::accumulate(m_onePathDuals.begin(), m_onePathDuals.end(), 0.0);
// }

// Master::Master(const Instance& _inst)
//     : Base(DualValues(_inst))
//     , m_inst(&_inst)
//     , m_obj(IloAdd(m_model, IloMinimize(m_env)))
//     , m_onePathCons(getOnePathPerDemandConstraints(*m_inst, m_model))
//     , m_dummyPaths([&]() {
//         std::vector<IloNumVar> retval(m_inst->demands.size());
//         for (int i = 0; i < retval.size(); ++i) {
//             retval[i] = m_onePathCons[i](1.0)
//                         + m_obj(2 * m_inst->demands[i].d
//                                 * num_vertices(m_inst->network));
//             m_model.add(retval[i]);
//             m_integerModel.add(retval[i] == 0);
//             setIloName(retval[i], "dummyPaths" + toString(m_inst->demands[i]));
//         }
//         return retval;
//     }()) {
//     m_solver.setOut(m_solver.getEnv().getNullStream());
//     m_solver.setWarning(m_solver.getEnv().getNullStream());
//     m_solver.setParam(IloCplex::Threads, 1);
//     m_solver.setParam(IloCplex::RootAlg, IloCplex::Dual);
//     m_solver.setParam(IloCplex::DPriInd, IloCplex::DPriIndSteep);
//     m_solver.setParam(IloCplex::Param::Advance, 0);
// }

// double Master::getReducedCost_impl(
//     const ServicePath& _col, const DualValues& _dualValues) const {
//     const auto& [nPath, locations, demandID] = _col;
//     return -_dualValues.m_onePathDuals[demandID];
// }

// IloNumColumn Master::getColumn_impl(const ServicePath& _col) {
//     const auto& [nPath, locations, demandID] = _col;
//     const auto& [id, s, t, d, chain] = m_inst->demands[demandID];
//     return m_onePathCons[demandID](1.0) + m_obj(d * (nPath.size() - 1));
// }

// void Master::getDuals_impl() {
//     const auto get = [&](auto&& _cons) { return getDual(m_solver, _cons); };
//     std::transform(m_onePathCons.begin(), m_onePathCons.end(),
//         m_duals.m_onePathDuals.begin(), get);
// }

// Solution<int> Master::getSolution() const {
//     return {*m_inst, getIntegerObjValue(), getRelaxationObjValue(),
//         getServicePaths(), getNbColumns()};
// }

// void Master::removeDummyIfPossible() {
//     if (m_dummyActive) {
//         const double dummySum = std::accumulate(m_dummyPaths.begin(),
//             m_dummyPaths.end(), 0.0, [&](auto&& _acc, auto&& _var) {
//                 return _acc + m_solver.getValue(_var);
//             });
//         if (epsilon_equal<double>()(dummySum, 0.0)) {
//             std::cout << "Removed dummy\n";
//             m_dummyActive = false;
//             for (auto& dummy : m_dummyPaths) {
//                 m_model.add(dummy == 0);
//             }
//             solve();
//         }
//     }
// }

// } // namespace SFC::Uncapacitated
