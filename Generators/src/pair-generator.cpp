#include "pair-generator.hpp"

namespace Generator {
PopulationPairGenerator::PopulationPairGenerator(const std::size_t _nbNodes,
    const double _alpha, const boost::multi_array<double, 2>& _distance)
    : name("population_" + std::to_string(_alpha))
    , pairs(genAllNodePair(_nbNodes))
    , population(genPopulation(_nbNodes, _alpha))
    , distance(_distance)
    , nodesDistr([&]() {
        std::vector<double> distribution(pairs.size());
        std::transform(pairs.begin(), pairs.end(), distribution.begin(),
            [&](auto&& pair) -> double {
                assert(pair.first != pair.second);
                return population[pair.first] * population[pair.second]
                       / double(distance[pair.first][pair.second]);
            });
        return std::discrete_distribution<std::size_t>(
            distribution.begin(), distribution.end());
    }()) {}

std::string PopulationPairGenerator::getName() const noexcept { return name; }

std::vector<std::pair<int, int>> genAllNodePair(std::size_t _nbNodes) {
    std::vector<std::pair<int, int>> retval(_nbNodes * (_nbNodes - 1));
    std::size_t i = 0;
    for (std::size_t u = 0; u < _nbNodes; ++u) {
        for (std::size_t v = 0; v < _nbNodes; ++v) {
            if (u != v) {
                retval[i++] = {u, v};
            }
        }
    }
    return retval;
}

std::vector<double> genPopulation(std::size_t _nbNodes, const double _alpha) {
    std::vector<double> population(_nbNodes);
    std::generate(
        population.begin(), population.end(), [&_alpha, i = 1]() mutable {
            return std::pow(1 / double(i++), _alpha);
        });
    return population;
}

UniformPairGenerator::UniformPairGenerator(const std::size_t _nbNodes)
    : name("uniform")
    , pairs(genAllNodePair(_nbNodes))
    , nodesDistr(0, pairs.size() - 1) {}

std::string UniformPairGenerator::getName() const noexcept { return name; }

} // namespace Generator
