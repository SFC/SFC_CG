import sys

try:
    from skbuild import setup
except ImportError:
    print(
        "Please update pip, you need pip 10 or greater,\n"
        " or you need to install the PEP 518 requirements in pyproject.toml yourself",
        file=sys.stderr,
    )
    raise

from setuptools import find_packages

setup(
    name="sfc",
    version="0.2.1",
    description="Binding for the SFC algorithms",
    author="Nicolas HUIN",
    license="MIT",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    cmake_install_dir="src/sfc",
    include_package_data=True,
    extras_require={"test": ["pytest"]},
    python_requires=">=3.10",
    cmake_args=['-DENABLE_DEVELOPER_MODE:BOOL=OFF',
                '-DOPT_ENABLE_CONAN:BOOL=1',
                '-DCONAN_DETECT_QUIET:BOOL=0']
)
