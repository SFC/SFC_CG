import random

demands = {}
nbServers = 16
servers = [i for i in xrange(nbServers)]
sfcs = [[0, 1, 2, 3, 4], [0, 1, 2, 1, 0], [0, 1, 2, 5, 4], [0, 1, 5, 3, 4]]

total = float(36456 + 9476 + 78 + 6168)
percentage = [
	9476 / total, 
	6168 / total, 
	36456 / total,
	78 / total]
chains = [[0, 1, 2, 3, 4], [0, 1, 2, 1, 0], [0, 1, 2, 5, 4], [0, 1, 5, 3, 4]]

for s in xrange(nbServers): # for each server
	sameRack = []
	differentRack = []
	for i in servers:
		if i % 4 == s % 4:
			sameRack.append(i)
		else: 
			differentRack.append(i)
	nbFlow = random.randint(10, 80) # select a number of outgoing flow randomly
	for f in xrange(nbFlow): # for each flow, select charge and chain randomly
		charge = 1.0 / nbFlow
		# Find chain
		r = random.uniform(0, 1)
		chain = -1
		for i in xrange(len(percentage)):
			if r <= percentage[i]:
				chain = i
				break
			else:
				r -= percentage[i]
		assert(chain != -1)
		#
		# choose destination
		t = random.choice(sameRack) if random.uniform(0, 1) > .8 else random.choice(differentRack)
		# aggregate similar flows
		if (s, t) in demands and demands[s, t][1] == chain: 
			demands[s, t][0] += charge
		else: 
			demands[s, t] = [charge, chain]

overallTraffic = sum([c for _, c in demands.values()])
print overallTraffic
filename = "./instances/energy/4-fattree_1_demand.txt"
with open(filename, "w") as f:
	for (s, t), l in demands.iteritems():
		row = [s, t, l[0]] + sfcs[l[1]]
 		f.write('\t'.join([str(v) for v in row])+'\n')
print "Saved to ", filename
print len(demands)