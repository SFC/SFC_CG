import math
import operator
# import all variables initialized in gen_instance
from sage.all import *
import sys
import csv


name = sys.argv[1]

def gen():
    # importing graph
    d, classicDemands = getNetwork(name, p=1, folder="./instances")
    # each demand is splitted in 4 demands based on the percentage of traffic
    with open(demandsPath) as f:
        for line in f:
            data = line.split()
            for i in range(0, 4):
                demands_functions[i, data[0], data[1]] = chains[i]
                demands[i, data[0], data[1]] = (float(data[2]) * scale_factor * percentage_traffic[i], )
    
    for u in d.vertices():
        node_capacity[u] = int(sys.argv[2])

    for (u, v, cap) in d.edges():
        edge_eliminated[(u, v)] = False

def save():
    # with open(sys.path[0] + "/output/" + name + "_topo.txt", "w") as f:
    #     for key in demands.keys():
    #         f.write(str(key[0]) + "\t" + str(key[1]) + "\t" + str(key[2]) + "\t\t" + str(demands[key]) + "\n")
    # f.close()
    print "Exporting to ", "./instances/energy/" + name + "_nodeCapa.txt"
    with open("./instances/energy/" + name + "_nodeCapa.txt", "w") as f:
        capaWriter = csv.writer(f, delimiter='\t')
        for u in xrange(d.order()):
            capaWriter.writerow([u, node_capacity[u]])
    f.close()

    print "Exporting to ", "./instances/energy/" + name +"_demand.txt"
    with open("./instances/energy/" + name +"_demand.txt", "w") as f:
        demandWriter = csv.writer(f, delimiter='\t')
        for (s, t), (charge, chain) in demands_functions.items():
            demandWriter.writerow([s, t, charge] + chain)

if __name__ == "__main__":
    gen()
    save()
